let svg = d3.select("body").append("svg").attr("width", 1000).attr("height", 800).attr("style", "margin-top: 40px;");
let rightMenu = d3.select("body").append("svg").attr("width", 300).attr("height", 800);
let timesvg = rightMenu.append("g").attr("width", 300).attr("height", 40);
let scoresvg = rightMenu.append("g").attr("transform", "translate(0, 40)").attr("width", 300).attr("height", 40);
let bussvg = rightMenu.append("g").attr("transform", "translate(0, 80)").attr("width", 300).attr("height", 40);
let rainsvg = rightMenu.append("g").attr("transform", "translate(0, 120)").attr("width", 300).attr("height", 40);

let id = "";
let abs = x => {
    if (x < 0) { return -x; }
    return x;
};
let actions = { "lifts": {} };
function updateActions(data) {
    let actions = { "lifts": {} };
    for (var liftID in data["lifts"]) {
        let people = data.lifts[liftID].people;
        if (people && people.length > 0) { // if someone in lift, let them get out first
            actions["lifts"][liftID] = data.people[people[0]].target_floor;
            continue;
        }

        // no one inside, go get closest person
        let closestPersonID = closestPerson(liftID, data);
        if (closestPersonID) {
            actions["lifts"][liftID] = data.people[closestPersonID].floor;
        } else {
            actions["lifts"][liftID] = data.layout.lifts[liftID].floors[0]; // if no people left, go to first floor
        }
    }
    return actions;
}

let MAX_DISTANCE = 1e10;
function closestPerson(liftID, data) {
    let minDistance = MAX_DISTANCE;
    let minIndex = -1;
    let keys = Object.keys(data.people);
    if (keys.length === 0) { return null; }
    for (let i = 0; i < keys.length; i++) {
        let distance = manhattanDistance(liftID, keys[i], data);
        if (distance < minDistance) {
            minDistance = distance;
            minIndex = i;
        }
    }
    if (minIndex === -1) { return null; }
    return keys[minIndex];
}

function manhattanDistance(liftID, personID, data) {
    let liftX = data.layout.lifts[liftID].x;
    let liftY = data.lifts[liftID].height;
    let personX = data.people[personID].x;
    let personFloor = data.people[personID].floor;
    if (personFloor === null) { // ignore people already in a lift
        return MAX_DISTANCE;
    }
    if (personFloor == data.people[personID].target_floor) { // ignore people already in its floor
        return MAX_DISTANCE;
    }
    // ignore people that can't go to desired destination in this lift
    if (!floorValidForLift(personFloor, liftID, data) ||
        !floorValidForLift(data.people[personID].target_floor, liftID, data)) {
        return MAX_DISTANCE;
    }
    let personY = data.layout.floors[personFloor].height;
    return abs(liftX - personX) + abs(liftY - personY);
}

function floorValidForLift(floorID, liftID, data) {
    let floors = data.layout.lifts[liftID].floors;
    return floors.indexOf(floorID) !== -1;
}

rainsvg.append("text").attr("class", "rain")
    .attr("x", 0)
    .attr("y", 24)
    .attr("fill", "black")
    .attr("font-size", "24px");
bussvg.append("text").attr("class", "bus")
    .attr("x", 0)
    .attr("y", 24)
    .attr("fill", "black")
    .attr("font-size", "24px");
timesvg.append("text").attr("class", "time")
    .attr("x", 0)
    .attr("y", 24)
    .attr("fill", "black")
    .attr("font-size", "24px");

function draw(data) {
    data = JSON.parse(data);
    id = data["id"] + "";
    actions = updateActions(data);

    let peopleInFloors = [];
    for (var k in data["people"]) {
        if (!data["people"][k]["floor"]) {
            continue;
        }
        let person = data["people"][k];
        person["id"] = k;
        peopleInFloors.push(person);
    }

    let floorsMap = data["layout"]["floors"];
    let floorsList = [];
    let maxy = 0;
    for (var k in floorsMap) {
        let height = floorsMap[k]["height"];
        floorsList.push({ "id": k, "height": height });
        if (height > maxy) {
            maxy = height;
        }
    }

    let liftsList = [];
    for (var k in data["lifts"]) {
        let lift = data["lifts"][k];
        lift["id"] = k;
        lift["x"] = data["layout"]["lifts"][k]["x"];
        lift["floors"] = data["layout"]["lifts"][k]["floors"];
        liftsList.push(lift);
    }

    let width = data["layout"]["width"];
    yscale = d3.scaleLinear().domain([0, maxy]).range([750, 100]);
    xscale = d3.scaleLinear().domain([0, width]).range([50, 950]);

    function minLiftHeight(lift) {
        let minFloor = 1e10;
        lift["floors"].map(floor => {
            if (minFloor > +floor) {
                minFloor = +floor;
            }
        });
        return floorsMap[minFloor]["height"];
    }

    function maxLiftHeight(lift) {
        let maxFloor = +lift["floors"][0];
        lift["floors"].map(floor => {
            if (maxFloor < +floor) {
                maxFloor = +floor;
            }
        });
        return floorsMap[maxFloor]["height"];
    }

    let score = scoresvg.selectAll(".score").data([data.score]);
    score.enter().append("text").attr("class", "score")
        .merge(score)
        .attr("x", 0)
        .attr("y", 24)
        .attr("fill", "black")
        .attr("font-size", "24px")
        .text(d => "Score: " + d);

    if (data.iot_inputs.rain.seconds_to_start !== 0 || data.iot_inputs.rain.duration !== 0) {
        if (data.iot_inputs.rain.seconds_to_start > 0) {
            rainsvg.select(".rain")
                .text(`Rain starts in ${data.iot_inputs.rain.seconds_to_start}s`)
        } else if (data.iot_inputs.rain.duration > 0) {
            rainsvg.select(".rain")
                .text(`Rain stops in ${data.iot_inputs.rain.duration}s`)
        }
    } else {
        rainsvg.select(".rain").text("")
    }

    if (data.iot_inputs.bus_stops_in) {
        bussvg.select(".bus").text("Bus arrives in: " + secondsToTimeLeft(data.iot_inputs.bus_stops_in))
    } else {
        bussvg.select(".bus").text("")
    }


    timesvg.select(".time").text("Time: " + secondsToTimeLeft(data.time))


    svg.selectAll(".floors")
        .data(floorsList)
        .enter()
        .append("line")
        .attr("class", "floors")
        .attr("x1", xscale(0))
        .attr("y1", d => yscale(d["height"]))
        .attr("x2", xscale(width))
        .attr("y2", d => yscale(d["height"]))
        .attr("stroke", "gray");

    svg.selectAll(".floors_index")
        .data(floorsList)
        .enter()
        .append("text")
        .text(d => d.id)
        .attr("class", "floors_index")
        .attr("x", xscale(0))
        .attr("y", d => yscale(d["height"]) - 3)
        .attr("stroke", "gray");


    svg.selectAll(".lift_line").data(liftsList, d => {
        return d.id;
    }).enter().append("line")
        .attr("class", "lift_line")
        .attr("x1", d => xscale(d.x))
        .attr("y1", d => yscale(minLiftHeight(d)))
        .attr("x2", d => xscale(d.x))
        .attr("y2", d => yscale(maxLiftHeight(d)))
        .attr("stroke", "black");

    let liftStops = [];
    for (let lift of liftsList) {
        for (let id of lift.floors) {
            liftStops.push({ "height": floorsMap[id]["height"], "x": lift.x });
        }
    }

    let stops = svg.selectAll(".lift_stops")
        .data(liftStops);

    let stopsEnter = stops
        .enter()
        .append("circle")
        .attr("class", "lift_stops");

    stops.merge(stopsEnter)
        .attr("cx", d => xscale(d.x))
        .attr("cy", d => yscale(d.height))
        .attr("r", "4")
        .attr("fill", "black");

    let peopleCircle = svg.selectAll(".people_circle").data(peopleInFloors, d => d.ID);
    peopleCircle.exit().transition()
        .duration(d => d.floor === d.target_floor ? 1000 : 0)
        .attr("cx", d => {
            let radius = yscale(maxy + 1.2 * maxy / 10) * 2;
            return d.target_exit === 'left' ? 0 - radius : 1000 + radius;
        })
        .attr("cy", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10))
        .remove();
    peopleCircle.enter().append("circle")
        .attr("class", "people_circle")
        .attr("cx", d => xscale(d.x))
        .attr("cy", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10))
        .attr("r", yscale(maxy + 1.2 * maxy / 10))
        .attr("fill", "white")
        .attr("stroke", "black");
    peopleCircle.transition()
        .duration(1000)
        .attr("cx", d => xscale(d.x))
        .attr("cy", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10));

    let peopleTargetFloor = svg.selectAll(".people_target_floor").data(peopleInFloors, d => d.ID);
    peopleTargetFloor.exit()
        .transition()
        .duration(d => d.floor === d.target_floor ? 1000 : 0)
        .attr("x", d => {
            let radius = yscale(maxy + 1.2 * maxy / 10) * 2;
            return d.target_exit === 'left' ? 0 - radius : 1000 + radius;
        })
        .attr("y", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10))
        .remove();
    peopleTargetFloor.enter().append("text")
        .attr("class", "people_target_floor")
        .text(d => d.target_floor)
        .attr("font-size", "14px")
        .attr("font-weight", "bold")
        .attr("x", (d, i, nodes) => {
            let textCentered = nodes[i].getComputedTextLength() / 2;
            return xscale(d.x) - textCentered
        })
        .attr("y", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10))
        .attr("fill", "black");
    peopleTargetFloor.transition()
        .duration(1000)
        .attr("x", (d, i, nodes) => {
            let textCentered = nodes[i].getComputedTextLength() / 2;
            return xscale(d.x) - textCentered
        })
        .attr("y", d => yscale(floorsMap[d.floor]["height"] + 0.3 * maxy / 10));

    let liftCage = svg.selectAll(".lift_cage").data(liftsList, d => d.id);
    liftCage.enter().append("rect").attr("class", "lift_cage")
        .attr("rx", "2")
        .attr("x", d => xscale(d.x - 0.25 * width / 10))
        .attr("y", d => yscale(d.height + 1 * maxy / 10))
        .attr("width", d => xscale(d.x + 0.25 * width / 10) - xscale(d.x - 0.25 * width / 10))
        .attr("height", d => yscale(d.height) - yscale(d.height + maxy * 1 / 10))
        .attr("fill", d => d.doors_open ? 'white' : 'darkgray')
        .attr("stroke", "black");
    liftCage.transition()
        .duration(1000)
        .attr("fill", d => d.doors_open ? 'white' : 'darkgray')
        .attr("x", d => xscale(d.x - 0.25 * width / 10))
        .attr("y", d => yscale(d.height + 1 * maxy / 10));

    let getLiftText = lift => {
        let capacity = data["layout"]["lifts"][lift.id]["capacity"];
        let peopleInside = lift.people ? lift.people.length : 0;
        return peopleInside + "/" + capacity;
    };
    let liftText = svg.selectAll(".lift_text").data(liftsList, d => d.id);
    liftText.enter().append("text").attr("class", "lift_text")
        .text(d => getLiftText(d))
        .attr("x", d => xscale(d.x - 0.25 * width / 10))
        .attr("y", d => yscale(d.height + 1 * maxy / 10) - 2);
    liftText.transition()
        .duration(1000)
        .text(d => getLiftText(d))
        .attr("x", d => xscale(d.x - 0.25 * width / 10))
        .attr("y", d => yscale(d.height + 1 * maxy / 10) - 2);

    let targetFloors = [{}, {}, {}, {}, {}];
    liftsList.map(lift => {
        if (lift.people) {
            lift.people.map((personID, i) => {
                let targetFloor = data.people[personID].target_floor;
                targetFloors[i][lift.id] = "" + targetFloor;
            })
        }
    })

    targetFloors.map((targetFloorStops, i) => {
        let liftTargetFloor = svg.selectAll(".lift_target_floor"+i).data(liftsList, d => d.id);
        liftTargetFloor.enter().append("text").attr("class", "lift_target_floor"+i)
            .text(d => targetFloorStops[d.id])
            .attr("x", d => xscale(d.x - 0.25 * width / 10) + ([0, 2, 4].indexOf(i) === -1 ? 25:5))
            .attr("y", d => yscale(d.height + 1 * maxy / 10) +([0,1].indexOf(i) !== -1 ? 20 : [0,1].indexOf(i) !== -1 ? 40 : 60));
        liftTargetFloor.transition()
            .duration(1000)
            .text(d => targetFloorStops[d.id])
            .attr("x", d => xscale(d.x - 0.25 * width / 10)+ ([0, 2, 4].indexOf(i) === -1 ? 25:5))
            .attr("y", d => yscale(d.height + 1 * maxy / 10) + ([0,1].indexOf(i) !== -1 ? 20 : [2,3].indexOf(i) !== -1 ? 40 : 60));
    });

    d3.selectAll("text").attr("font-family", "Open Sans");
}

function get(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

function post(theUrl, data, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlHttp.send(JSON.stringify(data));
}

let i = 0;
setInterval(function () {
    i++
    let url = "http://localhost:5000";
    if (id) {
        post(url + "?id=" + id, actions, draw);
    } else {
        get(url, draw);
    };
}, 1000);

function secondsToTimeLeft(seconds) {
    var measuredTime = new Date(null);
    measuredTime.setSeconds(seconds);
    return measuredTime.toISOString().substr(11, 8);
}
