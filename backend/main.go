package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	_ "net/http/pprof"

	. "github.com/oriolf/hackathon-2019/backend/structs"
)

const ( // configuration constants
	MIN_FLOORS              = 5
	MIN_FLOOR_HEIGHT        = 1
	FLOOR_HEIGHT_MULTIPLIER = 1
	FLOORS_VARIATION        = 10

	MIN_LIFT_MARGIN        = 0.5
	LIFT_MARGIN_MULTIPLIER = 1
	MAX_LIFT_CAPACITY      = 5
	MIN_LIFT_STOPS         = 4

	MIN_RAIN_DURATION      = 20
	MAX_RAIN_DURATION      = 60
	MIN_RAIN_START_SECONDS = 10
	MAX_RAIN_START_SECONDS = 12

	MIN_PEAK_TIME_SECONDS  = 10 * 60
	MAX_PEAK_TIME_SECONDS  = 15 * 60
	GAME_DURATION          = 3600
	PROBABILITY_MULTIPLIER = 0.5

	BUS_MIN_PEOPLE_OUT                          = 3
	BUS_MAX_PEOPLE_OUT                          = 10
	BUS_START_TIME_SECONDS                      = 7 * 60 * 60
	BUS_END_TIME_SECONDS                        = 23 * 60 * 60
	BUS_STOP_MAX_DELAY_SECONDS                  = 2 * 60
	BUS_STOP_INTERVAL_SECONDS                   = 10 * 60
	DEFAULT_PEOPLE_CREATION_PROBABILITY float64 = 0.09

	rainStartsIn     = "rain_starts_in"
	rainStopsIn      = "rain_stops_in"
	peakTime         = "peak_time"
	peopleEntering   = "people_entering"
	peopleLeaving    = "people_leaving"
	peopleInRooftop  = "people_in_rooftop"
	peopleOutRooftop = "people_out_rooftop"
	peopleInBetween  = "people_in_between"
	busStopsIn       = "bus_stops_in"
	mainjs           = `
	let width = 800;
let height = 500;
let margin = { left: 40, right: 10, top: 70, bottom: 30 };

let svg = d3.select("#chart")
    .append("svg")
    .attr("width", width)
    .attr("height", height);


let parseToData = (dataset) => {
    return Object.keys(dataset).map(t => {
        let vals = info[t].map(v => {
            if (v.timestamp.toString().length > 13) {
                let time = v.timestamp.toString().slice(0, -6);
                v.timestamp = v.timestamp.toString().slice(0, -6);
                let date = new Date(time * 1);
                v.date = d3.timeFormat("%d %m, %H:%M")(date);
            }
            return v;
        })

        return { team: t, values: vals };
    });
};

let colors = (t) => {
    let teams = Object.keys(info);
    let c = d3.scaleOrdinal(d3.schemeCategory10)
        .domain(teams.map(function (tc) {
            return tc;
        }))
    return c(t)
};


let globalDatasetFunc = (dataset) => {
    let g = []
    Object.keys(dataset).forEach(t => {
        dataset[t].forEach(v => {
            g.push(v)
        })
    })
    return g; script
}


let data = parseToData(info);
let global = globalDatasetFunc(info);


let xScale = d3.scaleTime()
    .domain(d3.extent(global, d => d.timestamp))
    .rangeRound([margin.left, width - margin.right]);

let yScale = d3.scaleLinear()
    .domain(d3.extent(global, d => d.score))
    .range([height - margin.bottom, margin.top]);



let lineFunc = d3.line()
    .curve(d3.curveLinear)
    .x(d => {
        return xScale(d.timestamp);
    })
    .y(d => {
        return yScale(d.score)
    });





let axisY = svg.append("g")
    .call(d3.axisLeft(yScale)
        .tickFormat(d3.timeFormat("%Y-%m-%d %H:%M")))
    .attr("transform", "translate( " + margin.left + ", 0 )");

let axisX = svg.append("g")
    .attr("transform", "translate(0," + (height - margin.bottom) + ")")
    .call(d3.axisBottom(xScale));



let creatAuxTooltip = () => {
    let tooltipWrapper = d3.select("body")
        .append("div")
        .attr("class", "aux_tooltip")
        .style("opacity", "1")
        .style("position", "absolute")
        .style("transition", "all 0.3s ease-out")
        .style("left", (svg.node().getBoundingClientRect().width + svg.node().getBoundingClientRect().x + 20) + "px");

    tooltipWrapper.append("div")
        .attr("id", "tooltip-text")
        .attr("style",
            "background: #fff;"
            + "display: inline-block;"
            + "pointer-events: none;"
            + "text-align: left;"
            + "padding: 0.6em 2em;"
            + "padding-left: 7px;"
            + "padding-right: 7px;"
            + "white-space: nowrap;"
            + "border: 1px solid #ddd;"
            + "font: 12px sans-serif;"
            + "background: white;"
            + "border-radius: 8px;"
        );

    tooltipWrapper.append("div")
        .attr("id", "pointer")
        .attr("style",
            "position: absolute;"
            + "top: 50%;"
            + "width: 18px;"
            + "height: 18px;"
            + "background: #ddd;"
            + "transform: translate(-40%, -50%) rotate(45deg);"
            + "transform-origin: center center;"
            + "z-index: -1;"
        )
    return tooltipWrapper;
}


let createTooltip = () => {
    let teams = Object.keys(info);

    let tooltipWrapper = d3.select("body")
        .selectAll(".tooltips")
        .data(teams)
        .enter()
        .append("div")
        .attr("id", d => {
            return "tooltip" + d.replace(/ /g, '')
        })
        .style("opacity", "1")
        .style("position", "absolute")
        .style("transition", "all 0.3s ease-out")
        .style("left", (svg.node().getBoundingClientRect().width + svg.node().getBoundingClientRect().x + 20) + "px");

    tooltipWrapper.append("div")
        .attr("id", "tooltip-text")
        .attr("style",
            "background: #fff;"
            + "display: inline-block;"
            + "pointer-events: none;"
            + "text-align: left;"
            + "padding: 0.6em 2em;"
            + "padding-left: 7px;"
            + "padding-right: 7px;"
            + "white-space: nowrap;"
            + "border: 1px solid #ddd;"
            + "font: 12px sans-serif;"
            + "background: white;"
            + "border-radius: 8px;"
        );

    tooltipWrapper.append("div")
        .attr("id", "pointer")
        .attr("style",
            "position: absolute;"
            + "top: 50%;"
            + "width: 18px;"
            + "height: 18px;"
            + "background: #ddd;"
            + "transform: translate(-40%, -50%) rotate(45deg);"
            + "transform-origin: center center;"
            + "z-index: -1;"
        )
    return tooltipWrapper;
}
createTooltip();



let drawLine = svg => {
    d3.selectAll(".aux_tooltip").remove();
    let center = l => {
        if (l.length == 0) {
            return 0;
        } else if (l.length == 1) {
            return l[0].x;
        }

        let sum = l[0].x + l[l.length - 1].x;
        return sum / 2;
    };

    let computeMedianIndex = l => {
        let c = center(l);
        for (let i = 0; i < l.length; i++) {
            if (l[i].x > c) {
                return i;
            }
        }
        return 1;
    };

    let cluster = (l, vitalMargin) => {
        if (l.length <= 1) {
            return l;
        }
        let medianIndex = computeMedianIndex(l);
        let left = l.slice(0, medianIndex);
        let right = l.slice(medianIndex);
        let leftCenter = center(left);
        let rightCenter = center(right);
        if (rightCenter - leftCenter < vitalMargin) {
            return [l];
        }
        return [cluster(left, vitalMargin), cluster(right, vitalMargin)];
    };

    let flatten = l => {
        let f = [];
        for (let i = 0; i < l.length; i++) {
            if (l[i][0].x !== undefined) {
                f.push(l[i]);
            } else {
                let aux = flatten(l[i]);
                for (let j = 0; j < aux.length; j++) {
                    f.push(aux[j]);
                }
            }
        }
        return f;
    };

    let points = data.map(t => {
        return { x: yScale(t.values[t.values.length - 1].score), score: t.values[t.values.length - 1].score, d: t }
    })
    points = points.sort(function (a, b) { return a.x - b.x });

    let clusters = [];
    if (points.length <= 1) {
        clusters = [points]
    } else {
        clusters = flatten(cluster(points, 40));
    }


    let absoluteTopPosition = svg.node().getBoundingClientRect().y
    clusters.forEach(c => {
        if (c.length === 1) {
            let tooltip = d3.select("#tooltip" + c[0].d.team.replace(/ /g, ''))
            tooltip.style("opacity", "1")
            tooltip.style("top", absoluteTopPosition + c[0].x - 15 + "px");
            tooltip.select("#tooltip-text").html(d => "<div>" + d + ": " + c[0].score.toFixed(4) + "</div>")
            tooltip.select("#pointer").style("background", d => colors(d));
        } else {
            c.map(subC => {
                let tooltip = d3.select("#tooltip" + subC.d.team.replace(/ /g, ''))
                tooltip.style("opacity", "0")
            })
            let legend = c.map(subC => {
                let color = colors(subC.d.team);
                let content = "<div>" +
                    "<div style='border-radius:20px;display:block;width:13px;height:13px;position:absolute;background:" + color + ";'></div>" +
                    "<div style='margin-left:22px;'>" + subC.d.team + ": " + subC.score.toFixed(4) + "</div>" +
                    "</div>"
                return content;
            }).join("");
            let auxTooltip = creatAuxTooltip();
            auxTooltip.select("#tooltip-text").html(legend)
            let auxTooltipHalfHeight = auxTooltip.node().getBoundingClientRect().height / 2;
            auxTooltip.style("top", absoluteTopPosition - auxTooltipHalfHeight + (yScale(center(c.map(d => { return { x: d.score } })))) + "px");
        }
    })


    axisY.call(d3.axisLeft(yScale))
    axisX.call(d3.axisBottom(xScale).tickFormat(d3.timeFormat("%A %H:%M")).ticks(5))
    let lineSelect = svg
        .selectAll(".lines")
        .data(data);

    let group = lineSelect.enter().append("g").attr("class", "lines").merge(lineSelect);

    let pathGroup = group
        .selectAll("path")
        .data(d => [d]);

    let m = pathGroup
        .enter()
        .append("path")
        .attr("stroke", (d) => colors(d.team))
        .attr("stroke-width", "1")
        .attr("fill", "none")
        .merge(pathGroup)
        .attr("d", d => lineFunc(d.values))
        .attr("transform", null)


    let circleGroup = group
        .selectAll(".circle")
        .data(d => d.values);

    circleGroup
        .enter()
        .append("circle")
        .attr("class", "circle")
        .attr("cx", d => xScale(d.timestamp))
        .attr("cy", d => yScale(d.score))
        .attr("r", 3)
        .attr("fill", (d, i, nodes) => {
            let team = d3.select(nodes[i].parentNode).datum().team;
            return colors(team);
        })
        .merge(circleGroup)
        .attr("cx", d => xScale(d.timestamp))
        .attr("cy", d => yScale(d.score))

}

drawLine(svg);

	`
)

var trainSeeds = []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
var greenTrainResults = map[int64]float64{ // TODO update if we change game probabilities
	1:  104891,
	2:  74526,
	3:  175570,
	4:  199047,
	5:  584665,
	6:  45691,
	7:  26930,
	8:  601650,
	9:  14856,
	10: 69113,
}

var competitionSeeds []int64
var greenCompetitionResults map[int64]float64

var competitionLock sync.Mutex
var competitionMap = map[string]map[int64]float64{}

var peakTimes = []int{
	8 * 60 * 60,
	10*60*60 + 45*60,
	15 * 60 * 60,
}

var allGames = AllGamesType{Games: map[int]GameData{}}
var peopleDirectionByEventsProbabilitiesKeys []string

const (
	PEOPLE_ENTERING_BASE = 0.03
	PEOPLE_LEAVING_BASE  = 0.03
	PEOPLE_INROOF_BASE   = 0.3
	PEOPLE_OUTROOF_BASE  = 0.03
	PEOPLE_BETWEEN_BASE  = 0.1
)

var peopleDirectionByEventsProbabilities = map[string]map[EventsState]float64{
	peopleEntering: {
		EventsState{}:                     PEOPLE_ENTERING_BASE,
		EventsState{Rain: true}:           0.5,
		EventsState{PeopleGoToWork: true}: 0.7,
		EventsState{BusStopsIn: true}:     PEOPLE_ENTERING_BASE,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
		}: 0.8,
		EventsState{
			Rain:       true,
			BusStopsIn: true,
		}: 0.5,
		EventsState{
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.7,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.8,
	},
	peopleLeaving: {
		EventsState{}:                     PEOPLE_LEAVING_BASE,
		EventsState{Rain: true}:           0.05,
		EventsState{PeopleGoToWork: true}: PEOPLE_LEAVING_BASE,
		EventsState{BusStopsIn: true}:     0.6,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
		}: 0.05,
		EventsState{
			Rain:       true,
			BusStopsIn: true,
		}: 0.3,
		EventsState{
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.6,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.3,
	},
	peopleInRooftop: {
		EventsState{}:                     PEOPLE_INROOF_BASE,
		EventsState{Rain: true}:           0.01,
		EventsState{PeopleGoToWork: true}: PEOPLE_INROOF_BASE,
		EventsState{BusStopsIn: true}:     PEOPLE_INROOF_BASE,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
		}: 0.01,
		EventsState{
			Rain:       true,
			BusStopsIn: true,
		}: 0.01,
		EventsState{
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: PEOPLE_INROOF_BASE,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.01,
	},
	peopleOutRooftop: {
		EventsState{}:                     PEOPLE_OUTROOF_BASE,
		EventsState{Rain: true}:           0.6,
		EventsState{PeopleGoToWork: true}: 0.2,
		EventsState{BusStopsIn: true}:     0.3,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
		}: 0.7,
		EventsState{
			Rain:       true,
			BusStopsIn: true,
		}: 0.7,
		EventsState{
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.4,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: 0.8,
	},
	peopleInBetween: {
		EventsState{}:                     PEOPLE_BETWEEN_BASE,
		EventsState{Rain: true}:           PEOPLE_BETWEEN_BASE,
		EventsState{PeopleGoToWork: true}: PEOPLE_BETWEEN_BASE,
		EventsState{BusStopsIn: true}:     PEOPLE_BETWEEN_BASE,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
		}: PEOPLE_BETWEEN_BASE,
		EventsState{
			Rain:       true,
			BusStopsIn: true,
		}: PEOPLE_BETWEEN_BASE,
		EventsState{
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: PEOPLE_BETWEEN_BASE,
		EventsState{
			Rain:           true,
			PeopleGoToWork: true,
			BusStopsIn:     true,
		}: PEOPLE_BETWEEN_BASE,
	},
}

func init() {
	for key := range peopleDirectionByEventsProbabilities {
		peopleDirectionByEventsProbabilitiesKeys = append(peopleDirectionByEventsProbabilitiesKeys, key)
	}
	sort.Strings(peopleDirectionByEventsProbabilitiesKeys)
}

func main() {
	http.HandleFunc("/", handle)
	log.Fatalln(http.ListenAndServe(":5000", nil))
}

func handleFile(w http.ResponseWriter, filename string) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.Write(content)
}

func handle(w http.ResponseWriter, r *http.Request) {
	origin := r.Header.Get("Origin")
	if origin == "" {
		origin = "*"
	}

	w.Header().Set("Access-Control-Allow-Origin", origin)
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "X-Requested-With,content-type")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	if r.Method != "GET" && r.Method != "POST" {
		return
	}

	switch r.URL.String() {
	case "/classification":
		handleClassification(w)
		return
	case "/score_chart.js":
		handleFile(w, "score_chart.js")
		return
	}

	queryID := r.URL.Query().Get("id")
	seed := r.URL.Query().Get("seed")
	team := r.URL.Query().Get("team")

	id, _ := strconv.Atoi(queryID)
	if id == 0 {
		id = createGame(seed)
	} else {
		stepGame(team, id, r)
	}

	allGames.Mutex.Lock()
	defer allGames.Mutex.Unlock()
	game := allGames.Games[id]
	b, err := json.Marshal(game)
	if err != nil {
		log.Println("Error marshalling game:", err)
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(b)
}

func handleClassification(w http.ResponseWriter) {
	info, err := getTeamsInfo()
	if err != nil {
		log.Println("Could not get teams info:", err)
		return
	}

	infoString, err := json.Marshal(info)
	if err != nil {
		log.Println("Could not marshal teams info:", err)
		return
	}

	index := fmt.Sprintf(`<!DOCTYPE html>
	<html>
	<head>
		<title>Clasificación - Hackathon 2019 Nayar Systems</title>
		<link rel="shortcut icon" href=" data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAhYAAAIWCAYAAAALR8TTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR42u3df3DU9b3v8dfCAvklWZtIgiQkGJsNgiZBhRbtgDbCnXsV6Lm919Z7j8Tq0R5plTujI39Z7F90ZKZoxV57tMTec1B6zulN1HvmUtHCjFBxpiYZ4ZCNRhM2lBAS2ABhE37M3j+yy426QDb7+e5+fzwfMztarF+/38/3+82+8v788n3WoZgAALi6PeP+vjv+kaQ2SRFJ3VXBWDfN5G0+ggUAwLCeeOhIBI7dhA6CBQAAVtgTDxxtktqqgrE2moRgAQCAFWFjt6TdVcFYhCYhWAAAYEp7ImQQNAgWAACYtkdSczxk0HVCsAAAwJiecSGjmeYgWAAAYMpQPGQ0EzIIFgAAmJSoZDTRXUKwAADApHZJTfGQwcBPggUAAMa8Hg8Yu2kKggUAAKa0S9pSFYw10RQECwAATBmStCUeMugmIVgAAGDM65I2soeJWVNoAgCAR62V9EVXyNfUFfJV0hwECwAACBgECwAACBhuxRgLAACSe04M8kwZFQsAAJL7maTurpBvPU0xcVQsAAC4uh5JjSy0dXVULAAAuLoKSX/qCvmaGX9BsAAAwJTVktq6Qr6NNEVydIUAADA57ZLW0z3yZVQsAACYnFqNdY9s6Qr5AjQHwQIAABOe1Fj3yHKagmABAIAJicGdW7zeEIyxAADArHaNTU1t8+LFU7EAAMCsWkmtXl1Yi4oFAADWadFY9cIzy4JTsQAAwDqJdS/qCBYAAMCECo11jTQSLAAAgCnbukK+JrdfJGMsAADIrHZJy9067oKKBQAAmVWrse3YXTnugmABAEDmFUra7cZxFwQLAACyFy62uW29C4IFAADZ9Us3DeokWAAAkH1r3RIumBUCAIB9OH7GCBULAADso1ZjgzoDBAsAAOD5cEGwAACAcEGwAACAcEGwAAAALg4XBAsAAAgXBAsAAAgXBAsAAODicEGwAADAWeGiiWABAABMWW3n5b8JFgAAOM/arpBvC8ECAACY8mRXyNdIsAAAAKZs6wr5lhMsAACAKc1dIV8dwQIAAJhQKKnJLtNQCRYAADhfraRmggUAADBlmR1mihAsAABwjye7Qr41BAsAAGBKU1fIV0mwAAAAJhQqi+MtCBYAALhPbbbGWxAsAABwp6yMtyBYAADgXhlf34JgAQCAe2V8vAXBAgAAd1vWFfKtJ1gAAABTNmZqCirBAgAA9yuU1ESwAAAApmSkS4RgAQCAd1jeJeKnjd1l+5s36nA4n4YArqAmOPSl/z23/Izy8i4oL++C5pafoYHgZokukeUEC0zI4XC+Qp0BGgK4gom8I8HqSDxoDKu4eETFRSOqCUZoPLjBsq6Qb01VMGbJNFSCBQBcIXy0thV/6c/Ly89obvmw5pafUU0wQoUDTrWlK+TbXRWMGU/LBAsASEE4XKBwuEB7VXLpz4LVEdUEh7SofoCgAaeokLRe0kbTB/Z91qEY7esem56vpSsEyKLc3AuqCUa0qH5Qi+oGlJd3gUaBnc2rCsa6TR6QigUAGBSN+tXaVqzWtmK9pqCC1WMh486lfYQM2FGTDA/kpGLhMlQsAPuqrxugkgE7uqsqGNtt6mBULAAgQxKVjO25VVpUP6gVDb2MyYAdNEmqJFgAgENFo37t3VeivftKVF5+RisajujOpX00DLKloivka6wKxppMHIyuEJehKwRwptzcC1rRcEQrGnrpJkE29EiqMzH9lCW9AcAGolG/Wt6u0FMblqj5rUoNDObQKMikxPTTtBEsAMCGAePpDUu0/c0bdfYsPdbImPVdIV/aJW+CBQDY1LvvzblUwSBgIAMKZaBqQbAAABsb30Xyx11lNAislnbVgmABAA4JGG/sqNJTG5aoI8QAbVgm7aoFwQIAHGRwMEe/2FyrF7cuoHsEVkmrakGwAAAHam0rpnsEVimU1EiwAACPSXSPbHq+lumpMG3S3SEskOUyphbIeuapdhoTrnb2rF+HwwWSpIHBHA0MzNDZqF/h+J85TW7uBa1Z1aMVDb3cXJjy0GRW46SDDknVBCM0AlxvUf1A0j8fCxo56ggFNDCYo8PhfNsHjkT1oiNUqEceCrF6J0xYr7F9RAgWAJCO4qIRFReNfC1gd4QC8U+hbZfOHxt7EdAT6w7yCwLSVdsV8i1PdedTggUATFBNMPKlL+uOUEAftxbr47YiDdpojEM06tcvNtdq9X09WrOqmxuHdDRKSilYMMbCZUyNsdj2D3toTCAFA4M5+ri1WB/sK7FVt0l93QBdI0jXvKpgbMIJlVkhAGBAcdGIVjT06ufP/kXPb9qve757REVFI1k/r9a2Ym3aXHtpoCowCY2p/J8JFgBgQch44AefafOm/frp4wcVrM7uWIdwuECbnq9lxU4QLADA6RbVD2jD0+16ftN+3bH0mHJzs9MlkRh38cG+Um4KUlXRFfKtIVgAgI0UF43okYc6tHnTfq2+rydrAeO1bUFtf/NGbghS1UiwAAAbysu7oDWrurMaMN59b45e3VbDzUAqVk90/xCCBQBkOWDc890jGf/v791XQrhAqhoJFgDggIDxwA8+0/Ob9qu+biDj4WLT87XskgqCBQC4TXHRiJ5Yd1DPPNWe0Wmqoc6ANm0mXGBCartCvkqCBQA4SE0wcmn8RaaEwwWEC0zUVWeHECwAwI4/vVd167ln/6Ly8jMZCxfbdzBbBFfVSLAAAIeaW35GP3/2LxmrXjCgExNw1e4QggUA2NyaVd0ZG3tBuMBEHskr/UM61GCZxBbTgN2M36HUKVuL1wQj+vmzf9Gr24JqbSu2PFzMLT+jFQ29PCxIplHSFoIFshIsWt6uoCFgO8mey/LyMyouGtHc8mHNLT+juXPH/red5OVd0BPrDqr5rUrL3603dlQpL++C7lzaxwODr6rtCvkqL7fjKcECADQ2eDEcLvhSNSA394JqghHVBIdUE4xoboYGUl7NmlXdmlt+Rq9uCyoate7H+PY3q8ZClk2uG7ayXFITwQIAUhCN+tXaVnwpbOTmXtCi+kEtqhvQovqBrJ7bovoBbSge0YtbF2hwMMey69/0fK02b9qvvLwLPBD4Ur69XLBg8CYApPBFu3dfiX718gI9/sQdenVbjT5uLc7a+SRmjVg5JTUa9WvT5lpuPr5q+eX+AcECANIMGU9tWKLmtyo1YFHl4Ery8i5ow1Ptli4HHg4XsCMqvqqwK+RbTrAAAAsMDuao5e0KPb1hiV7dVqPD4YKMh4sn1h3UHUuPWfbfePe9OfpgXyk3G+MlDRaMsQAAg/buK9HefSUKVke0ZlVPRqezPvJQx6VzsML2N6tUE4zYbrYM7BUsqFgAgAVCnQH9YnOtNj1fm9Eukkce6rCschGN+vXi1gXcXCQsI1gAQBYCxtMblmj7mzdmbJMvK8NFOFyg5rcqubGQJCUbZ0GwAIAMePe9OXpqw5KMjVOwMly0vF3BqrpIIFgAQLZEo369ti2Yse6RB+7/zLKpqK9uC7LNOggWAGAHoc6Ann3uVv1xV5ml/53EVFQrwsXgYA5dIpCSjLMgWABAFkSjfr2xo0ovbl1g6W/+iamoubnmV8589705GZ9aC/vpCvnqCBYAYBOtbcV69ue3WvoFXVw0og1Pt1ty7Fe3BbmJIFgAgJ0MDuboZz+/1dKBnXPLz+jhh0LGjxsOF1jepQOCBQBgEl7bFtSr22osO/6dS/ssmSnS/FYFAzkJFgQLALCjvftKLB13YcVMkWjUr+072EvEw740gJOICdvLzb2guRbu3gjvOBv1K+yAwYatbcXatDlHG55qN75deV7eBT3yUEibnq9VNGruK2DvvhKtWdXNct8e1RXy1VUFY20ECzjC3PIzlg08g3cNDOZoYCDn0l87QoU6HC4w+mWbjnC4QJs211oSLuaWn9GaVT16Y0eV0eO++tsg76p3VUoiWADwruKikaS/XR8OF6gjFFBHqFCtbcWuDRcrGnqNX2OoM6COUCCjG6/BNuokNUuMsQCAr/02v6KhV0+sO6itL+zVww+FVF83kPVwYcWYi0ceChlf36L5rQoeIu8GCxEsAOAK8vIu6M6lfZdCxg/v71JRFsYQWBUu8vIu6IEfdBk9ZqJqAc8JECwAIMUv4RUNvdq8ab8efiikYHVmy/3hcIEli1HdubTP+LVQtfCkZQQLAEjjy3jD0+165qn2jFYwWtuKLVnn4pEfmV04i6qFN3WFfAGCBQCkoSYYuVTBsGIvjmT27isxvkJncdGIVt/XY/SYVC08qY5gAQAG3Lm0T5s37bdkVctkXtsWNL63yIqGXqPVl1BnICNbw8NWqFgAgCljC0916Jmn2jNSvdj0vNnBnHl5F7RmlemqRSUPhrdQsQAA0xLdI1YP7oxG/Xpx6wKjx7xzaZ/RqsXefSXsIeJBBAsAMCwv74I2PN1ufNzCV4U6A8Z3Fn3E8A6oVu7YCttZTrAAAAutWdVt+cDON3ZUGR1vUROMGK22/HHXHB4EjyFYAICFElNTrQwXpte3MDnWYnAwh6mn3lFJsACADEhspGdVuAiHC4x2idQEI0bHWtAd4hkVBAsAcEm4aH6rwuhASZNVi49bixjE6SEECwBwQbiIRv3avuNGY8czOUMkGvXr4yzvFIvM6Ar5AlNikvi452OKnc5F3Fc+LvqUl5/RMxaFi737SnQoFDB2rncaXPDr49Yi7r83PnVULAAgC5WLhw1P60xoMbiU9j0NvcaO1dpWTHeIR0whXlGyoGTBh0/mP4vqBixZ5yLUGVBHR8DIOeblXtAd3zZXtWhtLebee+DjjxGucJnvcTscw4pjAXax6r5uHQ7nq9Xw+IOWtysUDJpZi6K+fkB7/1xi5FiHQgEtXdrHjXd9xQIAkDU/agwZ33o91BlQyNDaEfV1A8bOr7WtiBvufgGCBQBkUV7eBT3caH68Rcvb5sZa1NcNGjlONOo3FnhgWwzeBIBsCwYjavjuEaPHNLlt+R0Guy86OgkWbsd0UzF2MxnGbvLhk9nPqvu6jU9BbXmr0tgUWWPdIUw7df3HL/mIV67is9lxTJ0LzyncLS/von5wf5e2NZnb96O1rUhnz05TXl76gaW+blC73kt/Q7Fwb4Gxc4JNKxY0AQDYwx1LjylYPWTseNGo39iASVPjLCQp1FnIzXZzsKBsQ3dIMm48Fz58nPC5z/DaFrt2lRk5r+pgxFhXzeFwAfdabu4KYYEAXO7b3A7HsOJYgI0FqyOqrh5Sp6Hf6sO9+RocyDEyRiJYPaS29vQrIKFQQLq3h5vt1ooFTQAA9rLKcNXCVHeIqUW3OukKIVgAADInUbUwZd++UkPnZe6cwuECbrRLsaQ3kopxPkBWLf12n9HukOGz/rRnYpSVn1Fu7gVFo+lvJnY4nK+y8jPcaBeiYgEAdgwWS48ZXdeizVB3SHn5sJHjDBpavAsECwBACuHClJChFS+D1RFbnQ8IFgCAiQYLg1uWm+pWKTfUfTE4OIMb7FKMsUBSzDYFsq8svpS2iW6DwcEcDQymP+20zGBXCO+1O1GxAAAbqzO54mUo/aqFyS3emRniTiyQBWeUCHhO4VHV34zoPQN7dEhSb7hAiqXfvWJqAa/oWT/vthuDBfcU5ArAvmoNVizCvQVG3qWxqkX6weJwOF/fNDQYFPZBVwgA2JypxbLC4XwjxzHVHWJiPQwQLAAAKQcLM7/VR6N+nT2b/pe5qWDBWhYECwBAFpSVmVuhsrc3/QGTRUWjBAtcFmMsXCZmo+OY3jYd8CpTX+SSNDA4Q99M8ximVgTl3XYnKhYAYHMm99QwUSUwdT6DAyySRbAAAGQnXJQNu+6aTpygK8SN/JKPVkASPpscw4pjAc6Tm2em+6GzMyDpsMt+1sBWwYL+LSTDOhaAvRQVjehTFdrmfSorG1Zvbz7vNr4eLLirJALLjsPoTcBcsPjGqK3eJ2NbuvNuuw5jLAAAAMECAJA61o6A1RhjgaTYNh1w3zspSSdOzLDV+8S77T5ULAAAAMECAADYD10hSIrppgDvN+82JoOKBQAAIFgAAAD7YYEsJEdfCMD7zbuNyQQL7ik/L6w6DgtvAvb8/rXb+w13oSsEAAAQLAAAgP3QFYKkGGIB8H7zbmMyqFgAAACCBQAAIFgAAAAXY4wFkmJ3U8B976Qd3yfebRcGC+4qSBYAyYJkAXPBQj5aAUn4OB+A95t3GyljjAUAADCGMRZIinUsAN5v3m1MBhULAABAsAAAAPZDV4jLXF82bJsJHW7d3fRIb77mlA3zsNnYZ58W6rPOwrSPc2P1kG785pAtrsluk0Ls9LMGNgsWNIG7fO/7n9MIFntpy836yfpPCBd2Dhadhdr5b3MNHOmwbYIFP2vgFHSFACkaifr10pabdaQ3n8YAgK9ggSw4g82e00S4+MmTVC549rgm4EvBgucL/BxMI1y8cLPWES549rgm4BK6QoA0w8XWF+gWAQCCBUC4AACCBUC4AAD7YowFLOO1zU0T4eJxxly45tmz0xoqbBYMp6BiARgOFy9TuQBAsABAuAAAggVAuAAAG/HH6GyDVQx2dDvxOU2Ei8ef+ETXM+aCZ88u1yRnvk9wDioWgNXh4sWb9VcqFwAIFgAIFwCQGn9MPloBljD1bMUMHiu74eIW/T3dIg579ny2efZMngc/92ElKhZAxsLFVP2aygUAggUAwgUAECwAwgUAZBhLesMR3PacJsLFjxlzwbPHNcFlqFgAWQwX/5PKBQCX8RNdwa9Y2Q8XP37iE10/h8oFzx7XBOejYgHYJFz89QiVCwDOxxgLOOKXIrc/p4lw8RiVC1s9L2ybDqSOigVgo3DxCpULAAQLAIQLABhDVwgcwUvPaSJcPEq3CM8e1wQHomIB2DRc/IbKBQCCBQDCBQCCBQDCBQAY4I/R2QaLmHq2YgaP5eRw8ehPP9Fsxlxk9tmL2efZM3ke/NyHlahYAE4JF7+6WUepXAAgWAAgXADwCqabwhF4Tr8cLv6ObhGePa4JNkXFAnBguPgHKhcAbMov+WgF2JxPPKfJw8Xf/fQAlYuMPH9cEzBRVCwAR4eLhVQuANgKYyxgGXZjzFy4eITKhSXPC7ubAqmjYgG4IFy8SuUCgE34ia7w1K+gbg4XL1K54NnjfUL2UbEA3BIuRqhcAMg+xliAX7BcGC4epnLBs8f7hCyhYgG4MFy8RuUCAMEC8I6cnIuECwAECwBmzC4b1t888BnhAoDrMMYClnHjWgIm26Z+cb8k6Q/bb7Q8XPzIY2MuWMcic8cCvoqKBZBF9Yv7M1K5+C2VCwAEC4BwQbgA4DT+GDUxWMVgPTrmwr6Q8ddUd3u/YjHpf79hbbfIb3+1UD/6yQGVur1bxI3PnsHz4Oc+rETFArCJ+sX9+t4PM1C5eGmh+qhcACBYAIQLwgUAggUAwgUAz2C6KSzD9LjJX1Pd4n7FJDVbPebipYV6yIVjLphu6r33CTYKFjSBu7R+NEuREzPSPs5d/yFMY2ZZYp0Lq8PFNpeGC/CzBgQLGND20Sx1d83kZSdcEC7AzxpkBWMsAAeEizUZGHOxjTEXAAzwx+SjFfA1Zp4Ln83Ox7ltXLf4uKTMVC4af3LQBZULn7Hj2OfZc+f7xHeQ+1CxAByibvHxjFQuml5aQOUCwKT5GR6My/wawfnY8Jrqbs9M5aLppQVqXHeQMRc8e7QvUg8W3FN+Xlh1HHY3teaaam8/rpikFqu7RbY6N1ww3dRZ7zfcha4QwIHqbj+u1RZ3i4yOTFXTVrpFABAsAMIF4QIAwQIA4QKA0zHGAkkxdtM511QbH9Bp5ZiLRLhY68EBnTx7tC9SQ8UCcIHaDFUuXqdyAYBgARAuCBcAMsUfow6FJEw8FyafLTc+p1Zc0y23HVcsJr31prXdIq9vXaAHH7dvt4ipto3F7PPsufV94jvIfahYAC5Te/txrfqB9ZWL371M5QIAwQIgXBAuABAsABAuADgB002RFNNN3XFNt8SX/37b4jEXv3t5bMxFiQunovLs0b5IDRULwOVqbz+u+zJUuThG5QIgWNAEAOGCcAGAYAGAcAHAdhhj4TJsm+6c+5SNa7olvvx3JsZc/G0Wx1ywbbqz3m+4CxULwGNuyVDl4n9RuQA8ya+Yj1ZAkl8jfJyPi6/pltsGpJhPb++osjxc/O3j/66S64e5T1yTd9rX46hYAB51y+3Hdd/9XZb+N8bCxU069lcqF4BXMMYCyX+J4Hw8cU03x9e5eMfyysVN+u8OrVzw7NG+SA0VC8Djbrn9uO7NQOXiH6lcAAQLAIQLwgWAVNAVgqTsNB3N9LHc1MYmJbpF/o/F3SL/+PJN+m8Wd4sw3dQ5zx7fQe5DxQLAJbfcflz/KQOVi3+icgEQLAAQLggXAAgWAAgXADLGH6ODy10M3c+Yzdb8jblwTW+7X9PNt40t/231mIt/evkmPfD3hsdcuPHZM3gednq/+Q5yHyoWAK4YLjJRudj+ayoXAMECAOGCcAGAYAGAcAHAKqxj4TJsm+6c++S0a1p429g6F/9m8ZiL7b8eG3MxK40xF6xj4az3G+5CxQLAhN1823H9xwxVLvqpXACO5KcJgNT88Mf/nvYxcnIvOjpclFx/ViPRqfY9x9uPa27VqbSPU/iNUR54INVgQRkKybC76eWVG/jCsmMbp+K6DO1SOtk2mnntqGZeO+r5++SEa+I7yH3oCgEAAMb4iYtwxK8RPKeAO98n3m3XoWIBAACM8cfkoxWQ5JcIny2OYcWxAK++k3Z8n3i33YeKBQAAIFgAAAD7YbopkmLsJsD7zbuNyaBiAQAACBYAAIBgAQAAXMwfo4PLVYztOBizxzES18RzCs+/2zF7HStmw+uCPVCxAAAABAsAAECwAAAALsY6FkiKdSwA3m/ebUwqWNAEQGrCn89M+xg5ORd03fVnaUsDbWmlwmtHNfPaUR56gGABWOefX5mf9jHKbjil//rYIc+35akTM7Tzn2+w7fl9u+GIvn1PLw89kEqwoAyFZGI2OYYVx3JTGzvdTbcdV0zSH20aLmI2uk9ufZ94D9yHwZsAsmrBbce14r98TkMALuEnLsIRv0ZQsnB3uLj1uCSbVi549mhfpISKBQDbhAsqF4DzMcYCjvglgl8aveEmG1YuePZoX6SGigUA24ULKheAc/klH63gKj6bHcfUufhceJ949y4fLgYk2aVy4cb75LNRu/AeuA0VCwC2DRdULgDnYYyFy8RsdByT5xJz4X3i3bu6+bcOKCbp3SxVLljHwhk/a2AvVCwA2NpNtw7oHioXAMECAAgXgPewQJbb0BfinPvEu5dauFg0IMWkd//lBm/eJ/pC4JRgwT2FE9511hKANDbmQspsuODZo32RGrpCADguXNzzfbpFAIIFABAuANejKwRJsW26M9rYy2riU1F3WdgtwhAL3gOkjooFAMeaf+uAGqhcAAQLACBcAAQLACBcALgixlggKaabck1Okxhz8Z7hMRc8e7wHSDFYcFdBsuCa3GL+orF1LoyGC5493gOkhK4QAK4LF9+lWwTIGrpC+EXEsuOwore3rslOagxVLphu6oyfNbAXKhYAXBsuqFwABAsAIFwABAsAIFwAYIwFLoNJIVyTmwQXjU1FfX8SYy549ngPkGKwkHy0ApLw2eQYVhzLTW2MiapZNCjJp/f/ZV6K98jnwufFx3sAy9AVAsBD4WJAd3//CxoCsBALZCE5tjd1Rhsj9XBRPyDFpPf/dd7E7hHzTXkPkFqw4J6CXMHPU69JjLn401XCBbmC9wCpoysEgCfVLBrQXf+ZbhGAYAEAhAvAtugKQVJMN+WavCIYX/77ct0iPHu8B0gNFQsAhAsqFwDBAgAIFwDBAgAIF4CLMcbCZdg23Tn3iXfPnqrjU1F3/+s8pps65GcNbBYsuKtwxNtuo/OZPe902scoLj3LT1QbC8YX0XLtNx+jN2FpsACQklUPd9AIXggX8dkiAFLDGAsAAGAMYyyQFD0hAO837zYmg4oFAAAgWAAAAIIFAABwMcZYICm2TQfc907a8X3i3XYfKhYAAMAYv+SjFZCEj/MBeL95t5F6sIhRh3IXQ/czZrM1f3lOwbtt8FA2er95t92HrhAAAECwAAAABAsAAOBiTDd1GbZNB3i3nfZ+w12oWAAAAIIFAAAgWAAAABdjjAWSYtt0gPebdxuTChbcVZAsAN5vzgWm0BUCAACMoSsElv0Swe6mgD3fAQoWsBIVCwAAQLAAAAAECzhE3xfX0AgAgJQxxgJJ2WkZ7b7ua+iHBQy/35wLrELFApb5RulZGgEACBZwsoLAOducy/Sci8aOdSYyg5sLAA7gj1GHcpX8wKiR45w+OUMllafTOobJR+v0yenKLxzlBsOzTP6sNnGsmA2vCzYJFpKPVnAVM/dzrEKQ3rFKK88Yu6oTR/OMHg/w6rtt/lhuOheYQFcIHOFEXx6NAAAEC2Ra6bzTRo5zoi/XyHHS7U5JOEmwAABHYLqpy5i6n+dGpho5VkHgnI4ZCjo8q4C9fk647VxgBhULlykwNHjTVIXA5CyVvm4W7QIAggUyHCzMfJGfG5lq5DglhrpmJCl8KMANBgCCBTIt31C4MFEhMLlIFhULALA/xli4UEFgVMOR6Wkf58zJ6YpVpneMaTkXlR84Z+R8Tvbl6nRkuq0WAQMyhW3T4ZhgwV11n2tLozpm4Lf7E315qooNpn2c0srT6morMnJt4UMBzf9WPzcZIFnApugKcSFzAzjtNeVUkrrairnBAECwQCZ9ozRq5DjHDI1pMBksTvblslgWANgYYyxcKGBwwOSJvjxdm+bx8gPndG1p1FgF5NCHs7R0TTc3GpgkekJgJSoWLjQ9PmDShGPdBUaOY7JqEe4IGJsOCwAgWGACTE3zNNUdUlU3aOzazo9MVceHJdxkALAhukJcKlAaVbgj/QWl+rqvMVKqDJSeNTbtVBrrDgl+6+PucAYAAA9GSURBVJim51zkZsMT7DbdNGbD64I9ULFwKVNdD+dHphpb3ru8JmLs+qhaAADBAg4MFpKMVD4kqeZbx4xeY8eHsxhrAQA2wwJZLnZtSVQnj6U/E6P3UEC3LPtr2sfJLzxn7JykeNXizyW6ZflfudlwP/pC4JRgwT11r1mVp418iZ88lqszkelGZpoEv3VMH7ZUGrvGT/bM1g11A8ZmwQBkFO+eC8ygK8TF7NgdckPdoKbNMDvg8s/NldxsACBYwGplBgdLfmFwKe0aw3t99Pdco44PZ3HDAcAG/JKPVnBzuAgOqTdUmPZxTh7L1XBkhqHukH59sme20ev8ZPf1Kqk8o2sNLWcOuJvPJscweRzYxZSYxvq4+LjzM8voBmBFRs5pWs5FzasdNPognx+dqj83V2p0ZCr3nY9rP6a47Vz42OtDV4jbKxZGu0OKjB3r5uVHjV9r5FiuPv6/Zdx0AMhmxYImcLf8wDkFSsx0DwwPTVe/oSW+8wPnFFzSb/x6v2gv0ucGAxAAIDX+mLRH0jKawr3m1Q2qdaeZ3+Q7Ppyl6wx1ryxYflSftxXp/KjZRa72t1RoWs5Fo9UawE2YbgpLgwV31f3KghFjweJIqFDDJ82saTF9xkUFv9WvA4YHckrS/uYK5a8dZTAnQLJAhtEV4gEmu0MkKbTf3NTOhcuOKr/Q/OJW50en6v3Xq3WyL5cHAAAyHCyoF3tA0ODaEV+0FRndo2PJ6h5LrplwAQBZCBYxqY3pMe7/zAka3Fl0dKpC+2cZO7frKk9rTnDI8nDBc8CH6aZj7HQ+3Femm8KhpuVcVKXBtSM6P5yl80arFt3Gl/r+arj4op3ZIgBgecVCUhvN4A3zak8Yr1qYDD5WdYkkzvejlgpLBooCAL4cLBhj4RGzKk8bHShpumoxpyaiagvWthjv4J7Z+mBHldHzBgAQLDxrgcEVL01XLaSxWSImZ7AkcyRUqJ2vzDe22BcAYFywWNdwiq4QDykLRoyOZTi4Z7aGI9ONHW+sS8S68RYJw0PT9afffVOtO8uoXgCAyWAR/+sQTeEN03IuqtrwtuWtO8uNHi9QGrV0vMV4nftnaecr83WkI8DDAQAGgwVVCw8xvUfHkVCh8W6FOTURLVh2NCPtMTw0XR/8/ga9/3q1Iqx5AQBGgkU3TeEdpqeeStJHLRXGz3PhsqPGz/NKjvcUaOdv5mt/S4XR7h0AIFjA9RYargYMD023ZCrnktU9uq7iTEbbpru9SO+8uFDvv15NFwkATDJY0BXiMfmBc8arAQf3zLakK+E793dZPlMkmeM9Bfrg9zfonRcW6oDhQaoA4PZg0U1TeM9CC8Yw7G+pNH7MaTkXdffazqyEC2msGnNwz2y98+JC7Xxlvg5YFKAAwDXBgimn3pQfOGd8gGTkWK4lXSLZDhfjr+/gntna+Zv5+sMvavXBjiod2DNb/d3XUNEAAEn+cX/fLqmWJvGW4JL+sRU0R82t5XBwz2yVBSMKlJoNAYlw8f7r1Yocy37F4PzoVB0JFepIqFAHx/15YkxIfmBU+YFzPGQwggXd4MRg0U2w8J7EuhYHDVcZPthRpZWPHdK0nIvGz/futZ3a31KpI6FCW7bp8Z6CL/0VALxk/O6mdId41MJlR43uISKNjUuwYrxFIlzceX9XRqeiAgBSDxa7aQ7vWmzBSpdHQoXqNLyXyHhLVvdYct4AADPBgoqFh82qPK05QfMru7fuLLO0b3he7aDuevBTy/cWAQCkGCzWNZyKSOLXPw+rXxm25Av6gx03WDo9c1blad335IGML6QFALhCsIjbTZN4V37gnBYuN7+2xfnRqdrfUmnpLqKJQZ2Z2l8EADCxYEF3iMdVL+m35Df/yLFcvf96teVblC9cdlQrHz2U9fUuAIBgMWY3TYIlq7st6RKJHMu1bKbIeIHSqFY+dojqBQBkO1jEV+Acolm8zaouEWlspsh+C3ZCTWbhsqO69wnGXgBA1oJF3G6aBVZ1iUhju4dmKlzkB87p7rWduuvBT+keAQCCBbLpO/d3WTaNM5PhQhqbObLysUNavLrH+GJgAACCBSZgbIXLzy07fqbDhTS27sW9Tx7Q4tU9VDAAIBPBgnEW+Opv+lYOgkyEC6tniyQLGCsfO6S7HvzUkoXBAMCr/Jf582ZJa2keSGODIPu7r7FsU63u9iJF+vJ099pO45uWTSQ4zao8reHIdB0JBdT54SwND7H9OQBM1pTL/Plumgbjfef+LkvHJiTWuRiOZOdLPT9wTtVL+nXvkwe08tFDql7Sz1gMADAYLJppGoyX2FHUyj05IsdytfOV+ZYu/z0RgdKo6lf26t4nD+jeJw6ofmWv5gSH2I8EACYbLOL7hrTTPPjqF+4Si3cTPT86VTt/M19ftBfZ4poTlYw77+/S3zzTrpWPHlL9yl5V1g6yPgYAJOG/wj9rkvRLmgjjzamJaPHqHn1k8WyOj1oq1N9doEUrezM+7uJq4SpQ+uXZJMOR6RqOzNDw0PRLXTlW7ugKAE4NFrtpHiQzr3ZQkb5cde6fZel/JzGoc8nq7q99mdtJfuCc8gNfGY/BcuIAPOpyYywS007ZRh1JJboDrJYY1Gl1iAEAWBws4hjEictasronI+Hi/OhUte4sy+qsEQCAmWDRRBPhauEiU4MYj/cUaOcr86leAIBTgwXdIZiI79zflbHlsaleAICDg0XcFpoJVzIt56LuXtuZ0b03jvcU6J0XF+rAntkZXw4cAJBesGCcBSYULlY+digjYy7GO7hntt5+YaFt1r0AAILFVaxrONUtaQ9NhYnI1IDO8c6PTtVHLRV6h4ABAPYPFnFNNBXsHC4kaXhoOgEDABwSLJrFVupIMVwsXp2dcb8EDACwebCI7x3CWAukZF7toBav7sna5l2JgPGHX9TqwJ7ZzCIBALsEizhmh2BS4eLutZ1Z3Rn0/OhUHdwzW++8uFAf7KjSkY4ANwYAsh0s4mtasOMpUhYojeq+Jw9kdDrq5RwJFeqD39+gP/yiVvtbKtgsDAAM86f4/98iaRvNhlQlpqO27iyzxcqZ50enqru9SN3tRZo246Lm1ERUFhzSnJoINwsA0uCLxWIp/Qtbd82MSCqk6TDpqkFHQPtbKnR+1J4LW11XcUazKk9rVvyvAL7u/derdbynIO3j3P/sxzSmuzznn8S/tEXSz2g7TNacmohWlp7VBzuqFDmWa7vzO95ToOM9BTo4LmhcW3pWgdKori05a+st3AEg2wgWyIr8wDmtfOyQDuyZrYN7Ztv6XBNBY7xASVTTci5eqmjMim/Elh8YVX7gHDcYgGel3BUiSVt3zWyStJbmgwmRvlztb6m0ZfUi7QBVeE55BA249L010Z1JV4jrTKorRJI2EixgSqA06pjqRaqGh6ZreIj1MwB4x5TJ/Evx/UNep/lg0sJlR7Xy0UO6Lt6tAADwSLCI20jzwbRAaVR3r+3M6oqdAIAsBAt2PYWV5tUO6r4nD6h6ST+NAQBeCBZxG2lCWGVazkXVr+zVvU8cyMpuqQCADAeLdQ2ndktqoRlhpfzAOS1Z3aO7HvyU8RcAYHN+A8dYL2k1TQmrzao8rbsrT6u/+xod2DPbyKp/AACz0u0KYYYIshMw1nbqrgc/pYsEAGzGb+g4G8W6FshCwJhVeVoLlx1V5/5Z+qKtyLb7jwCAV0wxcZB41eI5mhPZkB84p/qVvbrvyQNavLrHFtuzA4BX+Q0ea4vGxluw8ymyYlrORc2rHdS82kFF+nL1RXsRVQwAyLBJ7RVyOVt3zVwv6Zc0K+zkSEdAvaFCHekIEDIAm2GvENd5zmiwiIeLbkkVtC3sHDKOd1/DHh4AwQIWBAu/BQdtlPQn2hZ2NKcmojk1EUljuzP291yj3o4AU1cBwBDjFQtJ2rprZrNY2wIO0999jfp7ChTpy1OkL5eKBpABVCxcx5KKhTRWtegWAznhIInpqwnnR6bqZF+e+nsKNByZruHIDCobAHAVlgSLdQ2nIlt3zdwoBnLCwablXPxa2BgfOM6PTNXJY7mSpEhfns6NTL30zyPxPwcAr7GkKyRh666ZuyUto5kBAPCE56ZY/B9YTxsDAOAdlgaLdQ2n2sSKnAAAECwMhouNktppagAACBamNNLUAAAQLIyId4n8D5obAACChalwsUXSHpocAACChSmNkoZodgAACBZpW9dwqluMtwAAgGBhMFw0S3qBpgcAgGBhKlysF1NQAQAgWBi0Roy3AACAYGEC4y0AACBYmA4XzWLJbwAACBYGw8VGSa9zKwAAIFiYwmBOAAAIFmasazgVkbRcDOYEAIBgQbgAAAC2ChbxcNGmsW4RAABAsDASLpokPcStAQCAYGEyXLDsNwAABAtj4WK9mIYKAADBwmC4aCRcAABAsCBcAABAsLB1uNjD7QIAgGBhyhqxOicAAAQLE8YtoEW4AADApnyxWMxxJ71118wmSWu5fQAA2MpzU5x41gzoBADAnqY49cQJFwAAECysCBes0AkAAMHCWLhYL/YWAQCAYGEwXDTFwwVbrgMAQLAwFi6WEy4AACBYmAoXbZLqxFoXAAAQLAyFi26NVS5auL0AAGSWIxfImqitu2ZulPQzbjMAABnx3BQ3X926hlMbJX1PjLsAACAjprj9Atc1nGoWe4wAAECwMBgu2uLhgpU6AQCwkKvHWCSzddfMRklbJBVy+wEAMOq5KV674vh6F0xJBQDAAp6rWIzHrBEAAIzyXsVivPiskXpRvQAAwAhPVyzGo3oBAEDavF2xGI/qBQAA6aNikUS8erFezBwBACAVVCySiVcv6sR+IwAApISKxVVs3TVzuaQmSRW0BgAAV/QcwWLiAWOj6B4BAOCKwYKukAmKd49UimXBAQC4LCoWk7B118xKSRslraU1AAC4hK6QNAPG8njAWEZrAABAsCBgAABAsLB1wGgUXSQAAIIFDAaMSjEGAwBAsIDhgBHQ2BTVRrEOBgCAYAGDIaMxHjAYhwEAIFjAWMCo1P+vYrDYFgCAYAFjIaNR0hpJq2kNAADBAqYCRiAeMNZLqqVFAAAEC5gKGZXxkLFGjMcAABAsYDBkBMaFjOViTAYAgGABg0FjeTxgLBfVDAAAwQIGQ0YgHjDqCBoAAIIFrAgbiaCR+DAQFABAsIDxsFEZ/yT+nlVAAQAECxgNHHWSAhqrbATGhQ+JbhUAAMECFgeQ8ZbTMgCAJHb/P03KVFK47t1sAAAAAElFTkSuQmCC" type="image/png" sizes="any">
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
		<script src="https://d3js.org/d3.v5.min.js"></script>
	</head>
	
	<body style="font-family: 'Open Sans'">
		<div style="margin: 20px 55px 0px 55px; color: #333333; display: flex; align-items: center; justify-content: space-between;">
			<h1 id="title-heading" class="pagetitle">
				<span id="title-text">
					Clasificación Hackathon 2019
				</span>
			</h1>
			<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEBLAEsAAD/7QAsUGhvdG9zaG9wIDMuMAA4QklNA+0AAAAAABABLAAAAAEAAQEsAAAAAQAB/+E7LGh0dHA6Ly9ucy5hZG9iZS5jb20v
			eGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE
			1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5
			bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogICAgICAgICA8ZG
			M6Zm9ybWF0PmltYWdlL2pwZWc8L2RjOmZvcm1hdD4KICAgICAgICAgPGRjOnRpdGxlPgogICAgICAgICAgICA8cmRmOkFsdD4KICAgICAgICAgICAgICAgPHJkZjpsaSB4bWw6bGFuZz0ieC1kZWZhdWx0
			Ij5ucy5MT0dPX0hPUklaT05UQUwuMTA3eDM1bW08L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6QWx0PgogICAgICAgICA8L2RjOnRpdGxlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPH
			JkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBHSW1nPSJodHRwOi8v
			bnMuYWRvYmUuY29tL3hhcC8xLjAvZy9pbWcvIj4KICAgICAgICAgPHhtcDpNZXRhZGF0YURhdGU+MjAxOC0wOS0xOFQwOTowMjo1NSswMjowMDwveG1wOk1ldGFkYXRhRGF0ZT4KICAgICAgICAgPHhtcD
			pNb2RpZnlEYXRlPjIwMTgtMDktMThUMDc6MDI6NTdaPC94bXA6TW9kaWZ5RGF0ZT4KICAgICAgICAgPHhtcDpDcmVhdGVEYXRlPjIwMTgtMDktMThUMDk6MDI6NTUrMDI6MDA8L3htcDpDcmVhdGVEYXRlP
			gogICAgICAgICA8eG1wOkNyZWF0b3JUb29sPkFkb2JlIElsbHVzdHJhdG9yIENTNiAoTWFjaW50b3NoKTwveG1wOkNyZWF0b3JUb29sPgogICAgICAgICA8eG1wOlRodW1ibmFpbHM+CiAgICAgICAgICAg
			IDxyZGY6QWx0PgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHhtcEdJbWc6d2lkdGg+MjU2PC94bXBHSW1nOndpZHRoPgogICA
			gICAgICAgICAgICAgICA8eG1wR0ltZzpoZWlnaHQ+Mjg8L3htcEdJbWc6aGVpZ2h0PgogICAgICAgICAgICAgICAgICA8eG1wR0ltZzpmb3JtYXQ+SlBFRzwveG1wR0ltZzpmb3JtYXQ+CiAgICAgICAgIC
			AgICAgICAgIDx4bXBHSW1nOmltYWdlPi85ai80QUFRU2taSlJnQUJBZ0VBU0FCSUFBRC83UUFzVUdodmRHOXphRzl3SURNdU1BQTRRa2xOQSswQUFBQUFBQkFBU0FBQUFBRUEmI3hBO0FRQklBQUFBQVFBQ
			i8rSU1XRWxEUTE5UVVrOUdTVXhGQUFFQkFBQU1TRXhwYm04Q0VBQUFiVzUwY2xKSFFpQllXVm9nQjg0QUFnQUomI3hBO0FBWUFNUUFBWVdOemNFMVRSbFFBQUFBQVNVVkRJSE5TUjBJQUFBQUFBQUFBQUFBQUFBQUFBUGJXQUFFQUFBQUEweTFJVUNBZ0FBQUEmI3hBO0FBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBUlkzQnlkQUFBQVZBQUFBQXomI3hBO1pHVnpZd0FBQVlRQUFBQnNkM1J3ZEFBQUFmQUFBQUFVWW10d2RBQUFBZ1FBQUFBVWNsaFpXZ0FBQWhnQUFBQVVaMWhaV2dBQUFpd0EmI3hBO0FBQVVZbGhaV2dBQUFrQUFBQUFVWkcxdVpBQUFBbFFBQUFCd1pHMWtaQUFBQXNRQUFBQ0lkblZsWkFBQUEwd0FBQUNHZG1sbGR3QUEmI3hBO0E5UUFBQUFrYkhWdGFRQUFBL2dBQUFBVWJXVmhjd0FBQkF3QUFBQWtkR1ZqYUFBQUJEQUFBQUFNY2xSU1F3QUFCRHdBQUFnTVoxUlMmI3hBO1F3QUFCRHdBQUFnTVlsUlNRd0FBQkR3QUFBZ01kR1Y0ZEFBQUFBQkRiM0I1Y21sbmFIUWdLR01wSURFNU9UZ2dTR1YzYkdWMGRDMVEmI3hBO1lXTnJZWEprSUVOdmJYQmhibmtBQUdSbGMyTUFBQUFBQUFBQUVuTlNSMElnU1VWRE5qRTVOall0TWk0eEFBQUFBQUFBQUFBQUFBQVMmI3hBO2MxSkhRaUJKUlVNMk1UazJOaTB5TGpFQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUEmI3hBO0FBQUFBQUFBQUFBQUFGaFpXaUFBQUFBQUFBRHpVUUFCQUFBQUFSYk1XRmxhSUFBQUFBQUFBQUFBQUFBQUFBQUFBQUJZV1ZvZ0FBQUEmI3hBO0FBQUFiNklBQURqMUFBQURrRmhaV2lBQUFBQUFBQUJpbVFBQXQ0VUFBQmphV0ZsYUlBQUFBQUFBQUNTZ0FBQVBoQUFBdHM5a1pYTmomI3hBO0FBQUFBQUFBQUJaSlJVTWdhSFIwY0RvdkwzZDNkeTVwWldNdVkyZ0FBQUFBQUFBQUFBQUFBQlpKUlVNZ2FIUjBjRG92TDNkM2R5NXAmI3hBO1pXTXVZMmdBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBWkdWell3QUEmI3hBO0FBQUFBQUF1U1VWRElEWXhPVFkyTFRJdU1TQkVaV1poZFd4MElGSkhRaUJqYjJ4dmRYSWdjM0JoWTJVZ0xTQnpVa2RDQUFBQUFBQUEmI3hBO0FBQUFBQUF1U1VWRElEWXhPVFkyTFRJdU1TQkVaV1poZFd4MElGSkhRaUJqYjJ4dmRYSWdjM0JoWTJVZ0xTQnpVa2RDQUFBQUFBQUEmI3hBO0FBQUFBQUFBQUFBQUFBQUFBQUFBQUdSbGMyTUFBQUFBQUFBQUxGSmxabVZ5Wlc1alpTQldhV1YzYVc1bklFTnZibVJwZEdsdmJpQnAmI3hBO2JpQkpSVU0yTVRrMk5pMHlMakVBQUFBQUFBQUFBQUFBQUN4U1pXWmxjbVZ1WTJVZ1ZtbGxkMmx1WnlCRGIyNWthWFJwYjI0Z2FXNGcmI3hBO1NVVkROakU1TmpZdE1pNHhBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQjJhV1YzQUFBQUFBQVRwUDRBRkY4dUFCRFAmI3hBO0ZBQUQ3Y3dBQkJNTEFBTmNuZ0FBQUFGWVdWb2dBQUFBQUFCTUNWWUFVQUFBQUZjZjUyMWxZWE1BQUFBQUFBQUFBUUFBQUFBQUFBQUEmI3hBO0FBQUFBQUFBQUFBQUFBS1BBQUFBQW5OcFp5QUFBQUFBUTFKVUlHTjFjbllBQUFBQUFBQUVBQUFBQUFVQUNnQVBBQlFBR1FBZUFDTUEmI3hBO0tBQXRBRElBTndBN0FFQUFSUUJLQUU4QVZBQlpBRjRBWXdCb0FHMEFjZ0IzQUh3QWdRQ0dBSXNBa0FDVkFKb0Fud0NrQUtrQXJnQ3kmI3hBO0FMY0F2QURCQU1ZQXl3RFFBTlVBMndEZ0FPVUE2d0R3QVBZQSt3RUJBUWNCRFFFVEFSa0JId0VsQVNzQk1nRTRBVDRCUlFGTUFWSUImI3hBO1dRRmdBV2NCYmdGMUFYd0Jnd0dMQVpJQm1nR2hBYWtCc1FHNUFjRUJ5UUhSQWRrQjRRSHBBZklCK2dJREFnd0NGQUlkQWlZQ0x3STQmI3hBO0FrRUNTd0pVQWwwQ1p3SnhBbm9DaEFLT0FwZ0NvZ0tzQXJZQ3dRTExBdFVDNEFMckF2VURBQU1MQXhZRElRTXRBemdEUXdOUEExb0QmI3hBO1pnTnlBMzREaWdPV0E2SURyZ082QThjRDB3UGdBK3dEK1FRR0JCTUVJQVF0QkRzRVNBUlZCR01FY1FSK0JJd0VtZ1NvQkxZRXhBVFQmI3hBO0JPRUU4QVQrQlEwRkhBVXJCVG9GU1FWWUJXY0Zkd1dHQlpZRnBnVzFCY1VGMVFYbEJmWUdCZ1lXQmljR053WklCbGtHYWdaN0Jvd0cmI3hBO25RYXZCc0FHMFFiakJ2VUhCd2NaQnlzSFBRZFBCMkVIZEFlR0I1a0hyQWUvQjlJSDVRZjRDQXNJSHdneUNFWUlXZ2h1Q0lJSWxnaXEmI3hBO0NMNEkwZ2puQ1BzSkVBa2xDVG9KVHdsa0NYa0pqd21rQ2JvSnp3bmxDZnNLRVFvbkNqMEtWQXBxQ29FS21BcXVDc1VLM0FyekN3c0wmI3hBO0lnczVDMUVMYVF1QUM1Z0xzQXZJQytFTCtRd1NEQ29NUXd4Y0RIVU1qZ3luRE1BTTJRenpEUTBOSmcxQURWb05kQTJPRGFrTnd3M2UmI3hBO0RmZ09FdzR1RGtrT1pBNS9EcHNPdGc3U0R1NFBDUThsRDBFUFhnOTZENVlQc3cvUEQrd1FDUkFtRUVNUVlSQitFSnNRdVJEWEVQVVImI3hBO0V4RXhFVThSYlJHTUVhb1J5UkhvRWdjU0poSkZFbVFTaEJLakVzTVM0eE1ERXlNVFF4TmpFNE1UcEJQRkUrVVVCaFFuRkVrVWFoU0wmI3hBO0ZLMFV6aFR3RlJJVk5CVldGWGdWbXhXOUZlQVdBeFltRmtrV2JCYVBGcklXMWhiNkZ4MFhRUmRsRjRrWHJoZlNGL2NZR3hoQUdHVVkmI3hBO2loaXZHTlVZK2hrZ0dVVVpheG1SR2JjWjNSb0VHaW9hVVJwM0dwNGF4UnJzR3hRYk94dGpHNG9ic2h2YUhBSWNLaHhTSEhzY294ek0mI3hBO0hQVWRIaDFISFhBZG1SM0RIZXdlRmg1QUhtb2VsQjYrSHVrZkV4OCtIMmtmbEIrL0grb2dGU0JCSUd3Z21DREVJUEFoSENGSUlYVWgmI3hBO29TSE9JZnNpSnlKVklvSWlyeUxkSXdvak9DTm1JNVFqd2lQd0pCOGtUU1I4SktzazJpVUpKVGdsYUNXWEpjY2w5eVluSmxjbWh5YTMmI3hBO0p1Z25HQ2RKSjNvbnF5ZmNLQTBvUHloeEtLSW8xQ2tHS1RncGF5bWRLZEFxQWlvMUttZ3FteXJQS3dJck5pdHBLNTByMFN3RkxEa3MmI3hBO2JpeWlMTmN0REMxQkxYWXRxeTNoTGhZdVRDNkNMcmN1N2k4a0wxb3ZrUy9ITC80d05UQnNNS1F3MnpFU01Vb3hnakc2TWZJeUtqSmomI3hBO01wc3kxRE1OTTBZemZ6TzRNL0UwS3pSbE5KNDAyRFVUTlUwMWh6WENOZjAyTnpaeU5xNDI2VGNrTjJBM25EZlhPQlE0VURpTU9NZzUmI3hBO0JUbENPWDg1dkRuNU9qWTZkRHF5T3U4N0xUdHJPNm83NkR3blBHVThwRHpqUFNJOVlUMmhQZUErSUQ1Z1BxQSs0RDhoUDJFL29qL2kmI3hBO1FDTkFaRUNtUU9kQktVRnFRYXhCN2tJd1FuSkN0VUwzUXpwRGZVUEFSQU5FUjBTS1JNNUZFa1ZWUlpwRjNrWWlSbWRHcTBid1J6VkgmI3hBO2UwZkFTQVZJUzBpUlNOZEpIVWxqU2FsSjhFbzNTbjFLeEVzTVMxTkxta3ZpVENwTWNreTZUUUpOU2syVFRkeE9KVTV1VHJkUEFFOUomI3hBO1Q1TlAzVkFuVUhGUXUxRUdVVkJSbTFIbVVqRlNmRkxIVXhOVFgxT3FVL1pVUWxTUFZOdFZLRlYxVmNKV0QxWmNWcWxXOTFkRVY1SlgmI3hBOzRGZ3ZXSDFZeTFrYVdXbFp1Rm9IV2xaYXBscjFXMFZibFZ2bFhEVmNobHpXWFNkZGVGM0pYaHBlYkY2OVh3OWZZVit6WUFWZ1YyQ3EmI3hBO1lQeGhUMkdpWWZWaVNXS2NZdkJqUTJPWFkrdGtRR1NVWk9sbFBXV1NaZWRtUFdhU1p1aG5QV2VUWitsb1AyaVdhT3hwUTJtYWFmRnEmI3hBO1NHcWZhdmRyVDJ1bmEvOXNWMnl2YlFodFlHMjViaEp1YTI3RWJ4NXZlRy9SY0N0d2huRGdjVHB4bFhId2NrdHlwbk1CYzExenVIUVUmI3hBO2RIQjB6SFVvZFlWMTRYWStkcHQyK0hkV2Q3TjRFWGh1ZU14NUtubUplZWQ2Um5xbGV3UjdZM3ZDZkNGOGdYemhmVUY5b1g0QmZtSismI3hBO3duOGpmNFIvNVlCSGdLaUJDb0ZyZ2MyQ01JS1NndlNEVjRPNmhCMkVnSVRqaFVlRnE0WU9obktHMTRjN2g1K0lCSWhwaU02Sk00bVomI3hBO2lmNktaSXJLaXpDTGxvdjhqR09NeW8weGpaaU4vNDVtanM2UE5vK2VrQWFRYnBEV2tUK1JxSklSa25xUzQ1Tk5rN2FVSUpTS2xQU1YmI3hBO1g1WEpsalNXbjVjS2wzV1g0SmhNbUxpWkpKbVFtZnlhYUpyVm0wS2JyNXdjbkltYzk1MWtuZEtlUUo2dW54MmZpNS82b0dtZzJLRkgmI3hBO29iYWlKcUtXb3dhamRxUG1wRmFreDZVNHBhbW1HcWFMcHYybmJxZmdxRktveEtrM3FhbXFIS3FQcXdLcmRhdnByRnlzMEsxRXJiaXUmI3hBO0xhNmhyeGF2aTdBQXNIV3c2ckZnc2RheVM3TENzeml6cnJRbHRKeTFFN1dLdGdHMmViYnd0MmkzNExoWnVORzVTcm5DdWp1NnRic3UmI3hBO3U2ZThJYnlidlJXOWo3NEt2b1MrLzc5NnYvWEFjTURzd1dmQjQ4SmZ3dHZEV01QVXhGSEV6c1ZMeGNqR1JzYkR4MEhIdjhnOXlMekomI3hBO09zbTV5ampLdDhzMnk3Yk1OY3kxelRYTnRjNDJ6cmJQTjgrNDBEblF1dEU4MGI3U1A5TEIwMFRUeHRSSjFNdlZUdFhSMWxYVzJOZGMmI3hBOzErRFlaTmpvMld6WjhkcDIydnZiZ053RjNJcmRFTjJXM2h6ZW90OHAzNi9nTnVDOTRVVGh6T0pUNHR2alkrUHI1SFBrL09XRTVnM20mI3hBO2x1Y2Y1Nm5vTXVpODZVYnAwT3BiNnVYcmNPdjc3SWJ0RWUyYzdpanV0TzlBNzh6d1dQRGw4WEx4Ly9LTTh4bnpwL1EwOU1MMVVQWGUmI3hBOzltMzIrL2VLK0JuNHFQazQrY2Y2Vi9ybiszZjhCL3lZL1NuOXV2NUwvdHovYmYvLy8rNEFEa0ZrYjJKbEFHVEFBQUFBQWYvYkFJUUEmI3hBO0JnUUVCQVVFQmdVRkJna0dCUVlKQ3dnR0JnZ0xEQW9LQ3dvS0RCQU1EQXdNREF3UURBNFBFQThPREJNVEZCUVRFeHdiR3hzY0h4OGYmI3hBO0h4OGZIeDhmSHdFSEJ3Y05EQTBZRUJBWUdoVVJGUm9mSHg4Zkh4OGZIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGYmI3hBO0h4OGZIeDhmSHg4Zkh4OGZIeDhmLzhBQUVRZ0FIQUVBQXdFUkFBSVJBUU1SQWYvRUFhSUFBQUFIQVFFQkFRRUFBQUFBQUFBQUFBUUYmI3hBO0F3SUdBUUFIQ0FrS0N3RUFBZ0lEQVFFQkFRRUFBQUFBQUFBQUFRQUNBd1FGQmdjSUNRb0xFQUFDQVFNREFnUUNCZ2NEQkFJR0FuTUImI3hBO0FnTVJCQUFGSVJJeFFWRUdFMkVpY1lFVU1wR2hCeFd4UWlQQlV0SGhNeFppOENSeWd2RWxRelJUa3FLeVkzUENOVVFuazZPek5oZFUmI3hBO1pIVEQwdUlJSm9NSkNoZ1poSlJGUnFTMFZ0TlZLQnJ5NC9QRTFPVDBaWFdGbGFXMXhkWGw5V1oyaHBhbXRzYlc1dlkzUjFkbmQ0ZVgmI3hBO3A3ZkgxK2YzT0VoWWFIaUltS2k0eU5qbytDazVTVmxwZVltWnFibkoyZW41S2pwS1dtcDZpcHFxdXNyYTZ2b1JBQUlDQVFJREJRVUUmI3hBO0JRWUVDQU1EYlFFQUFoRURCQ0VTTVVFRlVSTmhJZ1p4Z1pFeW9iSHdGTUhSNFNOQ0ZWSmljdkV6SkRSRGdoYVNVeVdpWTdMQ0IzUFMmI3hBO05lSkVneGRVa3dnSkNoZ1pKalpGR2lka2RGVTM4cU96d3lncDArUHpoSlNrdE1UVTVQUmxkWVdWcGJYRjFlWDFSbFptZG9hV3ByYkcmI3hBOzF1YjJSMWRuZDRlWHA3ZkgxK2YzT0VoWWFIaUltS2k0eU5qbytEbEpXV2w1aVptcHVjblo2ZmtxT2twYWFucUttcXE2eXRycSt2L2EmI3hBO0FBd0RBUUFDRVFNUkFEOEE3eG91bEpxQ1ArOFNJUkpDQVBSaWNrdkNya2tzcFBVNXdYWmZaRWRRRHVJOEloL0JBODRBbmNpK2JrenkmI3hBOzBqYnJ5MllJSG1qbVI1STFMckdZbzR5M0VWTkhqQ3NENzVuNmoyZUdPQmxHVVRLSXV1Q01icnVNUUpEM3NSbXRNZkw5N0pkV0xlb3gmI3hBO2RvWE1mcU45cGh4VjFKOStMaXVianNiVVN5NGZVYk1UVm5tUlFJdnpvN3RlUWJ2T3RKL01yekRjK1J0VVRWb0hzUE1JMHkvdXRGdmwmI3hBO1Q5M2QvVmtrQVpCUXFzMGJKVjR5Ti90QVVPMjJZSjk1L3dET21zNkRwT2tOcGRzMTVmM0RMZFhzYXJVaXd0RUUxNDNzeFdpTDg5dDgmI3hBO1ZVdk5YbTdXL0xlcHJxZHZGTHJHaWE1YkxGcFZyR2xURnFRRllFcUFDSXJwVzNMZlpaZmZGVU41ajgxZVovS2VuZVhiS1ZuMWpVMFAmI3hBOzEzekxjcWkvRFpJd1M0WlFxcUFGZWRmVDJyUk1WUS81cythdGYwZlZkT0dsYXBMWXJKWVhjOEVVTnVsMHQxZHh2RUxlQmxLU0drbk4mI3hBO2hWU1BHdUt2VExWNTN0b25uakVVN0lyU3hBOGdya1ZaUWU5RGlxcGlxbmMvN3p5LzZqZnF5RS9wUHVWNTc1YjhzUTZyYk0vcXgyNngmI3hBO0xBcXFMYTNrSkwyOGNqRXM2RmlTekh2bk1hSFFETkc3QXJoL2hpZjRRZW84Mm1NYlIrbytTVnNyT2E2aXVJNVpiZEdsRVpnaWhKRVkmI3hBO3FlTWtJUjFhblFnNWs1K3l2RGdaQWdrQy9wQTVlY2FJU1lVbi9sUy91THpTeVozTXJ3U0dKWm0rMDZBQmtacWZ0Y1dGZmZObDJkbWwmI3hBO1BINmpkR3I3Kzc3R2NEWWVlUS9tUjVsYnlIcVNhcEUxajVqL0FFZGNYZWkzOGFWanUxaURmRWdJNHJQSFQ0NHlQOG9iSGJQWk1nOC8mI3hBO2VkTlkwSFR0R09tMnozbDVPeTNlb29pOGl1bjJxQ1M3ZjJZZ2hWK2Z0aXFuNXM4MmEvNWIxVDY5YlJTNnhwR3UyNlE2UGJ4SUQ2T3AmI3hBOzBwQ2hJQ2tSWEN0V3JFMEtueHhWRCtZL05mbVR5cGErWExHVjMxZStqcGVlWjd0VUZGc2d3aW5kVlVMU2t0d0NnQXJ4VGZGVkw4MVAmI3hBO00ydTZUcXRrdWw2bk5aRnRQdXA3UzJndDB1VnZMMUpZRmd0bVZra05KRmtmZFNDT3RjVmVrd05LMEViVElJNVdVR1NNSGtGWWpkYTkmI3hBOzZIRlYrS3V4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFZobHRMUHFrVnhLWlJiVDNGeFl2NmlIanhab0VQd1ZyOUF6a2NYRnFSS1YmI3hBOzhFcHp4R3gwUEFPVE8wZGN5WHJYZHZIZkdOYnUzdHJ2bUVQOTRyS29XUlJUdnhOUjJ6TnkrSVp4R1hoNDR3eWN1dGdVUjh0KzVGbzMmI3hBO3l2ZlMzZW5FU0JRTFpsZ1RqWGRSREcxVFVuZjRzek95TThzbUtqL0JVUjd1R0ovU2dzVDB2VlBLOS9wZDM1SEdvNmxlM1ZxMXpiZnAmI3hBO0pWbmlsa3VJV2FXUklMb2ZBWllhZ1U1ZmVNMnFFZnBtdmFBYmpUdGZUVTdtK2o4eEpCcG1rMkFCb2pSYzJtWXhBN09DRzlaMit6eHAmI3hBO2lxRDh1NnBvZCsxdjVWczlaMUNlNDBlK1owdjVvcGd0ejlUbEx5VzMxZ2poTDZZWUkrOVNOOThWUkdoYTlwK29lYjlZdGpIcTdYTjQmI3hBO2tNVXNGemJTUTIxbkVrVGxhTlg0UFdJWnVYVmpRZGhpcVUrV1lMTHpacHRoYjIrb2E3OVRoMHVhMmgxS1dPUzFXY1BKSDZVNG1CbzAmI3hBOzBYQWNldGZpcjN4Vm1lbmVZOUlUUnRObWwxSmJ0Ym1aZE9pdkNBcG51bGRvVytFYkJpOGJWcGlxaHJubi93QXQ2SnJNT2tYMHMzMXUmI3hBO1NJWEVub3dTekpEQ3orbXNrN1JxM3Bxei9DQ2ZweFZrTXpsSW5jZFZVa1Y5aGtaR2dTckJJWkxyVjdZM0hyclozTnpmMlVucXhuangmI3hBO1pyT1A3QVBLdlhvZXVjN0NVczhlSytHVXNrRGYrWU9UVnpUSFVwTDJSZ2wvNlNYdHZwOStKRlFta2l1SXdzaUFqb2VCcU8yWldjekomI3hBO3FkY2NjZVQ0OHR4OHZna3BsNVN2WmJyU0ZFZ1VmVitFQ2NhN3FzU0VFMUozK0xNdnMzS1o0dC80ZHZzREtCMll4WWF2NVgxUFNicnkmI3hBO1l0L3FWNVBhcmNXNDFHTko0cFpwcllsNVJiM1FvalN4SGFuTGYzR1o3Skc2WnJtZy9XTlAxeE5UdWIrUHpOSEJwK2tXUXJSUFJWMm0mI3hBO2Iwd2RtQnFabmJkYVV4Vkx2THVwYUZxbjFIeXpaYTNxRTl4b2QrMGlYOGtVcXJlQ3pkdlV0emNFY0pSSDZpcSsrK3gzNjRxaWZMM20mI3hBO0hUZFU4MWExYUNQV1RkWHl3eFhGdmMyOHNFRmpFc0RGRkJyKzdNdnhOeTZzU1BBWXFnUExjTnI1cDB5enRZYjdYUmJKcEVsdGI2clAmI3hBO0ZKYWlZUExDOE55SmdhTk9ucGloL2FIS3ZVNHF6RFQvQURMcEkwZlM3aVRVRnVsdnBWc1lMdmlFO
			WU1QlpEOEs3QWxvbXJUYkZWSFcmI3hBO2ZQbmx6UjlaaTBpOWxsK3R2R3M4cGlobGxqZ2lrZjAwZWQ0MVlScXovQ0NmcDJ4VmtPS3V4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMksmI3hBO3NEMFRVSU5LdUo3UFViZHBIUXc4bFVCbVNTM1VScS9Ba0VxeXFHVmhuTGFMSkhCSXd5UnY2ZmdZaXJydUlvZ2hqeEp2ZSthTkpuZ2smI3hBO0F0NXpONmJyRXhpM0JkYWJHdTFjMk9idERGS0o5TXJvMXQzcDRsM2tZMTArN0I2cmNoVDh4YndnakhzVVZqbC9XLzNzVkJZeDVhVFYmI3hBO0lQTzhuMVBROVcwaUs2bnVKZGNna21pazBobWNFcmN3czRabWtsZWxSRngveWhUTndsTXZMM2xMVGJQOHhOZDFHTzB1NHhHa2N0bkomI3hBO01hMlFsdjZtN05vdkFjWFpyZERMOFIrMTJyaXFYK1ZrMVdEemt5MmVoNnJvMXJjUzNFbXQyc3NzTDZVSkc1TWx4YnN3Wm1lVnZ0Q0wmI3hBO2lCKzBQRlZsT2sydnArYi9BREJjZlY1NC9YanNSOVlrSDdtWGdrZ3BGOEkzU3Z4L0VlM1RGV0VmbG5GYWFMb1R5MjFwNW5TOXN0TW0mI3hBO2M2VHFTWHBnQmpJYjA3ZFhpU0F6RWdCQkd0ZHpRZGNWU2hmeTZ1Tk0waTNXSzExZWUxMEpMZnpGWlc2elJ2SStveUZQVnQxUVE4bWQmI3hBO1BRYzA4WDdjaFJWa2ZucGRSSG1hRzkwelE5WUdzQ0NPUFRkWTBxU01SVHB6NXRiWHFUZ3hSb3JNV3JLaDlqWGJGWG83SzBrSlYvaFomI3hBOzFvMU42RWpmQklXS1ZnZWhYOEdsU3kyT3BXclNORTBMY1ZDczhVMXZHSVEzQWtNVlpVREs2MUcrYzNvODBjSk1Na2JxdmdZamg1ZHgmI3hBO3F3UTB4TmMwMzFUelBwZHpZM0VhVzg1bmVHV09KakZ1RElwRksxMnJ0WE0zVWEvSE9CQUVyb2diZDdJeUN2NUhGTkxtQjZyUFEvTVEmI3hBO3hnNVoyVC9kbit0L3ZRbUhKaS9sY2FwQjV6WkxQUXRXMGUzdUpybVhXN2VXV0o5SzV2Vmt1TGQyRHN6eXRTcXhjZC90RE5velRUeTUmI3hBOzVPMHV3OCthM3FVVnJjcHhWSHMybE5iUkh2Zmp1amFyeEhGbWVKVEo4UjY5cTRxbFhrNU5VZzgzdWxwb2VyNk5hVHZjUzYzYVhFc1UmI3hBO21sTE05V1NhMWRnenUwci9BR3ZTS2dWM0dLc2wwR3o5THpwNW91ZnF0eEY5WStvZjZUS1AzRTNDQWo5eDhLL1k2UDhBRWQvREZXSC8mI3hBO0FKZUphNk5vVWsxcGJlYVByOWxwTHQraWRTUzlNQWFNSzNwVzZ5eExBWnVRQ29JMTZWb0tWeFZLVi9MdWZTTk10bGh0ZFd1TGJ5NEwmI3hBO2JYdFB0MW1SM2t2cG1qOWFBS0llVHRIOVhjbGYrTER0OFFvcXlMenYrazQvTTZYbWw2RnF3MWdReFJhZnErbVNSZWhjeDgrVDI5NnMmI3hBO3dhT05FTEVneUlmWWpGWHBLOHVJNUFCcWJnYml2ejJ4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFZMdFcvdzk4SDZYK3FmOFYvVy8mI3hBO1MvRDFNeGRSNEczaThIK2RYNlVHdXFYZjhnOC83VkgvQUU3WmkvNEQvdFgreFI2VTEwcjlEZlYyL1JQMWI2dHpQUDZwdzRjNkN0ZlQmI3hBOzI1VXBtYnAvQzRmM1hEdy8wYXEvZ2tWMFJ1WHBkaXJzVmRpcnNWZGlyc1ZkaXFYYXQvaDJxZnBmNnBYL0FIWDliOUwvQUlYMU14ZFQmI3hBOzRHM2k4SCtkWDZXSnJxbDMvSVBQKzFSLzA3Wmkvd0NBL3dDMWY3RkhwOGszMHY4QVJIMVkvb3I2djlWNUd2MVhoNmZQYXY4QWQ3VjYmI3hBO1ptNmZ3dUg5MXc4UDlHcSt4a0s2SXZMMHV4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFYvLzlrPTwveG1wR0ltZzppbWFnZT4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkFsdD4KICAgICAgICAgPC94bXA6VGh1bWJuYWlscz4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgICAgICAgICB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIKICAgICAgICAgICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyI+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6Rjg3RjExNzQwNzIwNjgxMTgyMkFDQkMyRjY5QTZDQTU8L3htcE1NOkluc3RhbmNlSUQ+CiAgICAgICAgIDx4bXBNTTpEb2N1bWVudElEPnhtcC5kaWQ6Rjg3RjExNzQwNzIwNjgxMTgyMkFDQkMyRjY5QTZDQTU8L3htcE1NOkRvY3VtZW50SUQ+CiAgICAgICAgIDx4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ+dXVpZDo1RDIwODkyNDkzQkZEQjExOTE0QTg1OTBEMzE1MDhDODwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06UmVuZGl0aW9uQ2xhc3M+cHJvb2Y6cGRmPC94bXBNTTpSZW5kaXRpb25DbGFzcz4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+dXVpZDphNGE4OTM0MC01MWE3LTA1NGYtOGYxNi05NmFjMWRkOTI3Yjk8L3N0UmVmOmluc3RhbmNlSUQ+CiAgICAgICAgICAgIDxzdFJlZjpkb2N1bWVudElEPnhtcC5kaWQ6MDI4MDExNzQwNzIwNjgxMTgwODM4NjdERjA4MTY1RTk8L3N0UmVmOmRvY3VtZW50SUQ+CiAgICAgICAgICAgIDxzdFJlZjpvcmlnaW5hbERvY3VtZW50SUQ+dXVpZDo1RDIwODkyNDkzQkZEQjExOTE0QTg1OTBEMzE1MDhDODwvc3RSZWY6b3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICAgICA8c3RSZWY6cmVuZGl0aW9uQ2xhc3M+cHJvb2Y6cGRmPC9zdFJlZjpyZW5kaXRpb25DbGFzcz4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkhpc3Rvcnk+CiAgICAgICAgICAgIDxyZGY6U2VxPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOjAyODAxMTc0MDcyMDY4MTE4MDgzODY3REYwODE2NUU5PC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDE4LTA2LTIwVDExOjA2OjE0KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBJbGx1c3RyYXRvciBDUzYgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpjaGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOkY4N0YxMTc0MDcyMDY4MTE4MjJBQ0JDMkY2OUE2Q0E1PC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDE4LTA5LTE4VDA5OjAyOjU1KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBJbGx1c3RyYXRvciBDUzYgKE1hY2ludG9zaCk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpjaGFuZ2VkPi88L3N0RXZ0OmNoYW5nZWQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICA8L3JkZjpTZXE+CiAgICAgICAgIDwveG1wTU06SGlzdG9yeT4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmlsbHVzdHJhdG9yPSJodHRwOi8vbnMuYWRvYmUuY29tL2lsbHVzdHJhdG9yLzEuMC8iPgogICAgICAgICA8aWxsdXN0cmF0b3I6U3RhcnR1cFByb2ZpbGU+UHJpbnQ8L2lsbHVzdHJhdG9yOlN0YXJ0dXBQcm9maWxlPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6cGRmPSJodHRwOi8vbnMuYWRvYmUuY29tL3BkZi8xLjMvIj4KICAgICAgICAgPHBkZjpQcm9kdWNlcj5BZG9iZSBQREYgbGlicmFyeSAxMC4wMTwvcGRmOlByb2R1Y2VyPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+/+IMWElDQ19QUk9GSUxFAAEBAAAMSExpbm8CEAAAbW50clJHQiBYWVogB84AAgAJAAYAMQAAYWNzcE1TRlQAAAAASUVDIHNSR0IAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1IUCAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARY3BydAAAAVAAAAAzZGVzYwAAAYQAAABsd3RwdAAAAfAAAAAUYmtwdAAAAgQAAAAUclhZWgAAAhgAAAAUZ1hZWgAAAiwAAAAUYlhZWgAAAkAAAAAUZG1uZAAAAlQAAABwZG1kZAAAAsQAAACIdnVlZAAAA0wAAACGdmlldwAAA9QAAAAkbHVtaQAAA/gAAAAUbWVhcwAABAwAAAAkdGVjaAAABDAAAAAMclRSQwAABDwAAAgMZ1RSQwAABDwAAAgMYlRSQwAABDwAAAgMdGV4dAAAAABDb3B5cmlnaHQgKGMpIDE5OTggSGV3bGV0dC1QYWNrYXJkIENvbXBhbnkAAGRlc2MAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAADzUQABAAAAARbMWFlaIAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9kZXNjAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAABZJRUMgaHR0cDovL3d3dy5pZWMuY2gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAuSUVDIDYxOTY2LTIuMSBEZWZhdWx0IFJHQiBjb2xvdXIgc3BhY2UgLSBzUkdCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2aWV3AAAAAAATpP4AFF8uABDPFAAD7cwABBMLAANcngAAAAFYWVogAAAAAABMCVYAUAAAAFcf521lYXMAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAKPAAAAAnNpZyAAAAAAQ1JUIGN1cnYAAAAAAAAEAAAAAAUACgAPABQAGQAeACMAKAAtADIANwA7AEAARQBKAE8AVABZAF4AYwBoAG0AcgB3AHwAgQCGAIsAkACVAJoAnwCkAKkArgCyALcAvADBAMYAywDQANUA2wDgAOUA6wDwAPYA+wEBAQcBDQETARkBHwElASsBMgE4AT4BRQFMAVIBWQFgAWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kHrAe/B9IH5Qf4CAsIHwgyCEYIWghuCIIIlgiqCL4I0gjnCPsJEAklCToJTwlkCXkJjwmkCboJzwnlCfsKEQonCj0KVApqCoEKmAquCsUK3ArzCwsLIgs5C1ELaQuAC5gLsAvIC+EL+QwSDCoMQwxcDHUMjgynDMAM2QzzDQ0NJg1ADVoNdA2ODakNww3eDfgOEw4uDkkOZA5/DpsOtg7SDu4PCQ8lD0EPXg96D5YPsw/PD+wQCRAmEEMQYRB+EJsQuRDXEPURExExEU8RbRGMEaoRyRHoEgcSJhJFEmQShBKjEsMS4xMDEyMTQxNjE4MTpBPFE+UUBhQnFEkUahSLFK0UzhTwFRIVNBVWFXgVmxW9FeAWAxYmFkkWbBaPFrIW1hb6Fx0XQRdlF4kXrhfSF/cYGxhAGGUYihivGNUY+hkgGUUZaxmRGbcZ3RoEGioaURp3Gp4axRrsGxQbOxtjG4obshvaHAIcKhxSHHscoxzMHPUdHh1HHXAdmR3DHeweFh5AHmoelB6+HukfEx8+H2kflB+/H+ogFSBBIGwgmCDEIPAhHCFIIXUhoSHOIfsiJyJVIoIiryLdIwojOCNmI5QjwiPwJB8kTSR8JKsk2iUJJTglaCWXJccl9yYnJlcmhya3JugnGCdJJ3onqyfcKA0oPyhxKKIo1CkGKTgpaymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bwRzVHe0fASAVIS0iRSNdJHUljSalJ8Eo3Sn1KxEsMS1NLmkviTCpMcky6TQJNSk2TTdxOJU5uTrdPAE9JT5NP3VAnUHFQu1EGUVBRm1HmUjFSfFLHUxNTX1OqU/ZUQlSPVNtVKFV1VcJWD1ZcVqlW91dEV5JX4FgvWH1Yy1kaWWlZuFoHWlZaplr1W0VblVvlXDVchlzWXSddeF3JXhpebF69Xw9fYV+zYAVgV2CqYPxhT2GiYfViSWKcYvBjQ2OXY+tkQGSUZOllPWWSZedmPWaSZuhnPWeTZ+loP2iWaOxpQ2maafFqSGqfavdrT2una/9sV2yvbQhtYG25bhJua27Ebx5veG/RcCtwhnDgcTpxlXHwcktypnMBc11zuHQUdHB0zHUodYV14XY+dpt2+HdWd7N4EXhueMx5KnmJeed6RnqlewR7Y3vCfCF8gXzhfUF9oX4BfmJ+wn8jf4R/5YBHgKiBCoFrgc2CMIKSgvSDV4O6hB2EgITjhUeFq4YOhnKG14c7h5+IBIhpiM6JM4mZif6KZIrKizCLlov8jGOMyo0xjZiN/45mjs6PNo+ekAaQbpDWkT+RqJIRknqS45NNk7aUIJSKlPSVX5XJljSWn5cKl3WX4JhMmLiZJJmQmfyaaJrVm0Kbr5wcnImc951kndKeQJ6unx2fi5/6oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3IrdEN2W3hzeot8p36/gNuC94UThzOJT4tvjY+Pr5HPk/OWE5g3mlucf56noMui86Ubp0Opb6uXrcOv77IbtEe2c7ijutO9A78zwWPDl8XLx//KM8xnzp/Q09ML1UPXe9m32+/eK+Bn4qPk4+cf6V/rn+3f8B/yY/Sn9uv5L/tz/bf///+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgBnQTwAwERAAIRAQMRAf/EARUAAQACAwEBAQEBAQAAAAAAAAAJCgcICwYFBAMBAgEBAAIBBQEBAAAAAAAAAAAAAAIDAQUGBwkKBAgQAAAGAgECAQQFDhUMDwcFAQABAgMEBQYHCBESCSETFAphFVa2F/AxUSLVFnaWN3cYWBkaQYGR0ZIjtNRVdbXG1ieXp7fXOHg5obEyk9NlNkaGpkdXcUJSMyQlNUWVZocoSIjIVGQmZ2io6OFiQ1M0YxEBAAECAgUDBxMLDwkIAwEBAAECAxEEITESBQZBBwhRYXGR0hMJgbEiMlKishR0lLQVVbUWVnYXGcFyU3OzVNSFxRg4odFCgsIjM9MkNCV1pdU38GJDg2Q1Rico4fGSk8NllWZjRCY2R//aAAwDAQACEQMRAD8Av8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQrPeWGZYnmmUYzCxzGJMSiu7Crjvyk2pyHmYchbKHHvNWLbXnFJT1PtSRdfwB0kc9vhO+dvmx53uJObzdHD/DmY3ZuXfGaydq5ejO99rosXaqKarmxmqaNuYjGrZppjHVEOTN2cE7uzu77OcuXr0XLlumqYjZwiZjHR5F5H7NHPPctiP5C5+ag4u+l257Pi1wt2s/8Ahj7vm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7k+zRzz3LYj+QufmoH0u3PZ8WuFu1n/AMMPm/3X9nv+d7l9KHzWyVHb6fhFFJ6Erv8AQ7Kwg9x93VJp883YdpEjyGR9ep+XyfGGv7q8L9ziWdn274N3LmNE7XeM3mcvjOOjDvlGawiKdExO1jPksYjyKq5ze5Kf4LM3Y7NNM+NsvdVPNXGHlJK8wm8rkn/ZKqrKBc9vlIvIUtqi7i7fL+AOaOGPC+83ebrpjjLg7fW76J1zk83ls/hp6l6jd2MYaeScdGE62m3+b3N0/wA2zNuuf86mqjxttmHHeSensiNDacrappK+n/B8iiyKgkden9nOeQqqLofkP/hB9OnyPKP1fwH4Qbopce1UWLfE1vdG8K8P3relm7kopx81mLlM5KMJ0ThmZwwx1YTOgZrhLf2V0zZm5R1aJir9SPJedZshToVlGamV0yLPhvF3MyoUhqVGdT/umn2FracL2SMx+wN0b53Pv/d9ve24c3ls7uq9GNu9l7tF61XHVouW6qqKo68TLb1y3cs1zbu01U3I1xMTEx4kv1DUkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQvbnUfws7E+i+7/Nzo8kXS2pj85zjz5U7w9kVuf9wTHtJlftFHjQxl3H8XT8YfnfZhq+MHcfxdPxg2YMYO4/i6fjBswYwdx/F0/GDZgxg7j+Lp+MGzBjB3H8XT8YNmDGDuP4un4wbMGMHcfxdPxg2YMYO4/i6fjBswYwdx/F0/GDZgxg7j+Lp+MGzBjB3H8XT8YNmDGDuP4un4wbMGMHcfxdPxg2YMYO4/i6fjBswYwdx/F0/GDZgxg7j+Lp+MGzBjB3H8XT8YNmDGDuP4un4wbMGMHcfxdPxg2YMYO4/i6fjBswYwdx/F0/GDZgxg7j+Lp+MGzBjB3H8XT8YNmDGDuP4un4wbMGMHcfxdPxg2YMYO4/i6fjBswYw9Dj2YZRiUn0zGb+2o3zUlTiq2c/FQ/2f2KZLLayYlN//tcSpJ/gkN+cCc53OLzYbx9tebzfe89zZ2aomqcpmLtmm5hqi9bpqi3ep/zLtNdE8sS+XNZLJZ6jvebtUXKf86InDsTrjxG2OBcyMkrVsQtgVDGQwiNCHLipQzXXTaenRbrsIvN1U9R+TolBQ+nl+WPyEOzjmU8K7x/uC5Z3Tz4brs793RGzTVnslTRlc/TH7KuuxGzk8zOrCi3GSw0zNdWiI2TvPgPKXYm5uuubVzzNWNVHYifLR4u03fwXZuFbHg+mYpdxpziEEuVWun6NbwPjEZTK140yG0Es+0nEkplZl8otReUdw3M10huaLn73R7a82m+MvnL9FETeylf7zncvqiYv5W5hdppifIxdpiuxXMT3u7XGlx3vHdG8N1XO9523NMclWumexVGjxNccsQ96Oa2mgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIWt0fVa2L5f8b7z83OjyTdLamfznOPPlRvD2RW563DP9C5X7RR40MZfh/wBUfnjZlq2112ftVcesn2zj0zI6W7oq2LCuZFI4xaKsCkLkRoNdOW6j0SFJb8ypuyQRdVEruSfk6dOv7b6NvQX5wuk1wNm+POE977lyG7spva7kKrebnM98m5ay+VzE1095sXadiac1RTGNUVbVNWMYYTO298cUZTc2Zpyt+i5XXVbivGnDDCZqjDTMafIsmfYV597q8Q/J3XzLH6D+iN56vjLwt28/+BtJ+H27vsN/zvdH2Fefe6vEPyd18yw+iN56vjLwt28/+Bnw+3d9hv8Ane6PsK8+91eIfk7r5lh9Ebz1fGXhbt5/8DPh9u77Df8AO90fYV597q8Q/J3XzLD6I3nq+MvC3bz/AOBnw+3d9hv+d7o+wrz73V4h+TuvmWH0RvPV8ZeFu3n/AMDPh9u77Df873R9hXn3urxD8ndfMsPojeer4y8LdvP/AIGfD7d32G/53uj7CvPvdXiH5O6+ZYfRG89Xxl4W7ef/AAM+H27vsN/zvdH2Fefe6vEPyd18yw+iN56vjLwt28/+Bnw+3d9hv+d7o+wrz73V4h+TuvmWH0RvPV8ZeFu3n/wM+H27vsN/zvdH2Fefe6vEPyd18yw+iN56vjLwt28/+Bnw+3d9hv8Ane6PsK8+91eIfk7r5lh9Ebz1fGXhbt5/8DPh9u77Df8AO90fYV597q8Q/J3XzLD6I3nq+MvC3bz/AOBnw+3d9hv+d7o+wrz73V4h+TuvmWH0RvPV8ZeFu3n/AMDPh9u77Df873R9hXn3urxD8ndfMsPojeer4y8LdvP/AIGfD7d32G/53uj7CvPvdXiH5O6+ZYfRG89Xxl4W7ef/AAM+H27vsN/zvdH2Fefe6vEPyd18yw+iN56vjLwt28/+Bnw+3d9hv+d7o+wrz73V4h+TuvmWH0RvPV8ZeFu3n/wM+H27vsN/zvdH2Fefe6vEPyd18yw+iN56vjLwt28/+Bnw+3d9hv8Ane6PsK8+91eIfk7r5lh9Ebz1fGXhbt5/8DPh9u77Df8AO90fYV597q8Q/J3XzLD6I3nq+MvC3bz/AOBnw+3d9hv+d7o+wrz73V4h+TuvmWH0RvPV8ZeFu3n/AMDPh9u77Df873T/AJXwr2ASVGjKcOUskmaErdu0JUrofaSllUOGhJn8cySoyL8AxC74I7nui3VNniThWq9FM7MVVZ+mJnDRE1RkqppiZ1zFNUxGnZnUzHH27cdNm/h+17p8Kdw721ESZsScRszJJK7YNzNbUZmai7C9sqivT3F06/H6eUvL8fpszfHgquk5uyia8lmOFt4VbOOzl89mKZx0+R/lWSy0YxhE68NMYTM44fRb463NX5aL9HZpj9zVUxvecftx4+lS5uC28lpJKPzlMcS+I0J+OvzdNJnPoLp5eikJPp+AOA+MOg90quCKKru9uDN55jL04zt5GbO8YmI/ZbOQu5i5THLhXRTOGuIarl+Jtx5nRbzNET/nY0eiiI/VYklxJcCQ5EnRpEOUyrtejS2XY8hpXTr2uMvJQ4hXQ/jGRD8w7y3TvPc2dubt3xl7+U3janCu1et12rlE9SqiuKaqZ60xDWqLtFymK7dUVUTqmJxjtw/P+H/VHw7MpbXXPw/6obMm1137qy0sqWdGtKiwl1ljDcJ2LOgSXYsuO4RGXe0+ytDiDNJmR9D8pGZH5BrPD3EHEHCW+cvxFwvnc1u7f2Vr27OYy12uzetVasaLluaa6ZwmYnCdMTMTjEzCu9btZi3Nm/TTXaqjTFURMT2YlvbqHlz3qiY/tMkJNXm48bMIbJJT1+VSk7+CyXaklH16yY6SSXk7miLucHc50XPCf1XbmW4I6SEU0zVs27W/LFvCMdER7ZZeiMIxnHazWWpimMae+ZaKe+X44633wVERVmdzz15tTPoJn0NXiTqhvfElxZ8ZibBkx5kOU0h+NLiPNyI0hlxJKbeYfZUtp5pxJ9SUkzIy+MO53du892753fZ3tufMWM3uvMW6blq9ZuU3bV23VGNNdu5RNVFdFUaaaqZmmY0xLjquiu3XNu5E01xOExMYTE9SYnU/QPuRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEKm6z/bb2N9F95+bXR5LOlp+k1x38qN4eyK3O+4Zw3NlftFHjMYdR+ecGrbUpPOFf1LL/6P7X3u4qPQx4Jj9HPfXy2znvZudxPx3OO97fqan0dxt8O0FsoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHn8gxPGMrjeiZLQVF7HIjShFpXxphs93x1R3Hm1OR1/IU2aVEfxjGyONubXm+5ych7Wcf7k3XvjJRExTTm8tavzRjy26rlM12qupVbqpqidMTEvpy2czeTq28pcrt1f5szGPZw1+K1SzvhtiFsl6XglpKxWcZGputnqet6NxRJPo0lx5Z20HvX8dZuySSXkJsdbXPJ4Knmv4mt3d58ze8czw5vicZpyuYmvO7vqnCcKIqrqnOZfaqwmbk3c1FMaKbGrDeO7+Oc9ZmKN4UU3rfmowpr/AFPIz2MKey0Yz7VGd60k+ZyqjfjRHHTai3EX/hlLNV0UaSj2DRebS6tKTUTTpNvEkupoIdPPPT0auePmB3h6V5yN0Xsvu2u5sWc9Z/f8hfnTMRbzNEbEV1RE1RZuxavxTEzVaphv/d2+t371o2sncia4jTTOiqOzH1Yxjrsc9RwRg1Tak6hgbUtgdK7/AMi1TNar5SpF3hUh4vTqNbvV2B5xfV2fRuOn2xpKe41KZ6pYkH1JXaoycT+3OiX01eOujbva3uTeNV/e/NNfu/yjd9VeNeW2qsa8xu+qvRauxjNVdjGmxmZxivYuTTft7a39w7lt8UTdowt5+I0V4a+tX1Y6+uOTGNEys4tlNFmdHByPG7BqyqbBvzjEhrqSkLT5HY8hlRE5GlR19UuNrIlIUXQyHpH5u+cXg7nW4PyfHfAedtZ/hrPW9q3co0TTMaK7V2icKrV63VjRdtVxFdFUTEw4gzeUzGRzFWVzVM03qZ0x9WOrE8kxregG9nzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIUd1q/bc2P8f/AAwvPzc6PJj0sqYnpM8dz/8AaN4eyK3Om4sfabLfaaPGYv7v9kfnrYhqvkkn/Co+urL/AOuBa+9zFR6FPBNxh0dN9fLXOe9m53FPHOPttbx+9qfR3G347P2zGP8AOtpYJrT2r+fa99pfbr072s/4subH0n2u9D9N/wCSa+f5nzPp7X++dnd3/K9eh9OFeeDpEczvMJ7XfOzvj2p9tvTHpX+SZ7Nd99K957//ADPLZjY2PTFn+E2Nrb8htbNWzqO7907w3pt+kbffO94bXkqacNrHDy0xjjhOpj/7KTRHu6/zYzH9jw4V+kQ6Hfxw/srff92tR+CfEH3v5+33Z9lJoj3df5sZj+x4PpEOh38cP7K33/dp8E+IPvfz9vuz7KTRHu6/zYzH9jwfSIdDv44f2Vvv+7T4J8Qfe/n7fdn2UmiPd1/mxmP7Hg+kQ6Hfxw/srff92nwT4g+9/P2+7PspNEe7r/NjMf2PB9Ih0O/jh/ZW+/7tPgnxB97+ft92fZSaI93X+bGY/seD6RDod/HD+yt9/wB2nwT4g+9/P2+7PspNEe7r/NjMf2PB9Ih0O/jh/ZW+/wC7T4J8Qfe/n7fdn2UmiPd1/mxmP7Hg+kQ6Hfxw/srff92nwT4g+9/P2+7PspNEe7r/ADYzH9jwfSIdDv44f2Vvv+7T4J8Qfe/n7fdn2UmiPd1/mxmP7Hg+kQ6Hfxw/srff92nwT4g+9/P2+7PspNEe7r/NjMf2PB9Ih0O/jh/ZW+/7tPgnxB97+ft92fZSaI93X+bGY/seD6RDod/HD+yt9/3afBPiD738/b7s+yk0R7uv82Mx/Y8H0iHQ7+OH9lb7/u0+CfEH3v5+33Z9lJoj3df5sZj+x4PpEOh38cP7K33/AHafBPiD738/b7s+yk0R7uv82Mx/Y8H0iHQ7+OH9lb7/ALtPgnxB97+ft92fZSaI93X+bGY/seD6RDod/HD+yt9/3afBPiD738/b7s+yk0R7uv8ANjMf2PB9Ih0O/jh/ZW+/7tPgnxB97+ft92fZSaI93X+bGY/seD6RDod/HD+yt9/3afBPiD738/b7s+yk0R7uv82Mx/Y8H0iHQ7+OH9lb7/u0+CfEH3v5+33Z9lJoj3df5sZj+x4PpEOh38cP7K33/dp8E+IPvfz9vu32a3kPpa1WhEXYNM0ayI0nZIsKdBde4/l3LeFBQ2fyv+2Munk+SQ3VuLpvdFLiK5Ta3fxtuq3VXq9NU5nJRpx8tVnbGXpp1T5aYw0Y64xpu8N78sxjXlq5+twq9DMso1N9R37HpVFdVN1G6JP0ipsYdkx0UXVJ+ehvPN9FF8by+UfobhrjHhDjPJ+2PB+9d2723fo/fcnmbOat6dMeTsV106Y1adLSb2XzGXq2MxRXbr6lVM0z2piH1huRSAPyT6+Baw5FdZwoljXzGzZlwZ0dqVEksq6dzT8d9C2XWz6fGURkNM31uTc3Ee6r+4uIMpls9uXNW5ovZfMW6L1m7ROui5auRVRXTPUqpmE7dy5Zri7aqmm5TOMTE4THYmGhe5eI6UNysj1Shf5WlciZhr7y3FKSku5aselPqU4pwuhn6K8szV5SbX17Wj6ZulT4MizasZnjvo4UVY0RVcvbjuXKqpmIjGqd23rkzVNWur0pfrmatMWLuPe8vVyFuTjKqZjLb3nXoi5EejiPRR4sa5aCvtPxX3o0ph6NJjurZkR32lMvsPNKNDjTzThJcadbWRkpKiIyMuhjphzmQze7s3d3fvCzcsZ+xcqouW7lNVFy3XTM01UV0VRFVNVMxMVU1RExMTExi5Dpq26YromJpmMYmNMTHWfy7v8AZHzbEM+SZp0vum71HfE8yb1hjFi80nIKI1/KPNl0R7YQO5RIj2sZv+xV5EupLsX5O1SP1j0T+lVxf0ZOMozWVm5neb3P3aI3lu+avI106KfTOXxnZt5y1T5WrRTepjvN3yOxXb0Pfm47O+cvs1YU5umPIV9TrT1aZ/U1x15fMeyCnyqlrshoZrVhUWsZEqFKa69Ftr6kpDiFETjL7LhGhxtZEttxJpURGRkPTzwTxrwxzi8KZHjbg3N289w1vGxTesXqMcKqZ0TFVM4VUXKKomi7brimu3cpqt1001UzEcM5nLXspfqy2Ypmm9ROEx/lridcTqmNMPsjdKgAAAAAAAAAAAAAAABCFw28XvD9q8w+S3BvfL9Nhe09e8k95690XlBG3W4/tDEMS2flNFjmFTPOLJiv2TTU1e0zHLqTd402XYRTSNEnMxoxSmnRim9GEQAAAAAAAAAAAAAAUqPE28Zrn/xq51chdH6j2njVDrnX+RY5X4xUTdYa9vZUKLY4Hil7KQ9bXGPTLKYa7K1eWSnXFmklEkvlSIinERgsimMGiH3wZ4o3+urEP3GtV/sVGdmGdmlZn8CLnNyP5y6m3zlfI7LqrLrvBdiY3j2NyarE8axNuHV2ONOWUth2PjVbWsS1rlpJRLdSpaS8hH0EaoiNSFURGpO+IogAAAAAAAAAAAACJHjV4nOLb88R/lpwtjP1SKPUFJSxtWWjDZtSsoynX8qVT7/iS5SluNy5NXk13FjwWWzSRw6eU+RGSlmnMxoxSmMIxS3DCIAAAAAAACob4vXiFeKV4f8Aymn4rim3qBWidnwn840jaTdPa4mqiUxPtx8jwKbazcXedn3OA276WVKW8++7WSoEl9ROyVJTOIiYWUxTMddFX98GeKN/rqxD9xrVf7FRnZhnZpZy41esS816LeutJ/JLOsdznRasliQdnUVXrHCqO3bxWzS5XT72onYxRQLhVnjHpJWLMdtztmLi+YUXa4fRNMcjE0xho1r4NJdVGSU1RkWP2UK6ob+sgXVJcVklqZW21RaxGp1bZV8thS2ZUKdDfQ604gzSttZKIzIxWrfTAAAAAAAAAAEO3jN+I5M4Acd634NbKqb5FbdtV0erI1hCiXDOP1NM7ClZnntjTzm3YU2HTQpLMKM2+lSHbCwZUaHW2XklmIx7CVMYz1lUL74M8Ub/AF1Yh+41qv8AYqJ7MJ7NJ98GeKN/rqxD9xrVf7FQ2YNmlc68MO/5d55xQwna/M/L42QbR2yhGd0ePRMNxjDCwbXtvEjLw+psYWN1lb6RfXNcRWsr0kvPRUzW4qkNusO90Jwx0K6sMdCQwYYAAAAAABrVzE5F0XEvjFurkRkCYzzGscHs7iorpb5RmL3L5htU2D42t/yqa+eTMbKDB7yJSkekdxJPp0PMaZZiMZwY78OvlZG5o8OtLb8cdh/PTkONlSbIhQ/Mtortl4k+5j2aN+hsobKtjWdtAVYw2DSRogTWDI1JUSjTGEkxhODdkYYAAAAAAAAAAAAAHO5++DPFG/11Yh+41qv9ios2YW7NJ98GeKN/rqxD9xrVf7FQ2YNmk++DPFG/11Yh+41qv9iobMGzS/oz6wh4ojTzTq9yYZJQ24ha472m9YpZfShRKUy6qPjTEgm3SLtUaFoX0P5VRH0MNmDZpZvwf1mDn9jkhossxLjxsOAbnWUi1wXKaCzUyZmrshT8WzuqgRXC8hEt2FJLt+Okz8pY2YY2ISh8efWftFZZOr6TkrofNNPrkKbjv5rr+7j7QxVl0yM3LC1opFbi2WU9eXTp5uE3eSCPp5DIzNONnqMTR1FijRXIvRvJrCmNh6D2jh+08RdWhl+zxW0blP1UxxvzyazIqd4o93jFwTJks4VjGiykoMlG2RGRnHDBGYmNbNAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhL3Yf7buyPoxvfzc6PJt0saZ/OY47+VG8PZFbm/cdX9D5b7TT4zF3UfnvZlqu0lD4T/Urv/rg2vvcxQehDwTsYdHXfXy1zfvZuhxZxvOO9rfqen0dxuEOz1s5H7zq/wBFv+W/60B0s+F/jH5vPx9+RnIXAc4emv8AVf8AqI/Oo6WNmXIW0dQ2ZNo6hsybR1DZk2jqGzJtHUNmTaOobMm0dQ2ZNo6hsybR1DZk2jqGzJtHUNmTaOobMm0dQ2ZNo6hsybR1DZk2jqGzJtHUNmTaOobMm0dQ2ZNo6hsybT9cGwn1kluZWzJdfMZPualwZL0SS0fUj6tvsLbdQfUvwDIalune++dwZ+jeu4s3mclvO1ONF6xdrs3aJ6tNy3VTXTPYmEK6bd2maLtMVUTyTGMdqWxmD8rdq4ipmPaWDWZ1TZkS4uRdzlh5v/beYvGe2w88r8BUg5KS/wByP3NzReEZ6RnNpctZPiDO2+KuHaJiKrO8saszs8ve8/RhmdueSrMzmqY+x6W28/wpunORNVqmbF3q0avFpnRh2Nnst8NY8jtd7LVHrmpiscyV4kpKgvHGmVyXj6EbdVYEZQ7MzUfyqCNuQoiM/NERGY7iej/05+ZHn7uWdyZbNVbj48uxERu7P1UUVXa9GNOTzMTFnNTMz5G3E28zVETV6XppiZjYO9OHN47sxuTT3zLR+zp04fXRrp7OmOuz8P2Y0AAaw7847VWzocjIcdaj1eeRWOrbxdjEPIkNJ+UhWpkkkpmdhdrEo/KnyIc6tkk2+vvpl9B/h3pAbsvcbcEW7G7+ePL2fI1xs27G86aI0WM5oiIv4RsWM3OmnyNq/NVmKKrG6eH+JL2664y2Zma93zOrXNHXp63Vp8WNOOMU1nW2FNYTKm2hSK+yr5DsSbCltqZkRpDKjS4062oiNKkmX+wZeUvIPObv/h7ffC2+s1w5xHlb+S37kr9Vm/YvUzRctXKJwqorpnTExPiTGExMxMS5XtX7d63TdtTFVuqMYmJ0TD8PUaRsys2m03GbdzmucgTjGQS+3CciloS84+s/NUFq6SWWrVBmfa1DkdEty/jESCS51/KzJXYh0BOlle5jONI5v+NMzhzT78zNMV1XKp2N3ZyrCijN046KbNzCi3nNURRFu/jHeaqa9qcT7kjeWX9NZen+XW45P2dPmezGunxY5dErxGRkRkZGRl1Iy8pGR/GMj/BIx6PImKoxjTEuJn+jIAAAAAAAAAAAAAADlq8/Jkyu8QTmrYV8qTBnweY/I6ZBnQ33YsyHMi7szJ+NKiyWFIejyY7yErQtCiUhREZGRkLY1Lo1LZ3gteNLD5Hw8c4p8rMjjQeQUGMxVa02XavtRYe7ocVom49FeyHDQzH2tHZR0Qs+ib9Jdyek4lJlQmMNMIVU4aY1LLgigAAAAAAAAAAAAAOa141/9KNy4+jDD/4LMDFkaltOpFmMpLq/qtH1A+VH138M95jwhUrr1rTQigAAAAAAAAAAAA018QPlJB4a8QN3cgHXoyb/ABPE36/X8SS23IRZ7Lyh1rHMChrhL6nNhtZLZsSZqEko0wI77hl2oUZZiMZZiMZwc3riTycyzjByt1HybiTbW0t8H2JEybLCTMcO0yzG7iQ9B2JTSJjznc7JyzF7WfFW46ai85I71degsmMYwWzGMYOpji2T0WbYxjmZYtZRrnGctoajJ8cuIaych2tFfV8e1qLKI4XkcjTq+W262r8FKyMVKX3gAAAAAAARqeK3wVr+e3ErLtcVkOL8LmFqd2DpC2febh+YzyogyW/nclznO1DVNm9U89WSCcUTDTzzEpZGqKjpmJwlmmcJc0ayrbCmsbCntoMqstaqbKrbOtnMOxZ1fYQX3Is2DMivJQ9GlRJLSm3G1kSkLSZGRGQsXPxALvfq4/P34V9SWvCnZV357P8ASNa5e6hl2UvvmZLp+RMJEzGWFPqU9JmazuJiW2U9/VNNOjMstpagOKKFUcquuOVZ2EUAAAAAAAAHwcpyjHsIxnIcyy64gY9iuJ0lpkmSX1o+mLW0tFSQn7K2tZ8lfyjEOBBjOOuKP+xQkzAcx/xJOauQ88uV2fbtmrnxMIYeLDtQY1NcV/8ADOsMfkSUUDC45mpMa1v3337exQRrJNhYPIQo20NkVkRhC6IwjBoYMspifBT4CK5wcsaqfmlKc/QmiXKjYG2DlRTeqsklpmOrwrWklSyNlws0ta51cxpX++U8CakjStTZjEzhCNU4R13RnSlKEpQhKUoSkkpSkiSlKUl0SlKS6ESSIvIQrVP9AAAAAAABUT9Z65ZGxD0twvxizLvnH8OW1mYziTUmLHcssa1jRSXGVqMifkpuLGTFdJJl5mvf6GSkKE6Y5U6I5WIPVjeWR4vtPbXDjJbNLVPtCsd2zrKPIcSltOe4hBjwM3p4KO/zjs7JMHZYnqLtNKGMccPqRq6KVRys1xyrpYgrAAAAAAAAAAAAAHIOFq8AAAAAAGeOOnJreXE/ZFZtbQewrzX2YV6mm5D1Y/5ypyCsQ+3IdocroZJO1GTUEpxpJuRJjLrXcRLSSXEoWlrYmMdboB+FZ4sGtfEUwaTQ28er17yUwisal7B1m3KX7XXdZ570Us71y7Oecm2mLvPG2mZGWpyXTSXkMvqcadiypNcxh2FdVOHYS7DCIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAhG3coy29sjy/443vyP/bnR5POlhEfnL8dfKfP+yK3Nm5Jj2oy32mnxmLe8/k/1h+fNmGqYwlH4SH11VkHX/WDa+9zFB6DPBQRh0dt9fLTN+9m6HF/G3+9bfqen0dxuIOzps9H1ztMy+CzofT/Dj9aA6XPC+Rj83n4+/IzkDgX/APa/1X/qI+e8/k/1h0ubMOQMYO8/k/1g2YMYO8/k/wBYNmDGDvP5P9YNmDGDvP5P9YNmDGDvP5P9YNmDGDvP5P8AWDZgxg7z+T/WDZgxg7z+T/WDZgxg7z+T/WDZgxg7z+T/AFg2YMYO8/k/1g2YMYO8/k/1g2YMYO8/k/1g2YMYO8/k/wBYNmDGDvP5P9YNmDGDvP5P9YNmDGDvP5P9YNmDGDvP5P8AWDZgxg7z+T/WDZgxg7z+T/WDZgxg7z+T/WDZgxg7z+T/AFg2YMYf6Ti0mSkqNKkmRpUXkMjI+pGRl5SMjGaZmiqK6JmK4nGJjRMTGqYnkmDGJbo6P5YWuMLh4zsmRJu8c6tsRMiX5yTdUiPIhJTTIlvXFc30L4/WU2XXtNwiS2Xal0TPCMcR8A3ctwDz63sxvXgbGm3Z3lO1dz2Rp1R3+fJV53LU6Mccc1ap2tiq/TFFinZe/OFbOaiczu2IozOuaNVNXY5KZ87PW0ykugT4VpCiWVbLjzoE6O1KhzIrqH40qM+gnGX2HmzUhxpxCiMjI+hkO+LdG991cQbqy+/Nx5izm9z5uzResX7NdNy1dtXKYqouW66ZmmqmqmYmJiZiYlxtct12q5t3Imm5TOExOiYmOSX6xqKDUjk5odGfVL2Z4rCL59qeN3S40dH5Zk1XGb8sVSCL8ttoTSesZRfLuoLzJ935V2dbPT66Hljnm4cu86fN7lI+djdljG9btx5LemUtU/wU0x5fOWKIxytUeTu0ROVnb/k/et28M7+nd92Mlmqv5FXOiZ/YVTy/Wzy9Ty3VxisNSiMyM+hl5DIyIjIy/APyDzwTRszhMYTDlPGH+d5/J/rDGzBjCU7iVtxWaYqvCbuV53JMPjNJhuvOEb9pjfcTER7p5FLdp1miM6f/APWpkzM1KUY9DPg3uklXzpc3dfNPxXmNvjjhixRFmuurG5m92Yxbs19WqvJTNGWuz9jqytVU1XK65cW8WbpjJ5v07Yj+TXp04aqa9c/+LXHX2uSIbejstbRAAAAAAAAAAAAAAByzPEI/l8c4P533JX+GfNBbGpdGpqTDmTK6ZFsK+VJgz4MliZBnQ33YsyHMiupfjSoslhSHo8mO8hK0LQolIURGRkZAyvG+C140sPkfDxzinysyONB5BQYzFVrTZdq+1Fh7uhxWibj0V7IcNDMfa0dlHRCz6Jv0l3J6TiUmVCYw0wrqpw0xqWXBFAAAAAAAAAAAABzWvGv/AKUblx9GGH/wWYGLI1LadSLMZSXV/VaPqB8qPrv4Z7zHhCpXXrWmhFAAAAAAAAAAAABTJ9Z15ZFf59p7hpjNkblbgMJO5NpMMOKNhWYZJEl0+vqaWglp7J1BiTs+etJpUhTN9HUlRKSoinTHKsojlVRBJNf19XZ5ZlvjhSelchsfS894tXbeErS+8hyZM1hkqrC71rYOJ6pNDNYTFjRtIJHaiNTsmalKWYhVGlVVGE4p/BFEAAAAAAABRo9Yu4BK0vu2BzI1vSLZ1nyAtXIWzo8CI2ivxTdjcVUl61e8wlsmI206yM7PM1JWpdxDsHXXOsplAnTPIspnHQrSiSbOHGzkBn3Fnems9/aym+iZfrTJ4V9DYcWtEK7ry74t9i9v5v8ALF0mU0UmTXzEp6L9GkrNBpWSVE1sTGOh1FOOG/MB5RaN1nv7WU05mG7OxeFkVc28tpU6omLNyJeY1bkwtxlu8xW+iya6chClIRLiuElSk9FHVOhVMYTgzaDAAAAAAAKrvrInPw8EwKj4La1ulNZZs2FAzHeUyA8knafW7UtbmL4O680o1sTc2uIPpsxrq24irhNIWlbFiJUxyp0RyqWAmsfWoKG6ym9pcYxurm3eRZHbV1DQ0tZHcl2VvdW8xmvq6uvitEp2TNsJ0htpptJGpbiyIvKYDppeGLwgpeBPE3BtP+ZgSNkW6CzjdOQwlJfTebLvosY7WJHmkRek0mJw2GKevUlLaHY0IpBoS8+8aq5nGVMzjKQkYYAAAAAAB8m/vqbFqK6yfIrKLT4/jlTZX17bznSZg1VNUQ3rCzspjyvI1Fgwo63XFH5EoSZgOWVzZ5KXPL3lTu3kPcHJba2Jm1hLxmvl9CfpMEqUNUGAULyULW0Umnw2rhMPqR0S7IQ450I1mLYjCMF0RhGDwHHTd+V8bN66n3zhK/8A4k1VnFDmEKKp1TLFtHrJiFWuPzXEEpaa3I6dciBK7fljjyVkXQz6gTpjB1U9Z7DxXbmusF2ng1gVrhuxsRx3N8WsS82SpdDk9TFuat11DbjyGpBw5iCcb7lG24RpM+pGKlL24AAAAAAAAAAAADkHC1eALA/EL1fncXLzjhq/kdjnILWmIUm0Kq2tIGN3eM5ROtKtuqyW6xpxqZLgOFEeW6/SqdSaC6EhZEflIxiasJwQmrCcGxr3qtPINLLqo/KPTbsgm1mw09iebMMuPEkzaQ6+gpK2W1r6EpZNuGkvKSVfGPG0bcNU9y+rl+InrCul2+HwNSb1iRWzkegaxzt+BkZx0qUTnWn2TR4EzIlNtpNfmIkmW44kyS2S3D7BnahnahCHnOBZxrHKbfB9j4fk+BZnj8k4d5imY0VnjeRVMkiJXmbCnt40SfEWpBkpPe2XckyMupGRjKTyYDKmkN07G47bYwTdmpsgfxnYOur+JkOO2rPctrz8fublVtnFJaG7KjuoDrsOfEcM2pcJ91lwjQtRAxrdOzhDyzwrm1xn1ryHwpDUBGW1i4mWYyUg5MjC89pV+gZficpxaGXnE1lq2pUV5bbZy4DseSlJIeSK5jCVUxhODbEYYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQhbuMvhf2T9GV7+bnR5QOlfTH5y3HXynz/sitzRuWr+icv9pp8Zi3qQ/P2zDU9qEpPCL6lOQfXCtfe3iY9BHgo4w6O++flpm/ezdDjLjOcd6W/U9Po624w7OG0UfHO//RX/AJcfrPHS/wCF5jH5vfx7+Rm/eB5w9Nf6v/1Ee/Uh0v7MN/bUNhOKx/t9YJ/lR7zciH7Q8HpER0wOEPxr7ybyaBxPOO47/wC0+6UJix6cHEYAAAAAAAAAAAAAAAAAAAAAAPK3WC4VkaFN32JY3cEojLusaWuluJ7uvVTbz0dbrS+p9SUlRKI/KRjjzijmj5q+N7VVnjDhvcW86aomMczkcteqjHXNNddua6auWKqaoqidMTEvqs57OZecbF25R2Kpj6rXnNOHursibeex0rHCrJSVG2uufcsak3T/ANtIq7J11zzZf7mPIjkQ/FXOl4Mro/cbWbuZ4JjO8K78qiZpnLXKszlJrnluZTM11VbPUoy+Yy0Rr6zcGT4u3nl5iMxs3rfXjCrxKo+rEtEto8fdg6sN2bZQE3OOJWRIySlJyRBbJXXsKxYUlMurc+MRm6jzJrPtQ4sx1FdIDoX88/R8m5vTfuUp3pwRTVEU7zyO1csUxOqMzbmIvZSrVEzdo7zNcxRbv3Jb33ZxBkN54UW6pozHmKtE+JOqrxNPViGDepD8mbMNa2oOpBswbUNo+Oe/5WsLVvHMjkSJWBWslKXUmanVY1MkOJJVvDb7VuKhKM+sphHlUXVxBG4Rpc7AuhD0x94cwPENHBHG969f5n9434iuJma53ZeuVRjnLNOFVU2Jmcc3Yo01RjftU1XqZt3tscQ7jo3nanMZeIjPUx/44j9jPX8zPiTo0xLOw+xKYZkxnmpEaQ02/HkMOIeYfYeQTjTzLrZqbdadbUSkqSZkoj6l5B6Nspm8pvDKWs/kLtu/kb9um5buW6ort3LdcRVRXRXTM010V0zFVNVMzFUTExMxLiyqmqmqaaomKonCYnXEv6j6GEWvLjUKMOyRrPKKKTWO5bKcRYssoJLNZkykuSHiSlKUpbj3LKFvoLy/lyHvjJ7CHn18JF0aLXNnx1b54OEcvFvgniTMVRmaKIwoyu9Jiq5XhEREU287RTXfoiJnC9Rmo8jRNqlybwpvec3lvSN+ccxajR16NUeLTq7GHXaddSHWTsw3dtQ93rPOp+uM2ocur+5XtbLSU+KR9Ez6qR+UWcFXl7er8RauwzIyQ6SV9OqSHL3MPztb45jedbc/OTubaq9I5mIzFqJwjMZS5+95qxPJjcs1VbEzExbuxbuxE1UQ+HeWTt7xyVzKXP2UaJ6lUaYnxJ19WMYTj1tjCt66BbVshuXX2cONPgymj6tyIkxlEiM+g/j9rrLhKL/ZHrO3HvrdfEm5cpxDuO9RmNzZ/LWsxYu0aablm9RTct109auiqKo60uFbluu1cqtXIwuUzMTHUmNEw/aNUQAAAAAAAAAAAAAHLM8Qj+Xxzg/nfclf4Z80Fsal0amoIMv0Q5kyumRbCvlSYM+DJYmQZ0N92LMhzIrqX40qLJYUh6PJjvIStC0KJSFERkZGQC8b4LXjSw+R8PHOKfKzI40HkFBjMVWtNl2r7UWHu6HFaJuPRXshw0Mx9rR2UdELPom/SXcnpOJSZUJjDTCuqnDTGpZcEUAAAAAAAAAAAc1rxr/6Ublx9GGH/wAFmBiyNS2nUizGUl1f1Wj6gfKj67+Ge8x4QqV161poRQAAAAAAAAAAB4/YOd4vq7A802Vm1mzS4dr/ABW/zPKbaQpCWq7H8Zq5VxbzFmtaEn5iDDWoi6l3GRF8cwHKx5Nb3yjk9yA27v8AzE1ovtq5zd5W7CU+uUilrJcg2cfxuK+50W5AxjH48WujdfKUeKghbGhdEYRgwYDKX7wPOWZcVefGuCvLH0HXW9EfAbnZvPIbgxDzCxgLwm+keeMo0f2lzyFXk9JUaTj10iX0USVrJWKoxhGqMYdHMVqgAAAAAAAGv/KbjjgPLXQOzePuyoqHcZ2NjcqpTYlGRKm4zfNds3GcwqG1uMpO4xS/jx58cjWlDjjBNudW1rSaJwZicJxcuzf2kM942bo2TonZ1d7WZxrDKrHF7xpCXyiTfRVJerbypXJZjvyaHJKh+PYV76kI9IgymnSIiWQtW62IQZWfPVxefhas2tbcJdkXSWMD3VZv5FqCXYPKKPj+3WYDaLHGGnnVE1FhbFpa5JMINRJO3hMtNJN6evujVHKhVHKu6iCsAAAAAa88reSOB8RuPm0OQuxnumO65xuRaNVjTzbM7Jshkrbr8XxGpU78p7a5RkMqNCZUr8raU95xw0toWpKIxZiMZwcu7fG6895G7j2NvLZ1odvnOzcosMovpRdxR2HJaktwamtaWpZxaahq2WIMFgjNMeHHabLyJIWrdTEoMrQ/q4fABey9oW3N/ZdGteCafnycb0vGnsEUbJNsPRUFcZYy0+lRSq3XdNNJthwkebVcTULadJ6udQUap5EK55F2oQVgAAAAAAAIGfWF+WZcfuD07UuP2hQtg8pLZ7W0NplSkzo+taxuNa7Us2//AOE4s2uehUL6V9VKavlGguqDWiVMaUqYxlz9BNaAL1Xq2HLT4V+LOX8ZMlsvP5fxuyH0vF2n1fl8vU2wZc+2q22lOKU7MXjeZNWsd0y+UjRJUFroRdvWFUacVdcacVkcRQAAAAAAAAAAAAcg4WrwB0pPBP8A6LniP9CGY/wqZ4K6taqryyU0YRAGhHPnw7NA+IHq+bh+0KKHT7Bq62S3rPctTWxl5vr20Ua346WZXdGdvcSlS1f8YUsh0oktClLQbEpLEpnMTgzEzDm18hdC7H4w7n2FofbNSmnzvXF/Io7dphTjtdYMkhuVU39JKeZjuTaDI6iQxOgvqbbU7FkNqUhCjNJWa1sTjpYZBlaQ9WM5Ry8T3ftviVe2fbje2sYc2hgsKTIUTTGxcEbjRMih1cU1kg5mT4HIVJlLJJqNnHGi8hJEao5UK45V2EQVgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIPN4dfhg2V9Gd9+b3h5RuldE/nK8c/KfP/d63Mu5pn2py/2mnxmLPKPz9sy1PGUp3B/6lGQ/XDtve3iY9AngpYw6PG+flpm/ezdDjTjKcd6W/U9Po625A7Nm0kenPL/RV/lz+s8dMXhdox+b38e/kZvzgmcPTX+r/wDUR6eUdMWzLfmMtheKnX4e8D/yo95mRD9n+D2jDpf8I/jX3l3k0HiaZ9o7/wC0+6UJkR6a3EoAAAAAAAAAAAAAAAAAAAAAAAAA/m600+04w+228y82tp5l1CXGnWnEmhxtxtZGhba0GZGRkZGR9DFOYy9jN2K8rmqKLuVu0TRXRXEVU101RMVU1UzExVTVEzExMTExMxMYMxM0zjGiYR5ciOK7MOPOznV0BSGmSXKvMOiN9yGmSI1vT8dZQXclDRF3OQyI+ieps9CImh0vdNHwfWV3bks1zscwWTqpy9qKrue3NZpximiIxrzG7qI0xTR5a5koicKcasthFMWG/dw8T1V1Rkt5VaZ0U3J8ar6lXb6qPryjps2Zb6xk8obMmMpGeHO5XJrStT5FKNciIy9Mw2U+vuW7DZSbs6g7lH1M4TaVSIxeXoyTqepJbbSO7TwZ3Sbvb0y09HfjTMTVnMtarvblu3Ksaq7NETXf3fjOmZsUxVmMtGnCzF+3jTRZs0zsDizdMU1e2liPIzOFyI6vJV4uqevhPLLf0dwzYzxexMKr9h4XkGH2Palq4gONR5Ck95wbBoyfrZ6C+Oaoc5ptzoXTuJJpPyGY4u56ea7c3PRzX755td97NOW3nlKqLdyYxmxmKJi5lsxTGvGzfpt3MIw2opmidFUvsyGcuZDOW83b10VaY6sapjxYxhBdcVU+htrOjtGTjWVPYTKyfHMyM2ZkGQ5Gkt9xeRRJdaMiMvIZeUh5MeJuG978IcR5/hTf9qbG/N25y9lcxbmcdi9YuVWrlOPLEV0zETGiY0xolzRavU37VN63ONuumJietMYw+d5RomzKzGUrPDbPFZLriTi0x5Tljg04ojXes1rVRWpvzKszUr5bpHkNyWEp8pIaaQReToRehfwZPO5c455kb/N/vO5Ne+uE83FmjanaqnIZvbvZWZmdP73cpzNimnTFFq1ZiJwmIjjHi3Jel94RmaY/e71OP7anCKu3GE9mZbejsjbVAAAAAAAAAAAAAHLM8Qj+Xxzg/nfclf4Z80Fsal0amoIMgD9EOZMrpkWwr5UmDPgyWJkGdDfdizIcyK6l+NKiyWFIejyY7yErQtCiUhREZGRkAvG+C140sPkfDxzinysyONB5BQYzFVrTZdq+1Fh7uhxWibj0V7IcNDMfa0dlHRCz6Jv0l3J6TiUmVCYw0wrqpw0xqWXBFAAAAAAAAAAc1rxr/wClG5cfRhh/8FmBiyNS2nUizGUl1f1Wj6gfKj67+Ge8x4QqV161poRQAAAAAAAAAABXN9ZF5aK09xLxvjjjVkuNmfJrIVx71MdTZPRNTYE/XXOTE44lRyIi8hyaRUQkF0JEqEU5s1dEqSqVMaU6I04qIAmsAH/TbjjLiHWlradaWlxtxtSkONuIUSkLQtJkpC0KIjIyPqRgOnr4Y3LBrmdwq0vuiZPZmZudCnCdqtt9xORtm4Ulqmyd99pRq8x88Xm2bllslL7Ilk0RqNXXpXMYSpmMJb9DDAAAAAAAACrL6yHwD+f/AF5S859bU3nMv1XAgYhu+LCL8tutZyJ5s4zmBxW09ZM/B7yzONLcIlOqq5qXHFExXl2ypnkTonkUqBNY+pRXlzjF3T5LjtpPo8gx61r7yiuqqU9BtKe5qZbM+rtK2bHW3Ihz6+bHQ6y6hSVtuIJSTIyIB0zvDA5w03PbibhG23H4DGzKNCcG3Vj8M2WjqNj0UWOmwsmILZJ9DpMxhus28BCSU2yzMON3rcju9K5jCVMxhKQ8YYAAAAUW/WKufhbz3lXcQdc3SZWrePVs7N2DIgvKVDyneK4sivnRHTSpTUiLrGolu1rZkSVJs5lkhZKJtlRTpjlWUxhpVshJNnbjJx6z3lZvnWPH7WsXz+WbMyeJRRpbjD8iFQ1aUuTsiyu3RGJT5UmJ4/Ek2Mw0EayjRl9pGroRp0MTOEYuonx20PgHGLSWttDaxrUVuF6zxiFj1YXm0NybOUjvlXWRWht/KvXeT3kmTYznC/3yXJcV5CPoKp0qpnGcWaAYAAAAAAAAc6Px1uWauUPPbPqikslTdc8em3NIYYhiWb9dLtMbsJTuxMgYab6xPP2ebvyohSGzX6TArYiu40pSSbKYwhbTGEIahlIASaeEPy2+w6516gz64tPazXObTz1Htdb0z0GubwjPpMOAV3aPKPzSYGG5OzXXbpqI+rVcpJGk1dxYmMYRqjGHS9FaoAAAAAAAAAAAByDhavAHSk8E/wDoueI/0IZj/Cpngrq1qqvLJTRhEAAFQr1objLWtReP3Lyir241lIsJuhtiS2WSI7JK4VpmutZkg2iSn0iC1XZBHdecJS3G1xmu4ksoSc6Z5E6J5FQMSWN4fDS2pJ0xz94i7AjyfQ2Ie9MFxu5ledJgmMX2Bat6+y5xThkaewsXyiZ3JPoSy6pM0kfUk6mJ1OogKlIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACDjeKiLcOyy/65335vdHlL6VkT+cpxz8ps/wDd63MG55/ovL/aqfGYr7iH5/wlqWKVHg6ZHqfIen+sO297eJD0AeCojDo875+Web97d0OOOMP950faKfR1tyh2aNqI8uehkXwU9f8Arz+s4dMvhdIx+b78e/kZvngv/wDZ/wBX+7R49xDplwlvnFsPxSUR78wIvoo95eRD9m+D4ifzveEfxr7y7xaFxL/uW9+0+6UplB6Z3FQAAAAAAAAAAAAAAAAAAAAAAAAAAACMLlxpBrFbE9mYvDJrH7uYTWSQY7fRqovJSlKRYNIR5Gq+4c6kouhJaleQj6PIQnoc8Iz0U7HAO+p58+ActFvg/euZineVi3T5DJ567MzTmKIjymXzlWMVxhFNrNaIqwzNq3RyHwxviczb9r8zON+iPIz5qmOTs0/qx2JlpD3EOq7CW78X1qG+sMauqrIKiQca0pbCLZQXyIzJEmG8h9rvSRl5xpSkdq0H5FoM0n5DMbi4R4o33wPxRu/jHhy7Njf2685azNivkpu2a4rp2oxjaomYwronRXRNVNWMTMKr1q3mLNVi7GNuumYnsSnYwPL4GfYdj2YVhdsS+rWZnmTUS1RZJGpifBWsiIluQJ7LjKjLyGps+g9ZvNJzj7o53ebXc3OTuONnIb3yNF7Yx2ptXYxozFiqdU1Ze/Rds1TGiarczGhw3ncrXks1cytzy1FWHZjknxYwnxXrRyK+ZFXzRwdGPbDr8uit9kLN643JPanokrulTHhzTLtLtST8F2Kvy+VThuGPPz4T3mno4P558nzjbut7O6+KcltXcI0RnslFuzf1aIi5YqytenTVcm9Vp0uSOE87N/I1ZWry9mrR9bVjMdqcfEwacdxDrRwlurFtJxCzA8c2/AqnHTTBzGunUL6VK6NFMbbOzrHjT18rpyYXmEH+B6QfyR++vBv841zgfpI5TcF+uad08S5K/kLkTPkYvU0+msrXMctc3bHeKJ04emKo1TMtucUZX0xuuq5EeTtVRVHY1T+pOPiJdx6OnGAAAAAAAAAAAAAA5ZniEfy+OcH877kr/DPmgtjUujU1BBlnObxw29C4+UHKNOKyZ+k77P77WC8xre+ZFoM4oINRZrpMnbQ2S6RdvX3LTle65+UTPNuoQvzjS0E6zGOnBgwGX6IcyZXTIthXypMGfBksTIM6G+7FmQ5kV1L8aVFksKQ9Hkx3kJWhaFEpCiIyMjIBeN8FrxpYfI+HjnFPlZkcaDyCgxmKrWmy7V9qLD3dDitE3Hor2Q4aGY+1o7KOiFn0TfpLuT0nEpMqExhphXVThpjUsuCKAAAAAAAADmteNf8A0o3Lj6MMP/gswMWRqW06kWYykur+q0fUD5UfXfwz3mPCFSuvWtNCKAAAAAAAAAAAOat4xvLUuX3PHbeXUtmmy1xreWnTWrXGH1SIMjFMClzYthewXCMmnomXZjJs7VhwkIV6JMZQrqbfcdkRhC2mMIRbjKQAALQnqy/LP5xt57M4h5LaLboN207mwNcxJEnpGj7MwKtdcySBAiqUlJTMr18yuQ+4XVRoxxlPT8Eo1RyoVxoxXbRBWAAAAAAAA+JkuN0GZY5kGIZVUQMgxfKqS1xvJKG1jty6u7oLyA/WXFRZRHSU1KgWVdKcZebURpW2syPyGA5kXiVcJsg4F8rs90tJbspeBS3vnx05k9ggjVkusr6RIXSKdlJJLcq4xuQy/UWSyS15ydAcdS2hp1rrZE4wuicYxaDDLKYHwWefS+DnLOnYzO4XC0JvFdVr3baH3STW4689NUjDNlvEpSCa+ci2nOFMc6q7aabONLbjpMkWJjGEaoxjrujelSVpStCkqQpJKSpJkpKkqLqlSVF1I0mR+QxWqf6AAIyvFk521/AriTlmfVE2H8Meeef17o+oeUy46vM7WG6crLXoTiXjfp9f1HnLN41NqjvSkRYbikHMQoZiMZZpjGXNOsLCfbz51razplnaWcyTYWVlYSXps+wnzXlyZk6dMkrdkS5kuQ6px11xSluLUalGZmZixc/IAvD+rlcA/gg07a80Nj03mdi73qjpdUx5hdX8c0s1MjynbtDKkIONP2VeQG5CTV3n7UwIbjS0plvoVCqeRXXPIs1CKAAAAAAAADRfxJeVkfhlwx3XvFiWzGy+uxtzF9YsupQ6qXs3MTOhw5SIznREtmlnyztJTfUu6FAeP8AZiMZZiMZwcvuRIkS5D8uW+9KlSnnZEmTIdW9IkSHlqceffecUpx151xRqUpRmpSjMzPqLFz+QAAAOlh4PPLT7MDgfqLNLiy9sdi68hnpnabjiu+W9mGAQq+JFupqzV1dmZbiEurt31klDfpM51CSIkdCrmMJU1RhKUEYYAAAAAAAAAAByDhavAHSk8E/+i54j/QhmP8KmeCurWqq8slNGEQAAQtesD4tCyDwtd520pDan8HyjTGU1puJcUpubL27huFOLZNDiEocOuzCQkzWS09hqLp3GSk5p1pU63OzFi16/X1v87+e4Rf8Apntd7R5fjVv7YdenoPtbcwpvpnXorp6L5nv69D/sQHXCFSgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBnvM/24tl/Rnffm90eVDpV049JLjif/s2f+71uXd0Vf0Xl/tVPjMVdRwBsw1Lb7CVXg39SbIfriW3vaxId/vgqow6PW+flnm/e3dDjni6cd5UfaI9FW3MHZi2sju57n9Sj/Lr9Zw6aPC4xj83349/I7e/Bs4emf8AV/u0d/UdNGzDfG32GxHFA/2/sC/yp95eRj9meD6pw6XnCM/1p7y7xaHxHVjuW9H1n3SlMyPTE4sAAAAAAAAAAAAAAAAAAAAAAAAAAAAfCyfHKrL8euMYu2Ck1V5AkV8xvyEsm30GlLzKjJXmpMZztcaWRdUOISovKRDafHfBW4Ocbg3eXAvFFrv24N65O5l71Ojaim5ThFdEzE7N23Vs3LVcRjRcoprjTTC7L37mVv0ZizOFyiqJj/LqTqnrIH8wxixwrKb7E7Ui9PoLOVXPLSk0tyCYcMmJbJKPuKPNjmh1vr5exZdR5M+cjgHfHNhx7vbm+3/H9Lboz13LV1RGFNyKKvIXqInT3u9b2btvHTsV046XMOVzdGay9GZt+Urpiex1uzE6Jeb6jZOzD6NvsJLODOaKmUOWYHKdNS6WaxkFUlSup+g2pei2LLZf7VqLOjNueyuUY7wfBUc5le8eEeIeaXPXJm5uzNUbwysTOM94zUd6zFFMclFq/at3OvXmqpbB4vy0Rft5yn9nGzPZjTHbicPEb6jttbOapcx8XTe6dlW6GyVLxG5q7htZF+Weiynvaaa0k/8A+s02SHVl/wD8SP8AAHX74SngK3xb0bcxxDboireHDu8srnKZjy3ertfpK/TH+bhmaLtcf/hif2LcfC+ZmxvSLf7G7RNPix5KPGw8VEX1HnT2YcmbfYffxW9exnJ8dyNg1eeobyquGyQfQ1KrpzEvs+QZOE12mR+QyPofkG7uAOKMxwLx1ubjXKbXpndO9crnKcNczlr9F7Z7FWxszE6JiZiYmJUZmiMxl7lirDCuiae3GCf9txDzbbzSiW26hDja0/GWhaSUhRewpJ9R6+7F61mbNGYsVRVYuUxVTMappqjGJjrTE4uGJiYnCdb/ALFrAAAAAAAAAAAADlmeIR/L45wfzvuSv8M+aC2NS6NTUEGV7D1e7XGCbf8ACy2LrLZuLU+bYDm299q0OU4tfRSl1dvVy8U1v5xl5vqlxl5lxKXWH2lNvxn20OtLQ6hC0wq1q69avR4tXhK534fWduZxg7dxm3FfNrhxnCs1ebOXaYHaSzcfZ15sN5htLbNky2lRVtkaW2LZhszIkSEPMolE49lKmrHsoZhlJ+iHMmV0yLYV8qTBnwZLEyDOhvuxZkOZFdS/GlRZLCkPR5Md5CVoWhRKQoiMjIyAXjfBa8aWHyPh45xT5WZHGg8goMZiq1psu1faiw93Q4rRNx6K9kOGhmPtaOyjohZ9E36S7k9JxKTKhMYaYV1U4aY1LLgigAAAAAADmteNf/SjcuPoww/+CzAxZGpbTqRZjKS6v6rR9QPlR9d/DPeY8IVK69a00IoAAAAAAAAACNHxcuWpcOeCm4diVNmVbsPMq8tR6nWh9UecjPdgRJ0Bu4rHEGRpsMNxpiyvWuvVKl1ZJURkroeYjGWaYxlzPBYuAE/PMfwoJXHzwo+KnKRihkRdrNWZ5LyMS5GcjToWLb4OqkayYto8judgHrp2JV0z7BmlZWd6+ZoLykjETpwRicasEAwykytovcGVcf8Acur93YQ95nKtV5zjecUyVOKaYmSMfs489yrmmlK++tuI7S4spBpUlyO8tBkZGZGY1uqnp7aeJ7x1RrjceCy/TcP2hhONZ1jr6lNm+mryapi20aLNQ0txMeygpleYlMmfexIbW2oiUkyKpTqZIAAAAAAAAAQw+N5wCLmvxQssjwakRO37oBm2z7WvokRx+3yuhRES7nes4/o6XH5D2TVcFuVXMpbcW5cV8VlJtokPLGaZwlKmcJ6znWCxaAL/AD4AXPwuUvGUtCbAuky92cZ6yox1Tkx5SrLMdRGn2vwbJ+95XnJ83HEx/aWyWnvUjzMN99XnZpdYVRhKqqMJx5E/Aii/jIkR4kd+XLfZixYrLsiTJkOoZjx47KFOPPvvOKS20y02k1KUoySlJGZn0Ac2bxfuecjnjy2yPJcbsnpGkNW+na80jENC2GJuPQphHeZ0thZkv0zYN2wqahTiGn0VaIMd1BLjmLIjCFtMYQitGUkj3hY8GLbnvy0wzV8uLYNaoxY2893fkENKmk1uv6WWx52jjzu5CI15nNktmphGk1vM+kuyyacaiPEWJnCEZnCHTDqKmroKqsoqOuhU9JS18Kpp6msiswq2rq62M1Dr66vhRkNx4kKFEZQ0002lKG20klJEREQrVPoAAAAAAAAAKU/rN3LT58Nv6s4c41ZecpNQ1rO0tmRWVdWnNjZrVqYw2smoUozTMxjX8xyY2aSJKmclMjNRl0TOmOVZRHKqzCSbdTw8eLMzmVzE0joUo8hzG8jyuPcbFksLWydfrPE0KyLOnilo8kKXMoK52FDcV5DnymEeU1ERpnCGJnCMWUfFo4jt8Mucu3tZUdSip1xk89G1NRxo7SWYEfXudyJk+HSVraUp83Aw29YsKNoj6qNFYSjNXd3HiJxhimcYRtDKSx16tty0+CLlhlHGvJLL0fDeTGPF87zchX/Boe2dexLO7oOxx1RMwUZFib1vDc7ei5k1uA18spLZCNUaEK40Yr24grAAAAAAAAAAByDhavAHSk8E/wDoueI/0IZj/Cpngrq1qqvLJTRhEAAEG/rEefRcO8MnYWOyH22XdrbM1FgMJtamyVKlV2XR9orYZJaFqU4mFrZ50yQaVdjaj69pKI5U60qdbnrCa1kjTdFOyjb+qsarEKcssi2Rg1FXtpaeeU5Ot8nq6+IhLMdt191Sn5CSJKEqWr4xEZ9CAdakVKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEF29FdNx7MLp/jpffm972B5VOlTTP5yPHHymz/AN3rcs7pw9rLH2qnxmKe/wBj4vxBwFsy1HR1UrPBk+upci+uLbe9rER38+CtjDo975+Web97d0uPeLP940faI9FW3OHZe2ujs58n0+Cf/Lv9Zo6a/C3Rj83/AOPfyO3rwfh/KP8AV/u0dff7HxfiDpr2Zb10dVsVxPV13/gJfRT7ysjH7K8H5TMdLvhKf6095d4tE4iw9p737T0dKZ0el5xgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIsOcOLN1Ow6DKmGibby2hNmUsiIjftMddaiPOmZF5TKrmQkeXqZEj8ToS8KVzfWtw88e6OPspbiizxDumaLs4eXzW7qqbVdcz1Yyt7JUadUURpwmIjkDhTMd8yVeXqnTbr0dirT48VNJ+/2Pi/EHWJsy3Vo6rZ3iFkS6Td9DE855uPktbd0Eoz69qiVAXbRUGRF5TcsallJfINXyB+5vB08W3eFulFunITXsZPfeSzuQu68JibFWbtRo81mMpZpjqTVpwjGWg8SWYu7qrq11UVU1R28J/UmUxQ9ITjN4PaNMnIdbZ5Sm351dhiOQMMJ7e8yl+1klcJxKfwVtS0IUn2SIcT8/HDVPGPMpxZwzNG3cznDu8LduMMf370rcmzVEcs03YoqjrxD7N33e856zd1RTcp7WMY/qIFO/wBj4vxB5L9mXL2jqnf7HxfiBsyaOqnl1LaHdau15aKV3OzMLxtcg/8A3pNREal9PKryFJQsi6+X5PlHrI6Pe/auJeYng7flydq/mOGd21XJ/wDyxlLVN3ln/SU1YYzj1cJxhxFvG33rP3rfJF2rDsYzh+oyEOYHxAAAAAAAAAAAAOWZ4hH8vjnB/O+5K/wz5oLY1Lo1NQQZX5fVp/6O27/nH7L96uuBCrWrr1p1tj64wTb+CZTrLZuLU+bYDm1PKocpxa+ilLq7erlkXnGXm+qXGXmXEpdYfaU2/GfbQ60tDqELTFBz3fFq8JXO/D6ztzOMHbuM24r5tcOM4VmrzZy7TA7SWbj7OvNhvMNpbZsmW0qKtsjS2xbMNmZEiQh5lFkTj2VtNWPZQzDKT9EOZMrpkWwr5UmDPgyWJkGdDfdizIcyK6l+NKiyWFIejyY7yErQtCiUhREZGRkAvG+C140sPkfDxzinysyONB5BQYzFVrTZdq+1Fh7uhxWibj0V7IcNDMfa0dlHRCz6Jv0l3J6TiUmVCYw0wrqpw0xqWXBFAAAAAAc1rxr/AOlG5cfRhh/8FmBiyNS2nUizGUl1f1Wj6gfKj67+Ge8x4QqV161poRQAAAAAAAAAFFn1lHlp8KnKLDuMOM2vpOIccceTYZYzFf7okrbOwYUC2nsv+a6sTF4vhaKxhtRqUuLKmzmTJCycI50xoWURoxVtBJNIn4VHE4+ZXOPS+qLOs9ssBqLg9l7ZQ4S/Q/g2wF2NbXNdPU0fn242W2RwqElo+WQ9aoPqkiNacTOEIzOEOj1yC0nifIrRu09D5myk8W2lgt/hU9xttCnas7WA4xW3UBB9EIsMfsiZmxT+MiRHQf4Ar1Ko0Ti5WG1da5XprZuwdS5zB9rcy1pmWSYLk8IjUppm7xe3l01j6M6pKPSIbkmGpbLpF2utKStPVKiMWrngQZXiPVoOWR7G457B4o5NZk7k3H+/VlOCsPuJJ+TqvYk+ZPlworalrfkpxXPynLkOH0Q01dQ2kl0SIVRyq6404rNIigAAAAAAAAAOfR49XAP7EnlI9t/Aab0PR3JWfd5fStxC6wcR2Yl9udsLD/NoSSK+BNmWCbarb+UaKNLdjMJ7IK+llM4wtpnGOuglGUm3PBflvmXCHk5rTkJiHpU1jGLP2uzfGI8n0drN9dXRtw8xxOT3n6MpydW/l0Jx5LjcS0jxZXaa2EhMYwxMYxg6gOtNjYbt7X2F7S15dxckwfYGNU+W4reQ1dWLGkvITM+C8aD+XjyCaeJLzKyS6w6lTa0pWlRFUpQFesM8/U8euPbPFfXd2iPt7kjTzI2VuQZbjVph+j/Pu1+Qy1kyolMvbIlsPUbHf1Q7ARZ9Oi0NqEqY5U6YxnFQ6E1j/pttx5xDTSFuuurS2222lS3HHFqJKEIQkjUta1GRERF1MwHSC8GvgO1wV4l0cLLahuHvjcaa7YW5n3G+k+mlyIi1YrrlxaiJSG8BpphsyWyNaPbiTOcQtTa0dK5nGVVU4z1ktowiAAAAAAAA8BtbZWKaa1lsHbeczva7DdZ4ZkmdZPMIkqdZpMWqJdzY+jNKUn0iY7GhqQw0R9zrykoT1UoiA1uVdyD3VlfI3eG1d65s735PtTOMgzOxZStTjFam3nuvQKSEpXyxVtBWeZhRUn5UR46E/gC1dGiMGHgZXQvVjOJnzta125zJyarW1bbJnuah1fJkxvNrLBcWnRbPO7iukKSfpEDIs2jRYCjSZEiRjrqT69fJCqeRXXPIy36yrxMTtDjHhXKTG6xLuW8db9NPlzsWISpc7VWw59fVvPSnmusiQ3imapr3WUKSpuPHspz3VBd5qUzpwKJ04KMQmsey11n2Uaq2BhGzsJsV1OY68y3Hs2xazR390DIMXtol1USjJC21LQzPhINSe4u5PUuvlAdU/jVvTF+TWgtR79w1SSoNq4LRZcxD84TrtNYT4iU32OS3E/KKsMYvmpNdJ7TNPpEVfaZl0M6p0KZjCcGcAYAAAAAAAAAHIOFq8AdKTwT/AOi54j/QhmP8KmeCurWqq8slNGEQAAUifWYeXVXsjeOsuJuIWaJlVoWBOzDZK4j5ORXNl53ArvaejkJSpbapmHYYyl01J6Ghy9eZX8u0ZJnTHKsojRirCCSaSXwg9PP7u8SDifixRvSIGObOgbVuVLb85FardQRZey+2an+wONPn4wxD7VdUOOSUoMjJXQYnUjVqdMwVqgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEFW9T/bk2b9Gl/+b3h5WulR+khxx8ps/wDd63Ku6pn2tsfaqfGYo6mOAmoYylc4LfUkyL64tv72cRHfr4K/9HzfHyyzfvbulsDirTvCj7THoq26A7LW2UdPPz/RP/l3+s0dN3ha/wD/AJ/+PPyO3nwjOHpj/V/u0dPUx03N5Yy2L4mn/wB4DAP8qveVkY/ZPg/v0ueEvxp7y7xaNxBM+1F79p6OlNCPSy4zAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGjXO6uQ7r/DbcyLzkHMVVqT6l1JFrS2UpZEXaZ9DVTJ6/LEXk+Mfk6dWvhWNy27/NHw3xFMR33K8RzlonRjhmslmLtXJjhM5OnHyURojGJ0TTurhS5MZu7bjVNvHtVRH1UXHUx0Tt94yylpGa5B3DrF5vqal51jMM+iu0+yxtote75e1XUibkn1L8EvJ1Lr1HO3Rg3jd3V0jOBs1Zx26uKt22dE4eRzGbtZerThOjZuzjHLGjGMcY+Dekbe7b8Tq71VPaiZ+onaHqscUv5utNvtOMupJbTza2nEGZkSm3EmhaTMjIyJSTMvIKsxYtZqxXlr9O1YuUTTVHVpqiYmNGnTE4aGYmYnGNcK7j7Tkd56O6RJdYdcZcSRkoicaWaFkSi6kZEpJ+Uh46c1lruTzNzKX4wv2q6qKoxxwqpmaZjGNE6Y1w5jirGImNUv5dTFDOMpueNL65OjNdOLJJKTTyWCJBGRdkW2sYzZn1NR9xtskavwOvXp0LyD0+dCbNXM50WODb12KYqjd1y3oxwwtZvMWqZ0zOmaaImeTHHCIjRHF++4w3rej/Oj9WIlnMfqZpQAAAAAAAAAAADlmeIR/L45wfzvuSv8M+aC2NS6NTUEGV+X1af+jtu/5x+y/errgQq1q69awYIoPE7H1xgm38EynWWzcWp82wHNqeVQ5Ti19FKXV29XLIvOMvN9UuMvMuJS6w+0pt+M+2h1paHUIWkOe74tXhK534fWduZxg7dxm3FfNrhxnCs1ebOXaYHaSzcfZ15sN5htLbNky2lRVtkaW2LZhszIkSEPMosiceytpqx7KGYZSfohzJldMi2FfKkwZ8GSxMgzob7sWZDmRXUvxpUWSwpD0eTHeQlaFoUSkKIjIyMgF43wWvGlh8j4eOcU+VmRxoPIKDGYqtabLtX2osPd0OK0TceivZDhoZj7Wjso6IWfRN+ku5PScSkyoTGGmFdVOGmNSy4IoAAAAOa141/9KNy4+jDD/wCCzAxZGpbTqRZjKS6v6rR9QPlR9d/DPeY8IVK69a00IoAAAAAAAAMNciN2Yvxw0Xtne+ZH3Y5qnBMizSdFS6hh+2ep695+toITjhG2VlkVr5iDFJXkVIkII/jhGkjTODlXbR2Ple4dk57tfObBdrmWyMvyLN8nsFmrpJu8mtZVvYqaQo1eZjJky1JabL5VpokoSRJSRC1c8IDK8X6tHxKPW3HPP+VuT1hsZRyBvPnZwZ2UwlL8TVevp8yHImwnFEl9lvLc6VMJ9Bl2OtU0N1JmSiM4VTyK6504LMwigov+sp8Svgs5OYXyixmr9HxHkXQnVZc5Dh+bhwdr6/hQK6U/LeZIo0d7LcNcgPNIUlLkmTXznjNZ95pnTOjBZROjBWuEk0ifhUcsT4a849L7Xs7P2twG3uD1ptlbhr9D+DbPnY1Tc2M9LRefcjYlZFCviQj5Zb1UguiiM0KxMYwjMYw6bqVJWlK0KSpCkkpKkmSkqSouqVJUXUjSZH5DFap/oAAAAAAAADTPn5w+xPnPxb2Rx/yVUSBbXMJN9rnKJMdEhWF7NoG5EjEMkb6oW8iL6S65BsEsmh5+pmy2ELQbvcWYnCWYnCcXMHz7A8u1dm+W63z6im4xm2C5Fb4pleP2KEom09/RTnq6zgPkhS21qjy46iJaFKbcT0UhSkmRnYueSAWsPAX8VnBtD6o27xr5L5h7S4PrLFcw3bpu7nOtOP8AtXUxnrvYep6Vl59g51xYP99xRQk/LyZD1i2bhGcZs41RywhVTjphXp5icos75lcjdnchtgOvNWWd3zztJQqlnLh4ZhleXoOIYZWL81HaOHjtEyyyp1DTRy5JOynE+efcUqURglEYRg1lBlYP9X14BnyX5Iq5IbBpTl6Y40W1dbVzUxlKq3MN0+bbscPpO1xPbLi4U0aL2alB/KSEV7bqVNSVJPFU4QhVOEYcq/EK1YAAAAAAAAArV+sq8s06v4x4VxbxuzS1lvIq/TcZc1FlkmXB1VryfX2jzMplrpIjt5Xmqq9plalJbkR62cz0WXeSZUxpxTojTioxCax7PXGv8o2xsLBtX4RXnbZlsXLsdwfFawlGj07Icpt4lJTxlOElfmm3Z81slrMjJCeqj8hAOqhxs0Zi3GfQmpNCYY2hOP6rwajxOPJS2bS7afBipXe5DJQalds/Jb56TYSeh9vn5K+hEXQiqnSpmcZxev2trXFNy6y2DqTOYPtjhuzMMyTBcnhkaUuvUmU1EumsfRnVJV6PMajTFLYdIu5p5KVp6KSRgxqcq7kHpXK+OW8Nq6KzZrsyfVecZBhli8lCm2LJNRPdZgXcJKvljrb+s8zNiqPyrjyEK/BFq6NMYsPAyuferGcs05FrnbvDXJrMlW2u7Be39XR5MslPP4XlMuPWZ9TQIq+3zMHG8wXDnn2dxuPZC8Zkns+WhVHKrrjlWsxFAAAAAAAAAAcg4WrwB0pPBP8A6LniP9CGY/wqZ4K6taqryyU0YRf8OONstuOuuIaaaQpx11xSUNttoSaluOLUZJQhCSMzMz6EQCvt4nPjs6R4v4xk+q+MOUY3ubknLjSalm3oXY2R601LLc6x37fI7yMt2lyjJ6w+/wBHpojshDUtv/jE2kI9HkSinHXqSimZ16lDfKcoyLN8myHMsvurLJMryy7tMkyXIbiU7Otry+u5r9lb29nNfUt6XPsZ8lx51xRmpbizM/jia18IBcR9WL4hS62r29zXy2qUynIo7mltPvSW+hyKmFYw7nZ+RxW3WzSph+5rqyriymjJSXIdiyfkNRHGqeRXXPItwCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCPe3T4ZdnfRrf/qg8PLD0p6f+o/jf5S5/wC71uU91THtbY+1U+MxR5BwHsvv2oSwcE/qR5F9ca397OIDvw8FlGHR93x8ss3727pbC4pnHeFH2mPRVt0x2VNtI5ef/wDol/y8/WYOnLwtEY/AD8efkdvHhOcPTH7T92jm8g6ctlvHahsZxL6fZBYB/lV7yckH7G6ANOHS34S/GnvNvFo2/wCY9qL37X0dKacelRxsAAAAAAAAAAAAAAAAAAAAAAAAAAAAANLOdn1I8d+uNUe9nLx1q+FNjHo+7n+WWU97d7Ny8LThvCv7TPoqET/kHQfst+7UMjad6fC5qz642Ee+asHMfR1p/wCoLgX5Zbl98ss+PeMx7X3/ALTX6GU9o9W7ikAV3bjp7b2n6YzvzS6PH7xFT/8A0Gf9WXvulTl+3VHe6frY8Z87yDR9lPahNvxf+oNrv9LrL9X7YemzoPRh0VeD/UeY9n5pxlvz/et766PQwz4P1c0oAAAAAAAAAAAByzPEI/l8c4P533JX+GfNBbGpdGpqCDK/L6tP/R23f84/ZfvV1wIVa1detYMEUAB4nY+uME2/gmU6y2bi1Pm2A5tTyqHKcWvopS6u3q5ZF5xl5vqlxl5lxKXWH2lNvxn20OtLQ6hC0hz3fFq8JXO/D6ztzOMHbuM24r5tcOM4VmrzZy7TA7SWbj7OvNhvMNpbZsmW0qKtsjS2xbMNmZEiQh5lFkTj2VtNWPZQzDKT9EOZMrpkWwr5UmDPgyWJkGdDfdizIcyK6l+NKiyWFIejyY7yErQtCiUhREZGRkAvG+C140sPkfDxzinysyONB5BQYzFVrTZdq+1Fh7uhxWibj0V7IcNDMfa0dlHRCz6Jv0l3J6TiUmVCYw0wrqpw0xqWXBFAAAHNa8a/+lG5cfRhh/8ABZgYsjUtp1IsxlJdX9Vo+oHyo+u/hnvMeEKldetaaEUAAAAAAAAFVz1nDlonFdU6o4cYzZITc7Usm9rbOisqcJ9jAcPsFxMHrJie4mVw8nzhiRNSXRS0O46kz7SWXfKmOVOiOVSxE1jK+idO5ZyD3NrDSGCx1SMs2nm+O4TTH5pbzMN69sWIb1tNShSDRWUsRxyZLcNSUtRmHFqUSUmZGNTqpae1ZiejtUa405gsT0LD9X4TjWC46wpLZPqq8ZqYtTGlTVtIbTIspyYvn5Txl3vyHFuKM1KMzqU62SAEavi28S/sx+C24dcVNb7ZbCxKt+FjVDbae+Y5n2ARZthHp4BdppOZl+PvWFI33GlBLsiUpSe3uLMThLNM4S5nAsXADpH+Cxyz+y04Eart7q0XY7H1AyekdkqlSfSbCTbYLBgs43fzHnVnLlvZPg0usmSJLiS87YLlJJSzbUo66owlVVGEpYhhEAAAAAAAAAU5fWTOARU9tQ8+dZ0iG6y9epsB5DQ66I4RRb7sRW4BsyabSXGkM3UdtqgsHlmyhMpmsJJOOy3VFOmeRZRPIqUCSYAAMg6n1dm27dl4NqPW9K9kOd7FyepxLFqhk+z0q2uZbcSOqQ+ZG3DgRu83pMhfRqPHbW6syQhRkYdQXhPxRwnhVxp1lx5wgmJTeHUyHsryRuKmLIzXPrYkzcyzCakzW+R3Fy4v0Zp1x1cOvbjxSWpthArmcZVTOM4tqxhgAAAAAAAAAczXxa+WZ8xudO5NkVNodnrzFbMtUanWhSVQzwDAJEutjWtcovl1QcwyFyxvUec/LEladpkkkkhNkRhC2mMIRsDKSx56tpxMLbnK3K+SmSV3pGIcaMeQnHVyGUKizNrbCi2lPRm2TxG3K+dzFY1rMX2Ea4sx2C4ZoNSO6NU6EK50YL2ogrAFKf1m7iX85+39WcxsarfN0m3q1nVuzJTKejTexsKq1P4bZzVqSRqmZPr+G5DbJJmlLONGZkkz6qnTPIsonkVZhJNul4eXKWbw25iaQ30mVJYxvHMti1GxY8ZBvqsNZZUR49nkYonRSZkmNj9g9LiNmXknRmFkaVJSokxjDExjGDqKwZ0K0gw7OulMTq+xix50CbFdQ/FmQpbKJEWVGebNTbzEhhxK0KSZkpJkZeQVKX6gAAAAAAAAHIOFq8ASU6T8X3xEuOursR0vpvkN852tMEhzIGKY18E2jcg9qolhaz7uW17cZTrO8v53nrOzfd7pMp5Se/tSZISlJYwiUZpiXvrXxz/FUuG2mpfLG1ZSytS0HVaq0TROGpSe0yddpNXV7r6OnxkrUpJH5SLqGzBsw1E3Fzn5j8gIMuo3HyZ3RnmPzkG3Mxa2z2+aw+UhTaGlE/h9dLhYw53oQRH1idT8pn5TPrnCGcIhqmDIA3P4G8JNo89eQWNaT1zHegVZrZutk547EVIqNc4DGlsM2+SWBd7KJU9ZOlHrYROIXOnutt9zbfnXmkzgxM4Q6aWlNOYDx81Nr/SmrqZFDgOtMYrcVxquI0OSPQ69ro7PspKG2fbC7uJi3Zk+UpJOS5r7ry+q3FGKlOtlAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBvdRFubZ/0a3/AMn9EHh5ZOlNH/Udxv8AKXP/AHetyfuzH2us/a6fGYn7i+LqOBMH3Yyli4In11Fkf1x7f3s4gO+7wWkYdH7fHyxzfvbulsXifH0/Rj9hj0VbdUdlDbiOPxAT6fBL/l5+swdOnhZox+AH48/I7d/CuP7/AIf5n7tHJ3F8XUdOmDd2MtjeJKiPkHr8vor95OSD9i9AKP8Aq34T/GnvNvFpG/sfam7+19HSmqHpRccgAAAAAAAAAAAAAAAAAAAAAAAAAAAADSrncfTUWOfXHqPezl461/Clxj0ftz/LHKe9u9m4+GMfT9eH2GfRUIne4vi6joRwb6xlkbTqi+F3Vn1x8H+T7pqscxdHaP8AqB4F+WO5ffLLPj3hM+kL/wBpr9DKfEerRxcAK7Nwovbe1/TGd8n/ANpdHkB4ij/+gz3qy990qctW8e909iHzu4vi6jR8E8ZTd8XvLoXXR/3usv1fth6aeg/+itwf6jzHs/NON99/70vdmPQwz4P1a0oAAAAAAAAAAAByzPEI/l8c4P533JX+GfNBbGpdGpqCDK/L6tP/AEdt3/OP2X71dcCFWtXXrWDBFAAAHidj64wTb+CZTrLZuLU+bYDm1PKocpxa+ilLq7erlkXnGXm+qXGXmXEpdYfaU2/GfbQ60tDqELSHPd8Wrwlc78PrO3M4wdu4zbivm1w4zhWavNnLtMDtJZuPs682G8w2ltmyZbSoq2yNLbFsw2ZkSJCHmUWROPZW01Y9lDMMpP0Q5kyumRbCvlSYM+DJYmQZ0N92LMhzIrqX40qLJYUh6PJjvIStC0KJSFERkZGQC8b4LXjSw+R8PHOKfKzI40HkFBjMVWtNl2r7UWHu6HFaJuPRXshw0Mx9rR2UdELPom/SXcnpOJSZUJjDTCuqnDTGpZcEUABzWvGv/pRuXH0YYf8AwWYGLI1LadSLMZSXV/VaPqB8qPrv4Z7zHhCpXXrWmhFAAAAAAAH8JUqLBiyZs2SxDhQ2HpUuXKebjxYsWO2p6RJkyHlIaYYYaQalrUZJSkjMzIiAcvbxHeVUvmbzK3ZvREqS/idtk72N6zjSDUkoOscQ60GFpRGURJgvW1XDKylsp6kmfOfPuUZmo7YjCF0RhGDR4GVof1ZbiX8/G8Nm8vcmqvPY/pWnd15riXJY6subNzquUWS2Ne/1MimYrr19cV9Bl/YZI2oj6kI1TyIVzyLtQgrAABzWvGS4l/Yh889tYpTVvtfrnZslO6dXJbT2xWcYz6ZOk2lJFQlPZHjYpmsS1q47XcpZQ4jC1H1cIWROMLaZxhFmMpLC/q5HLI9J8x7PQWRWaYuDcoaBFDDRJcSiLE2phTNneYNJ846skMHdVcm2qUoQXfKmzYaT69iekao0IVxoxX2hBWAAAAAAAAAMdbc1VhG8tYZ7p7ZFQi9wTZOK3OH5TVqX5px+pu4bsN9yHJJKnINlDNwnoslHR2NJbQ6gyWhJkNTl7cz+LGb8MOSezuPOdJW/Mwm7UePXxN9kXLsJtkFZYdlkI0l5rsuqKQyt5tBq9FmE9GWfnGVkVsTiuicYxauAyALh/q2fAL0CuvufOzKTpMtkXOA8eYdlE6Kj1bbi63P9mQjdSrouxktu0Fe8jsWlpmzJRKQ80oRqnkV1zyLcQggAAAAAAAAAIr/GW5ZnxF4F7Zyelsfa/Ym0IxaU1ktp5bE2PkmfwLCNa3kF1o/Ox5mKYXEtLOO707EzIrCVH8uXXMRjKVMYy5rwsWgDpZ+D3xMLiBwP1BhVtXe1+w9gwC3JtNDrKGZjOY7AhQJzFJPQRGaZmIYnHrKd4u9aTfgOLSfRfQVzOMqapxlJ8MMADRfxJeKcfmbwx3Xo5iIzJy+xxtzKNYvOqQ0qJs3DjO+w5KJLnVERm6nxDq5TnQ+2FPeL8EZicJZicJxcvuRHkRJD8SWw9FlRXnY8mNIaWzIjyGVqbeYfZcSlxp5pxJpUlREpKiMjLqLFz+QDoheAlyzPkxwMw/E7+x9M2JxtmI0vkxPPLcmS8Wq4bUzWF2tCzW4mM7hrjVUS1LUp6VTSF/KkoklCqMJVVRhKa8RRAAAAAAAAcg4WrwAAAAAASq8CPCC5Yc7bikuajGJ+p9FSnmHrXeWfU06JQSKo1l59zX1I+qvsdkWS20rSyUJbdaTyeyTOjdeoxMxCM1RHZX4eF/CPQ3BHUkXU+jcdXDalLiWOa5pcLam5psTI48RMVV/lVshplLi0p7yjQ46GIEBDi0x2W+9w1wmcVczi27GGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFHtbizujKdlZzkdLjsCRU3eT29nXPuZBSR1uw5ctx1hxTD81DzSlIUR9qiIy/BHRJz7dBvpG8c88vE/GPDm6Mre3DvPfWazGXrqz+TtzXau3aqqKporvRXTMxMeRqiJjlhvfI773fZydqzcrmK6aIifI1a4jsMf/Ydb79y9d9M2P8AzQHE/wBHj0p/cTJ//I5H+PfV7f7s+yVf+Gr9ZIDxS1rl2rNeXOP5pAZrrSZmljcx2WJ0OwQqBIo8dhNOm9CefaSpUivdLtM+4iT16dDIdsHQU5muP+Y7mj3jwnzjZW1lN95jiPMZuiii9avxNi5kt32aaprs110xM3LF2NmZ2oimJmMJjHa2+85Yzubpu5eZmiLcRqmNONU8vZhs2P2m0dHB4gh9Pgk/y9/WWOnjwsX/AAD+PPyO3ZwvOHf/ANp+7RwdfZHTxg3btddshxHP/vCa+/yr95GSj9h9AT9LXhP8ae828Wk79nHdV39r6OlNcPSW48AAAAfAx3KKHK4cmdQWLFixBs7CmnE0Zk7BtaqSuJPgS2VEl2PIYeR8ZRF3oUlaeqFJUe1eEeNuF+O933t5cK5y1m8tls7fyl/ZnyVnNZa5Nq/Yu0ThVRcorp1VRG1RNFyiarddFdVt2zdsVRTdiYmYiY68TpiY/wAutrffG6lQAAAAAAAAAAAAAANXLfmDpijtrSlnzshTOqLGbVzEtUL7jaZdfJdiSCbcJ0icQTzR9Ffgl5R+IuIPCDdHThnf2e4c3pmd7RvPd+bvZa9FORrqpi7YuVWrkU1bXkqdumcJ5Y0tZo3Fn7lEXKYo2aoiY08k6Xzvs1dG/wDt+SfS9I/uo0j6SHoy/fW+fWFfdpfB/ePUo/8AE2BwHPMf2TjMPLcYclO0896YxHXMjKhyDcgynYcjuYWpSkkTzKuh9fKXlH6v5q+dHhTnk4Ly/H3BVd+vh7NXLtFub1ubVzas3KrVeNEzMxG3ROGnTGlpmZy13KXpsXsO+Rhq069L2Y5FfOAADSjnh9SHHPrkVHvYzAdbXhR/0f8Ac/yxynvbvZuHhqcM/X9pn0VKJjr7I6FsG+NrrskacP8Abe1X5f8ASRg/vnqxzD0d4/6gOBvljuX3yyz5M/V/IL/2mv0Mp9h6rXGIAhwsuIG+JFjPkNYzXKafmynm1HktAkzbdfWtBmk55GRmlReQ/KPO9vjwfPSizm981m7G5cnNi7mbtdM+2ORjGmquqqJwm/jGidUt/wBG/wDdsURE11Y4R+xq/Wfi+w6337l676Zsf+aA036PHpT+4mT/APkcj/Hpe3+7PslX/hq/WShaKxS7wfU+G4rkcZuJdU8KazPjtSGJbbTj1vYS2yTIjLdYdI2JCT6pUZF16fHId3HRf4E4l5suYfh3gXi+zRl+JN35a9Rft03KLtNNVeav3aYi5bqqoqxouUzjTVOGOGuJbM3nft5nPXL9qcbdUxhyckQy0Oe3wgAAAAAAAAAAAKnnIz1a/Nt68hN77uics8VxyLuPcuz9qxsekait7KRQx9hZveZczSv2LefQ27B6rbtyYU+llpLqmzUSEkfaU4qTivCGGvvVvPvtzcP/AHFbr+McNpnbhYS8MDgta+Hpxtn6FudjV+0ZczZmUbALJazG5OKxm2chqcZrUVh1kq5vHVOxVY+azd88RLJ0i7S7epxmcZQqnGUiwwwAAAA8TsfXGCbfwTKdZbNxanzbAc2p5VDlOLX0UpdXb1csi84y831S4y8y4lLrD7Sm34z7aHWlodQhaQqhbH9VnKxzrKbDVnLCHjGvJ1xKl4jjeXawm5JkdDTyFE6xT2mQV+a00W7erlLUymUmLHU+2hK1oSs1EJ7Se28V96t599ubh/7it1/GOG0ztw/TD9Vn2VXTIthX818Ygz4MliZBnQ9OX8WZDmRXUvxpUWSxspD0eTHeQlaFoUSkKIjIyMg2jbhaZ4t4HvbWGmcYwLkTt2k3rsTF0KqfhSqsVmYhPymgisx26iTlldLvL5M7LWUk43LnNONJmpSh1xvz6nnHIT1kJw5GwwMKw/OX1e/MeYHK3cXJGt5QY1gsLaVzS2sfE5urLS+lUyarE8fxpTL1uxnFWzNU+ukN4lJjt9pOEnofTqcoqwhOKsIwan/ereffbm4f+4rdfxjjO0ztwnG8J/w17vw2Ne7Zwi721VbZd2XmVJlTFjVYjLxFuobqaRdQqE7Gl39+qYt9S+8lktskl5Oh/HEZnFGqcUsQwiAAAAAADXPlxqLPt+8bdwaU1nsGHqrLdp4hMwdnP5tNKv28epMgejwMtWxVQrKmlPT7LE3ZsKM6iUwqK/JRII1G0SFZjWzGicVWP71bz77c3D/3Fbr+McS2k9uD71bz77c3D/3Fbr+McNo24WWPD/4c45wR4uYFx3o7aNk9jQPXd7meaR6o6VWaZlklm/Ps712uVMsFxkMQ/Rq+MhTzi0QoLCVKM0mZxmcZQmcZxboDDAAAIh/Fh8K2q8SvGtS+1Wwq/U+xNU3l56HmE/GH8pi2mF5PAa9ucblVsO5oXzkIvKmvlRH1PqRHSmSkmzOQak5icEqZwQn/AHq3n325uH/uK3X8Y4ltJbcPTYV6sZuHXeZYlsDD+b+K0uW4Nk1DmOLXEfStx6RU5HjNrEuqSzY7tjGnz0CzhNOp6kZdyCDag24W+IJTkwYabNcV2yKLHKwdgtPMQXJxMoKWuGxIekvsxVv9xtoW44tKOhGpR9TOCt+oAAAAAAAAAAQ6eK54SWL+JNF1hkVPndbqHbmt3p1KedS8UcypjI9d2SZU5zEbWDEuaCWtdRka0za185C24vpM5HmlHK728xOCVNWHYQxfereffbm4f+4rdfxjiW0ltw+nSeqvZM3c1DmQ8xqCTQIs4C7yNUadsotrIp0ymjs2KyVL2DJixrB2ES0suOtuNocNJqSoiMjbRtrcmvMAxHVWB4drPAaWLjmE4DjVLiOKUUIjKNVUFBXsVlZDbUs1OOqaiRkkpxZqcdX1WszUozOCt7EAAAAAAAAAAQm+LD4Ve0/EryfUiqvkXQ6n15qihvkw8Pn6/tcpkWmZ5TYMHd5LLnw8woYq2UUdNXRYjK463IykylJc6SVJKUTglTMQiJ+9W8++3Nw/9xW6/jHGdpLbhlfRPqxbuvNzawz3ZfJrGtiYJhWb47leTYFD1NYVK8yrsfsWLU8afsZucWkaLX3L0REeUZx3DOKtwkkSjIybTE1raIggAAAAqv8ALT1bVe+uSG4d1a25H43q7FNp5nYZ0xgU3Vk+9cx24yNLVllLDFpBzWniPQJuUPzJUZtEVhMZh9DBEfm+9Uoq0JxXo0td/vVvPvtzcP8A3Fbr+McZ2mduEofhW+EDtbw2twZ1nDvJjHtm4HsjBk4tleAwtcW2MKk3FVbR7XE8pYsns0uGCn48TlhGQlyOtKo1nIIuijSpOJnFGqqJTyiKIAAAAAAACmv96t599ubh/wC4rdfxjie0s24PvVvPvtzcP/cVuv4xw2jbg+9W8++3Nw/9xW6/jHDaNuHo4HqqNg5EaXac6YcOcfnPPxoHGt6yiN9HVk15qbI3zVPPd7JJUrrHb7VGaS7iIlG2mNtmzCvVaNAwH2lbG5TbgyqKRp8+zhWHYXgD7iSJrvJqTeubKbZNSicMjNpzoSkl0PtM142jbSh8efBW8OjjjNiXmP6Frdi5XCW06xlW6rGTs2W08wZqYkRsfuiTglfMYcV3ofjVDD5KIj7/AJVPbiapliaplKm222y22002hpppCW2mm0pQ222hJJQ22hJElCEJIiIiLoRDCL/sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABG94gx9Pgj/y+/WWOnrwsFO18Afx5+SG6uGf9P8AtP3aN3v9j+r/APoOnvvfXbrxbI8RldeQ2vi6e6z3kZKP2F0BqMOlnwnP9Z+828Wlb7/3Xd/a+jpTZD0juPwAAAEKVJujJtM7tz24p1KmVEzOMlZyLHnnlIhXEJF/YdDI+1ZRbGKS1HHkEk1NqMyMlNqWhXnB4a6Q/GnR26SfFPEXD1U5jh/M8Tbxoz+QrqmLObsxnr/Zi3mLcTVNi/FM1W6pmmqK7Vdy3Xv+5kbOf3fbt3NFcW6dmrlidmP1J5Y+rhKXnAM/xnZeMwMrxSembWzU9jrS+1E2umoSk5FbZRyUs4s6Kay7k9TSpJktBqbUlSu/nms50+DOeTgzK8c8DZqMxufMRhVTOEXsveiIm5l8xbiZm3etzMbVOM01UzTct1V2q6K6tj5nLXspemzejCuO1MdWOt/lre0HIqgAAAAAAAAAAAAAV8tlr/bH2B5P8dsq/B/v7P8AYHk/55aMed/ivT/xLvP2bfcm5Sf5La+10+NDxPf7H9X/APQcbd7676MU0HDc+ug8XP8Avlk36vzx6L/B5xh0WdyR/tm8fZ19sLfv+8q+xT6GG0Y/bTRwAAaTc8j6agxz65FP72MwHW54UWNrmA3PH/3HKe9u9m4OG/59X9qn0VKJXv8AY/q//oOhnvfXb1xZI02r9t/VXk/0kYN+D/1nqxzD0eKMOf8A4GnH/jHc3vllnyZ+f5De+1V+hlP2PVQ41AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABG34hP+iL/L79ZQ6ffCvf8Bfjv8kN0cNzh379p+6Rt9fZHT63Rj12yXEQ/wDvD698vus94+Sj9g9Aj9LLhT8Z+828Gl76n+jLv7X0dKbYekNsMAAABXt2Yf7Y+wPL/jvlf6vTx5ROeX/F/iv5Sbz9m33JOVn+S29P+jp8aHq9MboybTGTIuaZaptPNU0zkWOvPKRCuISFH0Mj6LKLZRSWo48gkmptRmRkptS0K3x0dukRxn0d+M6eIeHqpzG4MxNNGfyFdUxZzdmJnrTFvMW4mqbF+KZqt1TNNUV2q7luujPZKzn7Wxc0Vx5Wrlif1urH1U2eAZ/jGy8YgZXik9M2tmp7HWldqJtbNQlJya2yjkpZxZ0U1l3J6mlSTJaDU2pKlej7ms50+DOePgzK8c8DZqMxufMRhVTOEXsveiIm5l8xbiZm3etzMbVOM01UzTct1V2q6K6tiZjL3crdmzejCqO1MdWOs9oORVAAAAAAAAAAAACvbsw/2x9geX/HfK/1enjyic8v+L/Ffyk3n7NvuScrP8lt6f8AR0+NDxPX2Rxsvx66aPhr9QLF/wBMsn98FgPRX4PX9FrcnqzePs6+2Nvz/eNfYp8aG0o/bLSAAAaSc9fqQY39cmn96+Yjre8KH/gDuj5YZX3u3q17h2cM7V9qn0VKJLr7I6HG8seuyTpo/wBt/VPl/wBJODe+irHMPR5/x+4G+WG5vfHLPlz0/wAivaf9FX6GU/49UbjkAAAAAAAAAAAAAAAAAAEL+0/Hs8PnTuztjajzTItqMZjqzO8v1zljNdrKyn17OS4RkFhjN61BnNzUImw27SsdJp1JETiCJRF5RnZlLZl4P74z8NP3T7g/cntfz+M7MmzKTjiHzC0xze1TJ3LombkM/CYuW2+FOv5NQv45YleUcKpnz2018h15xUZMe6YNLnXoozMvwBiYwYmMNbaQYYAAAAAAAAabcxucum+C+LY7ne9ajZjeD5JZu0bWX4Vg8vL6GouybS9EqsklwJba6GXas96oZyEJaleZcS2tS0KSWYjFmIxR2ffGfhp+6fcH7k9r+fxnZlnZk++M/DT90+4P3J7X8/hsybMn3xn4afun3B+5Pa/n8NmTZk++M/DT90+4P3J7X8/hsybMpDOGHPbj5z1xjNMu4+WOVWNPgN9X45kK8qxiTjMhuzs69VnFTFYkvvqlMnFSZqWRkRK8gxMYMTExrboDDAAAAAAwXyT5G6r4naZzDfO6LqTRa9wlFT7bSq+A9bWkiTe3Vfj9RX1VTGMpNjPnWtoyhLaP7FBqcWaW0LUlEYsxGOhEv98Z+Gn7p9wfuT2v5/EtmWdmT74z8NP3T7g/cntfz+GzJsymB0RujEuROoMC3dgUTIomFbKo0ZLiqcrpnMfvJVDJkyGqy0kVLrr7kaLcxWEy4hmozdiPtOdC7+hRR1MtgAAAAADULmTzc0lwTwHG9mb7PNImG5PlbeFQbbEcTl5SiLkMiqsrqHDtExXmfQCnwKiUplSj6OGwoi8pDMRizEY6kcH3xn4afun3B+5Pa/n8Z2ZZ2Zf9J9Yx8NNSkpPKdvII1ERrVqe3NKSM+hqUSJyl9qfjn0Iz+QQbMmzKcqotqy/qqy9pJ8W1prqvhW1RaQXkSYNlWWMZqZAnw5DRqbkRZkV5Dja0maVoURl5DEUX0AAAAAAAAAGrfLzmFpjhDqmNuXe03IYGEystqMKafxmhfyOxO8vIVtPgNqr47rLiYyo9K+anOvRJkRfgjMRizEY6kY/3xn4afun3B+5Pa/n8Z2ZZ2ZetwHx/vDv2TnWF66xjI9ru5Ln2W45hWPNTNX2cSG5eZTcQ6Kpbly1zlIixlz57ZOOGRkhJmZ/GDZk2ZTXiKIAAAAAAAAAje5keKxxA4KbAx3WG+smy2JmeTYkzm8KqxHEZeUKi47KtrSlhS7N2LIZRBXOn0spLSFdVKSypXkLp1zETLMUzOpqJ98Z+Gn7p9wfuT2v5/GdmWdmWVdH+OXwU5E7bwDSOq5+3r3YGycih41jVc5q6yiRfSpJLekTrGY5OUiDU1MBh6XMfURkxFYccMj7egxsybMwmHGEQAAAAAARKckPGq4S8UtzZlobcs7a9HsPBXqtu6iQNa2FpWOsXdLXZDUz6u0YnJYsIE+ptWHUOILyGo0qIlpUks7MylFMzpYO++M/DT90+4P3J7X8/jOzJsy2e4keL3wt5rbZ+BTSeUZm5nrmMXOVwoGX4ZNxeJaQKB2AmyiVsyTJfRKtWY045JMERGcaO8516NmR4mJhiaZhJ8MMAAAAAAAgj++M/DT90+4P3J7X8/iWzKWzJ98Z+Gn7p9wfuT2v5/DZk2ZS6ceN9695QaZwXfGqpNrL1/sSBPscbkXdY7TWjsaturOglHMrHluORFlY1LxJI1H1QRK/BEZ0MTGGhmgGEQ/IDxt+EnGLb+baN3I5uTF9hYFaFXXVcerLKVDkMyIzM+quaie1P8zZUd7VSmZcOQjoTsd5BmSVdUlnZmUopmdLDf3xn4afun3B+5Pa/n8Z2ZNmX38U9YT8NfLMox3F2s82HRO5JeVVE1d5PrmypsbqXbacxAbsb+4dmONVNNDW+TkqStJoYZSpavlUmGzJsym8SpK0pWhSVIUklJUkyUlSVF1SpKi6kaTI/IYii/wBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABGx4hhmXwQ9P8Ar/8ArKHUD4VqMfgF+O/yQ3Nw7/pv2n7pGv3mOoHCG5sWynEJRnyI16X0We8bJh+v+gVEfnYcKfjP3n3g0zfH+7rn7X0VKbsej1sYAAABXo2ao/hI2D9G+Wfq9PHlL544j53eKvlJvP2bfci5Wf5Nb+sp8aHh+8xxvhC/FmLS+6so0vk6LmnWc2nmqaZyLHXnVIhXMJCj6GR9FlFsopLUceQSTU2ozIyU2paFfoDo8dIbjLo8cZU8QcPVTmNwZiaaM/kK6pizm7MT4sW8xbiapsX4pmq3VM01RXaruW6/iz2StZ21sV6K41VcsT+t1Y+qm41/sDGNmYxAyzE56ZtbNT2OtL7UTa2ahKTk1lnGJSzizoprLuT1NKkmlaFKbUlSvRxzW86XBvPFwbleOOB81GY3RmIwqpnCL2XvRETcy+YtxMzbvW5mNqnGaaqZpuW6q7VdFdWx8xl7uVuzZvRhVHamOrHWe1HIigAAAAAAAAAAFejZqj+EjYP0b5Z+r08eUvnjiPnd4q+Um8/Zt9yLlZ/k1v6ynxoeH7zHG+EL8U1PDM+ugcWP++WT++CwHol8Hx+i5uX1ZvH2dfbI31/vCvsU+NDaYftdpQAANIee5mWn8b6f6yqf3r5iOuHwoEY8wW6Plhlfe7erXOH/AOeVfap9FSiO7zHRDhDeGLJWmVH8MGqPrlYL76KscwdHuI+f3gf5Ybm98cs+XOz/ACO99qq9DKwKPU449AAAAAAAAAAAAAAAAAAByzPEI/l8c4P533JX+GfNBbGpdGpqCDK/L6tP/R23f84/ZfvV1wIVa1detYMEUAAAAAAAAHidj64wTb+CZTrLZuLU+bYDm1PKocpxa+ilLq7erlkXnGXm+qXGXmXEpdYfaU2/GfbQ60tDqELSHPd8Wrwlc78PrO3M4wdu4zbivm1w4zhWavNnLtMDtJZuPs682G8w2ltmyZbSoq2yNLbFsw2ZkSJCHmUWROPZW01Y9lDMMpAAAur+q0fUD5UfXfwz3mPCFSuvWtNCKAAAAAAp5+s88tTlWmmeFuL2Z+Yq2k7w2wzFfSaF2ExFljus6CabJk4h2FAO2spEV0zStMuvf7eqG1CdMcqdEcqo0JLGzXDXjhe8uOUGluO9AclhzZWa19Zd2URo3XqLDYCXbrOciQjsWhS6DD62bLQlfRK1spQZl3dQnRDEzhGLqd4xjVDheNY9h2K1cWjxjE6OpxrHKSCg24NPQ0UCPV09XDbUpSkRa+vitstkZmZIQRdRUpfcAAAAAAGmviB8W4PMriBu7j+6zGVf5Zib9hr+XJcbjorNl4u61keBTFzV9DhQ3clrGI01aTSaoEh9sz7VqI8xOEsxOE4uXHYV86pnzqq0hya+zrJkmvsYExlyNMgzoTy40uHLjupS6xJjSG1IcQoiUlSTIy6kLFz8gDoRer88sj5GcFaTXWQWaZmweMFmzqa3adcSqdIwJURVlqm4cbStfm4KMfQ/RsmrtUtdA6oy8vcqFUaVVUYSnPEUQAAAAAAAFfP1lj+jtpP5x+tPerscSp1p0a1BoTWNjuHP8rriv/OP0f8Awm4wDE6nVjFSkAAAAAAAB+Kzsq+mrbC4t50WsqqmFLsrOynPtRYNfXwWHJU2dMlPKQzGixIzSnHHFmSUISZmZEQDlvc/OUFhzG5ebv5ASHXjpcwzCVEwSI8h5lVbrfGm2scwGEuM+fdGmHi9XGemJJKCXOeec7UmsyFsRhC6IwjBp4DK1T6sbxKPKNn7Z5lZNWG5T6wr3dR6vkyGEqju57lsCPYZ3bwHzLvZsMYweRGgq6H2rYyVwjLqkukap5EK55F0gQVgAAAAAAqJ+s9cTTfh6W5oYxWF3wT+A3az0ZtJKVFkOWWS6xvZLbKEmZMSVXFdJlOmoz89XsdSJKEidM8idE8in0JLGwXFLkFknFXkdpvkLihPO2uq85qcjfr2HjjKvMf7l12W4w4+lSTajZVik6bWvH1L8qlKCdLExjGDqkYTmOObEwzEdgYfZM3WI51jFBmOLXEfu9HtscyeqiXdJZMdxErzM+snNOp6kR9qyFSl6cAAAAAAcg4WrwB0pPBP/oueI/0IZj/Cpngrq1qqvLJTRhFX58efw1fstNJHyJ1LQelchdC0M+XKrqyL5yz2hqmH56zu8VSyyk37DJMUM3rKmQjudfJUuGhDjspjzcqZw0JUzho5FBQTWgC9B6vn4lSuQGri4dbgvFSNx6Ux1t/Wt5bTTcm7G1HXrbhtVZuyFm5MyXWiXGYq0kfnJFMuO4lK1RZjohVHKrqjlWURFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEaviHH0+CD/L/wDWSOoPwrEY/AP8d/khuPh+cO/ftP3SNbr7I6g9iW5NtsrxAP8A7xOvP8rfeNkw/X3QMpw6V/Ck/wBZ+8+8Gm73qx3fcj630VKb4ejpsoAAABXk2af7ZOwfL/jxln6vTx5TueOmfnd4q+Um8/Zt9yBlq/5Nb+sp8aHh+vsjjjYlftnX2Q2JNtmPS26sn0tk6LmmWqbTzVMs5HjjzqkQrmEhR9DI+iyi2UUlqOPIJJqbUZkZKbUtCv0B0eOkJxn0eeMqeIOH6pzG4MxNNGfyFdUxZzdmJ7ExbzFuJqmxfimaqKpmmqK7Vdy3X8WdytrO2tivRXGqeWJ/W6sJu9f7AxjZuMQMsxOembWzU9jrS+1E2tmoSk5NZZxiUs4s+Kay7k9TSpJpWhSm1JUr0a81vOlwbzw8G5bjjgfMxmN0ZiMKqZwi9l70RE3MvmLcTM271uZjapxmmqmabluqu1XRXVsu/YuZa5Nq7GFUfq9eOs9qOQ1IAAAAAAAAArybNP8AbJ2D5f8AHjLP1enjync8dM/O7xV8pN5+zb7kDLV/ya39ZT40PD9fZHHGxK/bTWcMvqAYt+meUe+CwHoi8HzGHRc3LH+2bx9nX2zN8Tjn6561PjQ2nH7WaWAADSDnz9R7G/rlU/vXzEdcXhPox5g90fK/K+929WtbhnDOVfa59FSiL6+yOiTYlu3bZK0wf7cOqPL/AKSsF99FUOX+j3TPz+cD/K/c3vjlnzZyr+R3ftdXoZWCR6mWwQAAAAAAAAAAAAAAAAAAcszxCP5fHOD+d9yV/hnzQWxqXRqaggyvy+rT/wBHbd/zj9l+9XXAhVrV161gwRQAAAAAAAAAHidj64wTb+CZTrLZuLU+bYDm1PKocpxa+ilLq7erlkXnGXm+qXGXmXEpdYfaU2/GfbQ60tDqELSHPd8Wrwlc78PrO3M4wdu4zbivm1w4zhWavNnLtMDtJZuPs682G8w2ltmyZbSoq2yNLbFsw2ZkSJCHmUWROPZW01Y9lDMMpAC6v6rR9QPlR9d/DPeY8IVK69a00IoAAAAPPZdlWP4JimT5vltpGpMVw3HrrKsmupiuyHUY/j1bJt7m0lrIj7I0CuhuOuH+AlBgOV7y85EX/LHkxujkPkSZLEraGc2t7WVstxDr9DirJt1eF40pxo1NufO1iNfCgd6TMl+j93lM+otjRC6IwjBriDK3n6sNxKJbu5uaWUVhGTRK0fqd6UwozJxZVmRbMyCETxEglJb9qa2PKaI1dFWDHcRG4lUap5Fdc8i38IIAAAAAAAAOeL4+fEv7GrnhleaUFb6HrrkvDe3NjrjKekSNmFhNXE2pSkvtQRzE5f3W6kJT5tqNdx0EZmSiKymcYW0zjCEgZSTW+ApyyPjPz0w3FL6zTC1zyRip0xlKX3EoiRcmtZSJer7o0rW036UzmrbNX5xaySzDuJK+ijIiGKoxhGqMYdEQVqgAAAAAAAFfP1lj+jtpP5x+tPerscSp1p0a1BoTWNjuHP8AK64r/wA4/R/8JuMAxOp1YxUpAAAAAAAAQf8Aj88tT42cEslwTH7M4OxeTU17T+PlHfS1Ni4XIipm7VuEtGaXHYKsTP2lcUgyU09esr/AEqY0pUxjLnnCa1+iHDmWMyLX18WTOnzpLEODBhsOypkyZKdSxGixYzCVvSJMh5aUIQhJqWoyIiMzAdRDw6+LEPhpw40joc4kaPlFDirF7siRH804c/ZuWqPIc4dXLaW4U9muupy4ER01H1gQ2El0SlKSrmcZUzOM4t2RhgAAAAAAGtnMLjpQcs+Me6ePGReYai7Nwiyp6qwktE81R5bDU1c4RkZtGlRrPHMwrIM4kl0NXo/QjLr1GY0SzE4Ti5YGV4vf4PlGSYVldXLo8pxC/uMXyWlntqZnU9/j9jJqbirmMq6Kal19jEcacSflStBkLFz4AC+v6uNyzPdnDq20Dkdj6Tm/F7IUUMBL7y3Zc3VObO2V7g8pTjxkp32lt49vUpbbJSIsGFDSZl5xJCFUacVVcacVhoRRAAAAAHIOFq8AdKTwT/6LniP9CGY/wqZ4K6taqryyU0YRAFB/x7vDVTxS3QnkpqOhOLx+31fzXbWtrYhNVer9uTESLS2xtttrq1Dx3NWmZFpUoT2tsOtzYiENMx4xOTpnHQspnHRyq9wkmybpjb+faB2rgW59XXj2O59rjJK/J8atWjWaETYKz87CnMIca9Op7aG47EnRVH5uXDfdZX1QtRGY1um9wQ5la/52cbcI33gpx66baMHS7Bwwprc2dr7YlUywnJcUnOJJDrkdp15EqvfcbaXMq5MaQbbZumhNcxhKqYwnBuMMMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACNLxET+o//ANoH6yR1D+FV/wCA/wAd/khuLcE4d9/a/ukaPUdQ+DcW02X4fH/3i9d/5W+8XJh+vOgd+ldwr+M/efeDTt7Tju+5+19FCcQejRswAAABXd2cf7ZWw/o5yz9X7AeVXnhj/m5xT8o95ezbzfuWq/k9v6ynxoeG6jjnBftHUMDaOoYG0zLpXdeUaVyhFzTKVNp5qmWcjxx55SIVzCQo+hkfRZRbKKS1HHkEk1NqM0qJTaloVz70eukLxl0euMqeIOH6pzG4MxNNGfyFdUxZzdmJ8WLeYtxNU2L8UzNEzNNUV2q7luv485lbWctbFeiuNU8sT+t1YTga+2Di+zsXgZbiU9M2smp7HWl9qJtbNQlJyayzjEpZxZ8U1l3J6mlSTStClNqQtXow5rudHg3nh4Ny3HHA+ZjMbozEYVUzhF3L3YiJuZfMW4mZt3rczG1TjMVRNNy3VXarorq2bfsXMvcm1djCqP1evHWe2HIakAAAAAAABXd2cf7ZWw/o5yz9X7AeVXnhj/m5xT8o95ezbzfuWq/k9v6ynxoeG6jjnBftJsuF/wDJ+xX9M8o98NgPQ54Pv9F7cvqzePs6+2dvfTn6uxT40NqR+1GmAAA0d5+fUdxr65dN71syHXL4Tv8AwE3R8r8r73b1azuOcM3V9rn0VKIbqOinBuvaZM0sf7cWpvrl4J76aocvdH2P+ffBHyv3N745Z82cq/kl37XV6GVg8epJsQAAAAAAAAAAAAAAAAAAHLM8Qj+Xxzg/nfclf4Z80Fsal0amoIMr8vq0/wDR23f84/ZfvV1wIVa1detYMEUAAAAAAAAAAAeJ2PrjBNv4JlOstm4tT5tgObU8qhynFr6KUurt6uWRecZeb6pcZeZcSl1h9pTb8Z9tDrS0OoQtIc93xavCVzvw+s7czjB27jNuK+bXDjOFZq82cu0wO0lm4+zrzYbzDaW2bJltKirbI0tsWzDZmRIkIeZRZE49lbTVj2UMwykur+q0fUD5UfXfwz3mPCFSuvWtNCKAAAACvv6xdyzLRnC+No3HbH0bPOUd8vE3UMPIblwtWYoqBd7CnJ6GtfZbSnqulWhSCS9FtJJkslN9DlTGlKiNOKgsJrX2saxy8zHI6DEcYrJN1kuU3dVjmPU8NKVTLa8vJzFZU1kRK1ISqTPnym2myMyI1LLykA6m3CzjdS8ReLOk+PFJ5l34OMJgQL+ewSfNXOa2rj9/nd62ZNtq8zc5jazpLSVEam2XEIMz7eormcZxUzOM4toRhgAAAAAAABB54/nExXJHglkeeY9WnN2HxlsHNvURxohyJ8zCmopV+1KZDqerkeCjF+27e7Uq845QtIPoR9xSpnSlTOEueiJrX6oM6ZWTYdlXSn4NhXyo86DNiurYlQ5kR5EiLKjPtmlxl+O+2laFpMjSoiMvKA6iXh4cpofMrh1pHfRSGHclyLFI9NsWMwhDJQNm4mtWO50yURB9YUWbf17s2G2rynAlML8pKIzrmMJUzGE4N1RhgAAAAAAFfP1lj+jtpP5x+tPerscSp1p0a1BoTWNjuHP8rriv/OP0f/CbjAMTqdWMVKQAAAAAAAHPJ8fflqfJTndk+D0FmczXXGaE9p3HmmX0uwZOZxJipm1bxDaTWlqa5lfSmcUlRpdj0UdfRJmohZTGELaYwhCIMpJpfAa4mI5Oc9MLyLIK5E3XfHOGndeUlJZNyFNyCknxomtqNSuimjkyM0kMWXmnCNt+HUyUH8cYqnCEapwh0TBWqAAAAAAAAAFA71ijiYWh+abO68dq0wsC5SUb+Z97CVJix9pYwcGn2VDSk+40uWaZdZdOLNX5bJtn+0iJHQWUzoW0zjCv+MpJYfBZ5Z/Yl899VXF1aIrtcbeePSOylSpPo1fGqM6mwWccv5jzqiiRGcYzmLWTJElxJ+ar0SkkpBOKUWJjGEaoxh0kBWqAAAAAHIOFq8AdKTwT/wCi54j/AEIZj/Cpngrq1qqvLJTRhEAYY5DaE1xyf0vsHQ+2ak7jBNj0Eijt2mVNtWNc/wB7cqpyCklPMyG4V/jlvHYnQX1NuJalR0KUhaSNJtRE4aXMW5k8UNj8KuQ+f8fdmRnF2eJ2ByMcyNMN2JV53hFit13Fs3oyWt9tVdewEfljaHXThTW34jqvPx3UpticV0TjGLV4GUt3g9eIvZcAuScNzK50p7j1t9+pxPc9Sk33m6NpMh1rHtm10Ro1Gq0waVOcXJSht1cqpflsoQp9UdbeJjGEaoxjrujpVWlbeVdbd00+Ha09xAh2lTaV0hqZX2VbYR25cGfBlsLcYlQ5kV5DjTiFGhaFEojMjFap+8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABGf4iaunwPez8IP6yB1FeFSjH4Cfjr8ktwbi/0v7X90jP7/i+Ih1FbENfx67Zjh4rryM12X0Xe8XJx+u+ghTEdKzhWf6z9594NP3p/MLmnzPooTkD0XtoAAAAK7Oz1/tlbD8hf4c5b+D/f+wHlb54aI+dzin5R7y9m3m+svP8AJ6PrKfGh4bv9gvxRxzsQuxSGao470m7ONkCbDKNU59U3uTJorxSDS1NaTJbcKkuzbSpx6uecUfm3CJTkVxRrQSkm4252j8yvRU4d6QnRLy2eyHecjzmZHeWfjJ5yYwpu0xcpq9KZvZiaqrFVUzsV4VV5auqa6Iqoqu2ruh5nP15TPzE6bE0xjH1Y6/jtDsjx+8xG8ssbySskVF3USVxJ8CWkkusupIlJMjSam3mHm1EttxBqbdbUlaFKSojPrZ4q4S4h4I4hzfCnFeUu5HiDI3Zt3rNyMKqKo0xMTGNNVFVMxXbuUTVRcoqproqqoqiZ1m3cou0RctzE0Tql8Tv9gvxRt7YhPFmTSm7so0rlCLqlUc2nmqZZyPHHnlIhXMJCj6GR9FlFsopLUceQSTU2ozJRKbUtCufej30guMej3xjTxBw/VOY3DmJpoz2QrqmLObtRPixbzFuJqmxfimaqJmaaortV3LdfyZvKW83b2K8IqjVPLH/Z1YTi692Fi+z8XgZbiU9M2smp7HWl9qJtbOQlByayzjEtZxZ8U1l3J6mlSTStClNqQtXor5r+dDg7ng4Oy3G/BGZjMbozEYVUzhF3L3YiJuZfMW4mZt3rczG1TjNNUTTct1V2q6K6toX7FzL3JtXYwqjtT146z2w5CUgAAAAAArs7PX+2VsPyF/hzlv4P9/7AeVvnhoj53OKflHvL2beb6y8/yej6ynxoeG7/AGC/FHHOxC7FNpwuPrx9xU/755R74bAehjwf0YdF/cvqzePs6+2jvb+fVdiPGhtWP2k00AAGjfP4+mnMaP8A+ZlN71szHXP4TiMeYXdHyvyvvdvVrG5f51V9rnx6UQff8XxEOi3YhufHrsmaWX13HqYv/mZgfvpqhy70fqY+frgj5X7n98cs+fN/zW7p/wBHV40rCg9RrZAAAAAAAAAAAAAAAAAAAOWZ4hH8vjnB/O+5K/wz5oLY1Lo1NQQZX5fVp/6O27/nH7L96uuBCrWrr1rBgigAAAAAAAAAAAA8TsfXGCbfwTKdZbNxanzbAc2p5VDlOLX0UpdXb1csi84y831S4y8y4lLrD7Sm34z7aHWlodQhaQ57vi1eErnfh9Z25nGDt3GbcV82uHGcKzV5s5dpgdpLNx9nXmw3mG0ts2TLaVFW2RpbYtmGzMiRIQ8yiyJx7K2mrHspvvVaPqB8qPrv4Z7zHhGpGvWtNCKAAAADm/8Ajbcsy5Xc+NnSqO0Kx1xpY06Q14bClHDfjYXMmJzG6ZMujMpN5n8uzcYkoLo9Xoil1UlCTFlMYQtpjCERwyknx9Xg4mJ3/wA229w5FWpmYDxZpo+wnzkRCkwZWzb1yZUaur3VL6Ijyq+VGsL6M6RmpEmibIi+W7k4qnQjVOEOgAK1QAAAAAAAAA/Ba1ddeVdlS3EKNZVFxAmVdpXTGkPw7CusI7kSdClsOEaHo0qM8ptxBkZKSoyMBy3efHF+x4c8ud3cfpTUkqbDcwlycFmylredtdb5GhvIcAsXJKi7ZMxeLWUZuWaTUSJzTzfXqgxbE4wuicYxafgytZerGcs1Y5sfbvDXJrI01Oxa9e3tXMSZZpYYzXFocetz2mgRV93np2S4e3Enn2dvm2cedM+7u+VjVHKhXHKufiCsAAAAAAFfP1lj+jtpP5x+tPerscSp1p0a1BoTWNjuHP8AK64r/wA4/R/8JuMAxOp1YxUpAAAAAABpzz+5QwuHHELd/IB1xj28w/EZEPBIcgmXEWWxsmeZxvBIi4zxLKVERktpHfloJKzKEy8voZJMZiMZZiMZwcuGysrC5sbC4tp0qztbWbKsrOynPuyp1hYTn3JU2dMlPKW9JlS5LqnHHFmalrUZmZmYsXPxAOg56vpxNPjtwXp9l5BWOQdg8obNnalv6U2luXGwKK1Jq9U1hmlCe+DJx9x+8Y7jUovb5RGZdO1MKp0qqpxlOqIogAAAAAAAACIXxvuJhcq+A+zCpa703YujEL3lgRsMocnSTwyunLzWhY6F6TI9vMDk2BMxmzM5FixE+VUpCSGaZwlKmcJc4gWLX+pUpCkqSo0qSZKSpJmSkqI+pKSZdDIyMvIYDpt+FPyxLmTwb0vtezs/bLPqen+DPbK3DR6Z8JGAsxqm4sp6WiJhuTltYcK+JCPlUM2qE9EmRoTXMYSpqjCUiowwAAAA5BwtXgDpSeCf/Rc8R/oQzH+FTPBXVrVVeWSmjCIAAIXPGr8N2Pzr47u5drulQ/yW0hAsr7Wa4qUNzc8x9RFKybVUtxSkNvLum2fSqc3P94t2UNJWyzMlrPMThPWSpnCes520iPIiSH4kth6LKivOx5MaQ0tmRHkMrU28w+y4lLjTzTiTSpKiJSVEZGXUWLX8gFzn1djxKvnyx1rgRua/78qw+tl2XHK8tJXc9kGGwGnpt5qxb76vOO2WGR0Lm06OqzXTk/HSTTdeylyFUcquqOVa4EUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQ1bk5J7uxva2wqCkz+xr6iny27r62E3X0bjcWHFmutMMIW/VuvLS22ki6qUpR/gmOhfn56WHSH4T56OKOGeHeJszltxZHfebsWLVNjKVRbtW7tVNFETXl6qpimIwxqqmerMtzZbJZSvL0V10RNc0xM6Z/XY1+yu5B/6yrT/ozHvmOOJfz0elB8bc363yP4Kv8ASGR+xx25/XSV8Ntg5jsnWN7eZveP39rEzyzqY8yQxDjrar2MfxeY1GJEGNFaNKJM51fU0mrqs/L0IiLtp6BvOdx5zr80O8uIucPeNzee+rPEmYy1F2ui1RNNijJbvu028LNu3ThFy9cqxmmavJTEzhERGh7zs2rN+KbMbNOxE+LjLbQftlpyM3xFv9Dv/aD+sgdRvhT4x+An46/JLXdyTh339r+6RmdR1G7Mte2obM8O/wCUdrr/ACu94mTj9c9BKmY6VfCs/wBZ+8+8HwbzmJyNfieihOYPRW2mAAAArp7PP9svYf0c5b+r9gPLBzw0z87fFPyj3l7NvN65eqO8UfWR40PDdRx1syu2oTS8HPqDV/0TZH+aGR3+eDtjDo25b+t896Oltbe045yfrYep5F8dKPdtH6ZD9GqM+qIy00V6pHa1NaT3OFSXZtpU49WvOKPzbnRTkRxRrQSkqcbc3h0qeitw90hOHvT+Q71keczI2pjJ5yYwpu0xjV6UzezE1VWKqpnYrwqry1dU10RVRVdtXYZHPV5SvCdNmdcfVjr+OhMyPHLvEbyyxvJK2TUXdRJXEnwJaCS6y6noojI0mpt5h5tRLbcQam3W1JWhSkqIz8+fFXCfEXBHEOb4U4ryl3I8QZG7Nu9ZuRhVRVGmJiYxpqoqpmK7dyiaqLlFVNdFVVFUTO6aLtFyiK6Jxpl8TqNv7Mp7UMzaT3blGk8oRdUq1TaaaplnJMbeeUiDdQUKPoZH0WUWziktRxpJJNTajNKiU2paFc99HzpA8ZdHzjGniDh+qcxuHMTTRnsjXVMWs1aiexMW79uJqmxfimZomZpqiu1Xct1/Jm8tazdvYr0VRqnqf9nVhORr3YWLbPxaBl2Iz0zayansdaX2onVk5CUHJrLOMS1nFnxTWXcnqaVJNK0KU2pC1eifmv50ODud/g7Lcb8EZmMxunMRhVTOEXbF2IibmXzFuJnvd63jG1TjMVRNNy3VXbrorq2pes3LFybdyMKo/V68PbjkJUAAAAAK6ezz/bL2H9HOW/q/YDywc8NM/O3xT8o95ezbzeuXqjvFH1keNDw3UcdbMrtqE3HCv+T5iv6aZT74bAehToARh0YNyx/te8PZ19tTemnO1diPGhtaP2g08AAGjPiA/Ubxr65tN71szHXT4TaMeYbdHyuyvvdvVq255wzVX2ufHpRAdR0Y7Mty7UMm6VP9uTUv1zcD99NUOXOj/TPz88E/K7c/vjlnz5qqPStz7XV40rDA9RDZgAgutOVO/wBizsWGtkWaGmZ8xppBVmPmSG25DiEJIzqDMySkiLyjzn756ZXSby2983l7HFmbpsW8zdppj0vktFNNdURGnK46IjDS3ZTkMlsxjbjHDqz+u/D9ldyD/wBZVp/0Zj3zHGm/no9KD425v1vkfwVn0hkfscduf10u3HfJr3MtMYLkuS2DlreWsCwdsLB1thlyS4zd2cVtSm4rTDCTSwwhPyqC+N8kd4nRb4u4j475g+HOLeLc1VneIs7lr1V69VTRTVcqpzeYt0zNNumiiMKKKafI0xq6uMtuZ2ii1mq6LcYURMYR4kM0jn58oAAAAAAAAAAACkryt9Xy54bo5R8k9xYfb6CaxLbG/dxbLxdu52FkkK3bx3O9iZFlNIi1hsYDMYiWSKy1aJ9pDzqG3e5JLURdxziqMFkVRgwH97UeIl+jfHD90vKf4uBnahnahaO8HXhjt7gnxLstJ7sk4dKzGXt7Mc4adwe6nX1L7S3tJiFfCSubYU9G+U0pFG95xvzJpSk0mSj6mRQmcZQqnGdCVgYRAAAAAAAAAAAAAHidj64wTb+CZTrLZuLU+bYDm1PKocpxa+ilLq7erlkXnGXm+qXGXmXEpdYfaU2/GfbQ60tDqELSEefht+HXE8O2byYxDFMr+enVO0dj47m+r/bFTqssx2kj0Eium4tla/R2Yc+ZSzFE3HnMK6TY3Y442y73tlmZxZmcUoQwwAADDXIiFt600Xtmo0E7SRt03OCZFS6zscjt5NFS0uXXNe9WVOQzrOHX2sphGOPy/T0pQwtTzkdLfVPf3pQRr0qQDnq1niLPOLddvuObrrq1OOOObNypbjji1Gpa1rVrk1LWtRmZmZ9TMWbULdqH/P3tR4iX6N8cP3S8p/i4Dag2oWnfCL4DWfh9cVWtbZu9jtjuLNsxvs62pcYvMlWlIue66mlxalqLSfXVU6VWU2I1cRSkrjtpRYSZakEaV96oTOMoVTjKUkYRAAAAAAAAAABXl8a7witnc+8v03tvjzL19UbJxShusC2EWeXVhj0S+w9E1F5hciFMqsfvHHZ+P2s+2bdS6kvOsz2+1SSZMlSpnBKmrDWgz+9qPES/Rvjh+6XlP8XAltQntQzfxq8BzxO+M2/dR79wy945HkGqs6osuYh/CflTTVzAgS0pvccluJ1x3prsnoXZNdJ7TJXo8pfaZH0MsTVEsTVExgu7iCsAAAAAAEU/jFcMdvc7OJdbpPScnDouYxNvYdnDrucXU6hpfaWipMvr5qUTa+nvHzmnIvGfNt+ZJKkkozUXQiPMThKVM4TpVcfvajxEv0b44ful5T/FwJ7UJ7UMu8ffV4OfOsd9aR2Tklxx+Xjuvdva1zi+RWbEyWVYrpcTzOlvrRFfGd1/GakTVQYDhNNqcbStzoRqSR9SxtQxtQvEiCsAAAAAAEF3jX8GuY/P3FdN6l4+TtW1WscQvLfP88PPMwucfsLzNkwl0GIMwodZjF60uvx+lsbRa1uLI3XbBPRKfM9VSpmISpmI1q9X3tR4iX6N8cP3S8p/i4EtqE9qHtdberSc13dh4I3tLJdFwdZrzDGy2FLxrPskscjj4T7cQzylzH4D2C1zMy7Kk8/6I2uQwhb/AGkpxBdVFjahjbheepKWpxumqMdoa+LUUVBWQKWlqoLKY8GsqauI1Brq+GwgiQzFhQ2ENtoLyJQkiL4wgrfTAAAAAAAAAAH/AA422824062h1p1Cm3WnEpW242tJpW24hRGlaFpMyMjLoZAKPvIP1bLlpYbw2rZ6CudHM6Xts5yC31nByfNsgpb6oxC2nu2NTQWVdDwi0itPY8xK9BJaH1JeRHJ3ojv7Ez2oWRXHKw997UeIl+jfHD90vKf4uBnahnahO54J/h881/D2vNz4fvSx1Pa6a2ZV02R1LGD5pc39vRbMx6SitTIRW2WJ0EdusyXFrB1E18nnHCeq4SUt9qlrTGqYlCqYnUsFCKIAAACgz97UeIl+jfHD90vKf4uBZtQt2oPvajxEv0b44ful5T/FwG1BtQuF+HHx8zzirwp0Nx/2c9QSM71tQZBWZC9i9hJtaBcizzfJ8hjHXWEyBVyZLZQLholGqO2ZOEouhkRGcJnGVczjOLdsYYAAAAVQ/FM8AjZXIfknY764ezNYY1X7QYlXm2MOze8s8XiQNjlJR6flOMrqccvWn4mcNPelTo60tKZs2pD5LWmWTbE4q0aU6asIwlGr97UeIl+jfHD90vKf4uBnahLah6vBfV4PFA1lmmK7EwHOtA4rm2EX9VlGKZHVbQydmxpL+kmM2FZYxFr1s42bsaWwlXatK21kRpWlSTMjbUMbULueoJW0Zur8Ff3ZUYxR7bPG65nYldhdrIusRTlcZko1vMxqwmQa6YqktJLJyozTzRPRmniZWpamzWqtX2GSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABX05BKMt4bW8v+PWRfI/RF4eZPpNRH5wnGfyjz33etu/KfzW39ZHjMP9yvk/1CHBuEPoTB+H4ZnprJuv8ArNuferhg7y/BmaOYfe3yuzXvdutt3e/85p+sjx6m9A7E2lIyfEZMy+B3p/8AML9Y46kPCmxj8Bfx1+SWubm/0n7X90jK7lfJ/qEOpDCGttm+HKjPkfrkjP3X/I9wmUD9b9BSI/Oq4W/GXvRvB8O8v5lX4nooToD0TtrAAAAK520FH8JmxPL/AI9Zb8j9H7AeWbngiPna4p+UW8vZl5vKx/AUfWR4zwvcr5P9Qhx1hC1NTwaMz0LX9fdPkn5oZHfz4PD9G7K/1vnvR0ts70/nc/Ww3CH7lac1m5G8c6TdtH6ZC9GqM/qIy00V6pHa1NaT3OFR3ZtoU49XPOKPzbhEpyI4o1oJSVONufknpT9Ffh3pB8Pen8j3rJc5eRtTGUzkxhTdpjGr0pm9mJqqsVVTOxXhVXl66proiqmq7au/dks7Vla8J02Z1x9WOv46EjI8evsRvLLG8krpNRd1ElcSwr5aCS6w6noojI0mpt5h5tRLbcQam3W1JWhSkqIz8+/FXCnEHBHEOb4U4ryl3I8QZG7Nu9ZuRhVRVGmJiYxpqoqpmK7dyiaqLlFVNdFVVFUTO56K6blMV0TjTL4ncr5P9Qht/CEmZtJbvyrSWUouqVap1NNUyzkmNvOqbg3UFCj6GRklZRbOKS1KjSSSam1GaVEptS0K576PvSB4w6PvGFO/+H6pzG4cxNNGeyNdUxazVqJ8WLd+3jVNi/FMzRMzTVFdqu5br+bNZW3mrezXoqjVPU/7OrCc7Xmw8W2hi1fl2I2CZ1ZOT2OtL7UTqychKDk1dpGJaziz4prLuT1NKkmlaFKbUhavRFzYc5/B/O/wfluNuCczGY3TmIwqpnCLti7ERNzL5i3Ez3u9bxjapxmKomm5bqrt10V1bWvWbli5Nu5GFUfq9eHtxyCqAAAAVztoKP4TNieX/HrLfkfo/YDyzc8ER87XFPyi3l7MvN5WP4Cj6yPGeF7lfJ/qEOOsIWpveFJ9ePeKGf6KZT74rEehDoBfoxbm9V7w9m322N5/zyrsR4za4fs5p4AANF/EDMy01jPT/WbTe9XMx12eEz08w+6fldlfe7ejVd0fzmr6yfHpQ+dyvk/1CHRphDcTJulFH8MupPL/AKTcC+R7qqocucwER8/HBPyu3P745dRmv5tc+sq8aVh4eoJs8AVp7tR+3Nv5f+c5/wAj/wBqdHk33/Ee3ud9V3vulTetPlY7D5ncr5P9QhpOEJJ4+J/l49a0/Sy098VwPR70Lv0YuE/UmY9nZptTeH88r7MeNDYgfqF8YAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK+PIMy+HDa30d5F+qLw8zHSZpj84PjL5RZ77vW3TlKv5NR9bDDvUhwdsw+jahMN4fX1Gcm+udc+9XCx3j+DOjDmI3t8rs1737raBvWccxH1kePLeodiLTUY/iN/6HP+0L9Y46k/ClRj8Bfx1+SWsbpnDvn7X6qMfqQ6k9mGs7UNnOG5/95DXP+V/vDygfrboLREdKjhb8Ze9GffHvCccpX4nooTqD0RNtAAAAK5W0TL4TNi/R1l3vgsB5a+d+mPnZ4o+UW8vZl5uyzVHeaPrY8Z4TqQ472YW7UJreDH1BK/6J8k/NDA79PB5aOjflv63z3o6W3d5ac1P1sNwx+5HwADWXkdxxo930fpsP0ao2BURlpor1SO1qa0nucKjvDbSpx6uecUfm3OinIjijWglJU425+SulN0WeHukFw96fyPeslzlZG1MZTNzGFN2mMavSmbmmJqqsVVTOxXhVXl66proiqmq7au/bk85VlqsJ02p1x9WP8tKEXJMcvMQvLLG8krJNRd1ElcSwr5aCS6w6noojI0mpt5h5tRLadQam3W1JWhSkqIz8/nFXCnEHBPEOb4V4qyl3JcQZK7Nu9ZuRhVRVGmJiYxpqoqpmK7dyiaqLlFVNdFVVFUTO4qLlFdMV0TjTL4fUht/ZhLahmjSO78p0jlKLqkWqbTTlMs5JjbzykQbqC2o+hkfRZRLOIS1KjSUpNTajNKiU2taFc9dH7n/4w6P3GFO/9wVTmNxZiaaM9kaqpi1mrUT4sW79vGqbF+KZmiZmmqK7Vdy3X8+ZsW8zRs1eWjVPU/y5YToa82Hi20cWr8uxGwTOrJyex1pfa3OrJzaUHJq7SMS1nEnxDWXcnqaVJNK0KW2tC1eh3mx5zuD+d7g/LcbcE5mMxunMRhVTOEXbF2IibmXzFuJnvd63jG1TjMVRNNy3VXbrorq21dtV2a5orjT473A5BVgAArlbRMvhM2L9HWXe+CwHlr536Y+dnij5Rby9mXm7LNUd5o+tjxnhOpDjvZhbtQnA4UfyesU/TTKffFYj0F9ASMOjHub1XvD2bfbc3jOOaq7EeM2wH7NfCAADRXxBfqM4z9c6m96uaDru8JjGPMRun5XZX3v3o1LdU4ZifrJ8eEPPUh0cbMNf2oZO0mZfDNqP652Be+qqHLfMDTHz78E/K7c/vhl1OYq/k9z6yrxpWIh6fW1ABWjuzL25t/0zn/mt0eT3f9Me3ud9V3vulTd9NUbMdh8vqQ0nZhLahPRxO/k86z/Sy098VyPRx0MNHRj4T9SZj2bmm2c9pzVfZ+pDYgfqB8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK9XIU/28tr/AEd5H+qLw8z3SXpn84LjL5RZ37vU3PlasMvRH+bDDvX/AGBwdsyv20xXh7/UYyb659171MLHeH4NGMOYne3ytzXvfutoW85xzEfWR48t7R2HtORh+I8fT4G/+0P9Yw6lvCjxj8Bvx1+SWr7qnDvn7X6qMTr/ALA6ltmWr7bZ7hqf/eS1x/lf7w8oH616DFMx0p+Fvxl70Z98mfqxylcdjx4Tsj0OtuAAAAK4e0j/AGzdi/G/w7y73wWA8uPO9TPzscUfKLeXsy83VZq/eaPrY8Z4Tr/sDjvZlZtpsuC31A6/6KMl/NLI78PB6Rh0cct/W+e9HS2/vGcczPYhuKP3E+EAAGsfI/jhR7wo/TYXo1RsCojLTQ3qkGlmc0nucKjvDbSpx6tecUfm3CJTkRxRrQSkqcbc/JnSl6LXD3SB4e9PZHvWS5yslamMpm5jCm7TGNXpTN7MTNViqqZ2K8Kq8vXVNdEVU1XbV37Mpm6stVhOm1OuPqx/lpQf5Jjl7iF5Z41ktbJqLyokriWFfLQSXWHU9FEZGk1NvMPNqJbTqDU262pK0KUlRGfQBxTwpxDwTxBm+FeKspdyW/8AJXZt3rNyMKqao0xMTGMVUVRMVUV0zVRcoqproqqoqiZ3DTdprpiqmYmmXxOv+wNv7Ms7bNOj945VpDKkXdKs51LOUyzkuNPPKbg3UFtR9DI+iyiWcQlqONJJJqbUZpUS21LQrnno/wDP9xj0f+MKd/7gmcxuLMTTTnsjVVMWs1aifF73ft4zNi/ETNEzNNUV2q7luv58zZozNGzVhtRqnqJ1td7ExXaOK1+X4hYJnVk5PY60vtROrJyEoOTV2kYlrOJYRDWXcnqaVJNK0KW2pC1ehnmx5zuEOd3hDLca8FZmMxunMRhVTOEXbF2IibmXzFETPe71vGNqnGYqiabluqu3XRXVt27brtVzRXrh7gcgKwBXD2kf7Zuxfjf4d5d74LAeXHnepn52OKPlFvL2Zebqs1fvNH1seM8J1/2Bx3sys204vCX+Txin6aZV74rEegfoDRh0Zdzeq94ezbzb28JxzVU9aPGbZD9lviAABol4hH1GMZ+ufS+9TNB14eEujHmJ3T8rcr7370ajuycMxP1k+PCHXr/sDo82Za7tsn6SP9ufUXxvqn4D766kct8wVM/PtwV8rdz++GXVZir+T1/WVeNKxQPTu2sAKzd2f/HVv8b/AJUn/mt0eUTf9M+3ud9V3vulTdtNXkY7D5nX/YGkbMs7ae3iZ/J41n+ldp747kejToZaOjJwn6kzHs3NNt53Tmq+zHjQ2KH6efKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACvPyFUXw5bY8v8Aj5kfyf0ReHmj6S0T+cDxl8os793qbiy38BR9bDDncn5P9QxwfhK9Mb4epkel8n6f6z7r3qYUO7/waejmK3t8rc1737raLvH+Hj6yPHlvcOw18CMHxIDIvga6/wDzE/WMOpnwokY/Ab8c/klqm7P2fifVRhdyfk/1DHUzhLVWz/DNRHyT1wRH7sPk+4LKR+s+g1E/nS8L/jL3oz75c7/NqvE8eE7g9DLQAAAAEBGyNObdnbEz2bC1VsmZCmZplMqJLi4NlEiLKiyLyc7HkxpDNWtp+O+0slIWkzSpJkZGZGPOPzo8x3PVvDnM4iz+Q4P4ov5G/v3P3Ldy3urPV27luvN3aqK6K6bE010V0zFVNVMzFUTExMxLcFq/Zi3TE1047McsdR4v4Etz/wCqLaH0gZZ8yBsT5gufX4lcW/8AxG8PwdZ3+x5ujtwl94ZY7kGL6Sg1WTUVzjlojI8gfXW31XOp7BLL0hpTLyodgxHkpadIuqVGnoovjDuv6C/DHEvCPMHl9zcV7vz27N7070zlc2M3Yu5a9FNVdM01TavUUVxTVHlZ2cJ5GjZ2qmu/jTMTGEam1w/Yj5AAAAGsXJDjfR7xo/TYXo1RsGojLTRXqkdrU5pPc4VHeG2lTj1a84o/NudFORHFGtBKSpxtz8ndKPoucP8ASA4f9PZHvWS5yclamMpm5jCm7TGNXpTN7MTNViqZnYrwqry9dU10RVTVdtXfqy2ZqsVYTptzrj6sIepuiN1wJkqE9qXY7jsOQ9GdchYXkNhDccYcU0tcWfAr5EKbHUpPVDrLi2nE9FJUZGRjpHz3R25+t3529kL/AAZxPVes3aqKqrW7M5etzNEzTM271qzXau0TMY03LddVFcYVU1TTMS1mMxYmMduntw/N8CW5/wDVFtD6QMs+ZA+X5gufX4lcW/8AxG8Pwdnv9jzdHbhmrR6+SOj8qRd0mpdpTqWcplnJcaewXLm4N3BbUfQyP2nWUSziEtSo0lKTU2ozSolNqWhXPHMBV0peYHi+nf24eDOLsxuLMTTTnsjVureMWs1aifU097v28ZmxfiJmiZmmqK7Vdy3XRmPS1+jZqroiqNU4xoTS41etZNRVl61W3dOmyjIfOryOonUV3XuGZpdiWNXYNMyY0hh1JpM+im1kXe2paFJUfe5wtxDa4q4fynEFnK5/JU5q1Fc5fO5e7lM1Zq1VW71i9TTXRXRVE0zomirDbt1126qa6tEqp2app0Th1NMPuDX0UBGyNObdnbEz2bC1VsmZCmZplMqJLi4NlEiLKiyLyc7HkxpDNWtp+O+0slIWkzSpJkZGZGPOPzo8x3PVvDnM4iz+Q4P4ov5G/v3P3Ldy3urPV27luvN3aqK6K6bE010V0zFVNVMzFUTExMxLcFq/Zi3TE1047McsdR4v4Etz/wCqLaH0gZZ8yBsT5gufX4lcW/8AxG8PwdZ3+x5ujtwmP4f0F7jOiMZqMkpbbH7ZixyVb9XeVsypsWUP3095hb0GezHktpeZWS0GpJEpJkZdSMd4XQn4c4h4U6PO6dycU5DO7t3zbzWemvL5qxcy96mK83eqomq1eporpiqmYqpmaYiqmYmMYloucqpqzE1UzExhGrsNnB+sXygAA0R8QoyLS+Mdf9Z9L71M1HXl4SzTzFbp+VuV9796Pv3d/Dz9ZPjwhy7k/J/qGOkDCWtMn6RUXw0ai8v+k/Afk+6upHLPMHE/PrwV8rd0e+GXVX/4Cv6yfGWLB6c22wBWXu1J9urfy/8AOlh+Af8A7W6PKTv+J9vc76rvfdKm6KfKx2HzO5Pyf6hjScJZT4cSz68d9ZGX6F2nvjuR6L+hp+jNwp6kzHs3Mtv5z+c1dn6kNix+nXzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAArw8hvq57Z+jzJP1SfHmp6SsR+cBxj8oc792qa/l5nvFH1sMNjhDZhdjKZHw8vqLZP9dC696mFDu78GtGHMXvbD42Zr3v3W0jPzjej636st8R2FPhRfeJH/AKGf+0T9Yo6nfCgxE/AfH/3n8lNS3dMxt+J9VF8OpzZhqeMtoeGP8pTW/wDlh7wspH6x6DkRHSk4X/GXvRn3zZyZ9LVeJ48J4R6FGhgAAAAAAAAAAAAAAAAAAAAAAAAAAAADQ7xDfqLYx9dCl96majr18JTGPMXunH42ZX3v3o+7IThen636sIbh0ibMNXxllHSH1adQ/XQwD311I5Z5hIj59OC/lZuj3wy6q9M95r+tnxljEemxt8AVkbv/AJat/wBNLD81ujyob/pj29zvqu990qbkpmdmOw+WNJ2YZxlPrxJ/k66x/Su198lyPRV0NtHRn4U9SZj2bmWhZr+cVdn6jY0fpt84AAAAAAAAAAAAAAACDXxA/Hb4xcNLC81pryP9kTvmpW7BscVxa4Zr8EwqyT51pyPm+etxrNj21r30fl1VWsTJaVpU1JXCUZKEopxSimZ7CqzyL8dLxGeQUye1D3M9ovFJS1+iYnoSIvAVwWzT5tBpzpMiw2Y9JNvoa1e3KGfOdVNtN+RJS2YTimIRh5luDbexXpEjYO0ti53Ill2yn8yzfJsnekpJTayTIdu7Oct4iW0k+ijPypI/wCGWXwMdzXMsQcS9ieW5NjDyXlSEu47fWtK4mQprzCn0uVsuMonlM/KGrr3GnydegMpB9FeMB4i/H+dDexjk9sDMqeMpKXcV3DYK25QS4iSIkwCPOjuLunip7S7faybBdQRdErJJqI8YQjsxKyHwn9ZT1Bs2bT4JzJwiPovJphtQ0bWwxVtf6mnTnnm2mVXdE+ixy/AY7inST503rqG32qdfkRmuvbGaeojNHUWacfyGgy2jqcnxW8p8mxu+gRrWjyHH7OFc0dzWTGkvQ7Kptq5+TAsYEtlZLbeZcW24kyNJmQig+wAAADD3IDRuA8ltLbJ0Rs+uOywbZ+LzsZu2mvNFMhG/2SKu8qnH2nmY15jdzGj2EB5SFkxNitOdp9vQNRE4aXLr5RcdM+4m782bx82XG81lWtskk065zbK2YORUryG5+NZbUocW4v2nyvHpcawjEozcQ1IJDhJcStJWxpXROMYsBgysg+rt8/C0NvmZxJ2NdFF1VyKtoysGkTnlJh4tvEmGK+mYaNSuxiLsuAw3VOdCUpdmxWkXYlTyhGqOVCqMYxXsBBWAAAAAAAAAPyzp0GrgzLOzmRa6trosidYWE6QzEgwYMRlciXMmS5C22IsWKw2pbji1JQhCTMzIiMwHNV8WrnhN57cs8nzeknzFaY176Xr/AEhVvHIZZViVdNWc7MnID6WVRrXYFo2qwd720SGofokV3qcVJiyIwhbTGEIwhlJJp4TnBKw568tsTwC3hTPgcwTzGwd4W7CX2mkYXUzGijYkzNbUyTFxsC283WMklxMhmKuVMbSsoi0jEzhCNU4Q6WcGDBq4MOsrIcWura6LHg19fBjsxIMGDEZRHiQ4cSOhtiLFisNpQ22hKUIQkiIiIiIVqn6gAAAAETfP/wAYrirwI9PxC7sX9ub2aipei6XwOwie2FU680y/EVsLJ3G5tXgEWVHfQ6lt1qXZuMrS61CcaV3jMRMpRTM9hUy5K+sC+INviXOg4PmdLxwwp/uaYx/UNW01ka4/cpTTllsXIUW2WFZIJXRTtW5UMrIi/KCPqZzimE4piETmccgd9bNfelbJ3dt3YUqQl1MiTnGycyyx99MgnEvpeevrqe46l5LqiWSjPuJR9fjmMs4MfUWT5Li8opuM5DeY7NJyO8Uuitp9RKJ6I4b0V0pFfIjuk5GdPubV16oV5S6GDLeHSnimeILoGbDka/5WbbkV8M2kt41nuRvbRxL0VtaVLhNY3sdOUVVcw+hJoUqG3HeIjM0LSroosYQxhErDvDD1mmkuJdbhnOLWkfFnJDrERvc+n4VhNoGO7sb9KzHXE+bY3kFhskm4/Mp5c9S1rJLda2kjUIzT1EJo6i03rPaGutzYRQ7J1TmuN7BwPJ4aZ1FlWKWsW4p57J/KuITJiuLJiZEdI2pEd0kSIz6VNOoQ4lSSig94AAAAAAADke7B/wAPc3+i/Jf1Zmi1e8gAAAAAAAAAAAAA6yPHr6gWj/rP6095dKKlE62YAAAAVBvWsf8AwG/+aL/07CdKdHKqDiSwAAAAAAAAAb2+GB/SJcLP5x+q/fVACdTE6nUEFSkAAAAAAABrvy420Wh+LfIjcqJKYkzWmmNj5fTuGakm7kNPilpJxuG0pJpNL9hfpjMNn1SROOF1NJdTJGtmNMtE/BE5Wlyo4A6teubP0/YWkUHorPifdQuc89g8GAjDLl/uV6VK9u9fTKtb0twv+EWLcsu5a21mM1RhLNUYSl0GEQAAAAAAAABpr4gnKGDw54f7w346/GRfYriEquwCJJ82tNlsnJ1Ix3A4ZxlqSuXGayOyYky0I6rTBjvr6dEGZZiMZZiMZwYY8HjdMzfHhwcX8yt7F+zyGlwubrbIZEx9cmwVYatyK4wGK9YPuOOrel2NJQRJhrUo1LTJSpXRRmkk6yrRKTEYYAAAAAAAAAAAAR386vE+4q8AKVCdvZW/f7LtK/2wxfS+Cph3OxLqO4tTMaynw3pcSuxLHXX0q6T7R+M2+ll4oiZTzSmRmImWYpmVRzlD6xdzk3PYWFdpNzGeMOCOrksxIeJV1dmewZUB75VCLrPsuqpLDMxsiI0P0tXSuoM+ncr44nFMJxTCHfYHKPkttea5Y7O5B7r2BLceN/zmYbQzXIUtL6qNCYrNpdSWIbLJLNLbbSUNtI+VQlKSIhlLCGGq64t6iUqdU2tlVzVoW2uZXTpMKUtt1SVuNqkRnWnlIcWkjURn0MyIzBluNqLxH+eGi5LD2s+WG7aiNGcN5mius1ss4xJLxmRrdPDM7XkuJLcd7SJalQjNZERK6kREGEMYRKfDiB6zhmlVMrMU5r6qrsro1qbiuba03DbpcqhEpfQpuR68s7Asdvy7nOrrtXLqDZZb/K4chw+iozT1EZo6i2HobkPpXk9rys2robYuO7Kwa1NTTdvQyXPP185tCHH6i/p5rUS7xq8jIdSp2DYR40ttK0qU2SVJM4alcxhrZnAAFdzkQr9vXbX0e5J+qT481vSUpx5/+MflDnfu1TW7OHeafrYYa7z+R8X4o4R2VuhMr4eB9dK5P9dG696eEju48GxGHMZvX5WZr3v3Y0vO/wALGHmfqy3zHYQ+NF34kx9PgY/7Rf1iDqf8J/GPwH/HP5KahkMPJ49b6qLvvP5Hxfijqf2WoaG0XC9XXktrYvox94OUj9YdB6nDpR8Lz/WXvTn3z5rDvFXiePCeYehBo4AAAAAAAAAAAAAAAAAAAAAAAAAAAAA0M8Q8+mlcY+ujS+9PNh17+EnjHmM3V8rMr737zfZkv4WcfM/VhDV3n8j4vxR0j7LVNDKOj1ft1ag+ujr/AN9lSOWOYWn/AJ58F/KzdHvhl1d7DvVX1s+Msbj00NDAFYu8Wft1ceT/AJ0sPzW97I8qu/qf6dzvqu990qa/GGD5fefyPi/FGk7LOhP1xHPrx01if967X3yXQ9E3Q3jDo0cKepL/ALNzLRsz/D1f5cjY4fppQAAAAAAAAAAAAAACot453jKZBjN9lXCXibljlNYVZO0m/twY1YOM3EGyWS27HU2E2sTsVVyq9syRf2MZ05Db6lVzamVszCXOmOWU6aeWVPhSlLUpSlGpSjNSlKMzUpRn1NSjPqZmZn5TElj/AAAAAAAAS6+GB4t25fD4zGBjFpItdkcY761SrNtTy5i35GNpmySXYZjqp2XKai4/lTKlqdfiKUiuuE9zcgm3zZmxsTGPZRmnHsuhtqLbWvd7azwvb+qcmr8w17sCji5Bi+Q1jnfHmwZBrbdZfaV0fg2lZNZdizYjyUSYUxh1h5CHW1oTWq1MjAAAArQesX8Avhm0tX8ydb0npGytBVSq/Z8Wvid83J9JrkvS3rp42Uqdfk6vtJTk1Rmkkop5k51xZJitpOVM8idM8ijaJrH6oM6bVzYdnWTJVdZV0qPOr7CDIeiTYM2I8iREmQ5cdbb8WVFfbSttxCkrQtJGRkZEYDpT+Elzvg88+JWLZpdz4qtza6KHr3d1Y2tlEheWVsFv0DM0xGybNms2DVtpsGzS2lhuZ6XGbNXoqjFcxhKmqMJ6yUAYYAAAAAAAAVuPWJufvwE6Jh8RNc3fo21ORFO+/nsivl+bsMU0emU9As47qWlE6y9s+xiP1SOvVDlXGskKIjW2oSpjlTpjHSoqiax/WPHkS5DESIw9KlSnmo8aNHaW9IkSHlpbZYYZbSpx151xRJSlJGpSjIiLqA6S3g/cCY/A3iZQY7k1ZGj7x2r7X7C3bNT2OyYN7KhH7QYCcpBrS5D15USjiKS2tyOqzdnPtKNEgjOuZxlVVOM9ZKsMIgAAAK4vjf8Ai/zeJNc9xb43XcVPIzLKNEvOs3iOtyXtIYtcMIXWtV7fm3Yydk5RAdN+L50++pgKblm2bkmG4mVMY6eRKmnHTOpRWtbWzvbOxu7uxn3FzcT5lrb29rMkWFnaWdhIclz7Gxny3HpU6fOlPLdeedWpx1xRqUZmZmJrX4QAAAAABIF4f/iPb/8AD12S1lOsbVeQ66u58deytM30+YnCs5gpJuO/LbaaU587uZRIaOkC5jtqeYWhCH25UTzsV3ExixMRLowcT+VGpOZej8S3zpi69s8WyZlUexqpa4ichwzJ4bUdd3heXQIkmUirySkXJb8433qQ8w61IZU5HfZdXXMYKpjDRLY8GAAAAAByPdg/4e5v9F+S/qzNFq95AAAAAAAAAAAAAB1kePX1AtH/AFn9ae8ulFSidbMAAAAKg3rWP/gN/wDNF/6dhOlOjlVBxJYAAAAAAAAAN7fDA/pEuFn84/VfvqgBOpidTqCCpSAAAAAAAAg29Ye24nWnhrZxjLTymbDduxta6shLaWSXkMs3LuzLcyT/AGRx5FNrl+K6fTp2ye0/KohKnWlTrV6vVyOVpaT5m2Wichs/RMJ5QY2WORG33UNw4+0sLbsb/BJLjjqiJpdrWP29S2hBd0iZYRUn17U9M1RoSrjRivvCCsAAAAAAAAAU1vWe+VntxmOleG2OT++Dh8M927OaZc72l5NesWGOa5qHyQtJszKXHFWs51taVEtm5irSZGk+s6Y5VlEcra31Xjbfzw8Z+Q2lZD/npesNxU+cRErV8vHo9r4ozXR4jRd/T0Zu61nPf/sepOSVdVGRpJOKmK9az4IoAAAAAAAAAAAhX8YXxXaTw+deQsD1ymsyTlDs6mmScJqJvmJlVrnHDcdr1bKy2vNSjll6a24zTQHEk1YTI7q3TNiM627mIx7CVNOPYc+TPc+zXaWZZHsPY2U3mbZxl1m/c5LlWSWMm1u7qykdCclTp0pbjzqkoQlCE9SQ00hKEElCUpKxY8iDIAAAAA2u4fc0t+8HtrV21tE5fJp5RPRGcsxCet6ZhGw6Fh41v45mdB51uPYw3mnHEsyEG3OgLcN6I+w8ROEmMWJiJ1ujJwH556d8QPSMHbOr311N7VriU+ztbWclt/Itb5c7GN9dVOcQ2wm0pLFLa3qu0abQzPjJPqlmQ1JjMVzGCqYwbvjDCuxyIP8Ab1219H2Sfqk+PNl0kv8AH7jD5Q537tU1izM96p7EMNdRwngs2pTL+Hd9RXKPrpXfvTwkd2fg2/8AA3evyszPvfuxpucnG7H1v1Zb6jsGfKi58Sf/AEL/APaL+sQdUXhPP+CPxz+Sn3ZKcNrxPqoueo6o8H3bUto+Fx/95fW3+WPvByofq7oQfpQ8MfjH3pz6jMzjZq8Tx4T0j0FtKAAAAVwtpXVw3s3YqEWtkhCM7y5CEJnSkpSlOQWBJSlJOkSUpIuhEXxh5m+djfe+bfOnxLbt5vNU0U8QbxiIi7XEREZu9hERFWiI5IazRPkI1aoeE9vbr9F7T/pCX/dhx/7e78+/c3/51zuksetCbLgrJkS9BV70p96S6eUZKk3X3VvOGlMlntI1uKUroX4HlHej0Bczmc30estezVyu7e9tc7G1XVNU4RXThGMzMtMzX8L4kNxh+03zgAAAAAAANbORvH6JurHvP1Fg5j2e00dz2gumpEiPFmtkanfaS9THPuerpDhmbbpJU7EcUa0EpKnG3PzD0l+jrk+fHhv0xufM1bt5wslbn0pmaa66KLsaavSuaijTVZrqmdi5hNeXrma6Iqpqu2rl9i9NqdOmiUG2TRs4w69s8ZyZy9p7ynlLiWFfLmykvMPJIlJMlJfU28w82pK2nUKU262pK0KUlRGfQxxRlOO+C9/5rhbimreGS39krs271m5duRVTVGmJiYqmKqaomKqK6ZqouUVU10VVU1RM6nFUVRjGExL4ft7dfovaf9IS/wC7DQPb3fn37m//ADrndM49aD29uv0XtP8ApCX/AHYPb3fn37m//Oud0Y9aD29uv0XtP+kJf92D29359+5v/wA653Rj1oTjcJZEiVx4xR6S+9IeVaZUSnX3FuuGScisSSRrcUpRkki6F5fIO+boLZjMZro3bovZq5XcvTms/jVVVNVU4Zy9EYzMzOiGmZr+GnsQ2yH6+fOAADQrxEfqK4v9dKk96ebDr58JJ/gbur5WZb3v3m+rJzhdn636sIaOo6TMGpbUso6OP9urT/10tf8AvsqRyvzDf45cGfKzdHvhl1d2Z71V9bPjLHY9MTRwBWHvD/46t/00sPzW6PK3v6P6czvqu990qa3FU4Pl9RpWDO1Kf3iN/Jz1h+ldr75LoeiDoc/o1cK+pMx7NzLScxpvVf5cjY8fplSAAAAAAAAAAAAAIxPF15ryODPDDOti4tPjRNtZzJj6t06TpIddh5llESa5JyluMrvJz5yMagzrRo3G3Iy50eMy8Rpf7VZiMZZpjGXNQlSpU6VJmzZL8ybMfelS5cp5yRKlSpDinpEmTIeUt19991ZqWtRmpSjMzMzMWLn8AE4fhn+CHu7nZX122dgW8zRnHGQ8o63MJlN7YZlslEZ9TUtvXePzHocdNOTjS2V3kxRw23upR2Zqm3m28TVgjNUR2VrzT3gZeGfp+thxvsfIu0bmMlopOU7hyS/zeytFMvNvtuTKM5tbgbCu9siWUSnjIcQZoWlSTMjhtShtS97nPg2+GXsGJMiXHEXXFR6Z5tRysGkZTrqXGdZaQ0y7DewXIMe9H7PNkpTZEbLyupuoX3K7mMsbUoOObPqzbMKnuc64L7CtbKyhsyp56M2zY1y3bUkIW/7XYNsppiqjRZh9hMxYd6wbTql9z1q0SflpRV1Uor6qphmeF5drnLMgwTPcau8OzPE7WXR5Li+R10qovKO3gumzLr7OtmtsyYsllZeVKkl1LoZdSMjEljzICyh6u/4hc7SW7y4b7KvFfBFvq2W7rl+xkEUXB90rjJbhQ4rrrqCjVOz4sVFc4yknOty3ANtLZPS3FxqjlQqjGMV6EQVgAA/Ba1dbeVdlSXMCHa09xAmVdtV2MdqZX2VbYR3Ik6BOiPocYlQ5kV5bbra0mhaFGkyMjAc0TxV+C9jwK5a5jrauiSz1LmBv5/pG3fU/IJ/AbidIJvHJM581qk3ODWKHauSa1m882yzKUSUyUELInGFsTjCNgZSSmeENzyl8DeW2NZVkFjJZ0ls70PXm7q9Hc5HYxufM/wCJc4THLuI5+vbp5M7vQhb661U2M0XdJMYmMYRqjGHSeiyos6LGmwpLEyFMYZlRJcV5uRFlRZDaXo8mNIZUtp9h9pZKQtJmlSTIyMyMVqn9wAAAAABinee58D47af2LvDZtn7UYLrHFbPK8hlIJtUp6PXtf8Gq6th11lEy7vJ7jUKBH70qkzJDTST7lkBrcu3ljyVz3l7yE2fyF2M+ft/sTIpFhFqUPqfg4rjUVKIGK4dUqUhszq8Xx+NHhtLNJOP8Amjed7nnHFqtjQuiMIwa7AysV+rzcAlchuQr3KfYlIuRqHjbcQ5OKtzojbtZmG8PMNWGPQ0G8k0vM64iPs3j/AGdFtT11nXqhxxIjVOjBCqcIwXxhBWAAAA1V5t8n6Dhvxa3FyKvWoc13X+KvvYxSTXVts5LnVw+xR4PjjnmHG5ZxrbKLGK3JUz1cYh+de+M2ZlmIxlmIxnBy6tibBzDbGeZhs3YN5MyXN89yS4yzKr6eslSrS9vZz1hYy1kkkttIXIfPsbQSW2myShCUoSRFYueNASreG94SnIDxEbmReUbzWrdEUNgdflG6MkqZVhBfsGTaVKx3AqFEiuczTJYzTyVvoKTFgw0GXpEltxbLT2JnBGaojsre2ivAI8N7TlPDZyPVNvvTKGmEom5btzLLyecl5yOTcv0bEsZmY1hMWIt41LZJyvkSWCMi9IWZd5w2pQmqWwmXeEJ4aObRnYlzw71HCaejeirXiMK61/JJrq8fc1NwO5xuYxJ/L1flyHEveRPy3yiO1jLG1KHzmB6svqbIqe2yjhdsa811l7Db8uJq/adk9lWAXCkkpTVVUZiiKeY4k4ovIl+f7fIcWSUq8ylSnU5irqpRX1VQfdmj9scc9k5HqHdmDXevNiYpJKPcY5eMtk6lDqfORLGunRXZNZdUlkx0diT4Tz8OWyonGXVoMjE09bFQMplvBR8Qmx4R8o6bGcyvlxuO29bKqwzaUOc+r2pxS4kveh4htFlKlpbgvYzYyiZs3S6pcpZEg1IcdZjG3iYxjro1RjHXdFpKkrSlaFJUhSSUlSTJSVJUXVKkqLqRpMj8hitU/wBAAAAARHWHgT+FVaz51pP4s+fnWUyTPmv/AA38jWvPS5jy5El3zTO3m2W/OPOGfahKUl16ERF5BnalLaqfk+4MeFB9qp+/lyS/jhDak2qj7gx4UH2qn7+XJL+OENqTaqPuDHhQfaqfv5ckv44Q2pNqpWg8fvg3xa4TZfxmq+Mer/gzg7CxvZ8/L2Pn22JmftvLx2zwqPTu+d2Dl2VvQPQ2beQXbFUwlzznVZKNKTTOmZnWlTMzrV6RlNLJ4LXGfSPLTnDQ6f5BYV8/+upuuNhX0nHfnky3FfOW1HXxH6uX7b4TfY3eo9FddUfm0yiaX16LSohiZwhGqcI0LiP3BjwoPtVP38uSX8cIhtShtVH3BjwoPtVP38uSX8cIbUm1UfcGPCg+1U/fy5JfxwhtSbVR9wY8KD7VT9/Lkl/HCG1JtVJZMeoKnFaCjxegiegUWN09ZQUsHz8mV6FU08JiuronpM16RMkejw46EecdccdX06qUpRmZ4RfYAAABUG9ax/8AAb/5ov8A07CdKdHKqDiSxKB4O3HbTnKjnprDS++cP+fvWmRY5syfcY188GU4x6ZLx/AL+7qHfbjDbvHr+P6JZwmne1qUhLnb2rJSDNJ4nRCNU4Qua/cGPCg+1U/fy5JfxwiG1KG1UfcGPCg+1U/fy5JfxwhtSbVR9wY8KD7VT9/Lkl/HCG1JtVH3BjwoPtVP38uSX8cIbUm1UfcGPCg+1U/fy5JfxwhtSbVR9wY8KD7VT9/Lkl/HCG1JtVPd6v8ABk8NbTOxMK2xrXjf87ef68ySqy7Dr/4Yd93HtPkNJKbm1lh7VX+0rSlsPRpTSVealRn2F9Oi0KLyBjLG1KUEYYAAAAAAAAU9/Wm9uG5acTNDwpfaUWBsPbmSQe5Z+cOwkUuG4RL7CMm0+ZKsyBHUyUZ+c8naRK750p0Kn+DZpkut81xDYeGWj1Jl+CZPRZji1zHJCn6rIsatItzTWDSXErbWuHYw23CSojSrt6GRl1ISWOqXxY37jfKTjrpzkFihsoqdq4JSZQ5BYdJ8qS8eY9EyrGXnUrcSuZiuURJla/0UoifirIjP44qnRKmYwnBn0GAAAAAAAebzLLcfwDEMqzvLbFmnxXCsbvMtya2kH0j1eP43WSrm5sXzL4zMKuhOOq//AGpMByt+WPIPI+VfJHcvIbKfPN2e085tchjQX1pcXSY4g26zD8aS4lThLZxfEq+DXNn3KM24qTMzPynbGhdEYRgmw9WZ22rDub+wNWS5ps1e5NIXzcOD3GSZuX6/vKXJqhw0+UlKiYo7fGX4Jd5+X45HGrUjXqXvBBWAAAAAAAAAMbbj2riejNUbH3JnctULDtYYXkec5E832nIVWY3VybSRGhNrUkn7GaUcmYzRH3OvuIQXlUQGty0+UPIvYHLLfWy+QOzJi38o2Pkku3OAUl6VBxukQZRccxCmW+SXE0mKUTEeBFIyJSmmCUvq4pSjtjQuiMIwYDBlv1wE8OXkH4hmxZOJakrotHhWNORHNj7bydElrC8FhSyccjx3DjpOXkGUWTbKyg1UQjfeUXnHlxoqXpTWJmIYmYhcn46er1+Hvpingq2NiWScj81bQy5OyXZeQ29VRFMShBSCpsDwyxo6KPVOuINSGLNdw+33GRyFl0EZqlXNUtu7nwkfDYvq6NVzuG+l2I0WMcRp2mopWOWKmjQwjuk3GPWFXbzJPSOn8uefW8Rmo+7qtZqxjLG1KLXlP6tDxgz6otrjirmuW6FzUmXXafFcqt7LYuqpT6Oi2a+S9dKm7GpUSFEaFTfbS08ykyUUR00mlWYqnlSiueVT+5TcSt8cNNpWeot+4TMxLJInnJNRZNmqdiuZUpOm3HyTDMhbbRDvqSV5Plkdr8ZwzZktMSEOMonE4pxMTqa3AykA8NTnNlvAXlDh+3K6RYTNd3D8bEt04hFcUtnKdc2Uxn2zdahmomnsixZfSyql9UK9Kj+ZNaWJD6V4mMYYmMYdNbGskoMyxzH8vxW3gZBi+VUlVkmN31VIbl1d3QXkBizp7etltGpqVAsq6U28y4kzSttZGXkMVqVe3kWf7e+2/L/j9kn6pPjzcdJGmPn84w+UOd+7VNVtVYWqewwx19kcKbMLNpM74df1E8o+uld+9LCB3X+DejDmO3r8q8z737safmpxuR9b9WW+47A3zItfEr/0Lf8AaN+sMdU3hOYx+BH44/JT7MpOG14n1UWvX2R1TbMPt2m0vCw/+8xrXy+7L3gZUP1Z0IqcOk/wxP8AWPvTn1OYqxszHY8eE949AzTAAAAFanap/tobI8v+P2Ye+GxHmN52qY+dXib5Qbx9mXmrUVeQjsQ8D19kcf7MJ7ScXgX/ACfa76Ksm/NLA72ugBGHR3y0f+7Z30dLTszON3xG54/bD5wAAAAAAAABq5yW400O9aH02F6NTbDp4q00F+pBoZnMpNTpUV6bSVOPVjzijNp0iU5EcUa0EpKnG3Pyp0nejDw/z97g9PZLvWS5xslamMpm5jCm7TGNXpXNbMTNViqZnYrwqry9czXRFVNV21cvs3qrU4fsOogrybGr7Db6zxjJ6yVT3tPKXEsa6WjteYeSRKSZKSam3mHm1JW06hSm3W1JWhSkqIz6FuJ+Fd/cGb/zXC/FGVu5Lf2SuzbvWbkYVU1RpiYmMYqpqiYqorpmaLlE010VVU1RM6jFcVRjE6HwuvsjQdmGdo6+yGzBtJ2uDf8AJyxL9Ncs98lkO/DoIxh0a9zx/tef9m3mm5mcbs+I26H7CUAAA0I8RT6ieL/XSpPelm46/PCQxjzHbq+VeW9795vpys4XJ+t+rCGLr7I6UNmGobTKejT/AG7NPeX/AEpa+99tQOVuYen/AJ48GfKvdHvhl0LlX73V9bPjLII9LjSQBWAvT/47uPL/AM62H5reHli37THt5nPVd77pU1iKtEPldfZGlbMM7SwJxE/k5av/AEqtffJdD0N9DuMOjXwrH+yZj2bmWl39N2psgP0uqAAAAAAAAAAAAAFIj1oDd83JOS+itAw55uUOrNTyc9s4bEhZNJzHZ+QzYLjVhGSvzbkqvxfB692OpaTU23YudpkTi+s6dSyiNGKsKJJpIfCk4VNc7eZOBajyBuUWsMdizdl7hfhuPsSFa9xSRAbkUseVHNt2I/l9/ZQKcnkOIdjNzlvtmamSSeJnCEZnCHS6oKCjxWjp8YxinrMexzHqyDSUNDSQY1XT0tPVxmoVbVVVbCaZhwK+BDZQ0yy0hLbbaSSkiIiIVqn1gAAAQEeN94V9Fy+1LechdO4syzyl1ZQqsVt0sMinbowekjqcnYZZsMKQdjltNWoW9RSOx2U8poq4+5t5g40qZw7CVM4aORQVrKm1urWvoqessLa8trCJU1VNWQpM+1s7WfJbhwayvrorbsuZYTJbqWmmW0KcccUSUkajIhNauY+Ef4CsfW8vEeTfN6kYsc/hOVOT600FIcJ+swewjPtWVXkm0fMqNi5yuI6005HpCUuDAV19NKRIPzESE1ckK6quSFrARQAAAARNeMfwJa528Sb2qxWqRL3rp72y2JpR9tDXptrZsQ2/np12l1wiP0bYdLCTHaR5xpsreNXvOr82ypKsxOEpUzhPWc3d5l6O87HkNOMPsOLZfYeQpp5l5pRocadbWSVtuNrSZKSZEZGXQxYtfzAXvfV4efhcguP8vinsS6KRtvjjUxE4g7MeUuflujjeYrqR9ClqUqRK1zYyGqeR0JKUQH6z+zWp1RQqjlV1RhpWNBFAAAAAAUufWRefyM2zak4J6yvEP4xrqZAzHfM2ufNbFrsF2KmTiWBOPsqJp+NhVXL9PsGurrZ2cxhtZNyK5aROmOVZRHKqqCSbJWnNSZzvnamv9Na0qV3md7KyqoxHGa8idJlVjbykRymWDzLMhUKnrGTXKmyTQbcWGy48vohCjIxqdQzh1xcwThrxy1jx51+0y7W4LQstXl8mIUSZmeZ2B+nZfmdmg3JDpTMivXnnkNLddKJG81FbV5lhtKa5nGVUzjOLZsYYAAAAVQfWkd4S6rXHGPjrWy1oZzLLMu23lUdp1TalRsGrYeLYizJShZekQ5szNLR3sWRo89BbX/ZISZSpTo6qmYJrGzXDbjTkfMDk5pzjljMhdfK2XlrFdbXKGkvKx3EauLKv82yRLK+jch6gxGpmy2mVKSl95pDXcRrIwmcIxYmcIxdQ7T2ote6F1hg+nNU43CxLXuvKCHjmL0MBPRuLBiEpbsmU+rq/YW1pNddlzpjylyZs192Q8tbri1nUp1skgAAAip8Vvw0sG8QnR09murqqm5Ga+qp0/TOfrS1Effktk5Md11k8/ok5OG5Q/wBUEbpq9q5riZbXQvSGpGYnBKmcJ6znA5Li+RYdkt7huVUtlQZXjF3ZY3kOPWkV2JbU99TzXq20qLCE6kno8+DPjrZcbUXclxJkLFq0B4TPgHW+0U4zyN5x0FnjmtnUx7nBNATimU+UZ6yoidhXeyuxcW0xXEVl0cYqkmzZWRGS31RopE3MjNXJCFVXJC6XDhw66HEr6+JGgQIEZiHBgw2GosOHDitJYjRIkZhKGY8aOyhKG20JJKEkREREQgrfpAAAAAAAAAAFNf1qT/D7hl9B+6v1a1wJ0rKNSpsJJp2/Vzf6SzF/rQbY/UmCMVakatToLCtUAAAAAAAAAKg3rWP/AIDf/NF/6dhOlOjlVBxJYmr9Xz/pRtK/QhuX+CvKhirUjV5V0RhWqAAAAAAAAAAAAAAAAAHO59YF238KPiY7UqWJXplXp3ENdakq3kvedaR7X483m19FaT8Zj0HL86smHEf/ANzaz+OYsp1LadSFQZSXNvViuWZX+Abg4aZNY99pgU9zcmrmZDyDdXh2SSoVPsCkhtGaDbh4/lrkGwSREtTj19IMzJKEkIVRyq645VrsRQAAAAAABX69Yt5YFozhZH0hj9iuJnXKS+cxEyYWhEiLrDEnKy82LLJXcayRZvSqunWg0GTsW0kdFEaPLKmNKVEacVBMTWt9PC620WkfEK4jbBckphQmdz4ziFxNWaibhY9s45GsckmPGgyX5iLQ5hJccIupmhJkRK+MeJ1MTph0/RWpAAAAAAAAAFez1k3eEvW/A2n1dVSHGZ+/9t4xi9qltfmu7DMKYmbAuFE4kjcUpWS0dIypsu0lsvOdyuhdi5U60qNahCJrWQdTayyzdOz9e6iwSF7Y5lszMscwbGoiic8yu4ye2i1EJyW4026uPAjvSickPdppZYQtavlUmDDqN8ROLetuGvH/AF/x/wBXQUNUeHVTRW904w0za5pl0tppeTZrfraL8uuMisUKdUXU0R2SbjtElhlpCapnFVM4zi2VBgAAGknPngvqfn5oS909saLHrcgjNSbbV2x48FiTkGtc1S0n0O4rlrJLsmlsVMojW9f3obsIJmklNPojyGMxODMThLmebu0zsHjxtrP9JbUpV4/sDWuSTsZyWtNSnWPSohpcjWFbKU20U+kuq95mbAlJSTcuFIaeR8oshYt1pw/Cg8DbPeXjlBvfktFvtZcZVeYtMfpCJ+oz/djBqS7G9oUPNJkYzr6W38s7dLSUic0aU1yFJdOdGjNWHZRqqw0RrXsMFwbENZYZi+vMAx6sxLCcLo63GsWxqmjlFrKSjqIrcOvr4bJGoybYjtERqUalrV1UtSlGZnBWr9cjFF8PG3Po+yT9Unx5vOkhTPz98YfKDO/dqmo25nvcdhhfuL4uv4w4U2ZTxlM/4dJ9dJ5R9dO796WEDut8HBGHMfvXH415n2Bux8WY8vHY/Xb8jsBUIs/EtPp8Cv8A2j/rDHVR4TeJn4EYf+8fkp9WW/ZeJ9VFn3F8XX8YdVGzL6sZbTcKlEfJnWhfRl7wMqH6s6EdMx0nuGJ/rH3pz6q9P71P+XKnyHoCfAAAAArS7WUXwo7J+j7MffFYjzI87NM/OpxN8oN4+zLzUqJnZjsPA9xfF1/GHH+zKWMpyOBJ9ePlcf8A1qyb80sDvY6AUYdHjLf1rnfR0vhv/wAJ4jdAftZSAAAAAAAAAADVvktxpod7UPpsL0Wn2JTxVpoL9SDQzOZT3OlQ3xtIU49WPOKM2nSJTkRxRrQSkqcbc/KvSc6Me4OfrcHp7I96yXONkrUxlM3MYU3aYxq9K5rZiZqs1TM7FeE15euZroiqmq7auW2rs25/zUFGTY3e4bfWmMZPVyqa9ppS4djXTEdjzDyOiiMlJ7m3mHm1JW06hSm3W1JWhSkqIz6GeJ+FuIODN/5rhfijK3clv7JXZt3rNyMKqao0xMTGMVU1RMVUV0zNFyiaa6KqqaomfuirGMY1PhdxfF1/GGg7Ms4ynd4NH1444if99cs98lkO+3oJRh0bNzx/tef9m3nw3/4SW3Y/YKkAAGg3iLH00ni/106T3pZuOv7wj8Y8x+6sPjXlvYG81+X8vPY/WQwdxfF1/GHSlsy+3GWU9GKL4bNO/XT1977agcrcxFM/Phwb8q90+z8uhXM7E9iVkUelhpwAq+Xqi9u7j9NbD5P/ALW97A8s2/aZ9u856qu/dKmpxM4PldxfF1/GGlbMs4ysD8Qz68cdXn/eq198l0PQx0PIw6NnC3qW/wCzcy0+9/CS2RH6WVgAAAAAAAAAAAADnO+PtbzLLxVeSMOUslMUFZpKoriLv6tw3tDa0vloV3LUnqdhdvq+VJJdFF5OvUzsp1LafKocBlJb29VawytUrmdsJ5DblwwnSeGVznmyJ6HWyj2Vd3SCeMzNTdnKhwDNJEXQ4hGfXqXbGpXWt9iCAAAAAAjv0d4XfETQXJrbXLHDdfsSdrbOyWwySoduURJtFqh6+gxk5YjWFQcVLGOS8pulTZciWRrksMznIUVUeEa2XM4zhgzMzMYJEBhgAAAAAAFCT1g7gF9jVyMZ5K68pPRNM8lbWwsLdqBE8zW4buwkPWOV0qvNJNiNGzqKhd7BJRpU7J9sm0IS1FSZzpnGFlM4xhyq9Ikm2V4gcns64c8i9YchtfuOOWuBX7Mm2o/SlxYeX4jOI4GW4dZrSlxPoORUUh6OS1IWcZ5TchBedZbMkxixMYxg6huldv4Lv/U+vt060tm7vBdlYvV5Xjk9Bo876HZMEt2DOabW4US2qZaXIk2Oo++NLYcaX0UgyFSnUyeAAADRHxIOaWO8D+KOwN4WJw5mYqZTh+pMblOtJPJ9oZFHlN47FNlxSTk1tI1Hft7FCTJZ1te+SPyw0EeYjGWYjGXMcy3K8kzvKckzfMbmfkeW5hfW+T5PkFo8cmyu8gvp79pcW099XQ3plhYSnHXFeTqtZixc8+AuVera8Afndxu757bMpVt3eWM2+C8fYc5vtXBxNt1yuzvYrTKiUaXsksGF01e4fm3EQ4k5XRbM1pYjVPIrrnkWxxBAAAAAAUWvWgrWa9zi0rRuO9a6u4pYtaxWfL+Vzbnbu5oc93r16flzFDGL43+0/EnTqWUalbMSTWO/VjcNqrznNtDK7Btp+Xg/G/KH6JDjPcqLa5Dnuu6V6zZf86XmnWaRyZFNPYrvRMUfVPb0VGrUhXqXthBWAAAAAI7ZvhccQbfmhac5r/XzN/tidAx9+LQ2qIEnXtZnlEl+N8KrOLnBJmZn0mAiGlMmQt1mNLhpntNJnrOSWcZwwZxnDBIkMMAAAAAAAAAAAAKa/rUn+H3DL6D91fq1rgTpWUalTYSTTt+rm/0lmL/Wg2x+pMEYq1I1anQWFaoAAAAAAAAAVBvWsf8AwG/+aL/07CdKdHKqDiSxNX6vn/SjaV+hDcv8FeVDFWpGryrojCtUAAAAAAAAAAAAAAAA/m88zGZdkSHWmI7DTjz77ziWmWWWkmt1111ZpQ202hJmpRmRERdTAcoHkxtZe9ORW9tzqdkPN7U29sTPohyScS6zW5Vllrc1cTzbvy7DUGultMttn08022SCIiLoLV0amEAZbseHXyjlcOOZOjt7nJeYxqgy2PR7EYaNSkzNa5cheN5whcdKVlLer6OycnRWzLp6bEZURpUklEmMYYmMYwdRCHMh2MOJYV8uNPgT4zEyDOhvtSocyHKaS/GlxJLClsyI0hlaVtuIUaVpMjIzIxUpfpAAAAAAHOS8crlaXKPn9spmjs/bDXuh0J0VhBsuoXBkPYdOnLzy5j+YUqLJ9tNgzbJDMtBr9Kro0Q+40JQSbKYwhbTGEIexlJ+qDOl1k2HZV8h2JPr5UedClMq7Ho0uI8h+NIaV/tXWXm0qSf4BkA6yOiNnQt16Q09uOtShuDtbV+BbGisoUSijtZpi1VkaYx9DPtXG9sfNqSflSpJkflIVKJ1srAAAAAAAAAKfnrVWQLVO4R4q2p9DbETkDkExHynoshcp7TtdWqIiM3DfhphyiPqRF2vl06n16TpWUKiIkmmJ8BPF6nJ/FI47e27CZTOPQtrZRDjuJSpldtU6lzX2qfcJRGfdXzpCJTRpMjJ9hB/GIyPFWpGryro0CtUAAAAAI898+GBxH5KcoNccrNvYJ89Gba9x46R7GZDkdODZ5Irp0ebh1vsGkKN5zJpOGGcluOy66UWWy82zMbkMR2mSzjMRgzEzEYJCGWWYzLUeO00xHYabZYYZbS0yyy0kkNNNNIJKG2m0JIkpIiIiLoQww/oArk8jT/b5275f8f8AJf1SfHnD6R3+PXF/ygzv3ap9tE+QjTyML9fZHCqePXTR+HN9RLKfrqXfvSwcd1Hg4/8ABDevyqzPsDdj5b841+I37H7+UosPEw/0Kf8AaP8ArDHVZ4TP/gn8cfkt9FicMfERYdfZHVY+jHrtp+FB/wDeb1p5fdl/B/lY/VPQm/Sd4Z/GPvTnld2f3uf8uVPuO/18YAAACtBtc/20tleX/H/MffFYjzMc7H+KfEvyg3j7LvPupnyMaeR4Dr7I2Alj105nAf8Ak9130VZP+aWB3pdAT9HnLf1rnfR0vkvaa26Q/aioAAAAAAAAAAABqzyY4z0O96H02D6LTbEpoq00F+pBoZnMo7nSob42kqcerHnFH5p3opyG4o1oJSVONuflfpNdGXcHPzuD07ku9ZPnFyVqYymbmMKbtMY1elc1sxM1WapmdivCa8vXM10RVTVct3LbdyaJw/YoJMmxm+w2+tMYyeslU17TSlw7GumI7HmHkESkmRpNTbzDzakradQpTbrakrQpSVEZ9D3E/DG/+DN/5rhjifK3cnv7JXZt3rNyMKqao0xMTGMVU1RMVUV0zNFyiaa6KqqaomfqirHTE6E4vBj+ThiP6a5b75bId5XQV/Ru3R6rz/sy8+W9prbeD9fqgAAaCeIz9RLFvrqUnvSzgfgHwjn+CG6vlVlvYG811icK/EQudfZHSu+rHrsqaLP9u3Tvl/0qa999tQOVeYn/ABv4N+VW6fZ+XRrnyE6eSVkoelF8IAq7Xp/8d3Hl/wCdbD81vDy3b9/33nPVV37pU++J0a3yuvsjSmceusF8Qf5N+rv0qtffLdD0I9D79G7hb1Lf9mZl8Vzy8tkx+lUAAAAAAAAAAAAABz2/WJtdTsL8THPMokx3Gou3dZ6mz2ufUl4mpLNRiUbVj5trcUptRtS9cLSokdCLydS6mZnZTqW06kGQyksMerncwMR488r8x0vsG2iY/i3KOgxvGqW7nvtRoEfaeE2FvKwGssJD6m2ozGRwcot6+Ovu6rs5ERkkn501JjVGhCqMYX2RBWAAAAAAAAAAAAAADVjmnxVwjmlxr2dx5zlDMePmdI4vGcgWx56Thud1XWfhuXwiQaH++lu2mlSGm1oOXCU/GUrzb6yPMThLMThOLl77a1bm2kdm53qHY9O7Q51rjKLjEcoq3O5SY9rSzHIj7kV5SEFLr5ZNk9FkJLzciO4h1BmlZGdi3Wx6DK2D6trz8PGsovOBey7o00WZP2uc8f5U95JM1eXMxnrHO9fsuuqM24+UV8ZVvAZLsaROiTSLufnII41RyoVxyrmAgrAABzufHI5+fZocrpuH4Nc+naF48PXGCa9VFPpBynKFyI7Ww9hEZKcKWzc29a3BrnSUbS6quYfbS2uS/wB1lMYQtpjCOuhUGUm7Xh6cNsp518qNd6GoymwsdnSlZLtDJ4bJulh2sKB6M7lN2pZpU01NlpfZra7zhebctJ8ZC+iVKMkzhDEzhGLp34RheLa3w3FNfYPSwscw3B8dpsTxWgrWiZgUuPY/Xx6qorIjRde1iFAittp69TMk9TMz8oqUvUAAAAAACkx60lryZXckOM22FMrKBmWkb3XjMjo75tUzWueWmSSmepqNklts7YZV0IiUZK8pmXb0nTqWUalXUSTSw+C1y9xrhtztwTM8+smaXWezKG50rsO+krbRExylzKdS2VLkU5x7o1GqqbN8cqnp75qT6PXFIc8vb2KxMYwjVGMOka242822604h1p1CXGnW1JW242tJKQ42tJmlaFpMjIyPoZCtU/7AAAAAAAAAAAAAAAAAAABTX9ak/wAPuGX0H7q/VrXAnSso1Kmwkmnb9XN/pLMX+tBtj9SYIxVqRq1OgsK1QAAAAAAAAAqDetY/+A3/AM0X/p2E6U6OVUHEliav1fP+lG0r9CG5f4K8qGKtSNXlXRGFaoAAAAAAAAAAAAAAABpV4jm3T0XwR5XbOZlFCsqTSebVePTFOJaKLluZVi8Jw98jURktTWU5FDMm/Ibhl2EZGojLMa2Y1uXGLFwAAOix4EfK4+TnAPX9Ne2SZ2wuPUg9HZcTr6VTZNPjMKLI1xdOMn/wj0eXgUuFCVIcNZyp1bKX3dxKSmuqMJVVRhKZkYRAAAAaK+JRynY4b8Ld47ujzm4eX1+LvYtrJCjaN+Ts3NFfO7hzkaO8ZJmFR2E721ktF1M4UB9XxkmMxGMsxGM4OX4889IedkSHXH333FvPvvLU68886o1uOuuLNS3HHFqM1KMzMzPqYsXP5gADoz+AztpW1vDK0WzLllMudWzs41LcLJw3DYTi2V2M/GIikqUtbJxMDvalHaZ+UiJSSSlSUlXVrVVa0xQwiAAAAAAAAqietP6/n2OruIG1Gm1+1eIZ9tXX895JI82U/Y2O4hkdS26fb5w1nH1ZNNHQyIiJfUjMy6SpToUyRNY3R8PDk6zw65m6G5B2JSl4zheXLhZyxDackyHcAzCpscMzZyPBbUn2xnV+N38mXEZPyKmR2jLooiUSYxhiYxjB1BcayTH8yx2iy7E7mtyLF8op63IMdv6aWzYVN3R3ENmwqrasnR1uR5kCwgyEOtOIUaVoWRkfQxUpfbAAAAAAAAAVxORyjLfW3vjfVAyX9Unx5yOkbRE8/PF0/wD2DOfdqn00+VjsMLd5+x8X4Y4W2I66WKabw5D66Ryn66t570cHHdJ4OaMOZHekR8asz7A3You+W8Rv8P34rRW+JkZl8Cf/AGkfrCHVf4TCmKvgVj/7x+S1trlRW95+x8X4Y6r9iOuuxbU8JlGfJzWZeT/HP+D7Kx+qOhRREdJvhmf6x96s8hc8pKfsd/D5wAAAFa3atPdL2hshbdRZLbXnuYKQtECWpC0KyGxNKkqS0aVJUk+pGXkMh5sedXcW97nOhxJcoymaqoq3/vCYmLVcxMTm70xMTs6Ynkl9VM+RjsPBe0l7+g1p/wBHTP7kNhfB/fX3nm//ACrncs4pwuBseTF4/VzUuO9GeLKcmV5qQ04y52qksdquxxKVdD/APoO7/oHZXMZPo/5ezmrddu77aZ2dmumaZwmunDRMRL57nlm5o/ZiAAAAAAAAAAAAAA1Y5NcZqHfFB6dB9Fpti00Vacfv1oNLM9lHe6VDfG0lTj1Y84o/NOkSnIbijWglJU425+WOkz0Zdwc/G4fTuS71k+cTJ2pjK5uYwpu0xjV6VzWETNVmqZnYrwmvL1zNdEVU1XLdydFc09h/bh5jN/hujaTGMoq5VNfU1/mEOxrpiOx5h5GS2JpMjSam3mHm1JcadQpTbrakrQpSVEZ39D7hjf3BnMbkuF+Jsrdye/cnn94W71m5GFVNUZy9MaYxiqmqJiqiumZouUTTXRVVTVEziucasYbQj9QIgAA0B8Rs+mkcW+urR+9HOB+A/CMxjzI7rifjVlvYG81lry3iIWe8/Y+L8MdLexHXX4sqaKUfw3ac+N9VXXvvuqBypzF0RHPdwdP/ANq3T7Py7FXlZ7CygPSY+UAVcL1R+3dz8b/lWw/Nbw8um/aI9u856qu/dKn1vld5+x8X4Y0rYjrmKwhw/Prxu1af96rb3y3Y9BvRAjDo38LR/st/2ZmXzV+WlsoP0miAAAAAAAAAAAAAK0XrKPDuy21x5wTlRhNM7Y5Rx2sJtPsFENs1yn9QZk9GJ25dQlSnpLWEZfHjOGhtH5TCtZslxRNsKMpUzyJ0TpwUaxNY/oy89HeakR3XGH2HEPMPsrU08y80oltutOINK23G1pI0qIyMjLqQC2Z4ZnrEb2JVWO6R59P295U1zcSmxrknVwZNxkEKA15uPEj7eoK9l6zyRMKP8qd7XNPWbqG0elxJb63ppxmnqITT1Fu7X2xcB2xiFLn+sczxjYGEZFGOXR5Zh93X5DQWjCVqacVDtKuRJiOrjvtqbdQSu9p1CkLJK0mRQVvZgAAAAAAAAAAAAACpF6ybwDOzrKLn1rSlNU+mbpcB5ERoLKTVJplLj0+utkyUoShSnKqQ41j9g6pTi1MPVZJShuO8s50zyJ0TyKdYksemwrMsn13mGLZ9hVxMx7L8KyGnyrF72vc81Np7+gnx7Spsoq+hkT0OdFQtPUjSZp6GRl1IB07fDw5l4xzs4ra73xSqhRMmlRfnX2pjMJSjTiG0qCNERlVMlta3HWq+Yclmyru9Slrq58ZSzJZqSVcxhKmYwnBu8MMIMPHg5/FxB4tSdV4DclE3vySg3WG405CmKZtMK14TCImf56k4q0S4M5cKcmqqHe9lZTpi5TKlnAdQJUxjKVMYy57omtAHQo8CLgGXD/ivG2bntKULe/JCJT5nlSZjKk2OIYAhl6Tr3A+j6UuwZZV89draNkhpz0+cUZ4l+gsqKFU4yqqnGU5IiiAAAAAACFrx3+H1lys4M5HcYbUu22z+PdsW4sThxGzXYXGPVtfKr9jY3GSSHXHjlYnJXZNMNJN6TOqIzSD+XMlZpnCUqZwlzsxYtAFkXwtfHwzLi9S47oLlhFyHaei6ZmPT4XnlX5qw2VquoaS0xCpH2JT0cs5warbSZMMOvJs61g/NR3H47UaE3GacdSE046Y1rpuj9/6X5J4FXbO0RsnFdn4PZ9qGrvF7FMr0KWphmSupvax5LFvjd9GYkIU/X2MeLOj95E60gz6CGpXMYa2XwAAAAAAAAAAAAAAAAABTX9ak/wAPuGX0H7q/VrXAnSso1Kmwkmnb9XN/pLMX+tBtj9SYIxVqRq1OgsK1QAAAAAAAAAqDetY/+A3/AM0X/p2E6U6OVUHEliav1fP+lG0r9CG5f4K8qGKtSNXlXRGFaoAAAAAAAAAAAAAAABXr9ZT20vBuAdLrmJI7Zm7d14Xjk+KSySb2MYbDuNhz5BpMlG4iNkuOUyTSXTyukfXydDlTrSo1qEQmtAABPj6vByxVoPm6zp2+skxMB5S0zOASkSJBsQ42yaD0+51hYrSaux6VOlSLCjjo6dyn7tHl8nQ8VRoRqjGHQAFaoAAABSz9Zy5YnlO1dScOsZtfOU2rqpG2NnRIzjamXM+zCE9BwepsEdTeZn4zgzj85JERIWxkiDM1GkuydMcqyiOVVaEkwAAXKPVZdt+l4Dyu0RKldnzv5fgW26OEp7r6R8+NLZYdlcpiP/tfQ/nGpkOrL+y8+2R/GIRqV19VbJEEAAAAAAAAGgPifcSneanCncukqdlpzPHKmPm2rXHDYQZbHwd8r3Ha0pElaI8NvK0MP0j8hZ9GI1m458dJDMThLMThLmLToM2rmzKyzhyq6yrpUiDYV86O9EmwZsR5ceXDmRJCG34sqK+2pDja0pWhaTIyIyMhYuflATleFl41m0OBjcHUGz6u22/xhesHZDONR5rKc31c7YykP2djriXZutwZtS+4p2Q9QSno0N6U4p5iRDcckKfxMY9lGaceyvJcYeX3HXmLgbOwuPWzaHPahCI6bqqjuqgZZiM59vv9qswxSemPeY9PSZKJPn2SZkEk3I7jzRpcVCYwVzExrbKjDAAAAAAAK4HI8/2+9vfXAyb9U3x5zOkZH/Pji7+v8592qXRVoYV7vZHC2DO2mq8OI+ukMp+uree9HBh3ReDo/wAEt6fKrM+wN2qq5xlv+P30iiq8TU+nwJ/9pH6wh1Y+Eu/4K/HH5LTonDFFX3eyOrHBZttquEp/953WX+Wf8H2Vj9T9CmP+prhn8Y+9WeRqqxpwT/jv1VAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQDxHT6aQxb66tH70c5H4F8Iv/gluv5VZb2BvJKicJQq93sjpdwW7bKuiT/bv039dXXnvuqBypzFx/wA7eDvlVun2fl2Jq0LKg9JCkAVa71X/AB5c+X/nWx/Nbw8vG/Y/pvOeqrv3Spdtvld3sjSsGdtYU4ffybdW/pVbe+a7HoK6IX6OHC/qW/7MzKqqcZxbKj9JIgAAAAAAAAAAAAD5N/Q0mVUV1i+S1NffY5klTZUOQUVvEYn1V1SXEN6vtamzgyUORptfYwJDjLzLiVIcbWpKiMjMgHPt8YXwi8r4KZ1P23qGptck4l5rcGdHYIVLtrLT1zYKNxOCZnKc89KXSuPmpNLbPqV6Q12xpLhzEJclWROPZW01Y9lBsMpADa3irzc5O8LcrXlfHfal5hPpr7L2QYs4bNzgmWoZ7UeayfDbZEqis3DYSbSJXmkToyFH6O+0r5YkxEsTETrW2eE3rJeitp+1GE8w8V+ALN3/ADET4SMYbtMj07by19qPP2MPpYZjr/0h9ZJSl4reC0klOPzmEeQoTT1EJonkWTMVyvF85x2ny/Csjo8uxTIYTVlQ5LjVrBvKG6r3+vmZ1Xb1j8mBPiudD6ONOKSZkfl8gig++AAAAAAAAAAADx+wcCxLaeC5frXPaSHkmFZ5jdziWVUU9HnIlrQ30B+ts4Tpf2SPPRZCiStJkttXRSTJREZBzB+fXD/LeDXKPZHH/JvTJtVSz/bzXOTyo5sJzXWN69Jew/Jmu1JMLkuRWVw56WjW1HtYcphKleaMzticYXROMYtNgZTh+BPz9+w75TxtcZ7d+g6H5GSajC8tXPl+ZqcOzpMhcfAs/WbxlHhxmZs1dZZumppsoE70h5SihNEWKoxjro1RjHXdATOc2xbWuF5bsTObqHjmGYLjd1l2V39gs0QqbHcerpFtcWcpSSUvzMKBEccMkkaj7ehEZmRCtU5hniCcxsq508pdi77v/TYVDZTfnf1ljE1aFKw3WFE/JaxOhUhpx1hFg4w85OsVNqNt20mSXEdEKSRWxGELojCMGlYMpsvAy4BFzM5WQ83zulTP0Lx3fp85zxqaypdbluWqkPva+18olJUzLYsbWvXYWTKkracq692O72nKaM8VThCNU4R13Q/FaoAAAAAAAAAUe/G48Gqx0Rd5Ny54s4q7M0Vdynbja2tqGM/Iladupr6nJ+U0UBvzrjmsLWU75x5poulA+syJKa9SPRJ0zySspqx0TrVkhJMAZy0ByW3vxZzqPsjj/s/KNY5ayTbUibj8xHoFzEaWbiK3JaCc1Mx/KKnzh93oljFkx+/5bs7iIyYYsTETrWteE3rMWK3ftRg/ObAfnPsVeYh/DdqutnWeLvH5G/Tcy1z5ydkFL0Qjvfk0ztml11fRuvjtl5ITT1EJo6i0Rq7bGst24VT7G1FnmK7Hwa+ZS9V5Rh91CvKmQfm23HYrkiE676HZQ/OkiTEfJuTFd6tvNoWRpKKGpkIAAAAAAAAAAAAAAAFNf1qT/D7hl9B+6v1a1wJ0rKNSpsJJp2/Vzf6SzF/rQbY/UmCMVakatToLCtUAAAAAAAAAKg3rWP8A4Df/ADRf+nYTpTo5VQcSWM1cfORO4+K+0aXdGhsw+cTZeOw7mBT5L87+LZP6HEyCqlUlu17T5lSZDQSPS6ya613OxVqb7u5BpWRKJrYmMUiX3efxX/tq/wB43jb/ABPDGzDGzSfd5/Ff+2r/AHjeNv8AE8GzBs0n3efxX/tq/wB43jb/ABPBswbNJ93n8V/7av8AeN42/wATwbMGzSfd5/Ff+2r/AHjeNv8AE8GzBs0n3efxX/tq/wB43jb/ABPBswbNLbLgh4zfiU7m5m8YdT7K5IfPJgGw914BiOY0HwPaEp/bjHru/hwrOv8AbWg1bV3Vf6TFdUnzsWSw+jr1QtJ+UYmIwYmmMF8MQVgAAAAAApUetI7aTc724yaPjSzW3gGrcr2VZRmXCNlM7aGUM47CRMS2oyObGhauUtCXC72mZfcnol4zVOlZRqVZRJNkLA9W5tsuDsWfhlQ7ctar15YbSzFtjuN+BhNRkOM43b27baUKJxqrl5XGdf6mkkRkuL6n29pmGPQZfdxbJr3CsnxzMsWspNNk2JXtRk2O3EJZtTKm9obCPa1FlEcLytyYNhEbdbV+ApBGA6oPD3kXQcs+MeluQ+O+Yai7NwituLWvjOk81R5bDU7TZvjhOkpRrPHMwrJ0E1H0NXo/UyLr0Fc6JUzGE4NkxhgAeM2Nn2L6q1/m+zs2sUVOHa8xLIc2ymzX2dsHH8XqZd1byiJxbaVragwlmlPcXcroXXygOVjyX3pk/Jrf+3t+5ga0Xu1s7v8ALnYSnvSEUtdPmLTQY3FeNKFOQMXoGotdGNRd3o8VHUzPqYtjQujRGDB4MshX2rM4xjXWvNq3lJIr8J2pZ5zVYHbP9SRkD2uZGPwssfiF07VRa6wyNiP3depvIcSZESSNRhj0GU8/q5W3Ea68RqnwuVLWzC3jqbY2u22VLMojlxSxIG0q154jMmkSUsa9kx2Fn8sapJtp8rvQ8VakatToFCtUAAAAAAAAAKkvjn+DbZ5XYZNzX4mYhLtcinOybrkBqLGoKHZdw4TT0mw21hdVGJMibcvKR3X1dGQ69McX6e0g3fSzdnTPJKdNXJKnQJLABkfVO4Np6LzWs2NpzYGW6zzmo7kwcnw27nUVqiO4405IgSHoLzRT6qabCCkRHydiyUF2utrT5AY1rQPCb1mLK6P2owfnNgPz5VqfMQ/hu1XXQKvKWE/Kt+m5lrrzkHHrvqtZrfk0ztYpppHRuBIcPyxmnqIzR1FrzQ3JDRfJ/CI2xNBbRxHaGJvpYKRNxm0akTaWVIaN5usyaje8xeYrc+aLuVCsY0WUlPlNvoZGIYYITExrZtBgAAABW65IKP4fNv8Ax/qg5N+D/fN8edPpFR/z34u/r/OfdqksWFO4/Z/FHDGDOMdRNd4b59dH5V9da896GDDuc8HX/gnvT5U5n2Bu1GdKQIfvdhFP4nB9PgS/7Sf1gjq18JXH/wDi/wAb/ktmJwRT9x+z+KOrbBLGOo2s4RmZ8n9Zf5afg/8Ay9ywfqXoWR/1McNfjH3qzzEzoWBB33IgAAAAAAAAAAAAAAAAAAAAAAAAAAAACP3xID6aPxX661H70M5H4I8Ip/gnuv5U5b2BvJmNCFHuP2fxR0x4JYx1GVtEKP4cNNfH+qtrv8H/AK3045S5jY/52cH/ACp3T7Py7GKy0PSCiAKsl6o/bu5+P/yrYfg/+9vDzAb8j+m856qu/dKk8Y6j5Xcfs/ijS8DGOosM8O/Lxr1Z+lVv75rsegPoifo5cL+pb/szMozrbLj9IMAAAAAAAAAAAAAAA+JkmNY7mWP3OJ5dRU+UYvkVbLpr/HcgrYdxR3dTYMrjzqy2qrBmRBsIEyO4pDjTqFoWkzIyMgFV/nf6tbimWy73ZHBfLoeBW8n0myf0PsGXMkYXKlqU4+5FwLOeky2xZDqSJEeBaNT4pvL6emw45JQiUVdVOK+qqZb2457z4x5vJ11v3V2Xaty+OTjrVblFaqPGtojTpsKs8cuo6pNFlNMbxGhM6tkyoi1kZJcMyMinrTicdTCwMgDc7iH4gPKzg9kSrnj/ALRs6Klmym5WQ66vEnkWs8qUnsJZ3eHT3DgomutIJv2whHDtGmzNLUlsjPqmIliYida4NwZ9Yg4xchfabBuS0WNxh2rIZjxlX1vYnN0jkll+VsrXX5jI7JuCrlud73o96hEGK10b9tJDnTuhNM8iuaZjUsIVtnW3NfBt6ewhW1VZxWJ1bZ1stidX2EGU0l6LMgzYrjsaXFksrJbbjalIWkyMjMjEUX7QAAAAAAAAABA949vAMuWfF53cmAUpS95ca4NvldSiGypVjmOr1NFMz7DSQwk3Z02vYiIuKxBk4sn4j8dlJLnLMSpnCUqZwlz7xNaAJu+TfjK7P5E+HJpjhxYHcs7ArpvtFv7YMh1PXY2B6+XWu6shNSEPuyJEq8UbL2QuPkl+RPom3e9TU15ssRGnFGKcJxQiDKT0WIYlkmfZXjODYbTTciy7Mr+nxbF6CtbJ6wu8hv7CPVU1TBaNSSXLsLCU202RmRGpZdTIvKA6c/hzcL8b4IcVNfaNqyhzMtTG+ezbGSREkZZRtC/ixF5NOae82049VVXo7VZWmtKVlXQWO8vOGszrmcZUzOM4t6BhgAAAAAAAAAfzeZZksux5DTT8d9pxl9h5tLrLzLqTQ6060slIcacQoyUkyMjI+hgK2fPj1dLSe97G92ZxJv6rjzseyXKsZ2vJ8GVK0nkdo8pbqlV8asQ9caxXJecM1+1zE+sbSlKGa5nqpYlFXVTivqqf3KDhjyY4bZajD+RWp8k1/JmPSW6C/faatMKy1uL2qdfxPNKh2bjd75tlxtx1lmQcqKl1BSGmVn2icTEpxMTqavgyANjuNPLjkXxBzUs9477TyTXN295hu3h17zM7GsmiMOecRAyvE7VmdjmRw0n17Ey4zqmTM1NKbX0UTDFiYida3JwY9ZJ0/sv2lwDmnjDOks1dQzCTtvE2LG51HdzPI2h++pS9sMr169KcWlPcR29cRkt16TDa6ITCaeohNHUWW8QzHEdg4zTZpgeUY9mmH5FDTYUGU4pc12Q47dwVrW2mZVXNTIl11hGNxtSe9pxSe5Jl16kZCKD0gAAAAAAAAAAAACmv61J/h9wy+g/dX6ta4E6VlGpU2Ek07fq5v9JZi/1oNsfqTBGKtSNWp0FhWqAAAAAAAAAFQb1rH/wG/wDmi/8ATsJ0p0cqoOJLAAAAAAAAABvb4YH9Ilws/nH6r99UAJ1MTqdQQVKQAAAAAAc2zxutt/C/4mXJadHf89U4FfUepKlvu7vRfg2xqqxzIWOpLUn5bNI9o70Lp2+c6GXUj62RqW06kUIyktberLcdsf2MxzbzzOKdm2xXINe43x2fhSE90e6x/YaMgudkU759nUmXa6nqEKIlfLJf8peQusapQrlXL5XaAyDixyP3Nx7yY3nrHVeeXONRp77fml3ePpdTOxPJEt9jfYzk2KzYVg2Xano3JT5C+MJRpSicYxa+gyuKerB8rinUW7eGeS2SlSqN9O8dWsSXlLNVRYuVuM7LpYnneiI7Fba+089iO2ajccsJr3aXatSo1Ryq645Vt0QQAFdf1kLlcenOIFBx7x2yKPmXJzJfa63aYfSiXF1XgT9bf5W8fm+59grvInqevIldiJMR2YjqokrScqY0p0RpxUNhNY9NhWHZHsTMsS1/h9Y9dZbnOTUOHYtTx+30i2yPJrWJS0lYx3GSfPT7Oa00nqZF3LIBbm8c7hLjnH/wu+FeOYiwzM+xUzCq1vY2kdk202DO0MRspuwMrX8q0TS8p2bisOW6g0J6uzv9r29DjTOlXTOMyp5iSxtPwd26ehuYnGXbrkoodfg+7NeWmQPKcSyk8SeySBXZiwp5ZGhhMvFpsxo3DIyR39xkZF0CdTE6YdUoVKQAAAAAAAAAAQNeIR4CnG/mBaXu09RzmeOW9bb0uwtLHH6WPL1jnt0+pUhyfmeFxVQnKy7spP8A/otql1hxxbrkiVFnPn1OUVTCUVTCmnzD8OrlpwZvDgb61jNg4xJlqi0O0sVW5k+rskV3mhoq7LIkdpFbNkdpqRX2rNdadhd5xiQZKOcTErImJ1NHwZAGXdJ793RxwzeFsfRWy8u1dmkIktJusStn69U2ITqHlVl1B6uVmQUz7raTdgzmZMR7tLvbV0BjWtccGfWXKey9psA544aiglpZjwk781jVSpdRKeR5tn07PdaRESbGsW42hTsibQKlNrfWSWqqO15UxmnqITR1FpnVm2tZbvwmo2PqHO8X2Pgt82blXlGI28S5qpCkpQb0Vx+I4s4ljENZJkRXibkR3PlHUIURkIIamQwABW25ImXw+bg8v+kHJ/wf75vjzr9ImnHn24t/r/OfdqkcWFOpf7ovxRwxsMYymw8N3y6Oyry9f217z3oYKO5rwdsYcym9PlTmfYG7UoSCD96sopfE6Pp8CPl6fVK/WCOrjwlMY/Av8b/ktiZwRS9S/wB0X4o6t9hHGW1XCR1tvk9rBTjiEJNeYNka1pSRuO4BlTTSCNRkRrcdWSUl8c1GRF5TH6i6GE0Wuktw1XcmKacc/GM6NNW687TTGnlmZiIjlmYiNMsxOlYKHfakAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI+/Ei8mjsV8vT9tej96GdD8FeESjHmU3X8qct7A3kxKE/qX+6L8UdMuwjjLK2hzL4cdM+Uvqr67/B/63045S5jqf+dfB/yp3V7Py7OKy8PR6kAKsF6Ze3lz5f8AnWx/B/8Ae3h5hd+Uf01nPVV37pUhjL5XUv8AdF+KNK2DGVhzh1/Jr1X+lVv757wegDoixh0c+F4/2W/7MzKUamzA/R7IAAAAAAAAAAAAAAAAAxBuzQGleSGFS9d711lh+0cOmd6/afLahiw9AkuNm17Y0dh0btMeuG2z6NzYD8aW1/tHEhjgzEzGpVB5yerRX9T7Z55wPzReTQS9JmP6L2ncQoWQMf8A8iIeB7GdahU9shS1m2zEvSgrZabJTllJcV0E4q6qcV9VVq2fqnZelczttd7cwPK9b5xRu+btMXzKjn0FxHSa1oZlJiWDDK5NfLJs1R5LXfHkt9FtLWgyUcktbwAMgCSjgn4q3LDgXfVcfAMymZvp9EwnL/ROdWM6zwKwhvuLVOXjROLema/vnfOm4idVG0lyQltUxiYyk2FYmIlGYiV/ng1zt0bz709G2ppu2WzY1voNdsbXdu42nLta5PLjuPFTXrCCS3LgTCjurrrJgjiWDLajQaXmpDDMJjBXMTDdAYYAAAAAAAAAHOm8bjgGfCXlhZXuD0pwNBb9cts+1f6KylFZjFz6Sy5nutmfNpbbYbxa3sW5MBokEhunsYjRKccZeUVkTjC2mcY66GcZSAABaw9W54BfPhml3zu2XSecxrAJNnhmhodlE7o9vnT0c4eXZ/GS+kkPxsOrZKq2E6SXGzspchaVIfgJMo1TyIVzyLoIgrAAAAAAAAAAAAAHidh6219tvErbAto4Tiuw8KvWfMW+K5lRVuR0M9BdexUistY8qKp5hR9zThJJxpZEpCkqIjA1KvXOX1afDMm9ts94K5gjA7txcmc/pHZdtPscJlGfc76Hg+crj2GR44vontai2/tmw46vyzIrSehSirqpxX1VTbffG7evF7OZeuN+6vy3V+WxlyPMQslrVswLqNFd8w7aYvfR1SKHLKQ3T7Uzq2TKiLPyE4Z9SE8cU4mJ1MJAyAN0uHfiB8puDOWx8i0NsiyrqB2e3NyTV2QPTLzVeZpLzaXm8iw5yYxFTMfYbJorGCuHasNmZMymyM+qYiWJiJ1r+Phr+JvpvxF9aSLbGUN4PuTD4sX4UdO2Ni1MtKJTxoZbyXGZZojOZLgtjKV5tqalpt2M8ZMSm2lqaU9XMYKpjBJcMMAAAAAAAAAAApr+tSf4fcMvoP3V+rWuBOlZRqVNhJNO36ub/SWYv9aDbH6kwRirUjVqdBYVqgAAAAAAAABUG9ax/wDAb/5ov/TsJ0p0cqoOJLG6vh88Pfs7uUeFca/hE+Cz58KfM7X59PnS+fj2u+dHFbXJvMfO588+Iel+2HtZ5ju9Pa8139/RfTsNM4RixM4RisR/ep3/ANeX/wBrv/5EiO0ht9Y+9Tv/AK8v/td//IkNo2+sfep3/wBeX/2u/wD5EhtG31j71O/+vL/7Xf8A8iQ2jb6x96nf/Xl/9rv/AORIbRt9Y+9Tv/ry/wDtd/8AyJDaNvrM78X/AFbn7G7kTpbfn2Znz5/BBsjFdg/On9jt87vzxfOzax7P2o9vfh0vfan03zHZ6R6HK8316+bX06DG0TXjC0EIoAAAAAD4mS5DV4ljmQZXeP8AotLjNJa5DbyehH6PV0sB+ysH+ilISfmYkZavKZF5PjkA5MOzs7tNo7J2Fsy8/wCWti5xlmd3Hyxr/wCNMuvp+QWHy6jNS/8Ahdgvyn5TFq54cGXQX9XT1MWuvDdxnLXoKYs/du0tk7IdecSopkiBW2UXWFX53ziEutRTY14p6O3/AL2aHzeR/vxmcKtaqrWin9Z44pHR51pnmPjdZ2VubwPgZ2fJjtLJpGW49Hm3mvbaYskqSuZe4smxgqUZpJLNIwkiM1GYzTPIlRPIqgCSbbngfyasOH3LfR3IKK7KKqwfNISc0hxDcNy115fodx3PqxLLZLKRIkYpaSjjJUhZIloacJJqQkJjGMGJjGMHUpqrSuvKutuqebGsqi4gQ7SrsYbqH4dhXWEduXBmxH2zND0aVGeS42sjMlJURkKlL94Dm2+NPyuPlhz821c09kmw17qKQnR2uDYfS/Cep8Bmz4+R3UR5rpHlx8kz2XazY8hJdXILsdHctLaVCyIwhbTGEIoBlJYK9XN4p/DfzTm7xv4HpOEcXMbLK2lOt+ciytn5k3ZY9r6E6haO0/a6Cxb3Da0rJbMyrjeQyWfTFU6EKpwhbI8XbUqt0+G7y2xFiImZPqdWzNlVjaWzclFN1DZ1u0SRXkhK3vTZcbEXI6Et/LvE8bXlJwyOEa0KdbmWixcAOqzwx20vfHErjbuGRI9JsthaU1xkd853k4acnl4tWpymOpwiSTi4mRtymlK6F1NBn0L4wrnWpnRLZgYYAAAAAAAAAAB8fIMeoMspLPGsqo6fJccu4b1dc0GQVkK5pLevkJ7JEGzqrFiTBnw30eRbTra0KLyGRgK3HOT1b/Q+3vbPOeHl/G49Z896TLe13eqtbzTN/Mc/LEtwFJVYZRrhbr61qWqIVnXNoJLbFewkjUJRV1U4rnlVEeUXCzk3w1ys8T5D6lyXA1yJT8aiyZyOm0wTLCZ71+exTNqtUvG7s1Rkk6uO3I9MjIWRSGWV9UFOJiU4mJ1NWwZAGx/GflzyK4f5ujP+PO0ci15cunHRcQYLzc3GMphx3ScRXZbilk3Lx/I4JH17SlR3Fsmo1Mqbc6LJhixMROtfD8KvxhtY+IRTlrzMYNZq7lBj1SqfeYE3LV87efV8Ml+n5Pq6TPkvWEpiI0gnp9TIUudXIX3Jclx23JKa5jDsK6qcOwmfGEVdDlxCXXcj9sR3EklTmRNTiJKDQRt2lTW2TSuikpMzW1LIzPp0UZ9SMyPqPPz0qsjXkOkLxTYuREVVbwpuasNF6xavUzpiNcVxMzyzOMTMTirqnCWufUfn3BHahM34adol7V2f0xLI1wM+KzU10R1Qm2x2oipc6kfnDJw6Uy8pdC7fJ5eo7ffB07xpu82m/d0bXkrG/e/YaNEX8rYoierp9LzGnR5HRyrKZxhJAOwtJGh4mFS49gWtb4u7zVbl9nUL6GfZ5y7plTGu4vNmXd24+vp8uXk6+Q/jp65vCNbpuXuBuHd+Rj3rL72vWJ6mOYy83I5NeGVqw8lHLonXTGqcIQ59R1IYK9qGbON+RtYtvjVFy+55mO3mtNXyXj7e1mLdv+0kp5w1GRE01HsVKWfxySRmXU+g5k6PO/7fDHPhwvve9VsWKd8Ze1XVowpozFXpauqceSmm7MzOuIiZjSzFUYrJ49Ei0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAR8+JL9Q3FPrr0fvQzofgzwh/8Agruv5U5b2BvJGqcIQldR01YK9qGV9DH+3lpj66+uvffTjlHmP/xq4P8AlTur2fYZiYxWZB6N1oAqtXx/8e3X6bWP5seHmK33H9NZz1Vd+6VKdqHyeo0zA2oWIuHP8mnVX6U23vmux39dEf8AR04Y9S3/AGZmVtOmGzI/RzIAAAAAAAAAAAAAAAAAAADXnkZxQ468tMOVgvIfU2JbNo0IfKsfuoSmMixx6SlKXpuJ5bWOwcoxSe4lJEt6vlxluJLtWakmaTY4MxMxqVMOdvq2OxsBZuNh8HspmbYxiOh+fJ0tnc2sr9m1zKVeddaw7KUtVWM5syw0pRoiS0VdgTTSW212EhZEc4q6qcV9VWAyfFsmwnIbjEsyx67xPKsenv1d9jeSVc2kvaWyiq7JMC1qbJiNPgTGF+RbbraVp/BISTfCAbh8Guam1+CG/cY3brCa9IixnmqvYGDSJsmNj+x8HkvJO3xi7aZV5vz3m/y+vlKQ4qvsG2pCUr7FNrTGLExi6b2k9wYPyA1JrrdetrL22wbZ2JU+YY5LWSESUQbeKh9UCxYQtz0O4qZJuRJsc1GuNLZcaV8sgxUp1MoAAAAAAAAANAvEu4S0PPPihnmmX24EXP4DKs005kc1DaSx/ZtBElKo0PS1EaolPkrD71TYrIl+bhTnHUoU402RZicJZicJcyXJcbvsNyO/xDKqifj+UYrd2uN5JQ2sdyJZ0l9Rzn6y4qLKI6SXYs+tsIrjLzaiJSHEGR+UhYufFAbHcSeM2ecweQ2sePWu2lou9g5CxBn3KorkuDiWLxCVOyrMbZttbXWtxmhjvylo70KfW2lhszddQk0zgxM4Ri6iGkNN4Hx61FrvSWsalFLgmssVq8Tx2ERNnIci1zBJfs7J5ptopt3dzlOzZ8lSSclTZDry+q1qM6lOtlMAAAAAAAAAAAAAAAABizcWj9QcgsKn663brfD9oYVY9Vv4/mNJDuIjEnza2mrGtckNnKpriKlwzYmxHGJcdR9zTiFdDA1KsHOn1aSOpq2z/gTlrjb6Uyprugtn3pOMvH5FtV+vNk2BIcYV2l2NRMjccJSjNbtqguiROKuqnFfVVQdo6n2XpLNrrW+3MGyfXWd488TNvi2W1Mumtovf1NiQmPLbR6TAmNl3x5LJuR5DRktpa0GSjkmx8DLNPHnkFtTi5t/C946ZyR/GM8wezbn18lJuuV1pDUZN2eOZDAaeYK3xq/hGuNOiLUknmHDIjSskrS1sTGLpr8IeWeFc2uM+teQ+FIagIy2sXEyzGSkHJkYXntKv0DL8TlOLQy84mstW1KivLbbOXAdjyUpJDyRXMYSqmMJwbYjDAAAAAAAAAApr+tSf4fcMvoP3V+rWuBOlZRqVNhJNO36ub/SWYv8AWg2x+pMEYq1I1anQWFaoAAAAAAAAAVBvWsf/AAG/+aL/ANOwnSnRyqg4ksTV+r5/0o2lfoQ3L/BXlQxVqRq8q6IwrVAAAAAAAAAAAAAAAjd8XnbatLeG7yzy1iacGwuNYS9aVbrSjTKObtyzrdY9YSk/liJUeJlbr6XEdFsk0bhGk0dxZjWzTrczAWLgB1U+FOoz0NxE41afeiehWWAaU11RZAx2rbP5628YrpGXPqbcJK21y8nkS3lJMiNJrMvwBXOtTOmXi/EM4uQ+Y/DreOhTiRX8kyXEZNtruRKNDRV2zcUWjI8DklMV0XAjyshrmYct1BkZwJT7Z9yFqSpE4SROE4uXRMhzK6ZKr7CLJgz4Ml+HOgzGHYsyHMiuqYkxZUZ9KHo8mO8hSFoWklIURkZEZCxc/OA6GngDcslckuB2L4TkFmc7YfGewRpy/TIeNybKwyHERP1XcqR0PzcFOJr9pWzUo1uPUTyzIiUXWFUaVVUYS208Uvlf9hrwg3Tt+ssEQM7l0pa+1UZP+YlK2RnZO0tFPgfLJU9JxWIuVeKbIyNbFW55SGIjGWIjGXMYUpS1KUpRqUozUpSjM1KUZ9TUoz6mZmZ+UxYuf4A6NvgbcUFcWuAet3b2sKBsTe7i955x5xB+mR2cvhQm8DpnluEUhhNZr+FXOOxVEko1hKll29ylqVXVOMqqpxlLXkeP1WWY9fYtexkzaTJaa0x+4hqPomXVXMF+usIyjLykl+JJWkz9kYRcmDaGBW2q9l7E1hfpNN7rjOstwK6SpCmlJtsPv7DHrFKm1kS2zKZXL6pMiMvjGLVzwwMugp6ujtpWxvDfx3EZMtMibpDauyNam244a5jdbZz4W0axbpLUp5UVKNiuR46v97JEY2kf70ZJhVrVVa07oiiAAAAAAAAAAAAAPJZzgWD7Oxa3wbY+H4xnuGX8Y4d5imY0VZkmO20YzJXmbCnt40uBLQlZEpPe2faoiMuhkRgKzfOf1bHVWfN22fcIsnRqDLjRMnPadzWdZ3esLySafPIjYxkr6rHKcEkvu9/5XJO2r1KW222mCyg1HKKuqnFfVVDuQHG7eXFrYE7WG/da5LrTMoSVPNQb6GRQbmATimkXGM3sRcmjyijddQpCJtfIkxjcSpHf3oUlM9acTjqYQBl6/X+f5nqvN8U2RrvIrLEs5we+rcmxTJah1LNhTXlRJblwJsc1ocZc8282Xe24hbLyDNDiFIUpJmHSd8K7npWeIHxXoNnzmoVXtbEJp4FujHYRIZiw82rYUWUi/qYnnFPM47mVVKZnxSMu1h5ciIlThxVOKrmMJVVRhLUDxBKJdPyKs7E0dqcqxTF71KvL+WejRHcZNXXtSXVJ490+OfkIvL+AXSL07NxTurn+zO8JjCN57syeZiersUTk8dX+y4cuqNPJHz3PLNI+8/YH432YQSdeGZlCY+YbOw5xw+65x2lyOM2pR9hHjllIrpRtpP5UnHE5M33dOhqS2XxyT5Ox/wAHRxFRluK+I+Eq6p2s5u/L5uiJnR/JLtVqvCNWMxnKMcNMxTGOMU6LbeuYTDDtiWtTebeHuZfx0zU47ZuzcWVW5jFT0IySiklpK2cM/jpJmglS19S+R0+MZj8u9MjhKvizmA3x3inazm7Js5+iOpGXrjv89bZy1d+rxMNUoVxjSr495+wOiDZhQ/ozIeYdafYcU08y4h1l1szQ4262oltuIURkaVoWRGRl5SMhZauXLF2m/Zqmm9RVFVNUThMTE4xMTyTE6YkWcNN7Bi7T1fhOeRltqXkFFEfsW2jT2RbuMRwr6GXb0Iih3EZ9svIXVKSPoXXoPRxzS8d5bnL5t9z8b5eaZrz+SoquxGGFGYo/e8zb0fY79FyiNEYxEThGOD6YnGMWTByKyAAAAAAAAAAAAAAAAAAAAAAAAAAAAj38Sc+mjMU6f62KL3n52Pwf4QyMeZbdfyoy3sHeSu55XxUI/efsDpu2YUssaFUfw56X+N9VjXXvwpxyhzIRHz08IfKjdXs6wlT5aOys0D0YvoAFVS+Wft7dfG/5WsfzY8PMlvumPbrOeqrv3Sp8r5XefsDS9mBYm4bn140aqP8AvTb++e8Hfr0Sf0duGPUt/wBmZh9FHlYbND9GJAAAAAAAAAAAAAAAAAAAAAAA0h5O+HBwm5kZFT5fyN0LQZ/ltHBOshZREyHOMEyJ6t6kbNfb3eucoxGyyCFCMj9GanuyW4vevzJI7192YmYZiZjU1h+4MeFB9qp+/lyS/jhDalnaqPuDHhQfaqfv5ckv44Q2pNqpIjx8476g4saupdLaIxWThOsscmXM6ixd/Ksxy9qqkZDayry4KHZ5zkGS3bMabcTn5JsFJ8yl55xSUJNauuNaMzjrZrAAAAAAAAAAEam6fB/8ObkLs/Ldy7c43wsk2PnU5izyy/gbL3Nh7NxZR4EStKwdocJ2LjmNx5siNCbVIdZhtuSn+9943HnHHFZxlnamGLvuDHhQfaqfv5ckv44Q2pZ2qmzHF/w2uFHDLLr3PeNuj4Wu8xySgPFrbIHc22Xm09zH1T4tm9Vw17CzPLG6liVOgsOPnESwt82WycNRISRJmZ1sTMzrbyDDAAAAAAAAAAAAAAAAAAAADU/lFwb4o80K7Hq3kvpqh2YnE5LsnHLN21yjE8lqCfQtMmFEy3Bb3GMoTTylL73oJzDhOuoQ4tpTjaFJzEzGpmJmNTTP7gx4UH2qn7+XJL+OENqWdqo+4MeFB9qp+/lyS/jhDak2qm8PFzh1x04X4hkOBcasAk64xDKckPL7uiVnGw80hyskXWQKZ23YPYGWZW/XSZFXVRmXfRlsoeTHb7yUaEmSZx1sTMzrbNjDAAAAAAAAAA015U+H5xE5s2GGWnJzUnwmTtew7qBiD/z+7Nwz2oiZE9WyLhrzWvs0xRmf6Y9URz7pSX1N+b6INJKUSsxMxqZiZjU1N+4MeFB9qp+/lyS/jhDalnaqZ245+FhwN4l7Li7g4+6J+cDYsKntqGNkXwn7kyrzdTeNNsWkT2ozbYeSUS/Smmkl5xUU3UdOqFJMMZliZmdaQYYYAAAAAAAAAGoPKzgXxP5u/OF9k/qn4Tvgx+ej5x//AI52ThftJ8+nzu/PL9T3McT9svbL506//wD2ekeZ9H/Kuzvc78xMxqZiZjU1B+4MeFB9qp+/lyS/jhDalnaqZq4+eFBwC4r7Rpd0aG0J84my8dh3MCnyX4Ut05P6HEyCqlUlu17T5lsbIaCR6XWTXWu52KtTfd3INKyJRMZliaplIkMMAAAAAAAAAAAAAAAwJyQ4w6P5ca2XqLkJhbmfa7dvKrJHsdRlWZ4gl64pCk+1cp6zwXIsYunm4qpa1EwqSbCl9qlIUpCDSicGYmY1I/vuDHhQfaqfv5ckv44Rnalnaqfqg+BJ4U9bNh2MTiq0mVAlR5sZT26uRMtlMiK8h9k3Ysvbj8SS0TiC7m3ULbWXkUkyMyDak2qkuQwiAIr898FDwxdm5xmGx804wxrPMM9ye9zHKrKHtvfFBGscjyWzk3F1PZpMd2jU0NUiZZTHHPMQ4seM13drbaEkSSztSltS8n9wY8KD7VT9/Lkl/HCG1JtVNreK/h78ROFFrmF1xj1VK1jPz6vqqvL+zZO2cvg3cSjkzJdP6RVZ9nWU1TUqtesJHmZDTLchtEh1BL7HFpUmZnWxMzOt63lLww42808axnD+S+vpWyMYw+8fyXH6VGebIwqDEvpEB2rVaSG9e5fiZ2kpmvfdaZOWb5R0POk0SPOudyJw1ETMamkv3BjwoPtVP38uSX8cIbUs7VT+jPgO+FEw80+jim2pbLiHUJe3ZyMkMmptRLSTseRt51h9szL5ZC0qQovIZGRmQbUm1Ul0ZZZjMtR47TTEdhptlhhltLTLLLSSQ0000gkobabQkiSkiIiIuhDCL+gCLzZ3gw+GnuPYma7W2Nxrav8APtiZLb5hmV61tze9Em5yS+mO2FxaHUY5tCoo4Ds+c+t1aIsZlrvUZkkuozjLO1Lw33BjwoPtVP38uSX8cIbUs7VTdHixwk4x8KabLce4y61c1rTZ1Z1tzlNeecbGzRmys6mLIgwZiD2Dl2VuVzjcSSpC/RVMJeIk+cJRoR2pmZ1sTMzrbVjDAAAAAAAAAAAAAAAADXvkjxS488vMFb1vyO1bQbQxKNYMW1dGtHrapt6OzYW2r0/HMrxqxpMsxqVIbaJmQuvnRlSoylMPd7K1oUicGYmY1NB/uDHhQfaqfv5ckv44RnalnaqPuDHhQfaqfv5ckv44Q2pNqpttxW4B8TOE0rNJfGLV0rWLmxI9HGzNpOx9q5jCvEY05aO0Lj1bn+cZVXxpVYq7lk2+w008SJDiTUaVGQTMzrYmZnW1p8TbDzVG1hsBlr5Vp+5w6ye7S8pyG27qja7yLqXQo1gfQzP4/k6eXr1jeEW4Sqry/DfHNmnRRXfyF6rDzURmMvGP7TNThPicuPz39GFSJXqOrvZl8+1DZLiRnqNe8gNe20l7zNbbWisUtVGfa36Jk7S6hl19XckksQ7KRHkLM/IRM9R+gui1xpTwHz57i3pmK9jd2azM5K91NjOUzYpqqnRhTbu1WrtU8kUYp264iuFjQd/T7H4LWsg3dXZU1mwiVW28CZWWEVwurcmDPjuRZbCyPyGh5h1ST9gx8O893ZPfG7cxujeNEXd35qxcs3aJ1V27tM0V0z1qqapieyKwGy8HsdaZ9luB2xOemYxdzazzzjZtHMiNuecrbJDZ+VLNnXONSG/ktukY84fOHwVvHm8433pwVvPH03u3OXLO1MbPfKInG1diOSm9ami7T/m1w+GqdmqYnkeH6jZuzLG1CULw7N4tU9vZ6TyGYTULIX3r3CnpDpJbZvG2Ulb0aDX16e20NhMhhJGlJPMOERGt4h2QdAjnkt7o3pmeZ7f12Kcnn66szu+qqrCKcxFMd/y8Y/Z6KYu24iYjvlu5ERNd6F1muMdlMIO1x9IAAAAAAAAAAAAAAAAAAAAAAAAAAACPXxKfqF4p9dmi95+dj8IeEKjHmX3Z8qMt7B3kpvThT4qEPqOnLZl821DLOhD/AG9NL/XZ1z78Kccn8yNM/PRwh8qN1ezrCVFUbUdlZtHotfaAKp9//wAu3X6bWP5seHmX33TPt1nPVV37pU+Dah8nqNL2ZNqFivhr/Jn1T+lNv757wd+PRK0dHfhj1Lf9mZh9drykNmx+i1gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANaeXevF7J0DnlTEj+kW9LBRl9IhLZuvHPxpR2D7MVBJUtUqfUIlRWyT5TU/0/B6D899KXgSrnB5kN9bry1vvm9MpZjPZeIjGrvmUnvtVNEYTM13LEXrNMRpmbmHKruxjRPVVyO4/k/1R0HYPh2o6z/pDq21pcbWpC0KStC0KNK0LSZKSpKkmRpUky6kZeUjGaJqoqiuiZiuJxiY0TExqmJ5Jg2uwswcd9oNbg09hebKebdtZVYityVCDSRs5LUdIFz3NpIvMJlyWfSWkfgMPo8pkfU/QtzEc41vnU5qt0cX1V01bzuZeLWbiMPI5ux+938YjysV1099op5LdyjXjjP326tuiJ5Waxy8mii8RvSTz7VRvKgimv0RqJjWdNsp8qYxuG3j1+6RdOqW3XfQX1H1PouMRF0JRl1mdPnmduZi3leeTclrHvVNGU3jFMaYoxwyuZnrRVPpe5OvTl4iMIqmPmv0/s48VEf3H8n+qOr3B821HWfsr7KfUz4VpWTJECxrpUedAnRHVsSocyK6h+NJjvNqJbTzDyCUlRH1Iy6j6sjnc5uzO2d5buu12M/l7tNy3comaa6LlFUVUV01RpiqmqImJjTEwbXYWBuJ/J6m3zirNVbyY0DZ+Pwmk5HUGaGSuGGexgsnp2/lUuw5azT6S0gusN9faZebU0tfeP0ZekXunnq4bo3ZvW5bsc4uRsxGasaKe/004U+m7EaImiucO+0U6bFyrZmNiq1VX9tq7FcYfsm3Y/Ui0AAAAAAAAAAAAAAAAAAAAAAAAAAEeXiWH00Vif12qL3n54Pwn4QfTzMbs+U+W9g7xUZicKI7P66D/uP5P9UdO2D5NqOsy1oNR/Drpby/6Wtc/g/9cKYcncyUf85+EflPur2dYSoq8nGrXCzmPRO1AAVSb9R+3t15f+drH8H/AN8eHmd33H9NZz1Vd+6VNNmqMeR8juP5P9UaZgbUdZYw4aH14zaoP+9Nv76Lwd9fRM/R44Z9S3/ZmYfda024bOj9FLAAAAAAAAAAAAFQTl96wvy34z8o9+6Di6J0BYVeqdq5lh+PWVuzsc7e0xWuuZPzpWtqcbMoEQ7O0xpyJIkeZZbY884rzZdnaJxTEwsimJjFrl99Fcu/tfeOH9o2d+z0NmDYhPB4OPikZd4kWNbub2biWBYLn+przEVsU2CrvGoNnh+YQLZMGzXHyK5uZz0uHdY7LafW04TTaHWCUlKlka8TGCNUYJqBFEAAAAAAFUnxCvWCty8WOXu3+Pem9V6YzrDdWWNHjjuT5enNXbiZk54zT2WXQlnR5XUwUt0GRT5Fd0JhKiciL6mryGc4pxhOKYmMZaW/fRXLv7X3jh/aNnfs9DZhnYhNB4N/iqb68SLL96VmzdXaxwnGdR43hM9i1wBnLkyJF7mlnkEeDX2S8hyC7iky9X4zMcbJBNuGplXlURH0xMRCNVMQnoEUQAAAAAAAABV18Tjx0+Q3B/mFn/HTX+otMZbi+JUeB2kK8zFrOFX8l7K8NpskmNyjpMsqq7zcaVZLba7GEn5tJdxmfUzlFMTGKcUxMYtAfvorl39r7xw/tGzv2ejOzDOxCajwbfFZ3H4kOS76pNp661ngsfVFHgFpTO6/bylD1i9ls/Kok1uz+eLIbtBtRkULZteaJs+q1dxn5OmJjBGqmITwCKIAAAAAAAAAq6+Jx443J/grzCz/AI94lpnSWS4VSUeB5DiGQ5a1nqsitqzKMNprKzdsjqcqqasziZWqxis+YZJPo7DfcZud4lFMTCcUxMYtAfvorl39r7xw/tGzv2ejOzDOxCXHwgPGZ2H4hW59laZ3Dgestf3dBrprP8HPBPnlYVesVd/ApcqhTvnmyO6J+TEbv4L8duOlC/NJkLV1SjqnExgxVThGMLC4igAAAAAACtp4svjebR4G8mqzQGmdd6n2C3C1njeWZxPzn563bGkyjJbK+diY+yjHMmpmWmkYrFrpxk62pwynpMj7eglFOMJ004xjKML76K5d/a+8cP7Rs79nozswzsQkx8KDxo+SfiDcpn9HZvp7T2K4hVawy7Yl9fYS3myLuCzQ2GOUlf2neZTb1/osq6yeKy51Z7vywu1RH8fExEQxVTEQsriKAAAAAAAAAAgf8ZLxWdx+G9kuhaTVmutZ51H2vR5/aXLuwG8pW9XPYlPxWJCbrPndyGkQTUlF84bvnScPqhPaZeXrKIxSppiUK/30Vy7+1944f2jZ37PRnZhLYhI14WPjf8geeHLKn4/7F1Np3D8ascHzTKHbrCm81TeImYzCjyYkds73KreB6NIW8ZOdWTV0LyGQxNMRDFVMRGKzcIoAAAAAAAAACDvxmvFF294bP2N/wVa+1vnfwz/DB7ffCCjJ1+1XwdfBb7V+1HzuX9H09O+fuR6R57zv+8t9nb8t3SpjFKmIlB399Fcu/tfeOH9o2d+z0Z2YS2Ib/eGP46fIbnBzCwDjpsDUWmMSxfLaPPLSbeYc1nCb+M9imG3OSQ24p3eWWtd5uTKrUNu97Cj82o+0yPoZYmmIjFiaYiMVooRQAAAAAAAAAAAAAABTx5S+si8gdP8AJDeeptaaY0XkuC6w2nm2vccyHIkZ+7c3kPDb6bjrlrMcq8wra9fp8uucdbNphtPmlJ8n4JzimMFkURgwL99Fcu/tfeOH9o2d+z0NmDYhYF8HfxBd4eIrqrbu1Nta617gdNhWwazX+JPYA3kqI9xZs45GyPKm7I8jvLtSnqyLfVJt+ZU2RJkq7iPqnpGYwRqiI1JgxhEAAFPflV6wfzb4u8jtz8fsm49ceSsdV5/fYxGly4+zUu3GPsyTl4nkiOmbRe6NlGKy4ViyrzTXczKSfYnr2lOKYmFkUxMYtf8A76K5d/a+8cP7Rs79nobMGxB99Fcu/tfeOH9o2d+z0NmDYh6TDvWXebefZdiuCYnxs47XGVZrkdHiWM1MdjZpyLTIMjs4tPTVzBHnvQ3ptjMbaT/+5RBswbELqlR7alVVhXx16rz2vhe3KqhMlNUdr6M17YnWJmKXMTXnM7/Mk6ZuE329xmrqIK30AABHZ4pHLba/CDiTkPIzUeGYdnFriOY4TWZHV5wm7VSxMXyy2+dpdmgqC1p53prWR2day31eNvtfV1SZ9DLMRjLNMYzhKsV99Fcu/tfeOH9o2d+z0S2YT2IfTpPWiOUi7moTkGgOPrdCqzgJu3KxnZBWSKg5TRWS68384kMFNTDNZtGttaPOdOqTLyG2YNiF26LKjTosabDfbkxJjDMqLJZWTjMiNIbS6w+0tPVK23WlkpJl5DIxBW/uAAAAArueL94vnIrw599a81vrjVGps2wjPdTQs4Yv86i5qVq1kreXZXQXtIw9Q5RUVz0ODXVddIT+VedSqaZK8nYZyiIlOmmJhE199Fcu/tfeOH9o2d+z0Z2YZ2ISFeGB48W2uaXLbFuOe5tZ6hwOpzrF8xexS4whOYs2snM8aqFZNFqZDmQ5PcQCgzceqLI+hNJdXIQ0lKvKaVYmnCMWKqYiMYWfhFAAAAAAAEDXjG+Ltm3hzZXpLX+o8K11n+abCx7LMxzGvz4skU3juLwrKrpMOmV6Mdu6Vxa8itmLlCjdUsklW+QuqjMSiMUqacdaFz76K5d/a+8cP7Rs79nozswlsQ3J8Pzx7OVXMrmDpTjddaS0TRUWyLm+av7nHmtgIuquhxjDcjzO4l1y7PMbCvTLRX464SPPMuINRkXTqZBNMRDE0xEYrXykpWlSFpJSVEaVJURKSpKi6GlRH1IyMj8pCuqmmqmaaoiaZjCYnVMIK1PJjVi9O7mzHEG46o9I5NO9xRXaaWncZuluS61thRknziaxZuQVq6ERvRV9PIPP10g+bWvmr52N68L27c0bnqvemMnOGicpmJmu1FOrGLU7WXqnCImuzXhoaXdp2K5p5GBepfJIcLbPWV4pE/D03c3hWfzNV3kwmse2M8yukW86SGIGaxmvNRW0ko0pSeSQ0FFP46nJLMVBF5TH7u6DHO9Twhxtd5tt83NjcW/q6Zy81ThTbz9FOzREY4RHpq3EWZ1zVdoy9ERpl9OWubNWxOqfHTfDuBfe+Rf0NRlNHbY3fwWbOkvK+XV2kCQRmzLgzWVsSGVGk0rQam1n0UkyUhXRSTIyIxpe+9y7r4j3Pmtwb7s0ZjdGcsV2b1qrytdu5TNNVM4YTGMTomJiaZwmJiYiWJiJjCdSuNyP0NeaB2DMxuYUiZjNkb1jhmQOI/K7amN0ySxIeQ22yVzVGpLUxoiSZK7XCSTTrRn0Jc/fMrvnmT44u7gzUXL3D2Ymq7kMzMaL1ja0U1VREU9/s4xRfoiIwnZuRTFu7bmdMu0TbqwnVyNfupfJIcI7PWV4vvYzlN/hl9WZPi1vMo7+nkFKrbSA75qTGeJKkK6GZKQ6y80tSHG1kpt1tSkLSpKjI9Z4e4g35wnvrL8RcN5m7k995W5t2r1qdmuirTE9aaaomaa6aommumZpriaZmJzFU0zjGtNTx156YXsKPCxja0muwXNiS3HauH3Ci4hkbhJIidRNeV5vHrB0yM1MSVJjqV0808alkyntx5h+mjwnxzYs8O85Vdjc3F8RFMX6p2Mlmpw1xcqnDLXZ0427sxamcO93Zqqi1T91rMU1aK9FX6iQlKkrSlaFEpKiJSVJMlJUlRdSUky6kZGR+Qx+5KaqaqYqpmJpmMYmNUw+l/oyAAAAAAAAAAAAAAAAAAAAAAAAI8fEuMi0TifX/W1Re87PB+FfCCxjzM7s+U+X9g7xfNmv4OOz9SUHnUvkkOnrZ6z4MWW9BGXw7aV8pfVa1x78aYcncycf85uEflPuv2dYTtz++U9mPHWdh6IWqgCqLfmXt7deUv8Alax/Njw80e+6f6Zzej/9q76OppEzpfI6l8khpmz1mMVjPhl/Jl1R+lNx76L0d83RO/R54Z9S3/ZmYanY/gobPD9ErQAAAAAAAAAAAFCv1krjlM1fzep95wYDreKcktf01o9YJYW3E+EHW0KvwfJ6xtSUFHNxGLxcfmrMld7js1xSk/7ZU6dSyidCvEJJpIvCo5wyOBXLzDNq2zkx7VmUxndc7nrIaFyHntf5DMhPO3kKIkzJ+1w66gRLVlKS86+3FdipUkpKjGJjGEZjGHS2xzIqHL8fo8rxa3r8gxrJamuvsfvamU1Oq7mltojU+stK6YwpbMqFOhvodacQZpWhRGXxxWqfZAAAAAaN+IZzcwTgTxozPdOUSK+blq4sjHtR4RKf7JWebJnxXvaOpQy2tMj2mrVJOdbPp6ej10d00mp5TLbmYjGWYjGXMSy3KsgzrKsmzfLLSTd5VmOQXWVZNdTVd8y3yDIbKTb3NpLWRES5M+xmOOuH08qlmLFzz4C/H6t/xxm6h4OW237+AuFkHJTYNhltd55o2JJ65wppeHYcmQy4gni9KvI95YR3DMkPQrBhaE9qu9cKtaqudKwcIogAAAAAAAADnc+sGf0o26voQ01/BXiosp1LafKoVBlJbJ9Vb/w+5m/QfpX9WtjiNSFepcoEFYAAAAAAAAApw+tFccpkTLeO/LCprjVVXNHYaGzewZbWhqJcUsy3zrX3pRpLzTku5rbW/QTiujnmqxCDNSUoJE6Z5FlE8ipYJJtneGvJ7LeHHJfU/IrDkLmTde5I3JuqMnjYaynDrWO/S5niz6z7mm/b3GrCSwy6tKyjSVNPkk1tJCYxjBiYxjB1AtJ7m15yF1Tgu6dU37GS4BsSgh5DjtoyaEu+jySNEmvsY6VuKgXNPNbdiToqz85FlsuNL+WQYqU6mUgAAAAGCeS3IvWPFHSWe7525dN0+G4HTP2DzZLa9s8gtlpNmjxPHozq0FOyLJrRTcSG11JPnXSU4pDSXHEo0sxGOhy8OR++M05Pb22lv7YTxOZbtPLrLJ7CO289Ii1MV80RqTHa52R1eOoxiiixq6GSvKmLFbT+ALY0LYjCMGEwZXLPVeOOUunwfkNypu4DjKcztqTTWAynS80btRixfPRn8tlJn3SYM+5s6eOh0iJKX6x9BGpRKJEap5Fdc8i2KIIAAAAAAAAACmv61J/h9wy+g/dX6ta4E6VlGpU2Ek07fq5v9JZi/wBaDbH6kwRirUjVqdBYVqgAAAAAAAABUG9ax/8AAb/5ov8A07CdKdHKqDiSxNX6vn/SjaV+hDcv8FeVDFWpGryrojCtUAAAAAAAAAAAAAMdbf2FX6k1LtHa1sbJVWstdZtsKzOQvzccq/C8as8kmG+53I7GSjVqu4+pdE9T6kA5Mlzb2eQW9rfXUx6xuLuynW9tYSDJUidZ2Up2bPmPmkkpN6VKeWtRkRF3KMWr3zgHRl8BnUC9SeGbo9+ZH9FudrT822/cN9C7VoyrJZldi8hK+1Kl+l4FQVDp9S6pUs0kZkkjOurWqq1pjBhEAAFM31njikdHnWmeY+N1nZW5vA+BnZ8mO0smkZbj0ebea9tpiySpK5l7iybGCpRmkks0jCSIzUZidM8iyieRVAEkwBYK9XN4p/DfzTm7xv4HpOEcXMbLK2lOt+ciytn5k3ZY9r6E6haO0/a6Cxb3Da0rJbMyrjeQyWfTFU6EKpwhfnFasAAGlviMajVvTgpyt1iwyUiyvdJ5vY0EdSDcTIyrEatzM8SYUREakk9k2PxEmsiUpHXuJKjIknmNbMa3LgFi4AdQTwxdtJ3f4fnEjYapap82XpXFMWupy3CdcmZNrdhzW2VSnlkpfV9/JMSlKcLr1JZmRkRl0Kudamdbe0YYAAAAVkPWcuOU7O+NenuR9FXOS5mh86scXzB2M2XWJgm2WqqG1b2DhF3Li1eb43VQ2i8vY5cKMi6GsxKmdOCdE6cFH0TWMi6h2nl+j9p683DgE4q3NNZZjj+b4zLcSbkdNvjlnHs4rM1glI9Kr5S4/mpLJn2vMLW2r5VRgxrdQThhy11rzZ49YHv3Wc1g4eSV7UXK8a9KRItMCzqDHYLKMJu0Elt1ubTTnfylxbbaZsJxiW0RsvtqOuYwlVMYTg2oGGAAAAHi9jbEwrUmB5ds7Y2RV+J4LglBZZPlWR2jptQqmmqYy5UyS52pW8+72I7WmWkrefdUlttK3FJSYcxrxDeYN5zm5Y7P3/Ysya7Hbae1jmtMelElD+Naxxk3YOI1kltD0ltu1mRjXY2JIcW0dpOkm2ZNmhJWxGELojCMGk4MrP3qxXHGblvIjcPJ21gL+dvUGBlr7GZjzRpafz/ZEhp6a7AfUk0uvUWFUktqUhJkpCblgz8iyI41TowQrnRgu5iCtH94gmknNg6zj7Doofnsn1oUmXMSyjq/YYbJIl3LJkku51dM80ia33H0bZTI7S7lj8SdN3mir445v6OOdz2tviHh+K664pjyVzI16b9OjTM2Kopv046KbcX8IxrfNmqJqo241x4yCjyjpx2Wm7Uv7R5EiI+xKivvRpUZ5uRGkx3Fsvx32VpcZfYebNLjTzTiSUlSTJSVERkfUW2Lt/LXqMzlq6reYt1RVTVTM01U1UzjTVTVGExVExExMTExMYwbUrEnEjkRD31rxhVpIjtbCxZuPW5jXpNCHJiiR2QsmisJJBFCu0NmaySRJZlJcbIiQTZq70ejJz5ZXnn4Goq3jct08cbupptZ61GETXowt5uinR+934iZqiIiLd6LlGEU7E1apYuxdo0+XjW2uH6TXsTbo03iO8cIsMMyyOSfOEqRSXTLLblljlwlBpjWtctfafVBn2vNdyUSGTU2oyI+pcZ87PNVwxzwcIX+E+JbcRtRNWXv00xN3K34jyF61M4atVdGMU3KJqoqnCcYhcopuU7Mq7+6NJZzovLXsWzOCRIdJb9JfQ0urpchr0q7SmVslxtB+cbMyJ5hZJeYWZEpPRSFK6Mudjmh4w5neJquHOK7MbNWNWXzFGM2MzaicNu1XMRpjRFy3VhXbqmIqjCaaqtKuUV2qtmpiDyji/ZQ2pPKGybUth9Ucp926cRHhYrlz8zH4/QkYpkqDvceQ2XUyZiRpDiZlQ13H3GUF+L3H8fr1Mc582vSM53eauijJ8N70ru7jo1ZPNR6Yy0R5mimqYrs046ZjL3LOM68dKyi/co1To6je7D/ABPoammWc/1bJafSn8vssPumn2nl+Tr5mkumY64yfj/HsHTH7K4W8IZlpt02uNeHLlN6I8ldyOYiqKp/zcvfppmmOzma30052P2VPaZur/Ea48zEd0hrP6k+wldlhjUNxZKM+ht9au7skd5F5T8vb0/BHL2S6d3Mdmqdq/TvrLThjhcylEz2P3nMXYx8XDrrIzdrrvpfdDeOH6J5d9Ksr+7j7vz4+Yb743n6zr7pn03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aPuhvHD9E8u+lWV/dw/Pj5hvvjefrOvuj03Z6/aah80eVGpt4atocTwOZeSLivz+ryKSizpXq1gq2JjuVVrykPuOLSt4pVsyRI6dTSZn+APzB0sukbzac8HN1kuGuDLucr3pY33ZzVUXbFVqnvVGVzlqqYqmZxq271GEdTGeRRmL9FyiIoxxx/XRjeUdeuy+TallvQPX4d9KfXb1x78aYcm8ylP/ADl4S+U+6/Z1hO1M98p+ujx1n0ehxrAAqfX/AF9vbr9NrL82PDzU76p/pnN+qrvo6mizM4z2XyPKNM2WNqVjjhh/Jj1P+lNx76L0d8XRQ0dHvhr1Lf8AZeYarl/4GnsNoB+h1wAAAAAAAAAAACMjxaOC8fnnxDy7X1HEYPcGBur2RpOc4qOwp3NaWDKafxKTLfNtLVZndK+/Wr73W2GZjkWU73FFJJ5icJZpnCXNWuae2x23tcfv6yfS3tFZTqe6prWI/AtKm2rJTsKxrLKDKQ1JhT4ExhbTzTiUuNuINKiIyMhYufOATy+Ex41GacFTi6U3VEv9mcXp9guRXw691ubmmnpk1156fOwZqwlRY1pi9jMe89OpXX2UIeNcqItt5chqXiYx7KNVOPZXkOPnKLj9yqwyPnvH7a2I7Nx5xplcwqCyQd3QvvoJxEDKcZllGyLFrMkmRnGsIsd7oZKJJpMjOvDBXMTGtnwGABGbzj8WPiHwUqrevzrOIeebgixl+1WjtfTodzm789bZqiN5S6yt6s1/WrUpKnH7VbT6mDNcaPKWRNqzETLMUzKg3zu577y8QHcD20NvWDNfTVKJVZrjWdG/L+c7XGOPuocVAqGJKzcn3VmbDblpaPEUmwfQnyNR2o0aPZEYLYiIaSAy3O4CcN82508nNf6HxNmbGp7Gai92VlcZhTrGD60p5EdzKcjkum24w1KNh1EOuQ70RItJcZkzInDMkzhDEzhGLp7YFg2LaxwfD9cYNURqDDMCxiiw7FKOGRlGqcdxusjVFPXs9xmtSIkCI2juUZqV06mZmZmKlL1oAAAAAAAAAA53PrBn9KNur6ENNfwV4qLKdS2nyqFQZSWyfVW/8PuZv0H6V/VrY4jUhXqXKBBWAAAAAAAAANUubfFTEuafGTaXHfLXGYCc1pCcxjInI/pDuI5zTPN2uH5QwhPR80Vl3Fa9KbbUhcmCt+OaiS8oZicJZicJxcv3bOq870fsvN9RbOoJeL59rzIrHF8oo5hflkOzrXjaWth5P5VNr5rXY/FktGpmVFdbeaUptaVHYtY9BlLP4Xviw7a8OnMJVM5DmbJ475hZNzM71Q9PTGfrrJZRo7ub6/myCWxTZazBjpbeZX0hWrKEtSOxxuPJjYmMUZpx7K+bxQ53cWeamLx8j0BtagyawKEiZd4DYSWKXZeJ9SQl5rJcHmvFcw2o76/NemMofrn1pM2JDyeijhMTCuYmNbb0YYAGkHMLxE+JvBzHZlpvTZ9VEypMBc2k1RjD0XItq5Qs2vORGavEI8pt+BGnH0SifZuQKtKjLvkoGYiZZiJlQp8SrxRt1eIxnkVzImlYFpDDrOTM1ppysnrmQKuQ4w5CPKsusUtRSynN5MJ1xopKmm2IEd5xiI02TshcicRgsiIhGGMpMz8eNCbF5P7p15ofVNV7bZzsjIY1FVNuG4iBWxzSuVb5BcvtturiUON08d+fOeJK1NxY6zSlSuiTamJnDS6ifGLj7hXFXQOq+Pmvm/8A4Y1diUDHmJ64zcSVf2pG5NyPKrGO0t1pq1yzIpcqylJSo0FIlLJPypEKp0qpnGcWdwYAAAAAAAAAFNf1qT/D7hl9B+6v1a1wJ0rKNSpsJJp2/Vzf6SzF/rQbY/UmCMVakatToLCtUAAAAAAAAAKg3rWP/gN/80X/AKdhOlOjlVBxJYmr9Xz/AKUbSv0Ibl/gryoYq1I1eVdEYVqgAAAAAAAAAAAABEX45+2kal8MvkM4zI8xb7HjYpqWlR3mj0pecZTVxsij9xEZ/wCA8S1X0+Mrs6H0I+ozTrSp1ucELFr9dfXzbafBq62K9NsbKZGr4EKOg3JEubMeRHixWGy8q3pD7iUJIvKajIgHWR0VrKDpXSWn9PVnmzgar1hgeuoi2iMkOs4Zi9XjqHy7jUtSpHtd3qUozUpSjMzMzMxUonWyqAAADSzxDuLULmTw73foZUWM/kmRYlKuNdSJKyYTX7NxUyyHBJBy+qVQ40rIIDUOW4R+WDJfQZKSpSTzE4SzE4Ti5dMyHMrpkqvsIsmDPgyX4c6DMYdizIcyK6piTFlRn0oejyY7yFIWhaSUhRGRkRkLFz84Do4eBzxPLixwF1s5dV5RNhb3Mt6ZybjSkTIzeY10BOD0bxukUhlNPgUOvU7GUSUsWEiX0SSlrNVdU4yqqnGUwIwiAAD+bzLMll2PIaafjvtOMvsPNpdZeZdSaHWnWlkpDjTiFGSkmRkZH0MByfuSmq16M5Eb10ytDqU6q29sXX0ZTvnDU9BxLLbakr5ZLdUtxxuZBhNuoWald6FkrqfXqLV0amEwZXyfVoduJzXgpl+sZTxnZaX3bk9fFj95LS1iud1dPmVW+RdSU0b+TS7tJo6dv5USiUZqUlMKtaqvWsUCKIAAADF+7NP4PyA1JsXSmya322wbZ2JXGH5HEQaESUQbeKthM+ufWhz0O4qZJty4UgkmuNLZbdT8sggNTl4cuuL+xOHHIPY3H3ZkRxF3g9y61VXaYr0aszPEpilSMXzWiN01E7UZHUqbeSRKWqM95yM72vsuoTbE4ronGMWtoMpC/Dx8SDePh27Qey3XbiMr11lD0BjaGoLqfIi43m1bDeI0TIcltqX87WZV8ZTiIFu0w+pgnFIeZkR1LYXiYxRmIlff4WeJ3xE50UdcrUux6+o2M7CRIu9LZxIiY7s2kkpQRy2otNIkHHy2uimZGc+mdnREpWgnVNOmbSYTEwrmJhIOMMADWPk3zI418PcQkZjyD2xi+BxyhPTKjHZM5qbm+VqaJ4kRMRwyEp7IcgfffZNrvYYOO0ryvONIJS05iJlmImdSiZ4rHjDbI8Qi5TrvCYFxq3i/jtr6dTYLImt/PLsKyhvGdblez3a592vW9EJJOwaaO5IhVzqjWp6W+luQicRh2VkU4dlC6MpPUYRhWV7IzHFtfYJQ2GUZpmt/U4tiuOVLPn7K8yC8nM11TVwmjUlKpEybIQhJqNKS69VGREZkHTi8OXhnQ8EeJ+vND17kWflLTbuYbVyCGRkzku0sljQVZRYMKMkqdr61uFGqoC1JStVdXRzWXf3CuZxlTM4zi3nGGH83Wmn2nGXm23mXm1tOtOoS40604k0ONuNrI0rbWkzIyMjIyPoYhct271uq1dppqtVUzFVMxExMTGExMTomJjRMTomBXX5e8fpGh9lyU1cV74P8tclW+Gy+1SmYaFOkuwxh11RqP0mhdeSlvuUpTkRbKzM1msk9GPSd5kcxzOcfXKd3W6vgTvOqu/ka8J2bcTONzKTOnyWXmqIpxmZqs1Wq5mapriNGzNnvVejyk6v1mp3eXs/F+GPzbsy+dkzUe2cp0xnNTnWIyPNzq9fmZsB81HAu6l5aDn01k2kyNyHMQgvKXRbTiUuINK0JUW/+bLnH4k5qeMMtxjwxcinOWJ2blurHveYs1THfLF2I10VxEaY8lRVFNyiYrppmLLdyq1Xt061jnTO5MN3jhUHM8PmEpDhIj3NO84g7PHbcm0rk1Nm0noaXGjV1bdIvNyGjJxBmkx3uc1fOnwtzu8J2eKuGLsTRVEU37FUx33LXsImqzdjqxrprw2blGFdGMS1m1dpu07VP/cywOSFjwWyNZYTtrF5mI53SR7qolfljRr6tTq2YlKks2VTOb6SK+wY7j7XGzLuSZoWSm1KQrZfHvN9wlzl8PXeGOMsnbze7LmmMdFy1XEYU3bNyPJWrlOOiqmdMTNFUVUVVUzCuim5Ts1xjCEjkFwY2PqZ2df4WxO2HgLZuPlLr43nsno4xGtfZe00VPfKZjtF8vNiIUz0Sa3G45dEjqQ57Oh5x5zbXL2+uE6bu/OC6Zmrbt045vL0aZwzFiiMa6aY8tfs0zRhE13KLMYQ0y9la7emnyVH6rRbvL2fi/DH4/wBmXyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzId5ez8X4YbMh3l7PxfhhsyHeXs/F+GGzIy5oBZHvjSfx/qua39+VN7I5M5lYn55OEvlNuv2dYWWf4Wn66PHWgR6F2ugCpzfrL2+u/j/8r2X5se9kea/fVM+3Ob9U3fR1Nvzrnsvkd5ez8X4Y0zZlhY84X+XjFqY/70XHvpvR3u9FH9Hzhr1Nf9l5hrWW/gKex9VtCP0MvAAAAAAAAAAAAAFU/wAd7wgLLZ7mQc2+LmKuT8/jQjn7+1fQRjXLzaBWRe1W0MQq47ZuSsxgQWUouYLJd1pHaTLZQc5Mn02VM8kp01ckqX6kqQpSVJNKkmaVJURkpKiPoaVEfQyMjLykJrH+APV4Xnmca3v4uV67zPK8CymCSkwslwvIrfFr+GlakKWmLcUcyDYxyWptJmSHC6mkvkAJEsJ8Z7xPcAhx4NFy8z+ezGQpDa82pNf7KmKSpppkzkWGxsPyqfLWSGiMlOurUSjUoj7lKM8YQjswxntLxQfEH3LWP0mf8t9zzKWYw5En0+O5OrAqqziPIJt6JbV2AMYxDtorqE9FNSUOoV5TMupmM4QzhDRBxxx5xbrq1uuurU4444pS3HHFqNS1rWozUta1GZmZn1MwZf8AIDKOltLbO5DbOxHTuncRs832Hm9m3V0FBVtpNxxw0qdlTp0p1TcOqpqqG2uTNmyVtRYcVpbrq0NoUojGp0bvC78OHBvDq0SjEmHq3Kd0557X3e6tjxI5pbubqKy6VdiuOOyGGJ7WDYeiU81BQ8SHJL70iY4hpcnzDVcziqmcZSZDDAAAAAAAAAAAOdz6wZ/Sjbq+hDTX8FeKiynUtp8qhUGUlsn1Vv8Aw+5m/QfpX9WtjiNSFepcoEFYAAAAAAAAAAK/3jbeEqXNXDU8gdDU0VnlHrqjOHLpWFMQWt24TX98hrGZjrvm46c6x5KnDpJbimyktLVAkrNv0NyHKmcOwlTVhonUoQXFNb47bWdBkFVZUV7Sz5dVc0txBlVltU2kB9cWdW2ddNaYmQJ8KS0pt1l1CHG3EmlREZGQmtfOAfTpru5xy1g3uPW9nQ3dY+mVW3FNPlVdrXSkdSRJg2EF1iXEfQRn0W2tKi6/HASFa/8AF38SnWcONAxnmDtmZFhrjrjpziXS7QcQUVSVMtKkbMpsukuxi7SJTS1qaWj5VSTT5BjCGNmH49ieLR4kO0qyRT5XzA2+xXyu8pLOF2tfrJchp01m7Gek61rMSlOw3kuKQtlSzaW2fYpJo8gYQYQj7srKxuZ8u1t7Cba2lg+5Kn2VlKfnT5sp5Rrdky5kpx2RJfdUfVS1qUpR/HMZZfiAffxXFclznJaHDcNobbKcsym2gUOOY5QwJNpdXl1aSW4ddV1ddDbdlTJ0yU6lDbaEmpSj6EA6DHg1eFNXcBdayNkbViVltyn2fUsM5bLjPMWUHWOKOrZnR9bY7YNEpmROVIbbevZrClMS5jTbLKnI8Vt9+Ezj2FVVWPYTdCKIAAAAAAAAAAKa/rUn+H3DL6D91fq1rgTpWUalTYSTTt+rm/0lmL/Wg2x+pMEYq1I1anQWFaoAAAAAAAAAVBvWsf8AwG/+aL/07CdKdHKqDiSxkLVu2tm6RzOu2JqHO8o1tndSxYRazLcOuJlFfwI9rCfrbJiLZQXGpLLU6BJcZdIldFtrNJ+QwYbV/dP/ABEvt0+R/wC6plX5/DCDCD7p/wCIl9unyP8A3VMq/P4YQYQfdP8AxEvt0+R/7qmVfn8MIMIPun/iJfbp8j/3VMq/P4YQYQfdP/ES+3T5H/uqZV+fwwgwg+6f+Il9unyP/dUyr8/hhBhDcjw8vEN5z7D5z8TcGznllvnLMOyzfOuKHJsZvtj5HZUt7S2WRwo0+rtIEmatiZCmMLNDjayNKkn0MYmIwYmIwdDwVqgAAAFUz1pXbhVup+LWiY0kzcy/YGZ7VuIrbik+aj6/x+JilEuU2RES25z+xZxteUy7oijMupEYlSnR1VMETWN9vC51Kjd/iFcR9fPx/TIL25cby+4hGg3G5tBrApOzshhPJSaVFGmUeHyGnVEZKS2pRkZGXUYnUxOiHT7FakAAAAAc6rx3eJh8Yee+eXtHXeh665FMr3fiCmWVphxLvIZ0hjZVEl3oUcpMPOmJU9LDRJTGgWkRHTp0M7KZxhbTOMNTvDV4qv8AMvmlpHSEiGcnEJ2St5Zs1w/OoYj6ywpPzw5gy8+wSnIrt9AhFVRXOnQp1gwRmRGZlmZwhmZwjF1AmWWYzLUeO00xHYabZYYZbS0yyy0kkNNNNIJKG2m0JIkpIiIiLoQqUv6AAAAAOdx6wJqVWrvEy2tbNREwqrcWI6621UNIbNttxNhjjWFX8tB9qScVOzLBbN5xRderq1F16kZFZTqW06kKoyktFeq6bcOi5EcjtIyZfm4ux9TUGwIEd1S+x241blBU6moqTPzaJL9Tst9xZERGtuL1M+jZCNWpCvUuziCsAAAAARFeLf4X+M+Ihp5ibi51OM8ktZwpj+qcynJTGg3sJxZy52tcymNMuyTxq6e7nIj/AEUqqsV+fQRtOSmX8xOHYSpnDsOdxsfXGdahzrKdZbNxa4wnPsJuJVDlOLX0VUS0qLSIovOMvN9VNusutqS6w+0pxiSw4h1pa2loWqxY8UDL+8WVKgyo02FJfhzYb7MqJLivOR5UWVHcS9HkxpDKkOsPsOoJSFpMlJURGRkZAN+Na+Kl4iepKxFNhfLzcqKtpn0eNByrIW9iR4Ufq2aWK5Gw4mU+1zLfmiJCWPNpQkzJJESlEeMIYwh6zOvGJ8TTYkWVDv8AmDtCvZmed86vBUYvq+UjzyUJX6LO1pjuJToPQkF2eZcb82fU09DMzNhDGzCO7IslyPL7mfkeW393lGQ2rxyLO+yK1nXdzYyDIiN+faWT8mdMeMiIu5xaleyMpPigP0Q4cyxmRa+viyZ0+dJYhwYMNh2VMmTJTqWI0WLGYSt6RJkPLShCEJNS1GRERmYC8z4GPhEyOMdLA5a8k8a9G5CZZTut63wS6iIOZpbEriMpuTaWjDqnDhbKyyte8082aUSKaucXFWaZEmYy1CqcdEK6qsdEallARQAABibdensY3jr+2wTJkeaTKIpdNbtNIdmY/ex0OFX28MlGnuNlTikPN9ySfjuONGpJL6lxrzs81/D/ADu8F5ng7iCNmm55OxeiImvLZimJ73eoxwx2cZprpxjvluqu3MxFWMV3bdN2iaKv+6VbPZmt8r1Lmdxg2ZQFQbiofMicSThwrOC4ajh29W+tCPS62e0nubWREZH1QskuJWlPQ9x9wDxFzbcVZrhDimzNnemWr1xjsXbc47F6zVMRt2rkaaasMddNUU101UxoVymu1XNFcaYeC6jZuzCG1LL+ld3ZvonMGctwyWjo6hMW7oppurpshrSUavQrKO042rvaUo1sPINLrDnlSfapaVcnc0/OxxbzO8T08S8K3YwqiKMxl7mM2Mzaxx2LtMTE40zMzbuUzFdurTTOE1U1WWr9dqrap/71g3RPIbX+/wDHfbbE5vol1Bbb+eLEbB1tN5Qvr+VJTjaehTqx5f8AvMtojac/sT7HCW2juu5nue7grno3H7ZcN3e971s0x6ZydyY9MZeqeWYj+EtVT5S9R5CrVOxciqinWrN+i/TjTr5Y6jOw5hXADV/bnD/R24XJVjc4z87uTSzU47lWHrZpbV99SiUp+wjlHkVFs86ZES3JMZ140+RLifjl+e+czowc0XOhXcz29d3+kd/3JxnOZKabF6qqdM1XKdmqzeqnVVVdtV3MNEV061FzLWrumYwq6sI7c+8M3ZVQt6TrzMcczKEk1Kag3Lb+L3faZdUtI6qtKiQtB/KmtcmMSvIfaXUyL8PcZ9APjvdlVd/gjemR3plYmZi3firKX8OSI/hbNUxqmqq7aidezGOEfDXkbkfwcxMdpqtkXE3kfi7jjdhqDMZnmz6d+OwE5Y2su8kEpteLvW5LIzUR/JIvKZERH0/O2/OjZz58P1zbz3DG9LuE68tbjOROnDGJylV7HX2tM4YTh89VjMU66J8TT4zGMvWOyoC0tz9eZxCcURmlEvEr+MtRJPtUaUvQEKMkq8h/IMcf5nm848ydUUZzce97Vc6oryeZpmfEqtwrmm7Gume1L8nzgZ57ict+lu5/OY+b4EcYe5G8/Wt/uGMLnUntHzgZ57ict+lu5/OYfAjjD3I3n61v9wYXOpPaPnAzz3E5b9Ldz+cw+BHGHuRvP1rf7gwudSe0fOBnnuJy36W7n85h8COMPcjefrW/3Bhc6k9o+cDPPcTlv0t3P5zD4EcYe5G8/Wt/uDC51J7R84Gee4nLfpbufzmHwI4w9yN5+tb/AHBhc6k9o+cDPPcTlv0t3P5zD4EcYe5G8/Wt/uDC51J7R84Gee4nLfpbufzmHwI4w9yN5+tb/cGFzqT2j5wM89xOW/S3c/nMPgRxh7kbz9a3+4MLnUntHzgZ57ict+lu5/OYfAjjD3I3n61v9wYXOpPaPnAzz3E5b9Ldz+cw+BHGHuRvP1rf7gwudSe0fOBnnuJy36W7n85h8COMPcjefrW/3Bhc6k9o+cDPPcTlv0t3P5zD4EcYe5G8/Wt/uDC51J7R84Gee4nLfpbufzmHwI4w9yN5+tb/AHBhc6k9o+cDPPcTlv0t3P5zD4EcYe5G8/Wt/uDC51J7R84Gee4nLfpbufzmHwI4w9yN5+tb/cGFzqT2j5wM89xOW/S3c/nMPgRxh7kbz9a3+4MLnUntHzgZ57ict+lu5/OYfAjjD3I3n61v9wYXOpPaPnAzz3E5b9Ldz+cw+BHGHuRvP1rf7gwudSe0ytofCM0ibx0zKlYhlEaLG2vruRJkyKC2ZYjsM5fTuPPvvOREttMtNpNSlKMkpSRmZ9ByPzO8H8VZbnd4VzOY3XvG3l7fEe7KqqqstepppppztiaqqqpoiIpiImZmZiIiMZWWYr79TjE4bUcnXWXh34NeAFTTID/4+u/03svzY8PNrvqmPbjN+qbvo6m25qnF8jqNM2YNqVkDhd/Jh1N+lFx76b0d7HRT0dH3hr1Nf9l5hrmU05enxfHltEP0K+gAAAAAAAAAAAAAAFePxNPAR1Vyzsr/AHVxqnUGjt/2js23yWmkxJDWqtrXEk3H3593FrG35OD5XYyld8i1r40iPMcNa5UNyQ85LKUVYJRVhr1KX/JXh7yV4g5WrD+Q+o8r1zOdfdYqbewhpnYhkpNdyjexXM6pydi+RNebT3KKJKdcaLyOpQojSU8cVkTE6mtQMgAAAJSuEPhAcyub1hT22NYJN1bp2cqLKl7r2jW2VBisipddb85KwisfYZu9iy3I/nDY9rWzrjeb81Imxe4ljEzEIzVELzfALw0eOfh6YQ9T6sqnsm2PkENhnPtzZXGhO5vla0ebdcroSmG/MYrh7Utslx6mGrzZdiFyXZchJyFQmcVczMpDBhgAAAAAAAAAAABzufWDP6UbdX0Iaa/grxUWU6ltPlUKgyktk+qt/wCH3M36D9K/q1scRqQr1LlAgrAAAAAAAAAAAAQx+Jd4MGhefLc/Y+NvRNL8lm4SG2dmU9Yh6izz0JlLNfW7Vx+MbB3SmY7ZR2biOpu2isk2hapceOzELMTh2Eoqw7CkJy58O/lxwju5MDfGprmrxlMo41VtDG23sn1ZkCVPkxGdrszr2PQoMiapaTbg2SYFmRKLzkZBn0FkTErImJ1NJQZAAAAb+8OPDK5h84rOIrTOrrCLga5SI9pt/OEyMU1fUtkskSFtZDMjLeyaVE6l5yHSsWU1Hck1tJQfcWJmIYmYheG8NjwguP8A4eta3lzDnwsciLStdhX+4r+taiFSRprRNTqDWuPm9MbxCkfb6tvyDekWk8lLJ6QUdSIrUJnHsK5qx7CW4YRAAAAAAAAAAAAU1/WpP8PuGX0H7q/VrXAnSso1Kmwkmnb9XN/pLMX+tBtj9SYIxVqRq1OgsK1QAAAAAAAAAqDetY/+A3/zRf8Ap2E6U6OVUHElgAAAAAAAAA3t8MD+kS4Wfzj9V++qAE6mJ1OoIKlIAAACgZ6yLtxefeIWnX7EtDlfo7TuBYa5DaUSkM3+UlZbMs5TvlUaZkmpzKtaWXUkk3Gb+VJXcarKdSyjUgBGU1jj1ZfUqsw5ubD2nLiJerNO6QvDhyzbNZw8u2DfUuOVJJWaDQ0cnFY16nr3EsyIyIjSaukatSFepe5EFYAAAAAgT9Yc4mq3/wAIZG3Mdq1Tc+4t3L2xY647JuzJGs7dmNU7Rr0dDIm4sCExBvZDh9exijWRf2RiVM6UqZwlqR6sXxSPFdUbe5hZJV+at9qWp6m1pKkNLQ+nAMNnM2Gb2kB3tJt6tybO248JflNSZGNKLokvKpVPIzXPItRiKAAAAAAp7+tN6jNu04mb4hRO4pUDYeo8kndqy82dfIpcywiJ3kRtq88VnkC+hmky835O4jV2TpToVFhJYlK8FnbStPeJhxbtnZZRq3NMxnaltGluG2zPTtWgtcJpIjpkpHeacst655pPX5Z9lBdD+MMTqRq1OlOK1QAAAAAAIxfES8Krjt4h+NplZjHc17uujq1V+F7uxevjSL+vYbN52JSZbVOOxI+b4i1KeU56G+8xJYNS/RJUY3HTXmJmGYqmOwo1c0vCl5kcHrK1l7H1vPy/V8NbrsPdWt4ljk+una/zriGH76axDRPweYtKC7o9wxE6r6kyt5HRxU4mJWxMSjdGWQAAAG23Fjgvyo5nZEih496hyXMobUpMW4zR+OVHrvGj+UU6eQZzbnEx2C+ywvzpREvuT30JPzDDquiTTMQxMxGtdk8MvwONK8IpNLtzbcyp3lyWiNMSYGQuwHD15q+wIydWvWtLaMNy5t5Hd6JTkFg03NImyVEj1/e8lyE1Y9hXNWPYTriKIAAAAA1n5L8acW5EYp6JKNimzelYfViWVk0alxHV/liqq2S2XnZlDNdIvOI8q2Fn51ry9yHOBOfvmF4d57uHPS2Y2MrxZlaKpyecw00TOmbN7DTXl7k+Wp01W6v3y3p2qa/nzGXpzFOE6K41T/lyK7+e4Lles8rtsLzSpfpsgpnzZlRXi7m3W1fLR5sKQnq1Mr5jXRxl5szQ4gyMjHSTxhwZxHwHxFmeFuKctXld85WvZroq1TGumuirVXbrjyVFdONNVM4w0GuibdU0V6KoeP7v9gbZ2JQ8i9FimYZNg19X5RiF3YY9f1bvnYNpWSFx5LRmXa40vp1bkRpDZmh1lxK2nmzNC0qSZket8OcRb/4R3zZ4g4Zzd/I75y9W1bu2qtmqOrE8lVFUeRroqiaK6ZmmumqmZiZU1zRVtUTMVQmE0B4jGN5EmFjO8Y7GKXh+bjsZtWsPKxeycUsm0KuIKPPysekr7k9zqPPQjPvWo4yCJI7OuZnpv7k33Ta3Dzt26N3b20U0561TM5S7OOEd/txtV5aqdGNdO3YmdqqqbFMRS1WxvCiryN7RPV5P+xJnWWlZd18S2prGBb1U9lMiDZ1kyPPr5sdfXsfiTIrjsaSyrp5FIUaT+SP3tkN4ZDeuSt7x3Xfs5nd96mKrd21XTct10zqqoromaaqZ5JpmYajExVGNM4w/ePsZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFS7IFf8fXfxv+V7L82vDzdb6on24zfqm76OpterZ2p7L5Hd/sDTdiWPIrIvCw+vGDUp/wB6Lj303o71OirGHR/4bj/Zr3svMNwZP+bU4dT6stox+g30gAAAAAAAAAAAAAAAPPZViOKZ1RWGLZtjGPZjjNsyqPa45lVLW5DRWcdRGSmLCoto0uvmMqI/KlxtST+QAij3F4FHhnbhky7NWhPgwupvnzcs9O5XkODRmlPEo0qiYk1NsMBhejrUakE1UoT8ZKiUgiSWdqUtqWlt36r/AMJZcgnaDdXKGmZUt9b0afkOq7ttPnFkplqG43qmpeZZYT1T+WqfWouhmrqRmedqWduXo8U9WT4D0j0aVkWwuTeaOtINMqBYZzr6npZS/PpcStMeg1bX3cf8pR5syKxURkpRl0PtNDak25SP6E8KPw+eNsuDb6z4x6/cyaudbkw8uz1mx2hk8Oc117bGqtNhzslVQTS7jIl1yYfQj6EREMYyjtTKQ0YYAAAAAAAAAAAAAABCFzV8Czjzzg5CZXyL2Bt3c+JZRltZi1XNo8OcwhNBGZxTHa7G4bkUrvFLWx85Ji1qHHe95RecUfaRF0IpRVhGCUVYRg1S+9deIv2wXI/+3ay/YGG0ztyki8OzwptOeG9c7Tu9WbF2ZnUja9ZilXctZ+vFls1zOJSr2XCcrPndx+kWTkld84TvnTcLohPaReXriZxYmrFKQMIgAAAAAAAAAAAAD8dhXV9vBmVdrBh2dZYRnodhXWEZmbBnQ5LampESZEkocjyYz7SjSttaVJUkzIyMgEXm6vBY8NfeUuVbXvGrGsIvpTinV3OobK81Z0cc8ryzx7D7Cuwx9x5fyylvVjizV1PqRqV1ztSltS0PyL1YfgtYqcex7bPKLG3XHWTTHcy7WF1WMMoZJt1tliVqWPZ+dfcSThrcmOElRqIk9ppJOdqWduX98a9WL4H1bkWRkG0eUOUvMrcVIiOZnrSmp5iFGrzSHI9bqYrhnzaDLqbc9JqUXXyEfaG1JtykB0b4OfhxaAlQbbEeMuHZPkUA23W8j2tIt9rzjltJJLc9muzufdYzWTmlF3oXCgRfNuES0ElREZYxlGaplJhFixYMWNChRmIcKGwzFiRIrLceLFix20sx40aOylDTDDDSCShCSJKUkREREQww/uAAAAAAAAAAAAAAIt/ET8KbTniQ3OrLvaexdmYLI1RWZXV0zWALxZDNizlsqilzXLP54sfu1m5GXQtk15o2y6LV3Efk6ZicEoqwRu/euvEX7YLkf/btZfsDGdpnbluDwb8EDj/wP31X8gNdbZ3FmGS12M5Ji7VLmrmFKo1w8mjMxpchwqLFamf6THQyRt9HiT1PykYTVixNWMYJqRFEAAAAAAAAARleIx4XWovEm+B34VNg7HwT4GPhC9ofg+XjCPbX4RfnH9tPbf546G76+g/OJH9H8z5r/fnO/u+V7cxODMTgjK+9deIv2wXI/wDt2sv2BjO0ltyfeuvEX7YLkf8A27WX7Aw2jbk+9deIv2wXI/8At2sv2BhtG3J9668RftguR/8AbtZfsDDaNuT7114i/bBcj/7drL9gYbRtyfeuvEX7YLkf/btZfsDDaNuT7114i/bBcj/7drL9gYbRtyfeuvEX7YLkf/btZfsDDaNuWYOPvq7XGLjvu/VO9cY3dvi7yLUudY7ntLUXzuvjpbKyxuxZsosO0KvwyFOOE+6wSXPNOtudp+RRH5Q2ia5mFgoRQAAAAQE8mPV8+OnKXfW0OQWeb437AyvaeTv5Ja1tM/r86iqJUeNAgVFV7Y4ZNnFW1NbCZjME684smmkkajEoqwSirCMGC/vXXiL9sFyP/t2sv2BhtM7cpQvDr8MPTPhuV21oeqcwz3N5m3ZuIScits/cx5ybFjYSxkLVLXVxY9SUrCIqXcomOr70rWpbheXokiGJnFGZxSUDDAAAAAA+JkuOUeY45kGI5PWRrrGsqpLXHMhp5iVKh21HeQH6y2rJaUKQtUafAlONOERkZpWfQyAY80FpPCON+l9Z6J1xGfjYXq3EanEqM5i2nbGc3XMEUy5tnmGmGH7m+sXHps1xDbaHJUhxSUJIySTWTOOll4AAAAAAaPc/OB2r/EM0xS6W2pkWXYlU49sCn2NUZBhC6Vu+i3VRSZHjxRu69qbiEuumVmUSUuo80SjWTaiUXZ5cxODMThKHb7114i/bBcj/AO3ay/YGM7SW3L1ODerS8Xde5th2fY9yG5GNX+D5Tj2YUbrj2tfNtXGNW0S6rHF+bwVtzsRNhIM+1ST6F5DI/KG0bcrIIigAAAAAAAA/4cbbebcadbQ606hTbrTiUrbcbWk0rbcQojStC0mZGRl0MgEcO9vCM8OzkTKmW2fcYcEqsjnPPS5GUa09tNUXciwkKcW/ZWS9d2GOV9/PfW8pTjllHmecUZKURqSkyzjLMVTCPHKPVkeBFy9MlY/sbk9h7r3U4kCFnGu7elhGb6nDL0e81TNu5CEsq82kl2PUiSkzNR9xqztSlty+Tj/qwnBuAtL2Q7g5RZAtt/ziY8bKtXUkB1jsIvR5Tbepp89Zmvqfe1JZPp0Lp5DM21Jty3e0x4H/AIaWlZUO0g8dqzY17CcbdRb7jvr7ZLLymnPOtlJxO7nHr91KVfH/AOKC7yLoruIY2pY2pSpUdFSYzUV+P43TVWPUNTGRDqqSjr4lTUVkNrr5qJX1sBmPDhRm+vyrbaEpL8AhhF9UAAAAAAAABr/yA45YHyExoqrJo512Q1zTx4zmEFpB21FIc6KNtSTUhNlUSFpLz8R0+xZfLIU26SHU8M883Mfwfz07i9rt/Ud431Ypq9K523Ed+y9U6cJ1Rds1T/CWa5wmPJUTbuRTcpov5ejMU4VaKo1T1EAG7tAbG0JkKqbNaozrZLziaLKq8nH8ev2Ul3EqFLUhJsTEI/32K8Tchvp17TQaVq6audfmZ435nt9TuvinLzORrqmMvm7cTVlsxTGnGivCNmuI8tarim5Tr2ZommqrQL9i7l6tmuNHJPJLCPcXsfijinBRjPUO4vY/FDAxnqMs6w3ptXTk30rXuZ2tGwt4npdMbqJ2PWC+iUrVOoZ6JNW+8tsuwnvNE+hJn2LSflHI/AHO1zi82Ga9McFb0zOUszVtV2MYuZa5OqZuZe5FVqqqY0bexFymPKV0zpW2sxdszjbmY8btJHNbeKCkkMQtt6+UpaSJLuQYFIR1cMuiSNeNXsttKVH/AGS1osunX+xbLyEP3FwL0/MKKMrzkblmaojTmN31Rp7OVzFcRHVqqjM4eZtxqajb3pyXqfFj9af127GG8y+Nmats+hbQo6SU4X5ZBzApOJvR1mZdGnJd4zDqnVGRkfVmS6jy9OvUjIv1Xwx0oOYziqin0rv/ACmUzFWu3ndrJzTPUmu/TRZmevRcrjkxxiYj7aM5lq9VURPX0eO2Ip8ix/IWfSaC9p7yOaUrJ+ns4Vmz2LJKkr87CffR2qSsjI+vQyMhzVuzfW5t9Wu/7nzeVzdnDHas3aLtOE6YnGiqqMJxjtvpiqmrysxL7A1NkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVKcgUXt9d/G/5Xsvwf/fXh5wN9R/TGb9U3fR1Np1TO1OjlfI7i9j8UabgxjPUWSeFXl4v6k/Si499N8O8/or/4AcN+pr3svMNx5L+a0difHltIP0E+oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAecyzEMYzuhn4vmNFW5HQWbfmptXaRkSI7nTr5t5vu6ORpTCvlmnmlIeZWRKQpKiIy0PiPhrcHF25724OJspYz2579OFdq7TFVM9SY5aa6Z00V0zTXRVhVRVFURKNdFNdOzXETTKIXf3hvXlS5NyXRE1V9Vfl0l3ArmWhq9gp7jX5qguJKkRblhCDMksylMyUpQRE5IcV0Lra54eg9vTd1d3fvNHcnObv01Tu+/XEX7ca8MveqmKL1MRqouzRdiKYiK79dWjR8xu2uPJZecY6k6/E6v+WtF9eUN5jFrMo8jqLOhua9w2Z1VcQZNbYxHSLr2SIcxtmQ0oyPqXckupH1LyD8D723NvTcO8Lm6d95W/k96Watm5ZvW67VyiepVRXEVR4saY0tKqiqidmrGKo6r5PX2f6o07Zhja6519n+qGzBtdc6+z/VDZg2uu/o086w4h5l1xl1tRLbdaWptxtaT6pWhaDJSFJP4xkfUhO3VXari7amablM4xMTMTExyxMaYk2uu9Mznebx2kMx8yyphlsu1tpnIrdpptP8AuUNomJQkvYIhr9ri3iuzbi1Z3nvGi1TGERTmb0RHYiK8IS77cjVVPbf1+EHPfdxl/wBMtz+fRP4Y8X+6u8vXV/u2e/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbk+EHPfdxl/0y3P59D4Y8X+6u8vXV/uzv1zzU9uT4Qc993GX/TLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf8ATLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbk+EHPfdxl/0y3P59D4Y8X+6u8vXV/uzv1zzU9uT4Qc993GX/TLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf8ATLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbk+EHPfdxl/0y3P59D4Y8X+6u8vXV/uzv1zzU9uT4Qc993GX/TLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf8ATLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbk+EHPfdxl/0y3P59D4Y8X+6u8vXV/uzv1zzU9uT4Qc993GX/TLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf8ATLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbk+EHPfdxl/0y3P59D4Y8X+6u8vXV/uzv1zzU9uT4Qc993GX/TLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf8ATLc/n0Phjxf7q7y9dX+7O/XPNT25PhBz33cZf9Mtz+fQ+GPF/urvL11f7s79c81Pbl5FS1LUpa1GpSjNSlKUalKUo+pqUZmZmZmflMbbqxqmaqsZqmcZmeVDa67/ADr7P9UY2YNrrrJ3Cj+S7qT9KLn31Xw7yuiz/gFw56mvey8w3Lkf5rR2J8eW0w/QD6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGM9k6b1jt6vTW7Fw2nyZtltTcSZJZXHuK9CzNSk1l5Bci29cla/KpLL6ErMvliMbE445suA+cjJRkuNd2ZbPUUxMUV1UzTetxOme9X7c03rcTOmYoriJny0Squ2LV+MLtMT4/b1o1tn+F8y4cmw0/n3mDMjWzjWdsqca7jUalIZyenjm622SflW0O17iuvTud+OY/DfHvQItVzcznNtvjY5actn6cYxx0xTmrNOMRhoppqy9U6tq5rlpV7dPLYq8Sf14/WaF57xJ5D66VIXeawyGwr2FK62+LMJyutUynr/wAKW5QrnSIUdRF8eU0wovISiIzIh+RuLujlzz8F1V1b13DnL2Tomf37K0+m7UxH7OZy/fKqKevdptzHLES025k83a8tRMx1Y0+M11kNSIj7saVHejSWFm29HkNLZfZcSfRTbrThJcbWk/jkZEZDha9lr+Xu1WMxRVReonCqmqJpqiY5JiYiYnrS+adqNE638e/2Pi/FFezLGk7/AGPi/FDZk0nf7HxfihsyaTv9j4vxQ2ZNJ3+x8X4obMmk7/Y+L8UNmTSd/sfF+KGzJpO/2Pi/FDZk0nf7HxfihsyaTv8AY+L8UNmTSd/sfF+KGzJpO/2Pi/FDZk0nf7HxfihsyaTv9j4vxQ2ZNJ3+x8X4obMmk7/Y+L8UNmTSd/sfF+KGzJpO/wBj4vxQ2ZNJ3+x8X4obMmk7/Y+L8UNmTSd/sfF+KGzJpO/2Pi/FDZk0nf7HxfihsyaTv9j4vxQ2ZNJ3+x8X4obMmk7/AGPi/FDZk0rKXCc+vFzUZ/3ouffVfDvG6LUYcwXDkf7Ne9l5hurIfzSjsT48tpxz++wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB5fI8IwvMGyZy3EMXyllKTQlrI6Cpu20oUXRSCRZxJSSSovjl06GNB31wtwxxJRFviLduQz9uIwwzOXtX4wnkwu0VRghVbt1+XpiezGLCdxw94zXilrm6dxRg3O7uKnTY48ku9PYfYign1iGuhfG7SLoflLoflHF28ujbzGb1qmrNcNbvpmrH+Bi5l40xhojL3LUR1sMMJ0xp0qKsjlKtdFPiaPGeIl8AeKskkEzrmXANBqNRxM2zpZudenQl+nZHNIiT08naST8vl6ja2Y6IHR/vREW9yXLWHmM9n5x7O3ma9XWw6+Kud25Of2OHiz+u/F9z34ue4u4+nPKfmoPl/M55hPcvM+vc3/Gse1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8AGntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/ABp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/wAae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8ae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8ae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8ae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8AGntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/GntZlPMz25Pue/Fz3F3H055T81A/M55hPcvM+vc3/ABp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/xp7WZTzM9uT7nvxc9xdx9OeU/NQPzOeYT3LzPr3N/wAae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8ae1mU8zPbk+578XPcXcfTnlPzUD8znmE9y8z69zf8ae1mU8zPbltTgWC43rTEaXBsRhuwMcx9h+PVxH5cmc6y1JmSJ7pLlzHHpLxqkylq6qUZkR9PjEQ5/4R4U3JwPw5leFeHLdVncuTpqptUVV1XJpiquq5ONdczVV5KuqdMzrw1Q+y3bptURbo8rD143GmAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/Z" style="width: 18vw;"/>
		</div>	

		<div id="chart" style="max-width: 950px; margin: 0 auto;"></div>
		<script>
		var info = %s;
		</script>
		<script>%s</script>
	</body>
	</html>`, infoString, mainjs)
	w.Write([]byte(index))
}

type scoreFormat struct {
	Timestamp int64
	Team      string
	Scores    map[int64]float64
}

func getTeamsInfo() (map[string][]map[string]interface{}, error) {
	files, err := ioutil.ReadDir("./")
	if err != nil {
		return nil, err
	}

	var raw []scoreFormat
	result := map[string][]map[string]interface{}{}
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".json") {
			content, err := ioutil.ReadFile(f.Name())
			if err != nil {
				return nil, err
			}
			var x scoreFormat
			if err := json.Unmarshal(content, &x); err != nil {
				return nil, err
			}

			raw = append(raw, x)
			if _, ok := result[x.Team]; !ok {
				result[x.Team] = []map[string]interface{}{}
			}
		}
	}

	sort.Slice(raw, func(i, j int) bool { // return true if i is less than j
		return raw[i].Timestamp < raw[j].Timestamp
	})

	for _, x := range raw {
		result[x.Team] = append(result[x.Team], map[string]interface{}{
			"timestamp": x.Timestamp,
			"score":     computeScore(greenTrainResults, x.Scores),
		})
	}

	return result, nil
}

func computeScore(base, result map[int64]float64) float64 {
	var total float64
	for k := range base {
		total += result[k] / base[k]
	}
	return total / float64(len(base))
}

func stepGame(team string, id int, r *http.Request) {
	allGames.Mutex.Lock()
	defer allGames.Mutex.Unlock()
	game := allGames.Games[id]
	if game.CurrentTime == game.TotalTime {
		if !game.Finished && team != "" {
			saveGame(team, game)
		}
		game.Finished = true
		allGames.Games[id] = game
		return
	}

	game = addPeople(game)

	game.CurrentTime++
	game.Time = game.CurrentTime
	game = updateIOTinputs(game, game.CurrentTime)
	var people []Person
	for _, p := range game.People {
		people = append(people, p)
	}
	sort.Slice(people, func(i, j int) bool {
		// first people inside lifts; if they leave the lift, people outside can enter without exceeding capacity
		if (people[i].Lift != nil && people[j].Lift != nil) || (people[i].Lift == nil && people[j].Lift == nil) {
			return people[i].ID < people[j].ID
		}
		return people[i].Lift != nil
	})

	for _, p := range people {
		p := stepPerson(p, game)
		if p.X < 0 || p.X > game.Layout.Width {
			delete(game.People, p.ID)
		} else {
			extraScore := computeExtraScore(p)
			p.Score += extraScore
			game.Score += extraScore
			game.People[p.ID] = p
		}
	}

	// lifts after people is arbitrary; this way, a person can enter a lift just before it closes, but must wait another turn if
	// the lift still has to open the door; the other way it's just the opposite
	act := getActions(r)
	var ids []string
	for i := range game.Lifts {
		ids = append(ids, i)
	}

	for _, i := range ids {
		l := game.Lifts[i]
		game.Lifts[i] = stepLift(l, &game, act)
	}

	allGames.Games[id] = game
}

func saveGame(team string, game GameData) {
	seeds := trainSeeds // TODO change for the end of competition
	if !Int64InSlice(game.Seed, seeds) {
		return
	}

	competitionLock.Lock()
	defer competitionLock.Unlock()

	m, ok := competitionMap[team]
	if !ok {
		m = make(map[int64]float64)
	}

	if _, hadPreviousValue := m[game.Seed]; hadPreviousValue {
		m = make(map[int64]float64) // reset game because they didn't end the previous one
	}

	m[game.Seed] = game.Score
	competitionMap[team] = m

	for _, seed := range seeds {
		if _, ok := m[seed]; !ok {
			return
		}
	}

	// game completed, save in file
	timestamp := time.Now().UnixNano()
	filename := fmt.Sprintf("%d.json", timestamp)
	f, err := os.Create(filename)
	if err != nil {
		log.Println("Error creating file:", err)
		return
	}
	defer f.Close()

	contents, err := json.Marshal(map[string]interface{}{"team": team, "scores": m, "timestamp": timestamp})
	if err != nil {
		log.Println("Error marshaling contents:", err)
		return
	}

	if _, err := f.Write(contents); err != nil {
		log.Println("Error writing file contents:", err)
	}
}

func addPeople(game GameData) GameData {
	for _, person := range game.NewPeople[game.CurrentTime] {
		createPerson(*person.Floor, person.TargetFloor, &game)
	}
	return game
}

func updateIOTinputs(game GameData, currentTime int) GameData {
	events := getCurrentIotEvents(game, currentTime)

	game.IOTinputs.PeopleGoToWork = false
	game.IOTinputs.BusStopsIn = -1
	for _, event := range events {
		switch event {
		case peakTime:
			game.IOTinputs.PeopleGoToWork = true
		case rainStartsIn, rainStopsIn:
			game.IOTinputs.Rain = getRainDataFromCurrentTime(game, currentTime)
		case busStopsIn:
			game.IOTinputs.BusStopsIn = getBusNextStopIn(game, currentTime)
		}
	}

	return game
}

func generateNewPeople(game GameData, currentTime int) GameData {
	game = updateIOTinputs(game, currentTime)

	var srcFloor, dstFloor string
	var firstFloor, lastFloor = "0", strconv.Itoa(game.TotalFloors - 1)

	if game.IOTinputs.BusStopsIn == 0 { // create some number of people in floor 0 to other floors
		totalPeople := BUS_MIN_PEOPLE_OUT + game.Rand.Intn(BUS_MAX_PEOPLE_OUT-BUS_MIN_PEOPLE_OUT)
		for i := 0; i < totalPeople; i++ {
			dstFloor = randFloorExcludingSlice(game, firstFloor)
			game.NewPeople[currentTime] = append(game.NewPeople[currentTime], Person{Floor: &firstFloor, TargetFloor: dstFloor})
		}
	}

	// create rest of people according to the probability in the probabilities map
	for _, peopleDirection := range peopleDirectionByEventsProbabilitiesKeys {
		probabilities := peopleDirectionByEventsProbabilities[peopleDirection]
		creationProbability := probabilities[getEventsStateFromInputs(game.IOTinputs)] * probabilityMultiplier(game)

		if game.Rand.Float64() >= creationProbability {
			continue
		}

		switch peopleDirection {
		case peopleEntering:
			srcFloor = firstFloor
			dstFloor = randFloorExcludingSlice(game, srcFloor)
		case peopleLeaving:
			dstFloor = firstFloor
			srcFloor = randFloorExcludingSlice(game, dstFloor)
		case peopleInRooftop:
			dstFloor = lastFloor
			srcFloor = randFloorExcludingSlice(game, dstFloor, firstFloor)
		case peopleOutRooftop:
			srcFloor = lastFloor
			dstFloor = randFloorExcludingSlice(game, srcFloor, firstFloor)
		case peopleInBetween:
			srcFloor = randFloorExcludingSlice(game, lastFloor, firstFloor)
			dstFloor = randFloorExcludingSlice(game, lastFloor, firstFloor, srcFloor)
		}

		game.NewPeople[currentTime] = append(game.NewPeople[currentTime], Person{Floor: &srcFloor, TargetFloor: dstFloor})
	}

	return game
}

func getEventsStateFromInputs(inputs IotInputs) EventsState {
	return EventsState{
		Rain:           inputs.Rain.SecondsToStart == 0 && inputs.Rain.Duration != 0,
		PeopleGoToWork: inputs.PeopleGoToWork,
		BusStopsIn:     inputs.BusStopsIn > 0 && inputs.BusStopsIn < 60,
	}
}

func createPerson(srcFloor, dstFloor string, game *GameData) {
	personID := strconv.Itoa(game.PersonID)
	p := Person{
		ID:          personID,
		Floor:       &srcFloor,
		TargetFloor: dstFloor,
		X:           randEnterX(*game),
		TargetExit:  randEnterExit(game.Rand),
	}
	p.MinTime = computeMinTime(p, *game)

	game.People[personID] = p
	game.PersonID++
}

func getRainDataFromCurrentTime(game GameData, currentTime int) Rain {
	secondsToStart := 0
	duration := 0
	prevTime := currentTime - 1
	for _, time := range game.IotEvents[rainStartsIn] {
		if time < currentTime {
			continue
		}
		if time-1 == prevTime {
			secondsToStart++
			prevTime = time
		} else {
			break
		}
	}
	prevTime++
	for _, time := range game.IotEvents[rainStopsIn] {
		if time <= prevTime {
			continue
		}

		if time-1 == prevTime {
			duration++
			prevTime = time
		} else {
			break
		}
	}

	return Rain{
		SecondsToStart: secondsToStart,
		Duration:       duration,
	}
}

func getBusNextStopIn(game GameData, currentTime int) int {
	for _, stopTime := range game.IotEvents[busStopsIn] {
		if stopTime >= currentTime {
			return stopTime - currentTime
		}
	}
	return -1
}

func getCurrentIotEvents(game GameData, currentTime int) []string {
	events := []string{}
	for event, ocurrencies := range game.IotEvents {
		if event == busStopsIn {
			if currentTime > BUS_START_TIME_SECONDS && currentTime < BUS_END_TIME_SECONDS {
				events = append(events, event)
			}
		} else {
			for _, time := range ocurrencies {
				if time == currentTime {
					events = append(events, event)
				}
			}
		}
	}
	return events
}

func randFloorExcludingSlice(game GameData, excludedFloors ...string) string {
	targetFloor := randFloor(game)
	for stringInSlice(targetFloor, excludedFloors) {
		targetFloor = randFloor(game)
	}
	return targetFloor
}

func randFloor(game GameData) string {
	var floors []string
	for k := range game.Layout.Floors {
		floors = append(floors, k)
	}
	sort.Strings(floors)
	return floors[game.Rand.Intn(len(floors))]
}

func randEnterExit(r *rand.Rand) string {
	if r.Float64() < 0.5 {
		return "left"
	}
	return "right"
}

func randEnterX(game GameData) float64 {
	if side := randEnterExit(game.Rand); side == "right" {
		return game.Layout.Width
	}
	return 0
}

func computeExtraScore(p Person) float64 {
	if p.CurrentTime <= p.MinTime {
		return 0
	}
	multiplier := math.Ceil((p.CurrentTime - p.MinTime) / p.MinTime)
	return multiplier * 1
}

func getActions(r *http.Request) Actions {
	defer r.Body.Close()
	decoder := json.NewDecoder(r.Body)
	var act Actions
	if err := decoder.Decode(&act); err != nil {
		return Actions{}
	}
	return act
}

func stepPerson(p Person, game GameData) Person {
	p.CurrentTime++
	if p.Floor == nil {
		return stepPersonInLift(p, game)
	}

	if *p.Floor == p.TargetFloor {
		return stepPersonInDestination(p, game)
	}

	distance, lift := closestLift(p, game)
	if distance < 1e-2 && doorsOpen(lift, game) {
		return enterLift(p, lift.ID, game)
	}

	movement := clip(lift.X-p.X, -1, 1)
	p.X += movement
	return p
}

func stepPersonInLift(p Person, game GameData) Person {
	liftID := *p.Lift
	l := game.Lifts[liftID]
	f := game.Layout.Floors[p.TargetFloor]
	if doorsOpen(l, game) && l.Height == f.Height {
		p.Floor = &p.TargetFloor
		p.Lift = nil
		l.People = removeID(l.People, p.ID)
		game.Lifts[liftID] = l
	}
	return p
}

func removeID(l []string, id string) (out []string) {
	for _, x := range l {
		if x != id {
			out = append(out, x)
		}
	}
	return out
}

func stepPersonInDestination(p Person, game GameData) Person {
	if p.TargetExit == "left" {
		p.X--
	} else {
		p.X++
	}

	return p
}

func enterLift(p Person, liftID string, game GameData) Person {
	l := game.Lifts[liftID]
	if len(l.People) == liftCapacity(l.ID, game) { // do not allow more than maximum capacity
		return p
	}
	l.People = append(l.People, p.ID)
	game.Lifts[liftID] = l
	p.Floor = nil
	p.Lift = &l.ID
	return p
}

func liftCapacity(liftID string, game GameData) int {
	return game.Layout.Lifts[liftID].Capacity
}

func stepLift(l Lift, game *GameData, act Actions) Lift {
	if _, ok := act.Lifts[l.ID]; !ok {
		return stopLift(l) // no action for lift, stop
	}

	floors := game.Layout.Lifts[l.ID].Floors
	if !stringInSlice(act.Lifts[l.ID], floors) {
		return stopLift(l) // invalid floor for lift, stop
	}

	// moving
	liftHeight, floorHeight := l.Height, floorHeight(act.Lifts[l.ID], *game)
	aboveGoingUp := liftHeight > floorHeight && l.Velocity > 0
	belowGoingDown := liftHeight < floorHeight && l.Velocity < 0
	inPlaceMoving := liftHeight == floorHeight && l.Velocity != 0
	if aboveGoingUp || belowGoingDown || inPlaceMoving { // wrong direction, stop
		return stopLift(l)
	}

	aboveGoingDown := liftHeight > floorHeight && l.Velocity < 0
	belowGoingUp := liftHeight < floorHeight && l.Velocity > 0
	distance := abs(liftHeight - floorHeight)
	if aboveGoingDown || belowGoingUp {
		game.Score++         //Moving lift costs energy
		if distance <= 0.5 { // right direction and close enough, stop at floor
			l.Height = floorHeight
			l.Velocity = 0
			return l
		} else if distance <= 1.0 { // right direction, almost close enough, approach less so we don't overshoot
			l.Height += 0.5 * l.Velocity
			return l
		}

		l.Height += l.Velocity
		return l
	}

	// stopped, l.Velocity == 0
	if liftHeight == floorHeight {
		l.DoorsOpen = true
		return l
	}

	// stopped and wants to go to another floor
	if l.DoorsOpen {
		l.DoorsOpen = false
		return l
	}

	game.Score++          //Moving lift costs energy
	if distance <= 0.25 { // close enough, move in one second
		l.Height = floorHeight
		return l
	} else if distance <= 0.5 { // almost close enough, approach less so we don't overshoot, not changing velocity
		l.Height += (floorHeight - liftHeight) * 0.5
		return l
	}

	if floorHeight-liftHeight > 0 {
		l.Velocity = 1
	} else {
		l.Velocity = -1
	}

	l.Height += l.Velocity * 0.5
	return l
}

func doorsOpen(l Lift, game GameData) bool {
	return game.Lifts[l.ID].DoorsOpen
}

func stopLift(l Lift) Lift {
	if l.Velocity == 0 {
		return l
	}
	l.Height += l.Velocity * 0.5 // half velocity because stopping
	l.Velocity = 0
	return l
}

// TODO should be able to calculate the shortest path considering lift changes, e.g. lift 1 from 0 to 10 and lift 2 from 10 to 20
//		therefore the panic condition should be removed if ensured at game creation that every pair of floors have a connection
func closestLift(p Person, game GameData) (float64, Lift) {
	var availableLifts []Lift // list of lifts that could take person where they should
	for _, l := range game.Layout.Lifts {
		if validLift(p, l) {
			availableLifts = append(availableLifts, l)
		}
	}

	if len(availableLifts) == 0 {
		panic("impossible path!!")
	}

	minDistance := 1e10
	var minIndex int
	for i, l := range availableLifts {
		if distance := manhattanDistance(p, l, game); distance < minDistance {
			minDistance = distance
			minIndex = i
		}
	}

	return minDistance, availableLifts[minIndex]
}

func validLift(p Person, l Lift) bool {
	if p.Floor == nil {
		return false
	}
	return stringInSlice(*p.Floor, l.Floors) && stringInSlice(p.TargetFloor, l.Floors)
}

func stringInSlice(s string, l []string) bool {
	for _, x := range l {
		if x == s {
			return true
		}
	}
	return false
}

func manhattanDistance(p Person, l Lift, game GameData) float64 {
	return abs(p.X-l.X) + abs(personHeight(p, game)-liftHeight(l.ID, game))
}

func personHeight(p Person, game GameData) float64 {
	if p.Floor == nil {
		return 0
	}
	return floorHeight(*p.Floor, game)
}

func floorHeight(id string, game GameData) float64 {
	return game.Layout.Floors[id].Height
}

func liftHeight(id string, game GameData) float64 {
	return game.Lifts[id].Height
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func clip(x, min, max float64) float64 {
	if x < min {
		return min
	} else if x > max {
		return max
	}
	return x
}

func createGame(sseed string) int {
	s, _ := strconv.Atoi(sseed)
	seed := int64(s)
	if seed == 0 {
		seed = time.Now().UTC().UnixNano()
	}
	randSource := rand.New(rand.NewSource(int64(seed)))
	allGames.Mutex.Lock()
	defer allGames.Mutex.Unlock()
	allGames.I++

	game := GameData{
		ID:     allGames.I,
		People: map[string]Person{},
		Rand:   randSource,
		Seed:   int64(seed),
	}

	limitToOneHourGame(&game)
	game.Layout = generateRandomLayout(randSource, &game)
	game.Lifts = generateLifts(game.Layout, randSource)

	game.IotEvents = generateIotEvents(game)

	game.NewPeople = make(map[int][]Person)
	for i := game.CurrentTime; i < game.TotalTime; i++ {
		game = generateNewPeople(game, i)
	}

	allGames.Games[allGames.I] = game
	return game.ID
}

func limitToOneHourGame(game *GameData) {
	randomHourStart := 7*60*60 + game.Rand.Intn(14)*60*60 // start between working hours 7-20
	game.TotalTime = randomHourStart + GAME_DURATION
	game.CurrentTime = randomHourStart

	iotEventsInHour := map[string][]int{}
	for event, occurrencies := range game.IotEvents {
		for _, time := range occurrencies {
			if time >= randomHourStart && time < game.TotalTime {
				if _, ok := iotEventsInHour[event]; !ok {
					iotEventsInHour[event] = []int{time}
				} else {
					iotEventsInHour[event] = append(iotEventsInHour[event], time)
				}
			}
		}
	}

	game.IotEvents = iotEventsInHour
}

func generateRandomLayout(randSource *rand.Rand, game *GameData) Layout {
	floors := map[string]Floor{}
	game.TotalFloors = randSource.Intn(FLOORS_VARIATION) + MIN_FLOORS
	var height float64
	for i := 0; i < game.TotalFloors; i++ {
		id := strconv.Itoa(i)
		floors[id] = Floor{ID: id, Height: height}
		height += FLOOR_HEIGHT_MULTIPLIER*randSource.Float64() + MIN_FLOOR_HEIGHT
	}

	connectedFloors := createConnectedFloorsMap(floors)
	lifts := map[string]Lift{}
	var i int
	var x float64
	for !allFloorsConnected(connectedFloors) {
		var l Lift
		connectedFloors, l = addNewLift(randSource, connectedFloors)
		id := strconv.Itoa(i)
		l.ID = id
		l.X = x + MIN_LIFT_MARGIN + LIFT_MARGIN_MULTIPLIER*randSource.Float64()
		l.Capacity = 1 + randSource.Intn(MAX_LIFT_CAPACITY)
		lifts[id] = l
		x = l.X
		i++
	}

	return Layout{Width: x + MIN_LIFT_MARGIN, Floors: floors, Lifts: lifts}
	// return layout{
	// 	Width: 10,
	// 	Lifts: map[string]lift{
	// 		"0": lift{ID: "0", X: 5, Capacity: 2, Floors: []string{"0", "1", "2", "3", "4"}},
	// 		"1": lift{ID: "1", X: 2, Capacity: 1, Floors: []string{"0", "1", "2"}},
	// 		"2": lift{ID: "2", X: 8, Capacity: 1, Floors: []string{"2", "3", "4"}},
	// 	},
	// }
}

func addNewLift(randSource *rand.Rand, connectedFloors map[string]map[string]bool) (map[string]map[string]bool, Lift) {
	var floorIDs []string
	for k := range connectedFloors {
		floorIDs = append(floorIDs, k)
	}
	sort.Strings(floorIDs)

	floors := generateLiftFloors(randSource, floorIDs)
	sort.Strings(floors)
	for i, x := range floors {
		for j := i + 1; j < len(floors); j++ {
			connectedFloors[x][floors[j]] = true
		}
	}

	return connectedFloors, Lift{Floors: floors}
}

func generateLiftFloors(randSource *rand.Rand, allFloors []string) (out []string) {
	max := len(allFloors)
	TotalFloors := MIN_LIFT_STOPS + randSource.Intn(max-MIN_LIFT_STOPS+1)

	var newFloor string
	for i := 0; i < TotalFloors; i++ {
		allFloors, newFloor = choice(allFloors, randSource)
		out = append(out, newFloor)
	}

	return out
}

func choice(in []string, randSource *rand.Rand) ([]string, string) {
	i := randSource.Intn(len(in))
	v := in[i]
	in = removeID(in, v)
	return in, v
}

func generateIotEvents(game GameData) map[string][]int {
	events := map[string][]int{}
	setRandomRains(events, game)
	setPeakTimes(events, game)
	setBusStopTimes(events, game)
	return events
}

func setBusStopTimes(events map[string][]int, game GameData) {
	events[busStopsIn] = []int{}
	for i := BUS_START_TIME_SECONDS; i < BUS_END_TIME_SECONDS; i += BUS_STOP_INTERVAL_SECONDS {
		delay := game.Rand.Intn(BUS_STOP_MAX_DELAY_SECONDS)
		events[busStopsIn] = append(events[busStopsIn], i+delay)
	}
}

func setPeakTimes(events map[string][]int, game GameData) {
	events[peakTime] = []int{}
	for _, time := range peakTimes {
		duration := game.Rand.Intn(MAX_PEAK_TIME_SECONDS-MIN_PEAK_TIME_SECONDS) + MIN_PEAK_TIME_SECONDS
		count := time
		for i := 0; i < duration; i++ {
			events[peakTime] = append(events[peakTime], count)
			count++
		}
	}

	// events[peakTime] = []int{20, 21, 22, 23, 24, 25, 26, 27, 28, 29}
	// events[peakTime] = []int{30, 31, 32, 33, 34, 35, 36, 37, 38, 39}
}

func getRandomSecondInDay(offset, timeInterval, max int, game GameData) int {
	second := game.Rand.Intn(timeInterval - max)
	return game.CurrentTime + offset + second
}

func getRandomRainingTimes(game GameData) (out []int) {
	max := MAX_RAIN_START_SECONDS + MAX_RAIN_DURATION
	rainCount := 3 + game.Rand.Intn(4) // between 3 and 6 rains a day
	timeInterval := 3600 / rainCount
	for i := 0; i < rainCount; i++ {
		x := getRandomSecondInDay(i*timeInterval, timeInterval, max, game)
		out = append(out, x)
	}
	return out
}

func setRandomRains(events map[string][]int, game GameData) {
	startsRainingTimes := getRandomRainingTimes(game)
	for _, startsRainingTime := range startsRainingTimes {
		startsIn := game.Rand.Intn(MAX_RAIN_START_SECONDS-MIN_RAIN_START_SECONDS) + MIN_RAIN_START_SECONDS
		duration := game.Rand.Intn(MAX_RAIN_DURATION-MIN_RAIN_DURATION) + MIN_RAIN_DURATION

		count := startsRainingTime
		events[rainStartsIn] = []int{}
		for i := 0; i < startsIn; i++ {
			events[rainStartsIn] = append(events[rainStartsIn], count)
			count++
		}
		events[rainStopsIn] = []int{}
		for i := 0; i < duration; i++ {
			events[rainStopsIn] = append(events[rainStopsIn], count)
			count++
		}
	}

	// events[rainStartsIn] = []int{10, 11, 12, 13, 14, 15, 16, 17, 18, 19}
	// events[rainStopsIn] = []int{20, 21, 22, 23, 24, 25, 26, 27, 28, 29}
}

func createConnectedFloorsMap(floors map[string]Floor) map[string]map[string]bool {
	var floorIDs []string
	for k := range floors {
		floorIDs = append(floorIDs, k)
	}
	sort.Strings(floorIDs)

	all := map[string]map[string]bool{}
	for i, x := range floorIDs {
		m := map[string]bool{}
		for j := i + 1; j < len(floorIDs); j++ {
			m[floorIDs[j]] = false
		}
		all[x] = m
	}

	return all
}

func allFloorsConnected(connectedFloors map[string]map[string]bool) bool {
	for _, x := range connectedFloors {
		for _, y := range x {
			if !y {
				return false
			}
		}
	}
	return true
}

func generateLifts(lay Layout, randSource *rand.Rand) map[string]Lift {
	m := map[string]Lift{}
	ids := []string{}
	for k := range lay.Lifts {
		ids = append(ids, k)
	}
	sort.Strings(ids)
	for _, k := range ids {
		v := lay.Lifts[k]
		minHeight, maxHeight := computeLiftMinHeight(v, lay.Floors), computeLiftMaxHeight(v, lay.Floors)
		h := randSource.Float64()
		m[k] = Lift{ID: k, Height: h*(maxHeight-minHeight) + minHeight}
	}
	return m
}

func computeLiftMinHeight(l Lift, floors map[string]Floor) float64 {
	minHeight := 1e10
	for _, floorID := range l.Floors {
		if height := floors[floorID].Height; height < minHeight {
			minHeight = height
		}
	}
	return minHeight
}

func computeLiftMaxHeight(l Lift, floors map[string]Floor) float64 {
	maxHeight := -1e10
	for _, floorID := range l.Floors {
		if height := floors[floorID].Height; height > maxHeight {
			maxHeight = height
		}
	}
	return maxHeight
}

func computeMinTime(p Person, game GameData) float64 {
	_, lift := closestLift(p, game)
	return abs(lift.X-p.X) /* initial distance */ + verticalLiftTime(*p.Floor, p.TargetFloor, game) + horizontalTargetTime(lift.X, p, game)
}

func verticalLiftTime(a, b string, game GameData) float64 {
	heightA, heightB := game.Layout.Floors[a].Height, game.Layout.Floors[b].Height
	distance := abs(heightA - heightB)
	return distance + 1 // because 0.5m accelerating and decelerating increase 1 second over 1m/s steady velocity
}

func horizontalTargetTime(liftX float64, p Person, game GameData) float64 {
	if p.TargetExit == "left" {
		return liftX
	}
	return game.Layout.Width - liftX
}

func probabilityMultiplier(game GameData) float64 {
	base := 0.1
	switch len(game.Lifts) {
	case 1:
		return base
	case 2:
		return base + 0.03
	case 3:
		return base + 0.08
	case 4, 5:
		return base + 0.15
	case 6, 7:
		return base + 0.23
	case 8, 9:
		return base + 0.31
	}

	return base + 0.4
}

func Int64InSlice(i int64, l []int64) bool {
	for _, x := range l {
		if x == i {
			return true
		}
	}
	return false
}
