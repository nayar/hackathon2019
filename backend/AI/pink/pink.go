//Package yellow logic
//	move lift to the closest waiting person
//	if no person move go to the middle floor
package pink

import (
	"fmt"
	"math/rand"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/goml/gobrain"
	"github.com/goml/gobrain/persist"
	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

const MAX_DISTANCE = 1e10

type Algorithm struct {
	FilePath string
}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{
		Lifts: map[string]string{},
	}

	rand.Seed(0)

	ff := &gobrain.FeedForward{}

	if err := persist.Load(a.FilePath, &ff); err != nil {
		fmt.Println("Error loading", err)
	}

	rain := 0
	if game.IOTinputs.Rain.SecondsToStart == 0 && game.IOTinputs.Rain.Duration != 0 {
		rain = 1
	}

	peopleGoToWork := 0
	if game.IOTinputs.PeopleGoToWork {
		peopleGoToWork = 1
	}

	busStopping := 0
	if game.IOTinputs.BusStopsIn > 0 && game.IOTinputs.BusStopsIn < 60 {
		busStopping = 1
	}

	inputs := []float64{float64(rain), float64(busStopping), float64(peopleGoToWork)}
	result := ff.Update(inputs)
	for id, lift := range game.Lifts {
		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = game.People[lift.People[0]].TargetFloor
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" {
			act.Lifts[id] = *game.People[closestPersonID].Floor
		} else {
			if result[0] >= 0.5 {
				middleFloor := len(game.Layout.Lifts[id].Floors) / 2
				act.Lifts[id] = game.Layout.Lifts[id].Floors[middleFloor]
			} else {
				act.Lifts[id] = game.Layout.Lifts[id].Floors[0]
			}
		}

	}
}

func noOtherLiftAlreadyGoing(targetFloor string, act *Actions) bool {
	for _, floor := range act.Lifts {
		if floor == targetFloor {
			return false
		}
	}
	return true
}
