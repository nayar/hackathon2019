// Package purple logic
// keep going for people while lift is not full
// if no more people around or lift is full, go to nearests target floors
// if lift is empty and no people around go to middle floor
package purple

import (
	"fmt"
	"math"
	"math/rand"

	"github.com/goml/gobrain"
	"github.com/goml/gobrain/persist"
	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

const MAX_DISTANCE = 1e10

type Algorithm struct {
	FilePath string
}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{
		Lifts: map[string]string{},
	}
	var iotInputs []float64
	if a.FilePath != "" {
		iotInputs = getIOTInputs(game, a.FilePath)
	}

	for id, lift := range game.Lifts {
		if game.Layout.Lifts[id].Capacity == len(lift.People) { // if lift is full, drop closest person.
			act.Lifts[id] = getClosestFloorFromPeople(id, lift.People, game)
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" { // if there is a person near.
			if len(lift.People) == 0 { // if lift is empty, go for that person.
				act.Lifts[id] = *game.People[closestPersonID].Floor
			} else { // if lift has people, check if it's faster to pick that person or drop someone from lift.
				act.Lifts[id] = getClosestPersonOrFloor(lift, id, closestPersonID, game)
			}
			continue
		}

		if len(lift.People) > 0 { // if lift has people and no one is near, drop someone.
			act.Lifts[id] = getClosestFloorFromPeople(id, lift.People, game)
		} else { // if lift is empty and no one is near, go to best floor based on IOT inputs.
			if len(iotInputs) > 0 {
				act.Lifts[id] = bestFloorByIOT(iotInputs, id, game)
			} else {
				middleFloor := len(game.Layout.Lifts[id].Floors) / 2
				act.Lifts[id] = game.Layout.Lifts[id].Floors[middleFloor]
			}
		}
	}
}

func getClosestPersonOrFloor(lift Lift, liftID, closestPersonID string, game *GameData) string {
	closestTargetFloor := getClosestFloorFromPeople(liftID, lift.People, game)
	closestTargetFloorHeight := game.Layout.Floors[closestTargetFloor].Height
	closestPersonHeight := game.Layout.Floors[*game.People[closestPersonID].Floor].Height
	liftHeight := game.Lifts[liftID].Height

	if math.Abs(liftHeight-closestTargetFloorHeight) < math.Abs(liftHeight-closestPersonHeight) {
		return closestTargetFloor
	}
	return *game.People[closestPersonID].Floor
}

func bestFloorByIOT(results []float64, liftID string, game *GameData) string {
	if results[0] >= 0.5 {
		if results[1] >= 0.5 {
			topFloor := len(game.Layout.Lifts[liftID].Floors) - 1
			return game.Layout.Lifts[liftID].Floors[topFloor]
		}
		middleFloor := len(game.Layout.Lifts[liftID].Floors) / 2
		return game.Layout.Lifts[liftID].Floors[middleFloor]
	} else {
		return game.Layout.Lifts[liftID].Floors[0]
	}
}

func getClosestFloorFromPeople(liftID string, peopleIDs []string, game *GameData) string {
	lift := game.Layout.Lifts[liftID]
	if len(peopleIDs) == 1 {
		return game.People[game.Lifts[liftID].People[0]].TargetFloor
	} else {
		minDist := math.Inf(1)
		targetFloor := lift.Floors[len(lift.Floors)/2]
		for i := range peopleIDs {
			currentFloor := game.People[peopleIDs[i]].TargetFloor
			dist := math.Abs(game.Lifts[liftID].Height - game.Layout.Floors[currentFloor].Height)
			if dist < minDist {
				minDist = dist
				targetFloor = currentFloor
			}
		}
		return targetFloor
	}
}

func getIOTInputs(game *GameData, path string) []float64 {
	rand.Seed(0)

	ff := &gobrain.FeedForward{}

	if err := persist.Load(path, &ff); err != nil {
		fmt.Println("Error loading NN:", err)
	}

	rain := 0
	if game.IOTinputs.Rain.SecondsToStart == 0 && game.IOTinputs.Rain.Duration != 0 {
		rain = 1
	}

	peopleGoToWork := 0
	if game.IOTinputs.PeopleGoToWork {
		peopleGoToWork = 1
	}

	busStopping := 0
	if game.IOTinputs.BusStopsIn > 0 && game.IOTinputs.BusStopsIn < 60 {
		busStopping = 1
	}

	inputs := []float64{float64(rain), float64(busStopping), float64(peopleGoToWork)}
	return ff.Update(inputs)
}

func liftIDAlreadyGoing(targetFloor string, act *Actions) bool {
	for _, floor := range act.Lifts {
		if floor == targetFloor {
			return false
		}
	}
	return true
}
