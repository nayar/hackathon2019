//Package olive logic
//	move lift to the closest waiting person
//	if no person go back to floor 0
package olive

import (
	"sort"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

type Algorithm struct{}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{Lifts: map[string]string{}}

	var ids []string
	for id := range game.Lifts {
		ids = append(ids, id)
	}
	sort.Strings(ids)

	for _, id := range ids {
		lift := game.Lifts[id]
		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = getClosestTarget(lift.Height, lift.People, game)
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" {
			act.Lifts[id] = *game.People[closestPersonID].Floor
		} else {
			act.Lifts[id] = game.Layout.Lifts[id].Floors[0]
		}
	}
}

func getClosestTarget(liftHeight float64, people []string, game *GameData) string {
	targetFloor := game.People[people[0]].TargetFloor
	min := 1e10
	for _, personID := range people {
		if d := distance(liftHeight, game.People[personID].TargetFloor, game); d < min {
			targetFloor = game.People[personID].TargetFloor
			min = d
		}
	}

	return targetFloor
}

func distance(liftHeight float64, floorID string, game *GameData) float64 {
	floorHeight := game.Layout.Floors[floorID].Height
	return util.Abs(liftHeight - floorHeight)
}
