package black5

import (
	"fmt"
	"testing"
)

func TestSelectRandomFloor(t *testing.T) {
	testSelectRandomFloor([]float64{1, 1, 2, 1})
	testSelectRandomFloor([]float64{1, 10, 2, 1})
}

func testSelectRandomFloor(scores []float64) {
	frequencies := map[int]int{}
	for i := 0; i < 1e4; i++ {
		index := selectRandomFloor(scores)
		frequencies[index]++
	}
	fmt.Println("Frequencies:", frequencies)
}
