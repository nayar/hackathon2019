//Package darkyellow logic
//	move lift to the closest waiting person
//	if no person move go to the closest floor of the middle where no lift is in there
package darkyellow

import (
	"github.com/oriolf/hackathon-2019/backend/AI/util"
	. "github.com/oriolf/hackathon-2019/backend/structs"
)

const MAX_DISTANCE = 1e10

type Algorithm struct{}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	act = &Actions{
		Lifts: map[string]string{},
	}

	heighestFloor := 0
	for id, lift := range game.Lifts {
		topFloor := len(game.Layout.Lifts[id].Floors) - 1
		if topFloor > heighestFloor {
			heighestFloor = topFloor
		}

		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = game.People[lift.People[0]].TargetFloor
			continue
		}

		if game.IOTinputs.Rain.Duration != 0 {
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" {
			act.Lifts[id] = *game.People[closestPersonID].Floor
		} else {

			middleFloor := len(game.Layout.Lifts[id].Floors) / 2
			act.Lifts[id] = game.Layout.Lifts[id].Floors[middleFloor]

		}
	}

	// Get all lifts with nothing to do and if its raining send them to the rooftop or the entrance
	if game.IOTinputs.Rain.Duration != 0 {
		for id := range game.Lifts {
			topFloor := len(game.Layout.Lifts[id].Floors) - 1
			if heighestFloor != topFloor { // If floor does not goes to rooftop go to lower entrance
				act.Lifts[id] = "0"
			} else {
				lastFloor := game.Layout.Lifts[id].Floors[topFloor]
				if _, ok := act.Lifts[id]; !ok {
					act.Lifts[id] = lastFloor
				}
			}
		}
	}
}

func getLiftsGoingEntrance(act *Actions) {

}

func noOtherLiftAlreadyGoing(targetFloor string, act *Actions) bool {
	for _, floor := range act.Lifts {
		if floor == targetFloor {
			return false
		}
	}
	return true
}
