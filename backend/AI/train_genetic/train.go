package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	eaopt "github.com/MaxHalford/eaopt"
	"github.com/oriolf/hackathon-2019/backend/AI/black12"
	"github.com/oriolf/hackathon-2019/backend/AI/green"
	"github.com/pkg/errors"
)

const (
	url = "http://localhost:5000"
)

var (
	seeds        = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	greenResults map[int]float64
)

type algorithm interface {
	MakeActions(act *Actions, game *GameData)
}

// A Vector contains float64s.
type Vector []float64

func (X Vector) String() string {
	return fmt.Sprintf("%v", []float64(X))
}

// Evaluate a Vector with the Drop-Wave function which takes two variables as
// input and reaches a minimum of -1 in (0, 0). The function is simple so there
// isn't any error handling to do.
func (X Vector) Evaluate() (float64, error) {
	alg := black12.Algorithm{Parameters: []float64(X)}
	scores := computeScoresParallel(alg, seeds)
	return compareScores(greenResults, scores), nil
}

// Mutate a Vector by resampling each element from a normal distribution with
// probability 0.8.
func (X Vector) Mutate(rng *rand.Rand) {
	eaopt.MutNormalFloat64(X, 0.8, rng)
}

// Crossover a Vector with another Vector by applying uniform crossover.
func (X Vector) Crossover(Y eaopt.Genome, rng *rand.Rand) {
	eaopt.CrossUniformFloat64(X, Y.(Vector), rng)
}

// Clone a Vector to produce a new one that points to a different slice.
func (X Vector) Clone() eaopt.Genome {
	var Y = make(Vector, len(X))
	copy(Y, X)
	return Y
}

// VectorFactory returns a random vector by generating values uniformally over an interval
func VectorFactory(rng *rand.Rand) eaopt.Genome {
	if rand.Float64() < 0.05 {
		// already known almost good value
		//		return Vector([]float64{-3.028522846531095, -1.6735249533128793, 191.8375272361313, 166.064313288829, 75.25114122090928})
		//		return Vector([]float64{-2.2646059892653176, 0.852308719279737, 74.62608535694662, 366.6482306730257, 94.75942130132935})
		return Vector([]float64{-2, 1, 10, 10, 10, 10, 0, 10, 10, 10, 0, 0})
	}
	return Vector(eaopt.InitUnifFloat64(12, -10, 100, rng))
}

func main() {
	greenResults = computeScoresParallel(green.Algorithm{}, seeds)

	// Instantiate a GA with a GAConfig
	var ga, err = eaopt.NewDefaultGAConfig().NewGA()
	if err != nil {
		fmt.Println(err)
		return
	}

	// Set the number of generations to run for
	ga.NGenerations = 200

	start := time.Now()
	// Add a custom print function to track progress
	ga.Callback = func(ga *eaopt.GA) {
		fmt.Printf("Best fitness at generation %d took %s: %f %s\n", ga.Generations, time.Since(start), ga.HallOfFame[0].Fitness, ga.HallOfFame[0].Genome)
		start = time.Now()
	}

	// Find the minimum
	err = ga.Minimize(VectorFactory)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func compareScores(base, other map[int]float64) float64 {
	var proportion float64
	for key, value := range base {
		proportion += other[key] / value
	}
	return proportion / float64(len(base))
}

type gameWorkUnit struct {
	seed  int
	score float64
}

func computeScoresParallel(alg algorithm, seeds []int) map[int]float64 {
	m := make(map[int]float64)
	workersCh := make(chan gameWorkUnit, len(seeds))
	ch := make(chan gameWorkUnit)
	for i := 0; i < 2; i++ {
		go worker(workersCh, ch, alg)
	}
	for _, seed := range seeds {
		workersCh <- gameWorkUnit{seed: seed}
	}
	close(workersCh)
	for range seeds {
		s := <-ch
		m[s.seed] = s.score
	}
	return m
}

func worker(workersCh, ch chan gameWorkUnit, alg algorithm) {
	for x := range workersCh {
		score := playGame(alg, x.seed)
		ch <- gameWorkUnit{seed: x.seed, score: score}
	}
}

func computeScores(alg algorithm, seeds []int) map[int]float64 {
	m := make(map[int]float64)
	for _, seed := range seeds {
		m[seed] = playGame(alg, seed)
	}
	return m
}

func playGame(alg algorithm, seed int) float64 {
	act := Actions{}
	game := GameData{}
	i := 0

	for {
		i++
		if game.ID != 0 {
			var err error
			game, err = post(fmt.Sprintf("%s?id=%d", url, game.ID), act)
			if err != nil {
				log.Fatalln("Error posting action", err)
			}
			if game.Finished {
				return game.Score
			}
		} else {
			var err error
			game, err = get(fmt.Sprintf("%s?seed=%d", url, seed))
			if err != nil {
				log.Fatalln("Error getting game", err)
			}
		}

		alg.MakeActions(&act, &game)
	}
}

func get(url string) (game GameData, err error) {
	res, err := http.Get(url)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error get request")
	}

	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}

func post(url string, act Actions) (game GameData, err error) {
	jsonValue, err := json.Marshal(act)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error marshaling actions")
	}
	res, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error making post")
	}
	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}
