//Package green logic
//	move lift to the closest waiting person
//	if no person go back to floor 0
package green

import (
	"sort"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

type Algorithm struct{}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{Lifts: map[string]string{}}

	var ids []string
	for id := range game.Lifts {
		ids = append(ids, id)
	}
	sort.Strings(ids)

	for _, id := range ids {
		lift := game.Lifts[id]
		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = game.People[lift.People[0]].TargetFloor
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" {
			act.Lifts[id] = *game.People[closestPersonID].Floor
		} else {
			act.Lifts[id] = game.Layout.Lifts[id].Floors[0]
		}
	}
}
