//Package orange logic
//	move lift to the closest waiting person if no other lift is going for him
//	if no person move go to the middle floor
package orange

import (
	"github.com/oriolf/hackathon-2019/backend/AI/util"
	. "github.com/oriolf/hackathon-2019/backend/structs"
)

const MAX_DISTANCE = 1e10

type Algorithm struct{}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{
		Lifts: map[string]string{},
	}

	for id, lift := range game.Lifts {
		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = game.People[lift.People[0]].TargetFloor
			continue
		}

		closestPersonID := util.GetClosestPerson(id, game)
		if closestPersonID != "" {
			target := *game.People[closestPersonID].Floor
			if noOtherLiftAlreadyGoing(target, act) {
				act.Lifts[id] = target
			}
		} else {
			middleFloor := len(game.Layout.Lifts[id].Floors) / 2
			act.Lifts[id] = game.Layout.Lifts[id].Floors[middleFloor]
		}
	}
}

func noOtherLiftAlreadyGoing(targetFloor string, act *Actions) bool {
	for _, floor := range act.Lifts {
		if floor == targetFloor {
			return false
		}
	}
	return true
}
