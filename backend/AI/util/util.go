package util

import (
	. "github.com/oriolf/hackathon-2019/backend/structs"
)

const MAX_DISTANCE = 1e10

func GetClosestPerson(liftID string, game *GameData) string {
	minDistance := MAX_DISTANCE
	closestPerson := ""
	peopleIDs := []string{}
	for id := range game.People {
		peopleIDs = append(peopleIDs, id)
	}

	if len(peopleIDs) == 0 {
		return ""
	}

	for _, personID := range peopleIDs {
		if distance := ManhattanDistance(liftID, personID, game); distance < minDistance {
			minDistance = distance
			closestPerson = personID
		}
	}

	return closestPerson
}

func ManhattanDistance(liftID, personID string, game *GameData) float64 {
	liftX := game.Layout.Lifts[liftID].X
	liftY := game.Layout.Lifts[liftID].Height
	personX := game.People[personID].X
	personFloor := game.People[personID].Floor
	if personFloor == nil { // ignore people already in a lift
		return MAX_DISTANCE
	}
	personTargetFloor := game.People[personID].TargetFloor
	if *personFloor == personTargetFloor { // ignore people already in its floor
		return MAX_DISTANCE
	}

	if !floorValidForLift(*personFloor, liftID, game) ||
		!floorValidForLift(personTargetFloor, liftID, game) { // ignore people that can't go to desired destinationwith this lift
		return MAX_DISTANCE
	}

	personY := game.Layout.Floors[*personFloor].Height
	return Abs(liftX-personX) + Abs(liftY-personY)
}

func floorValidForLift(floorID, liftID string, game *GameData) bool {
	return StringInSlice(game.Layout.Lifts[liftID].Floors, floorID)
}

func StringInSlice(list []string, str string) bool {
	for _, value := range list {
		if value == str {
			return true
		}
	}
	return false
}

func Abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}
