//Package black12 logic
//	move lift to the closest waiting person
//	if no person go back to floor 0
package black12

import (
	"math/rand"
	"strconv"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

const MAX_DISTANCE = 1e10

type Algorithm struct {
	Parameters  []float64
	lastActions map[string]string
}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{Lifts: map[string]string{}}

	for id, lift := range game.Lifts {
		availableFloors := game.Layout.Lifts[id].Floors
		floorScores := a.computeFloorScores(game, lift, availableFloors)

		allScores := append([]float64{a.Parameters[0]}, floorScores...) // first parameter is weight of not moving
		floor := selectRandomFloor(allScores)
		if floor != 0 { // first option is not moving
			act.Lifts[id] = availableFloors[floor-1]
		}
	}
	a.lastActions = act.Lifts
}

func (a Algorithm) computeFloorScores(game *GameData, lift Lift, availableFloors []string) (scores []float64) {
	for _, floorID := range availableFloors {
		scores = append(scores, a.computeFloorScore(game, lift, floorID))
	}
	return scores
}

func (a Algorithm) computeFloorScore(game *GameData, lift Lift, floorID string) float64 {
	isFirstFloor, isLastFloor := floorID == "0", floorID == strconv.Itoa(game.TotalFloors-1)
	iot := game.IOTinputs
	res := dotproduct(a.Parameters[1:], []float64{
		-1,                                     // penalization to moving in general, to avoid innecessary movement
		minPersonDistance(game, lift, floorID), // minimum distance of all people in this floor
		personDistanceToDestination(game, lift, floorID), // if a person is inside that has to go to that floor or close
		a.keepActions(lift.ID, floorID),
		busIsComing(iot, isFirstFloor),
		busIsComing(iot, isLastFloor),
		rainIsComing(iot, isFirstFloor),
		rainIsComing(iot, isLastFloor),
		peakHourIsComing(iot, isFirstFloor),
		peakHourIsComing(iot, isLastFloor),
		winterIsComing(),

		// if another lift is closer avoid going
	})
	if res < 0 { // weighted selection only makes sense if all are > 0
		return 0
	}
	return res
}

func busIsComing(iot IotInputs, isTargetedFloor bool) float64 {
	if isTargetedFloor && iot.BusStopsIn >= 0 && iot.BusStopsIn < 60 {
		return 1
	}
	return 0
}

func rainIsComing(iot IotInputs, isTargetedFloor bool) float64 {
	if isTargetedFloor && (iot.Rain.SecondsToStart != 0 || iot.Rain.Duration != 0) {
		return 1
	}
	return 0
}

func peakHourIsComing(iot IotInputs, isTargetedFloor bool) float64 {
	if isTargetedFloor && iot.PeopleGoToWork {
		return 1
	}
	return 0
}

func winterIsComing() float64 { return 0 }

func (a Algorithm) keepActions(liftID, floorID string) float64 {
	if a.lastActions[liftID] == floorID {
		return 1 // keep doing last thing, to avoid jumping up and down
	}
	return 0
}

func minPersonDistance(game *GameData, lift Lift, floorID string) float64 {
	min := MAX_DISTANCE
	for _, person := range game.People {
		if person.Floor != nil && *person.Floor == floorID {
			if d := util.ManhattanDistance(lift.ID, person.ID, game); d < min {
				min = d
			}
		}
	}

	return normalizeDistance(min)
}

func personDistanceToDestination(game *GameData, lift Lift, floorID string) float64 {
	min := MAX_DISTANCE
	for _, personID := range lift.People {
		if game.People[personID].TargetFloor == floorID {
			if d := util.Abs(game.Layout.Floors[floorID].Height - lift.Height); d < min {
				min = d
			}
		}
	}
	return normalizeDistance(min)
}

func normalizeDistance(d float64) float64 {
	if d < 1e-10 {
		return 1e10 // 0 distance means almost no cost to do the action, should do the action
	}
	return 1 / d // more distance means less incentive to do it
}

func selectRandomFloor(scores []float64) (index int) {
	var cumulative []float64
	var total float64
	for _, x := range scores {
		total += x
		cumulative = append(cumulative, total)
	}

	threshold := rand.Float64() * total
	for i, x := range cumulative {
		if threshold < x {
			return i
		}
	}

	//return len(scores) - 1
	return 0
}

func dotproduct(a, b []float64) (res float64) {
	if len(a) != len(b) {
		return 0
	}
	for i := range a {
		res += a[i] * b[i]
	}
	return res
}
