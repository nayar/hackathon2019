package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	//"github.com/oriolf/hackathon-2019/backend/AI/black5"
	"github.com/oriolf/hackathon-2019/backend/AI/green"
	"github.com/oriolf/hackathon-2019/backend/AI/red"

	//"github.com/oriolf/hackathon-2019/backend/AI/olive"
	//"github.com/oriolf/hackathon-2019/backend/AI/orange"
	//"github.com/oriolf/hackathon-2019/backend/AI/purple"
	//"github.com/oriolf/hackathon-2019/backend/AI/yellow"
	//	"github.com/oriolf/hackathon-2019/backend/AI/olive"
//	"github.com/oriolf/hackathon-2019/backend/AI/purple"
	//"github.com/oriolf/hackathon-2019/backend/AI/red"
	"github.com/oriolf/hackathon-2019/backend/AI/olive"
//	"github.com/oriolf/hackathon-2019/backend/AI/red"

	. "github.com/oriolf/hackathon-2019/backend/structs"
	"github.com/pkg/errors"
)

const (
	url        = "http://localhost:5000"
	iterations = 10
	MIN_SEED   = 1
	MAX_SEED   = 10
	maxWorkers = 2
)

type fullAlgorithm struct {
	alg  algorithm
	name string
}

type algorithm interface {
	MakeActions(act *Actions, game *GameData)
}

type game struct {
	alg  algorithm
	seed int
}

var (
	algorithms = []fullAlgorithm{
		//{alg: yellow.Algorithm{}, name: "yellow"},
		//{alg: orange.Algorithm{}, name: "orange"},
		//	{alg: olive.Algorithm{}, name: "olive"},
		//{alg: red.Algorithm{}, name: "red"},
		// {alg: pink.Algorithm{FilePath: "../pink/ff.network"}, name: "pink"},
		//{alg: purple.Algorithm{FilePath: "../purple/ff.network"}, name: "purple"},
		// {alg: pink.Algorithm{FilePath: "../pink/ff.network"}, name: "pink"},
//		{alg: purple.Algorithm{}, name: "purple"},
		{alg: olive.Algorithm{}, name: "olive"},
//		{alg: red.Algorithm{}, name: "red"},
		//		{alg: pink.Algorithm{}, name: "pink"},
		//{alg: olive.Algorithm{}, name: "olive"},
		//{alg: purple.Algorithm{FilePath: "../purple/ff.network"}, name: "purple_iot"},
		//{alg: purple.Algorithm{}, name: "purple"},
		{alg: red.Algorithm{}, name: "red"},
		{alg: red.Algorithm{BestFloor: "1"}, name: "red_iot"},
		//{alg: black5.Algorithm{Parameters: []float64{-3.028522846531095, -1.6735249533128793, 191.8375272361313, 166.064313288829, 75.25114122090928}}, name: "black5_60"}, // ~ 60
		//{alg: black5.Algorithm{Parameters: []float64{-2.2646059892653176, 0.852308719279737, 74.62608535694662, 366.6482306730257, 94.75942130132935}}, name: "black5_09"}, // ~ 0.9
		//{alg: black5.Algorithm{Parameters: []float64{-1.442373474402709, 9.988292109695, 667.9510495469563, 644729.2508268792, -1.4200015003269901}}, name: "black5_08"},   // ~ 0.8
	}
)

func main() {
	var seeds []int
	for i := MIN_SEED; i <= MAX_SEED; i++ {
		seeds = append(seeds, i)
	}
	resultsGreen := computeScores("green", green.Algorithm{}, seeds)
	results := map[string]map[int]float64{}
	for _, alg := range algorithms {
		fmt.Println(alg.name)
		results[alg.name] = computeScores(alg.name, alg.alg, seeds)
	}

	for _, alg := range algorithms {
		var proportion float64
		for _, seed := range seeds {
			p := results[alg.name][seed] / resultsGreen[seed]
			proportion += p
			fmt.Printf("%s with seed %d: %f\n", alg.name, seed, p)
		}
		fmt.Printf("%s mean comparison %f\n", alg.name, proportion/float64(len(seeds)))
	}
}

func computeScores(algName string, alg algorithm, seeds []int) map[int]float64 {
	m := make(map[int]float64)
	for _, seed := range seeds {
		m[seed] = playGame(alg, seed, algName)
	}
	return m
}

func playGame(alg algorithm, seed int, algName string) float64 {
	act := Actions{}
	game := GameData{}
	i := 0

	for {
		i++
		if game.ID != 0 {
			var err error
			game, err = post(fmt.Sprintf("%s?id=%d&team=%s", url, game.ID, algName), act)
			if err != nil {
				log.Fatalln("Error posting action", err)
			}
			if game.Finished {
				fmt.Printf("With seed %d score %f.  \tPeople left %d in %d floors and %d lifts.\n", seed, game.Score, len(game.People), len(game.Layout.Floors), len(game.Layout.Lifts))
				return game.Score
			}
		} else {
			var err error
			game, err = get(fmt.Sprintf("%s?seed=%d", url, seed))
			if err != nil {
				log.Fatalln("Error getting game", err)
			}

		}

		alg.MakeActions(&act, &game)
	}
}

func post(url string, act Actions) (game GameData, err error) {
	jsonValue, err := json.Marshal(act)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error marshaling actions")
	}
	res, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error making post")
	}
	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}

func get(url string) (game GameData, err error) {
	res, err := http.Get(url)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error get request")
	}

	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}
