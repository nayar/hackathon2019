// package red logic
package red

import (
	"math"
	"strconv"

	. "github.com/oriolf/hackathon-2019/backend/structs"

	"github.com/oriolf/hackathon-2019/backend/AI/util"
)

type Algorithm struct {
	BestFloor string
}

func (a Algorithm) MakeActions(act *Actions, game *GameData) {
	*act = Actions{Lifts: map[string]string{}}

	for id, lift := range game.Lifts {
		closestPersonID := util.GetClosestPerson(id, game)
		if len(lift.People) == 0 && closestPersonID == "" { // nobody in and nobody to get, go to best floor
			act.Lifts[id] = a.bestFloor(id, game)
		} else if len(lift.People) > 0 && closestPersonID == "" { // somebody in and nobody to get, go leave the best
			act.Lifts[id] = bestToLeave(lift, game)
		} else if len(lift.People) == 0 && closestPersonID != "" { // nobody in and somebody to get, go get the best
			act.Lifts[id] = bestToGet(game, closestPersonID)
		} else { // somebody in and somebody to get, trace best leaving/getting route and go to first stop
			act.Lifts[id] = bestGetLeaveRoute1(lift, game)
		}
	}
}

func (a Algorithm) bestFloor(liftID string, game *GameData) string {
	if a.BestFloor == "1" {
		return bestFloor1(liftID, game)
	}
	return bestFloor0(liftID, game)
}

func bestFloor0(liftID string, game *GameData) string {
	available := game.Layout.Lifts[liftID].Floors
	return available[len(available)/2]
}

func bestFloor1(liftID string, game *GameData) string {
	hasBottom, hasTop := liftHasFloor(game, liftID, 0), liftHasFloor(game, liftID, len(game.Layout.Floors)-1)
	available := game.Layout.Lifts[liftID].Floors
	if !hasBottom && !hasTop {
		return available[len(available)/2]
	}

	if hasTop && !hasBottom && game.IOTinputs.Rain.Duration != 0 {
		return available[len(available)-1]
	}

	if hasBottom {
		return bestFloorOnIOT(liftID, game)
	}

	return available[len(available)-1]
}

func bestFloorOnIOT(liftID string, game *GameData) string {
	available := game.Layout.Lifts[liftID].Floors
	if game.IOTinputs.Rain.Duration != 0 ||
		(game.IOTinputs.BusStopsIn >= 0 && game.IOTinputs.BusStopsIn < 60) ||
		game.IOTinputs.PeopleGoToWork {
		return available[0]
	}
	return available[len(available)/2]
}

func liftHasFloor(game *GameData, liftID string, index int) bool {
	lift := game.Layout.Lifts[liftID]
	floorID := strconv.Itoa(index)
	return util.StringInSlice(lift.Floors, floorID)
}

// TODO maybe the best is not the closest... look at all inside and search best leaving route
func bestToLeave(lift Lift, game *GameData) string {
	return getClosestTarget(lift.Height, lift.People, game)
}

// TODO maybe the best is not the closest... look at all possible targets and search best getting route
func bestToGet(game *GameData, closestPersonID string) string {
	return *game.People[closestPersonID].Floor
}

func bestGetLeaveRoute0(lift Lift, game *GameData) string {
	return bestToLeave(lift, game)
}

// TODO maybe improve decision, not knowing how to do it yet...
func bestGetLeaveRoute1(lift Lift, game *GameData) string {
	// prefer leaving to getting, as we need emptying lift before taking in more people
	// so, look for route (up or down) that will allow us to leave the most people per unit of time
	// (for example, prefer going up 2m and leave 2 persons to going down 1m and leave 1 person, then 1m and 1 person again)
	// when the direction is defined, decide on first floor in that direction when a person can leave or get in
	var down, up []string
	for _, floorID := range game.Layout.Lifts[lift.ID].Floors {
		if game.Layout.Floors[floorID].Height == lift.Height && someoneGoesTo(game, lift.People, floorID) {
			return floorID // we are exactly in a floor, with someone that has to leave, let them leave first
		} else if game.Layout.Floors[floorID].Height < lift.Height {
			down = append(down, floorID)
		} else {
			up = append(up, floorID)
		}
	}

	down = reverse(down)
	efficiencyUp, availableUp := peoplePerSecond(game, lift, up)
	efficiencyDown, availableDown := peoplePerSecond(game, lift, down)

	if efficiencyUp > efficiencyDown {
		return availableUp[0]
	}

	return availableDown[0]
}

func peoplePerSecond(game *GameData, lift Lift, floors []string) (efficiency float64, available []string) {
	peopleInside, maxPeople, peopleLeft := len(lift.People), lift.Capacity, 0
	for _, floorID := range floors {
		if quantity := totalGoesTo(game, lift.People, floorID); quantity > 0 {
			available = append(available, floorID)
			peopleInside -= quantity
			peopleLeft += quantity
		}

		canEnter := maxPeople - peopleInside
		if quantity := totalWaitingIn(game, lift, floorID); quantity > 0 && canEnter > 0 {
			if quantity > canEnter {
				quantity = canEnter
			}
			peopleInside += quantity
			if !util.StringInSlice(available, floorID) {
				available = append(available, floorID)
			}
		}
	}

	if len(available) == 0 {
		return 0, available
	}

	height := lift.Height
	var totalTime float64
	for _, floorID := range available {
		floorHeight := game.Layout.Floors[floorID].Height
		totalTime += timeBetween(height, floorHeight)
		height = floorHeight
	}

	return float64(peopleLeft) / totalTime, available
}

func totalWaitingIn(game *GameData, lift Lift, floorID string) (total int) {
	for _, person := range game.People {
		if person.Floor != nil && *person.Floor == floorID { // && person.X == lift.X { // TODO condition removed because it caused panics when some person could get in but was not exactly in X, should revisit option
			total++
		}
	}
	return total
}

func timeBetween(a, b float64) float64 {
	distance := util.Abs(a - b)
	if distance <= 0.5 {
		return 1
	} else if distance <= 1 {
		return 2
	}
	return 2 + math.Ceil(distance-1) // accelerate + decelerate + 1s for each meter between both
}

func someoneGoesTo(game *GameData, people []string, floorID string) bool {
	return totalGoesTo(game, people, floorID) > 0
}

func totalGoesTo(game *GameData, people []string, floorID string) (total int) {
	for _, personID := range people {
		person := game.People[personID]
		if person.TargetFloor == floorID {
			total++
		}
	}
	return total
}

func getClosestTarget(liftHeight float64, people []string, game *GameData) string {
	targetFloor := game.People[people[0]].TargetFloor
	min := 1e10
	for _, personID := range people {
		if d := distance(liftHeight, game.People[personID].TargetFloor, game); d < min {
			targetFloor = game.People[personID].TargetFloor
			min = d
		}
	}

	return targetFloor
}

func distance(liftHeight float64, floorID string, game *GameData) float64 {
	floorHeight := game.Layout.Floors[floorID].Height
	return util.Abs(liftHeight - floorHeight)
}

func reverse(l []string) (out []string) {
	for i := len(l) - 1; i >= 0; i-- {
		out = append(out, l[i])
	}
	return out
}
