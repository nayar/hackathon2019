package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"time"

	// "github.com/oriolf/hackathon-2019/backend/AI/black"
	// "github.com/oriolf/hackathon-2019/backend/AI/green"
	//	"github.com/oriolf/hackathon-2019/backend/AI/orange"
	"github.com/oriolf/hackathon-2019/backend/AI/pink"
	"github.com/oriolf/hackathon-2019/backend/AI/purple"
	"github.com/oriolf/hackathon-2019/backend/AI/yellow"
	. "github.com/oriolf/hackathon-2019/backend/structs"
	"github.com/pkg/errors"
)

const (
	url        = "http://localhost:5000"
	iterations = 10
	maxWorkers = 2
)

type algorithm interface {
	MakeActions(act *Actions, game *GameData)
}

type game struct {
	alg  algorithm
	seed int
}

var (
	algorithms = []algorithm{
		yellow.Algorithm{},
		// green.Algorithm{},
		//		orange.Algorithm{},
		pink.Algorithm{FilePath: "pink/ff.network"},
		purple.Algorithm{},
		// black.Algorithm{Parameters: []float64{-81.84093507406982, 58.58962956782977}},
	}
	algorithmsNames = []string{
		"yellow",
		// "green",
		//		"orange",
		"pink",
		"purple",
		// "black",
	}
	poolCh = make(chan game)
	ch     = make(chan float64, len(algorithms)*iterations)
)

func main() {
	initPool()
	results := map[float64]string{}
	seeds := []int64{}

	for i, alg := range algorithms {
		fmt.Println(algorithmsNames[i])
		var score float64

		for i := 0; i < iterations; i++ {
			poolCh <- game{alg: alg, seed: i + 1}
		}

		for i := 0; i < iterations; i++ {
			score += <-ch
		}

		results[score/iterations] = algorithmsNames[i]
		fmt.Printf("Algorithm %s. Mean score: %f.\n", algorithmsNames[i], score/iterations)
	}

	//Supposed two equal values are not going to happen
	orderedResults := []float64{}
	for value := range results {
		orderedResults = append(orderedResults, value)
	}
	sort.Float64s(orderedResults)

	fmt.Printf("\n\nRESULTS:\n")
	for _, value := range orderedResults {
		fmt.Printf("%s:\t%f\n", results[value], value)
	}
}

func initPool() {
	for i := 0; i < maxWorkers; i++ {
		go worker()
	}
}

func worker() {
	for {
		select {
		case x := <-poolCh:
			playGame(x.alg, x.seed)
		}
	}
}

func playGame(alg algorithm, seed int) {
	act := Actions{}
	game := GameData{
		Seed: data.Seed,
	}

	for {
		if game.ID != 0 {
			var err error
			game, err = post(fmt.Sprintf("%s?id=%d", url, game.ID), act)
			if err != nil {
				log.Fatalln("Error posting action", err)
			}
			if game.Finished {
				fmt.Printf("With seed %d score %f.\n", seed, game.Score)
				ch <- game.Score
				return
			}
		} else {
			var err error
			game, err = get(fmt.Sprintf("%s?seed=%d", url, seed))
			if err != nil {
				log.Fatalln("Error getting game", err)
			}

		}

		data.Alg.MakeActions(&act, &game)
	}
}

func post(url string, act Actions) (game GameData, err error) {
	jsonValue, err := json.Marshal(act)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error marshaling actions")
	}
	res, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error making post")
	}
	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}

func get(url string) (game GameData, err error) {
	res, err := http.Get(url)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error get request")
	}

	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}
