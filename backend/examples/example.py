# python example.py
import requests
import json

URL = "http://localhost:5000"
MAX_DISTANCE = 1e10
game = {}
actions = {
    "lifts": {}
}


def manhattanDistance(liftID, personID):
    liftX = game["layout"]["lifts"][liftID]["x"]
    liftY = game["layout"]["lifts"][liftID]["height"]
    personX = game["people"][personID]["x"]
    personFloor = game["people"][personID]["floor"]
    if personFloor is None:
        return MAX_DISTANCE

    personTargetFloor = game["people"][personID]["target_floor"]
    if personFloor == personTargetFloor:  # ignore people already in its floor
        return MAX_DISTANCE

    if personFloor not in game["layout"]["lifts"][liftID]["floors"] or personTargetFloor not in game["layout"]["lifts"][liftID]["floors"]:
        return MAX_DISTANCE

    personY = game["layout"]["floors"][personFloor]["height"]
    return abs(liftX-personX) + abs(liftY-personY)


def getClosestPerson(liftID):
    minDistance = MAX_DISTANCE
    closestPerson = ""
    peopleIDs = []
    for personID in game["people"]:
        peopleIDs.append(personID)

    if len(peopleIDs) == 0:
        return ""

    for personID in peopleIDs:
        distance = manhattanDistance(liftID, personID)
        if distance < minDistance:
            minDistance = distance
            closestPerson = personID

    return closestPerson


def makeActions():
    ids = []
    for liftID in game["lifts"]:
        ids.append(liftID)
    ids.sort()

    for liftID in ids:
        lift = game["lifts"][liftID]

        # If someone in lift, let them get out first
        if lift["people"] and len(lift["people"]) > 0:
            actions["lifts"][liftID] = game["people"][lift["people"]
                                                      [0]]["target_floor"]
            continue

        closestPersonID = getClosestPerson(liftID)
        if closestPersonID:
            actions["lifts"][liftID] = game["people"][closestPersonID]["floor"]
        else:
            actions["lifts"][liftID] = game["layout"]["lifts"][liftID]["floors"][0]


while True:
    if game.get("id") is not None:  # Play game step
        try:
            response = requests.post(
                url=URL+"?id="+str(game["id"]), data=json.dumps(actions))
        except requests.exceptions.RequestException as e:
            print("Error posting actions ", e)
            sys.exit(1)
        game = json.loads(response.text)

        if game["finished"]:
            print("Score "+str(game["score"]))
            break

    else:  # Create game
        try:
            response = requests.get(url=URL+"?seed=1")
        except requests.exceptions.RequestException as e:
            print("Error getting new game ", e)
            sys.exit(1)
        game = json.loads(response.text)

    makeActions()
