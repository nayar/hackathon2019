// go run main.go
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"

	"github.com/pkg/errors"
)

type GameData struct {
	ID       int               `json:"id"`
	Layout   Layout            `json:"layout"`
	Lifts    map[string]Lift   `json:"lifts"`
	People   map[string]Person `json:"people"`
	Score    float64           `json:"score"` // number which is the sum of all people scores, plus the scores of people gone in the moment they left
	Finished bool              `json:"finished"`
}

type Layout struct {
	Floors map[string]Floor `json:"floors"`
	Lifts  map[string]Lift  `json:"lifts"`
}

type Floor struct {
	Height float64 `json:"height"`
}

type Lift struct {
	X      float64  `json:"x"`      // distance to the left, number in m
	Height float64  `json:"height"` // actual height, number in m
	People []string `json:"people"` // list of people ids that are inside the lift
	Floors []string `json:"floors"` // list of floor ids in which it stops
}

type Person struct {
	Floor       *string `json:"floor"`        // floor id in which the person is, null if inside a lift
	TargetFloor string  `json:"target_floor"` // floor to which the person has to go
	X           float64 `json:"x"`
}

type Actions struct {
	Lifts map[string]string `json:"lifts"` // each lift to which floor should go
}

const (
	url          = "http://localhost:5000"
	MAX_DISTANCE = 1e10
)

func main() {
	act := Actions{}
	game := GameData{}

	for {
		if game.ID != 0 { // Play game step
			var err error
			game, err = post(fmt.Sprintf("%s?id=%d", url, game.ID), act)
			if err != nil {
				log.Fatalln("Error posting action", err)
			}
			if game.Finished {
				fmt.Printf("Score %f.\n", game.Score)
				return
			}
		} else { // Create game
			var err error
			game, err = get(url + "?seed=1")
			if err != nil {
				log.Fatalln("Error getting game", err)
			}

		}

		makeActions(&act, &game)
	}
}

func makeActions(act *Actions, game *GameData) {
	*act = Actions{Lifts: map[string]string{}}

	var ids []string
	for id := range game.Lifts {
		ids = append(ids, id)
	}
	sort.Strings(ids)

	for _, id := range ids {
		lift := game.Lifts[id]
		if len(lift.People) > 0 { //If someone in lift, let them get out first
			act.Lifts[id] = game.People[lift.People[0]].TargetFloor
			continue
		}

		closestPersonID := GetClosestPerson(id, game)
		if closestPersonID != "" {
			act.Lifts[id] = *game.People[closestPersonID].Floor
		} else {
			act.Lifts[id] = game.Layout.Lifts[id].Floors[0]
		}
	}
}

func post(url string, act Actions) (game GameData, err error) {
	jsonValue, err := json.Marshal(act)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error marshaling actions")
	}
	res, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error making post")
	}
	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}

func get(url string) (game GameData, err error) {
	res, err := http.Get(url)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error get request")
	}

	defer res.Body.Close()
	if res.StatusCode < 200 && res.StatusCode >= 300 {
		return GameData{}, fmt.Errorf("status code %d", res.StatusCode)
	}

	bodyBytes, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return GameData{}, errors.Wrap(readErr, "Error reading response")
	}

	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return GameData{}, errors.Wrap(err, "Error unmarshaling body response")
	}

	return game, nil
}

func GetClosestPerson(liftID string, game *GameData) string {
	minDistance := MAX_DISTANCE
	closestPerson := ""
	peopleIDs := []string{}
	for id := range game.People {
		peopleIDs = append(peopleIDs, id)
	}

	if len(peopleIDs) == 0 {
		return ""
	}

	for _, personID := range peopleIDs {
		if distance := ManhattanDistance(liftID, personID, game); distance < minDistance {
			minDistance = distance
			closestPerson = personID
		}
	}

	return closestPerson
}

func ManhattanDistance(liftID, personID string, game *GameData) float64 {
	liftX := game.Layout.Lifts[liftID].X
	liftY := game.Layout.Lifts[liftID].Height
	personX := game.People[personID].X
	personFloor := game.People[personID].Floor
	if personFloor == nil { // ignore people already in a lift
		return MAX_DISTANCE
	}
	personTargetFloor := game.People[personID].TargetFloor
	if *personFloor == personTargetFloor { // ignore people already in its floor
		return MAX_DISTANCE
	}

	if !floorValidForLift(*personFloor, liftID, game) ||
		!floorValidForLift(personTargetFloor, liftID, game) { // ignore people that can't go to desired destinationwith this lift
		return MAX_DISTANCE
	}

	personY := game.Layout.Floors[*personFloor].Height
	return Abs(liftX-personX) + Abs(liftY-personY)
}

func floorValidForLift(floorID, liftID string, game *GameData) bool {
	return StringInSlice(game.Layout.Lifts[liftID].Floors, floorID)
}

func StringInSlice(list []string, str string) bool {
	for _, value := range list {
		if value == str {
			return true
		}
	}
	return false
}

func Abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}
