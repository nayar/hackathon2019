// node example.js

var http = require('http');

var actions = {};
function updateActions(game) {
    actions = {lifts: {}};

    ids = [];
    Object.keys(game.lifts).map(liftID => {
        ids.push(liftID);
    });
    ids.sort();
    
    for (var liftID in ids) {
        let people = game.lifts[liftID].people;
        if (people && people.length > 0) { // if someone in lift, let them get out first
            actions["lifts"][liftID] = game.people[people[0]].target_floor;
            continue;
        }

        // no one inside, go get closest person
        let closestPersonID = closestPerson(liftID, game);
        if (closestPersonID) {
            actions["lifts"][liftID] = game.people[closestPersonID].floor;
        } else {
            actions["lifts"][liftID] = game.layout.lifts[liftID].floors[0]; // if no people left, go to first floor
        }
    }
}

let MAX_DISTANCE = 1e10;
function closestPerson(liftID, game) {
    let minDistance = MAX_DISTANCE;
    let minIndex = -1;
    let keys = Object.keys(game.people);
    if (keys.length === 0) { return null; }
    
    for (let i = 0; i < keys.length; i++) {
        let distance = manhattanDistance(liftID, keys[i], game);
        if (distance < minDistance) {
            minDistance = distance;
            minIndex = i;
        }
    }
    if (minIndex === -1) { 
        return null; 
    }
    return keys[minIndex];
}

function manhattanDistance(liftID, personID, game) {
    let liftX = game.layout.lifts[liftID].x;
    let liftY = game.layout.lifts[liftID].height;
    let personX = game.people[personID].x;
    let personFloor = game.people[personID].floor;
    if (personFloor == null) { // ignore people already in a lift
        return MAX_DISTANCE;
    }
    let personTargetFloor = game.people[personID].target_floor
    if (personFloor == personTargetFloor) { // ignore people already in its floor
        return MAX_DISTANCE;
    }
    // ignore people that can't go to desired destination in this lift
    if (!floorValidForLift(personFloor, liftID, game) ||
        !floorValidForLift(personTargetFloor, liftID, game)) {
        return MAX_DISTANCE;
    }
    let personY = game.layout.floors[personFloor].height;
    return abs(liftX - personX) + abs(liftY - personY);
}

function floorValidForLift(floorID, liftID, game) {
    let floors = game.layout.lifts[liftID].floors;
    return floors.indexOf(floorID) !== -1;
}

function callback(game) {
    game = JSON.parse(game);
    if (game.finished) {
        console.log("Score", game.score)
        return
    }
    let id = game["id"] + "";
    updateActions(game);

    return post("/?id=" + id, callback);
}

function get(theUrl, callback) {
    http.get(theUrl, (resp) => {
        let data = '';

        resp.on('data', chunk => {
            data += chunk
        });

        resp.on('end', () => {
            callback(data)
        });
    }).on("error", err => {
        console.log("Error" + err.message)
    });
}

function post(urlPath, callback) {
    let str = JSON.stringify(actions)
    let options = {
        host: urlOptions.url,
        port: urlOptions.port,
        path: urlPath,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    }
    let req = http.request(options, res => {
        let data = '';

        res.on('data', chunk => {
            data += chunk;
        })

        res.on('end', _ => {
            if (res.statusCode === 200) {
                callback(data);
            } else {
                console.log("ERROR", data)
            }
        });

    });
    
    req.write(str);
    req.end();
}



let url = "http://localhost:5000";
let urlOptions = {
    url: "localhost",
    port: 5000,
}

function abs (x) {
    if (x < 0) { return -x; }
    return x;
};

get(url + "?seed=1", callback);