let width = 800;
let height = 500;
let margin = { left: 40, right: 10, top: 70, bottom: 30 };

let svg = d3.select("#chart")
    .append("svg")
    .attr("width", width)
    .attr("height", height);


let parseToData = (dataset) => {
    return Object.keys(dataset).map(t => {
        let vals = info[t].map(v => {
            if (v.timestamp.toString().length > 13) {
                let time = v.timestamp.toString().slice(0, -6);
                v.timestamp = v.timestamp.toString().slice(0, -6);
                let date = new Date(time * 1);
                v.date = d3.timeFormat("%d %m, %H:%M")(date);
            }
            return v;
        })

        return { team: t, values: vals };
    });
};

let colors = (t) => {
    let teams = Object.keys(info);
    let c = d3.scaleOrdinal(d3.schemeCategory10)
        .domain(teams.map(function (tc) {
            return tc;
        }))
    return c(t)
};


let globalDatasetFunc = (dataset) => {
    let g = []
    Object.keys(dataset).forEach(t => {
        dataset[t].forEach(v => {
            g.push(v)
        })
    })
    return g; script
}


let data = parseToData(info);
let global = globalDatasetFunc(info);


let xScale = d3.scaleTime()
    .domain(d3.extent(global, d => d.timestamp))
    .rangeRound([margin.left, width - margin.right]);

let yScale = d3.scaleLinear()
    .domain(d3.extent(global, d => d.score))
    .range([height - margin.bottom, margin.top]);



let lineFunc = d3.line()
    .curve(d3.curveLinear)
    .x(d => {
        return xScale(d.timestamp);
    })
    .y(d => {
        return yScale(d.score)
    });





let axisY = svg.append("g")
    .call(d3.axisLeft(yScale)
        .tickFormat(d3.timeFormat("%Y-%m-%d %H:%M")))
    .attr("transform", "translate( " + margin.left + ", 0 )");

let axisX = svg.append("g")
    .attr("transform", "translate(0," + (height - margin.bottom) + ")")
    .call(d3.axisBottom(xScale));



let creatAuxTooltip = () => {
    let tooltipWrapper = d3.select("body")
        .append("div")
        .attr("class", "aux_tooltip")
        .style("opacity", "1")
        .style("position", "absolute")
        .style("transition", "all 0.3s ease-out")
        .style("left", (svg.node().getBoundingClientRect().width + svg.node().getBoundingClientRect().x + 20) + "px");

    tooltipWrapper.append("div")
        .attr("id", "tooltip-text")
        .attr("style",
            "background: #fff;"
            + "display: inline-block;"
            + "pointer-events: none;"
            + "text-align: left;"
            + "padding: 0.6em 2em;"
            + "padding-left: 7px;"
            + "padding-right: 7px;"
            + "white-space: nowrap;"
            + "border: 1px solid #ddd;"
            + "font: 12px sans-serif;"
            + "background: white;"
            + "border-radius: 8px;"
        );

    tooltipWrapper.append("div")
        .attr("id", "pointer")
        .attr("style",
            "position: absolute;"
            + "top: 50%;"
            + "width: 18px;"
            + "height: 18px;"
            + "background: #ddd;"
            + "transform: translate(-40%, -50%) rotate(45deg);"
            + "transform-origin: center center;"
            + "z-index: -1;"
        )
    return tooltipWrapper;
}


let createTooltip = () => {
    let teams = Object.keys(info);

    let tooltipWrapper = d3.select("body")
        .selectAll(".tooltips")
        .data(teams)
        .enter()
        .append("div")
        .attr("id", d => {
            return "tooltip" + d.replace(/ /g, '')
        })
        .style("opacity", "1")
        .style("position", "absolute")
        .style("transition", "all 0.3s ease-out")
        .style("left", (svg.node().getBoundingClientRect().width + svg.node().getBoundingClientRect().x + 20) + "px");

    tooltipWrapper.append("div")
        .attr("id", "tooltip-text")
        .attr("style",
            "background: #fff;"
            + "display: inline-block;"
            + "pointer-events: none;"
            + "text-align: left;"
            + "padding: 0.6em 2em;"
            + "padding-left: 7px;"
            + "padding-right: 7px;"
            + "white-space: nowrap;"
            + "border: 1px solid #ddd;"
            + "font: 12px sans-serif;"
            + "background: white;"
            + "border-radius: 8px;"
        );

    tooltipWrapper.append("div")
        .attr("id", "pointer")
        .attr("style",
            "position: absolute;"
            + "top: 50%;"
            + "width: 18px;"
            + "height: 18px;"
            + "background: #ddd;"
            + "transform: translate(-40%, -50%) rotate(45deg);"
            + "transform-origin: center center;"
            + "z-index: -1;"
        )
    return tooltipWrapper;
}
createTooltip();



let drawLine = svg => {
    d3.selectAll(".aux_tooltip").remove();
    let center = l => {
        if (l.length == 0) {
            return 0;
        } else if (l.length == 1) {
            return l[0].x;
        }

        let sum = l[0].x + l[l.length - 1].x;
        return sum / 2;
    };

    let computeMedianIndex = l => {
        let c = center(l);
        for (let i = 0; i < l.length; i++) {
            if (l[i].x > c) {
                return i;
            }
        }
        return 1;
    };

    let cluster = (l, vitalMargin) => {
        if (l.length <= 1) {
            return l;
        }
        let medianIndex = computeMedianIndex(l);
        let left = l.slice(0, medianIndex);
        let right = l.slice(medianIndex);
        let leftCenter = center(left);
        let rightCenter = center(right);
        if (rightCenter - leftCenter < vitalMargin) {
            return [l];
        }
        return [cluster(left, vitalMargin), cluster(right, vitalMargin)];
    };

    let flatten = l => {
        let f = [];
        for (let i = 0; i < l.length; i++) {
            if (l[i][0].x !== undefined) {
                f.push(l[i]);
            } else {
                let aux = flatten(l[i]);
                for (let j = 0; j < aux.length; j++) {
                    f.push(aux[j]);
                }
            }
        }
        return f;
    };

    let points = data.map(t => {
        return { x: yScale(t.values[t.values.length - 1].score), score: t.values[t.values.length - 1].score, d: t }
    })
    points = points.sort(function (a, b) { return a.x - b.x });

    let clusters = [];
    if (points.length <= 1) {
        clusters = [points]
    } else {
        clusters = flatten(cluster(points, 40));
    }


    let absoluteTopPosition = svg.node().getBoundingClientRect().y
    clusters.forEach(c => {
        if (c.length === 1) {
            let tooltip = d3.select("#tooltip" + c[0].d.team.replace(/ /g, ''))
            tooltip.style("opacity", "1")
            tooltip.style("top", absoluteTopPosition + c[0].x - 15 + "px");
            tooltip.select("#tooltip-text").html(d => "<div>" + d + ": " + c[0].score.toFixed(4) + "</div>")
            tooltip.select("#pointer").style("background", d => colors(d));
        } else {
            c.map(subC => {
                let tooltip = d3.select("#tooltip" + subC.d.team.replace(/ /g, ''))
                tooltip.style("opacity", "0")
            })
            let legend = c.map(subC => {
                let color = colors(subC.d.team);
                let content = "<div>" +
                    "<div style='border-radius:20px;display:block;width:13px;height:13px;position:absolute;background:" + color + ";'></div>" +
                    "<div style='margin-left:22px;'>" + subC.d.team + ": " + subC.score.toFixed(4) + "</div>" +
                    "</div>"
                return content;
            }).join("");
            let auxTooltip = creatAuxTooltip();
            auxTooltip.select("#tooltip-text").html(legend)
            let auxTooltipHalfHeight = auxTooltip.node().getBoundingClientRect().height / 2;
            auxTooltip.style("top", absoluteTopPosition - auxTooltipHalfHeight + (yScale(center(c.map(d => { return { x: d.score } })))) + "px");
        }
    })


    axisY.call(d3.axisLeft(yScale))
    axisX.call(d3.axisBottom(xScale).tickFormat(d3.timeFormat("%A %H:%M")).ticks(5))
    let lineSelect = svg
        .selectAll(".lines")
        .data(data);

    let group = lineSelect.enter().append("g").attr("class", "lines").merge(lineSelect);

    let pathGroup = group
        .selectAll("path")
        .data(d => [d]);

    let m = pathGroup
        .enter()
        .append("path")
        .attr("stroke", (d) => colors(d.team))
        .attr("stroke-width", "1")
        .attr("fill", "none")
        .merge(pathGroup)
        .attr("d", d => lineFunc(d.values))
        .attr("transform", null)


    let circleGroup = group
        .selectAll(".circle")
        .data(d => d.values);

    circleGroup
        .enter()
        .append("circle")
        .attr("class", "circle")
        .attr("cx", d => xScale(d.timestamp))
        .attr("cy", d => yScale(d.score))
        .attr("r", 3)
        .attr("fill", (d, i, nodes) => {
            let team = d3.select(nodes[i].parentNode).datum().team;
            return colors(team);
        })
        .merge(circleGroup)
        .attr("cx", d => xScale(d.timestamp))
        .attr("cy", d => yScale(d.score))

}

drawLine(svg);
