package structs

import (
	"math/rand"
	"sync"
)

type AllGamesType struct {
	Mutex sync.Mutex
	I     int
	Games map[int]GameData
}

type GameData struct {
	ID          int               `json:"id"`
	Layout      Layout            `json:"layout"`
	Lifts       map[string]Lift   `json:"lifts"`
	People      map[string]Person `json:"people"`
	Score       float64           `json:"score"` // number which is the sum of all people scores, plus the scores of people gone in the moment they left
	IOTinputs   IotInputs         `json:"iot_inputs"`
	Time        int               `json:"time"`
	Finished    bool              `json:"finished"`
	TotalFloors int               `json:"total_floors"`

	Seed        int64      `json:"-"`
	Rand        *rand.Rand `json:"-"`
	PersonID    int        `json:"-"`
	CurrentTime int        `json:"-"`
	TotalTime   int        `json:"-"` // total number of "turns" or "seconds"

	IotEvents map[string][]int `json:"-"` //event - seconds
	NewPeople map[int][]Person `json:"-"`
}

type Layout struct {
	Width  float64          `json:"width"` // number in m
	Floors map[string]Floor `json:"floors"`
	Lifts  map[string]Lift  `json:"lifts"`
}

type Floor struct {
	ID     string  `json:"id"`
	Height float64 `json:"height"`
}

type Lift struct {
	ID        string   `json:"id"`
	X         float64  `json:"x"`        // distance to the left, number in m
	Height    float64  `json:"height"`   // actual height, number in m
	Velocity  float64  `json:"velocity"` // actual velocity, number in m/s from -1 to 1
	Capacity  int      `json:"capacity"` // number of persons that can be in lift at the same time, max of len(people)
	People    []string `json:"people"`   // list of people ids that are inside the lift
	Floors    []string `json:"floors"`   // list of floor ids in which it stops
	DoorsOpen bool     `json:"doors_open"`
}

type Person struct {
	ID          string
	Floor       *string `json:"floor"`        // floor id in which the person is, null if inside a lift
	Lift        *string `json:"lift"`         // lift id in which the person is, null if in a floor
	TargetFloor string  `json:"target_floor"` // floor to which the person has to go
	TargetExit  string  `json:"target_exit"`  // "left" || "right", if the person has to leave the building from the left or the right

	MinTime     float64 `json:"min_time"`     // minimum time that the person needs to accomplish its goal
	CurrentTime float64 `json:"current_time"` // current time spent in its goal
	Score       float64 `json:"score"`        // present score, (20 - 10 + (22 - 20) * 2)
	X           float64 `json:"x"`
}

type Actions struct {
	Lifts map[string]string `json:"lifts"` // each lift to which floor should go
}

type IotInputs struct {
	Rain           Rain `json:"rain"` //nil == no rain
	PeopleGoToWork bool `json:"people_go_to_work"`
	BusStopsIn     int  `json:"bus_stops_in"`
}

type Rain struct {
	SecondsToStart int `json:"seconds_to_start"` //If 0 means raining
	Duration       int `json:"duration"`
}

type EventsState struct { //struct created to get people creation probabilities
	Rain           bool
	PeopleGoToWork bool
	BusStopsIn     bool
}
