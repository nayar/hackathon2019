module github.com/oriolf/hackathon-2019/backend

go 1.12

require (
	github.com/MaxHalford/eaopt v0.1.1-0.20190731130203-1ec1eeffdeb9
	github.com/goml/gobrain v0.0.0-20190708142826-a9d9724c9e47
	github.com/google/go-cmp v0.3.1
	github.com/pkg/errors v0.8.1
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
